import Realm from 'realm';

export const USER_SCHEMA = 'user_schema';
export const APP_HINT_SCHEMA = 'app_hint_schema';
export const CHAT_LIST_SCHEMA = 'chat_list_schema';

//define user table schema
export const UserSchema = {
    name: USER_SCHEMA,
    primaryKey: 'id',
    properties: {
        id: 'int',    // primary key
        fullname: 'string',
        email_id: 'string',
        user_password: 'string',
        device_type: 'string',
        user_photo: 'string',
        device_token: 'string',
        gender: 'string',
        profession_name: 'string',
        location: 'string',
        brief_desc: 'string',
        birth_date: 'string',
        user_tag: 'string',
        fire_id: 'string',
        is_notify: 'string',
    }
};

//define AppData table schema
export const AppHintSchema = {
    name: APP_HINT_SCHEMA,
    primaryKey: 'id',
    properties: {
        id: 'int',    // primary key
        isVisibleHint: 'string',
        isChatDotVisible: 'string'
    }
};

//define Chat List table schema
export const ChatListSchema = {
    name: CHAT_LIST_SCHEMA,
    primaryKey: 'id',
    properties: {
        id: 'int',    // primary key
        chatListJSON: 'string'
    }
};

//create database option for create database
const databaseOptions = {
    path: 'opus.realm',
    schema: [UserSchema, AppHintSchema, ChatListSchema],
    schemaVersion: 2 //optional
};

//functions for Insert user
export const insertUser = user => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            realm.create(USER_SCHEMA, user);
            resolve(user);
        });
    }).catch((error) => reject(error));
});

//functions for Insert APPHINT
export const insertAppHint = data => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            realm.create(APP_HINT_SCHEMA, data);
            resolve(data);
        });
    }).catch((error) => reject(error));
});

//functions for Insert chat list
export const insertChatList = chat => new Promise((resolve, reject) => {
    // console.log('insertChatList', chat);
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            realm.create(CHAT_LIST_SCHEMA, chat);
            resolve(chat);
        });
    }).catch((error) => {
        reject(error)
    });
});

export const updateUser = data => new Promise((resolve, reject) => {
    console.log(data);
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            let updatingData = realm.objectForPrimaryKey(USER_SCHEMA, data.id);
            console.log(updatingData);
            updatingData.fullname = data.fullname;
            updatingData.email_id = data.email_id;
            updatingData.user_photo = data.user_photo;
            updatingData.gender = data.gender;
            updatingData.profession_name = data.profession_name;
            updatingData.location = data.location;
            updatingData.brief_desc = data.brief_desc;
            updatingData.birth_date = data.birth_date;
            updatingData.is_notify = data.is_notify;
            updatingData.user_tag = data.user_tag;
            resolve();
        });
    }).catch((error) => reject(error));
});

export const updateAppHintData = data => new Promise((resolve, reject) => {
    console.log(data);
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            let updatingData = realm.objectForPrimaryKey(APP_HINT_SCHEMA, data.id);
            updatingData.isChatDotVisible = data.isChatDotVisible;
            resolve();
        });
    }).catch((error) => reject(error));
});

//function for get user
export const getUser = () => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        const user = realm.objects(USER_SCHEMA);
        resolve(user);
    }).catch((error) => {
        reject(error);
    });
});

//function for get AppHintData
export const getAppHintData = () => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        const data = realm.objects(APP_HINT_SCHEMA);
        resolve(data);
    }).catch((error) => {
        reject(error);
    });
});


//function for get chatlist
export const getChatListData = () => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        const data = realm.objects(CHAT_LIST_SCHEMA);
        resolve(data);
    }).catch((error) => {
        reject(error);
    });
});

export const deleteUser = () => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            const user = realm.objects(USER_SCHEMA);
            realm.delete(user);
            resolve();
        });
    }).catch((error) => reject(error));
});

export const deleteAppHintData = () => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            const AppHintData = realm.objects(APP_HINT_SCHEMA);
            realm.delete(AppHintData);
            resolve();
        });
    }).catch((error) => reject(error));
});

export const deleteChatList = () => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            const data = realm.objects(CHAT_LIST_SCHEMA);
            realm.delete(data);
            resolve();
        });
    }).catch((error) => reject(error));
});

export default new Realm(databaseOptions);


// import Realm from 'realm';

// export const USER_SCHEMA = 'user_schema';
// export const APP_HINT_SCHEMA = 'app_hint_schema';

// //define user table schema
// export const UserSchema = {
//     name: USER_SCHEMA,
//     primaryKey: 'id',
//     properties: {
//         id: 'int',    // primary key
//         fullname: 'string',
//         email_id: 'string',
//         user_password: 'string',
//         device_type: 'string',
//         user_photo: 'string',
//         device_token: 'string',
//         gender: 'string',
//         profession_name: 'string',
//         location: 'string',
//         brief_desc: 'string',
//         birth_date: 'string',
//         user_tag: 'string',
//         fire_id: 'string',
// 		is_notify: 'string',
//     }
// };

// //define AppData table schema
// export const AppHintSchema = {
//     name: APP_HINT_SCHEMA,
//     primaryKey: 'id',
//     properties: {
//         id: 'int',    // primary key
//         isVisibleHint: 'string',
//         isChatDotVisible: 'string'
//     }
// };

// //create database option for create database
// const databaseOptions = {
//     path: 'Opus.realm',
//     schema: [UserSchema, AppHintSchema],
//     schemaVersion: 2 //optional
// };

// //functions for Insert user
// export const insertUser = user => new Promise((resolve, reject) => {
//     Realm.open(databaseOptions).then(realm => {
//         realm.write(() => {
//             realm.create(USER_SCHEMA, user);
//             resolve(user);
//         });
//     }).catch((error) => reject(error));
// });

// //functions for Insert APPHINT
// export const insertAppHint = data => new Promise((resolve, reject) => {
//         Realm.open(databaseOptions).then(realm => {
//             realm.write(() => {
//                 realm.create(APP_HINT_SCHEMA, data);
//                 resolve(data);
//             });
//         }).catch((error) => reject(error));
//     });

// export const updateUser = data => new Promise((resolve, reject) => {
//   console.log(data);
//     Realm.open(databaseOptions).then(realm => {
//         realm.write(() => {
//             let updatingData = realm.objectForPrimaryKey(USER_SCHEMA, data.id);
//             console.log(updatingData);
//             updatingData.fullname = data.fullname;
//             updatingData.email_id = data.email_id;
//             updatingData.user_photo = data.user_photo;
//             updatingData.gender = data.gender;
//             updatingData.profession_name = data.profession_name;
//             updatingData.location = data.location;
//             updatingData.brief_desc = data.brief_desc;
//             updatingData.birth_date = data.birth_date;
//             updatingData.is_notify = data.is_notify;
//             updatingData.user_tag = data.user_tag;
//             resolve();
//         });
//     }).catch((error) => reject(error));
// });

// export const updateAppHintData = data => new Promise((resolve, reject) => {
//     console.log(data);
//       Realm.open(databaseOptions).then(realm => {
//           realm.write(() => {
//               let updatingData = realm.objectForPrimaryKey(APP_HINT_SCHEMA, data.id);
//               updatingData.isChatDotVisible = data.isChatDotVisible;
//               resolve();
//           });
//       }).catch((error) => reject(error));
//   });

// //function for get user
// export const getUser = () => new Promise((resolve, reject) => {
//     Realm.open(databaseOptions).then(realm => {
//         const user = realm.objects(USER_SCHEMA);
//         resolve(user);
//     }).catch((error) => {
//         reject(error);
//     });
// });

// //function for get AppHintData
// export const getAppHintData = () => new Promise((resolve, reject) => {
//     Realm.open(databaseOptions).then(realm => {
//         const data = realm.objects(APP_HINT_SCHEMA);
//         resolve(data);
//     }).catch((error) => {
//         reject(error);
//     });
// });

// export const deleteUser = () => new Promise((resolve, reject) => {
//     Realm.open(databaseOptions).then(realm => {
//         realm.write(() => {
//             const user = realm.objects(USER_SCHEMA);
//             realm.delete(user);
//             resolve();
//         });
//     }).catch((error) => reject(error));
// });

// export const deleteAppHintData = () => new Promise((resolve, reject) => {
//     Realm.open(databaseOptions).then(realm => {
//         realm.write(() => {
//             const AppHintData = realm.objects(APP_HINT_SCHEMA);
//             realm.delete(AppHintData);
//             resolve();
//         });
//     }).catch((error) => reject(error));
// });

// export default new Realm(databaseOptions);
