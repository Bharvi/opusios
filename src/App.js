import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createAppContainer, } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import { } from 'react-native';
import { onBottomTabChange } from './Actions';

import Reducers from './Reducers';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import SplashScreen from './Components/SplashScreen';
import Home from './Components/Home';
import SignInPage from './Components/SignInPage';
import SignUpPage from './Components/SignUpPage';
import SocialLoginPage from './Components/SocialLoginPage';
import ForgotPassPage from './Components/ForgotPassPage';
import EditProfilePage from './Components/EditProfilePage';
import FeedsPage from './Components/FeedsPage';
import CreatePostPage from './Components/CreatePostPage';
import CommentPage from './Components/CommentPage';

import Search from './Components/Search';
import MyProfile from './Components/MyProfile';
import Settings from './Components/Settings';
import Notification from './Components/Notification';
import AboutUs from './Components/AboutUs';
import TermsofUse from './Components/TermsofUse';
import PrivacyPolicy from './Components/PrivacyPolicy';
import Gallery from './Components/Gallery';
import Bottom from './Components/Bottom';
import Testimonials from './Components/Testimonials';
import Friend from './Components/Friend';
import Posts from './Components/Posts';
import About from './Components/About';
import Tag from './Components/Tag';
import SearchResult from './Components/SearchResult';
import Chat from './Components/Chat';
import ChatMessage from './Components/ChatMessage';
import ZoomImage from './Components/ZoomImage';
import Profile from './Components/Profile';

import Testimonialsprofile from './Components/Testimonialsprofile';
import Friendprofile from './Components/Friendprofile';
import Postsprofile from './Components/Postsprofile';
import Aboutprofile from './Components/Aboutprofile';
import Tagprofile from './Components/Tagprofile';
import Demo from './Components/Demo';
import Demo2 from './Components/Demo2';
import Demo3 from './Components/Demo3';
import CompleteYourProfile from './Components/CompleteYourProfile';
import LocationAutosuggestion from './Components/LocationAutosuggestion';
import LocationAutosuggationEdit from './Components/LocationAutosuggationEdit';
import SearchUpdates from './Components/SearchUpdates';
import UserAppState from './Components/UserAppState';


const BottomTab = createBottomTabNavigator(
  {
    Search: { screen: Search },
    MyProfile: { screen: MyProfile },
    FeedsPage: { screen: FeedsPage },
    Chat: { screen: Chat },
    Profile: { screen: Profile },
  },
  {
    tabBarComponent: props =>
      <Bottom
        {...props}
      />
  }
);


const BottomTabChatFirst = createBottomTabNavigator(
  {
    Chat: { screen: Chat },
    Search: { screen: Search },
    MyProfile: { screen: MyProfile },
    FeedsPage: { screen: FeedsPage },
    Profile: { screen: Profile },
  },
  {
    tabBarComponent: props => {
      return (<Bottom
        {...props}
      />)
    }
  }
);

const BottomTabFeedFirst = createBottomTabNavigator(
  {
    FeedsPage: { screen: FeedsPage },
    Chat: { screen: Chat },
    Search: { screen: Search },
    MyProfile: { screen: MyProfile },
    Profile: { screen: Profile },
  },
  {
    tabBarComponent: props => {
      return (<Bottom
        {...props}
      />)
    }
  }
);

const BottomTabProfileFirst = createBottomTabNavigator(
  {
    Profile: { screen: Profile },
    FeedsPage: { screen: FeedsPage },
    Chat: { screen: Chat },
    Search: { screen: Search },
    MyProfile: { screen: MyProfile },
  },
  {
    tabBarComponent: props => {
      return (<Bottom
        {...props}
      />)
    }
  }
);

const RootStack = createStackNavigator(
  {
    Splash: { screen: SplashScreen },
    Home: { screen: Home },
    SignInPage: { screen: SignInPage },
    SignUpPage: { screen: SignUpPage },
    SocialLoginPage: { screen: SocialLoginPage },
    ForgotPassPage: { screen: ForgotPassPage },
    EditProfilePage: { screen: EditProfilePage },
    FeedsPage: { screen: FeedsPage },
    CreatePostPage: { screen: CreatePostPage },
    CommentPage: { screen: CommentPage },
    Search: { screen: Search },
    MyProfile: { screen: MyProfile },
    Settings: { screen: Settings },
    AboutUs: { screen: AboutUs },
    TermsofUse: { screen: TermsofUse },
    PrivacyPolicy: { screen: PrivacyPolicy },
    Notification: { screen: Notification },
    Gallery: { screen: Gallery },
    BottomTab: { screen: BottomTab },
    BottomTabProfileFirst: { screen: BottomTabProfileFirst },
    BottomTabChatFirst: { screen: BottomTabChatFirst },
    BottomTabFeedFirst: { screen: BottomTabFeedFirst },
    Testimonials: { screen: Testimonials },
    Friend: { screen: Friend },
    Posts: { screen: Posts },
    About: { screen: About },
    Tag: { screen: Tag },
    SearchResult: { screen: SearchResult },
    Chat: { screen: Chat },
    ChatMessage: { screen: ChatMessage },
    ZoomImage: { screen: ZoomImage },
    Profile: { screen: Profile },
    Testimonialsprofile: { screen: Testimonialsprofile },
    Friendprofile: { screen: Friendprofile },
    Postsprofile: { screen: Postsprofile },
    Aboutprofile: { screen: Aboutprofile },
    Tagprofile: { screen: Tagprofile },
    Demo: { screen: Demo },
    Demo2: { screen: Demo2 },
    Demo3: { screen: Demo3 },
    CompleteYourProfile: { screen: CompleteYourProfile },
    LocationAutosuggestion: { screen: LocationAutosuggestion },
    LocationAutosuggationEdit: { screen: LocationAutosuggationEdit },
    SearchUpdates: { screen: SearchUpdates },

  },
  {
    initialRouteName: 'Splash',
    headerMode: 'none',
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends Component {

  componentWillMount() {
    console.disableYellowBox = true;
  }

  render() {
    const store = createStore(Reducers, {}, applyMiddleware(ReduxThunk));
    function getActiveRouteName(navigationState) {
      if (!navigationState) {
        return null;
      }
      const route = navigationState.routes[navigationState.index];

      if (route.routes) {
        return getActiveRouteName(route);
      }
      return route.routeName;
    }
    return (
      <Provider store={store}>
        <UserAppState />
        <AppContainer
          onNavigationStateChange={(prevState, currentState, action) => {
            const currentRouteName = getActiveRouteName(currentState);

            // console.log('currentRouteName', currentRouteName);


            if (currentRouteName == 'Search') {
              store.dispatch(onBottomTabChange({ prop: "issearchseleted", value: true }))
              store.dispatch(onBottomTabChange({ prop: "isuserseleted", value: false }))
              store.dispatch(onBottomTabChange({ prop: "isfeedseleted", value: false }))
              store.dispatch(onBottomTabChange({ prop: "ischatseleted", value: false }))
            }
            else if (currentRouteName == 'Profile' || currentRouteName == 'MyProfile') {
              // this.props.navigation.navigate('BottamTab');

              store.dispatch(onBottomTabChange({ prop: "issearchseleted", value: false }))
              store.dispatch(onBottomTabChange({ prop: "isuserseleted", value: true }))
              store.dispatch(onBottomTabChange({ prop: "isfeedseleted", value: false }))
              store.dispatch(onBottomTabChange({ prop: "ischatseleted", value: false }))
            }
            else if (currentRouteName == 'FeedsPage') {
              store.dispatch(onBottomTabChange({ prop: "issearchseleted", value: false }))
              store.dispatch(onBottomTabChange({ prop: "isuserseleted", value: false }))
              store.dispatch(onBottomTabChange({ prop: "isfeedseleted", value: true }))
              store.dispatch(onBottomTabChange({ prop: "ischatseleted", value: false }))
            }
            else if (currentRouteName == 'Chat') {
              store.dispatch(onBottomTabChange({ prop: "issearchseleted", value: false }))
              store.dispatch(onBottomTabChange({ prop: "isuserseleted", value: false }))
              store.dispatch(onBottomTabChange({ prop: "isfeedseleted", value: false }))
              store.dispatch(onBottomTabChange({ prop: "ischatseleted", value: true }))
            }
          }}
        />
      </Provider>
    );
  }
}
