import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';

export default class HomeHeader extends Component {

  notificationCount()
  {
    if (this.props.notificationCount >= 10) {
      return "9+"
    }
    else {
      return this.props.notificationCount
    }
  }
  render() {
    return (
      <View>
        <View style={{ backgroundColor: "#CC181E", height: 15 }}></View>
      <View style={{ backgroundColor: "#CC181E", paddingTop: (DeviceInfo.getModel() === 'iPhone X' || DeviceInfo.getModel() === 'iPhone XS Max' || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 15 : 0 }}/>
      <View style={styles.container}>
      
      

      <Text allowFontScaling={false} style={styles.textStyle}> {this.props.headertext} </Text>
    

        <TouchableOpacity style={{ position: 'absolute', right: 0, top: 2, height: 50, 
        width: 50, alignItems: 'center', justifyContent: 'center' }} 
        onPress={this.props.onPressNotification}>
        <Image
        style={styles.notificationStyle}
        source={this.props.notification}
        />
        {

          (this.props.notificationCount != 0 && this.props.notificationCount != null) ?
        <View style={styles.badgeViewStyle}>
          <Text allowFontScaling={false} style={{ color: 'white', fontSize: 9 }}>
          {this.notificationCount()}</Text>
        </View>
        :
       null
      }
        </TouchableOpacity>

      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 50,
    backgroundColor: '#CC181E',
    alignItems: 'center',
   justifyContent: 'center'
  },
  textStyle: {
    fontSize: 16,
    color: '#ffffff',
    fontFamily: 'Lato-Regular',
  },

  notificationStyle: {
    width: 23,
    height: 23,
  },
  badgeViewStyle: {
     width: 15,
     height: 15,
     borderRadius: 15 / 2,
     backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute', right: 25, top: 5 
  },
  textStyleDone: {
    fontSize: 16,
    color: '#CCffffff',
    marginRight: 10,
    fontFamily: 'Lato-Regular'
  },
  backArrowStyle: {
    height: 20,
    width: 20,
  },
});
