/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image
} from 'react-native';

export default class ItemTags extends Component {
  render() {
    return (
      <View style={ styles.container }>
        {
          <View style={ styles.TagViewStyle }>
            <Text allowFontScaling={ false } numberOfLines={ 1 } style={ styles.TagNameTextStyle }>{ this.props.tagName }</Text>
          </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',

  },
  TagViewStyle: {
    backgroundColor: '#eaeaea',
    height: 30,
    borderRadius: 5,
    borderColor: '#e2e0e0',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 3,
  },
  TagNameTextStyle:
  {
    fontSize: 14, color: '#676767', fontFamily: 'Lato-Regular', flexWrap: "wrap", paddingLeft: 5,
    paddingRight: 5, margin: 2
  }
});
