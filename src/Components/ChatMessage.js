/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  Button,
  Alert,
  Keyboard,
  Dimensions,
} from 'react-native';
import Header from './HeaderChatMessage';
import { connect } from "react-redux";
import {
  getTestimonial,
  getAddfriend,
  getRating,
  ratingChange,
  ratingErrorChange,
  initialtestimonialStateData,
  testimonialChange,
  testimonialErrorChange,
  getChatNotification,
  userChatHistory,
  unfriendApi,
  getBlockfriend,
  getChatDetail,
  dotDisable,
  clerchatnotification
} from "../Actions";
import { getUser } from "../Database/allSchema";
import { Dialog } from 'react-native-simple-dialogs';
import Stars from 'react-native-stars';
import { GiftedChat, Send } from "react-native-gifted-chat";
import firebase from "react-native-firebase";
import Bubble from "./Bubble";
import DeviceInfo from 'react-native-device-info';
import Loader from './Loader';
import AsyncStorage from '@react-native-community/async-storage';

let userID = "";
let toUserId = "";
let fullname = "";
let fire_id = "";
let { width, height } = Dimensions.get('window');
let isUserFromNotification = false

class ChatMessage extends Component {
  constructor (props) {
    super(props);
    this.state = {
      OptionDialogVisible: false,
      RatingDialogVisible: false,
      TestimonialDialogVisible: false,
      emojiKeyBoardVisible: false,
      text: '',
      fullname: '',
      messages: [],
      chatCount: 0,
      is_block: false,
      is_friend: false,
      chat_available: true,
      is_dot_allow: false,
      isLoading: false,
      isPageVisible: true,
      unreadMsgCount: 0
    };

    getUser()
      .then(user => {
        if (user.length > 0) {
          userID = user[0].id;
          fire_id = user[0].fire_id;
          if (userID == 1) {
            this.user = 'afdsfsdfdsfsdfsdfsdf';
          } else {
            this.user = firebase.auth().currentUser.uid;
          }
          to_fire_id = this.props.to_fire_id;
          uid = this.props.to_fire_id;
          toUserId = this.props.toUserId;
          this.renderBubble = this.renderBubble.bind(this);
          this.chatRef = this.getRef().child("chat/" + this.generateChatId());
          this.chatRefData = this.chatRef.orderByChild("order");
        }
        this.listenForItems(this.chatRefData);
        // console.warn('this.chatRefData', this.chatRefData);
      })
      .catch(error => {
        console.warn(error);
      });
    // this.onSend = this.onSend.bind(this);
    if (this.chatRefData != null) {
      this.listenForItems(this.chatRefData);
    }
  }

  renderBubble(props) {
    return (
      <Bubble
        allowFontScaling={ false }
        { ...props }
        wrapperStyle={ {
          left: {
            backgroundColor: '#f0f0f0',
          }
        } }
      />
    );
  }

  generateChatId() {
    if (this.user > uid) return `${this.user}-${uid}`;
    else return `${uid}-${this.user}`;

  }

  getRef() {
    return firebase.database().ref();
  }

  listenForItems(chatRef) {
    // console.warn('chatRef_get', chatRef)
    this.setState({ isLoading: true })

    chatRef.on("value", snap => {
      // get children as an array
      var items = [];
      let chatCount = 0;
      let unreadMsgCount = 0;

      snap.forEach(child => {
        // console.warn('child', child)
        if (child.val().uid != this.user && this.state.isPageVisible) {
          child.ref.update({ isRead: 0 });
        }
        console.warn('in list')

        items.push({
          _id: child.val().createdAt,
          text: child.val().text,
          createdAt: new Date(child.val().createdAt),
          isRead: (child.val().isRead) ? child.val().isRead : 0,
          user: {
            _id: child.val().uid
          }
        });
        chatCount = chatCount + 1;
      });
      this.setState({
        isLoading: false,
        messages: items,
        unreadMsgCount: (items.length > 0) ? items[0].isRead : 0,
        chatCount
      }, () => {
        console.warn('item pushed', this.state.messages);
      });
      // if (this.props.toUserId === '1') {

      //   let createdAt = new Date();
      //   if (items.length > 0) {
      //     createdAt = '';
      //   }
      //   getUser()
      //     .then(user => {
      //       if (user.length > 0) {
      //         console.log('toUserId', this.props.toUserId);
      //         items.push({
      //           _id: '1',
      //           text: 'Hello, ' + user[0].fullname + '. Welcome to Opus. You can make new penpals here by just searching them by tags!  Feel free to reply to this message for any help. Happy penpalling!',
      //           createdAt: createdAt,
      //           user: {
      //             _id: '1'
      //           }
      //         });
      //       } else {
      //         items.push({
      //           _id: '1',
      //           text: 'Hello there. Welcome to Opus. You can make new penpals here by just searching them by tags!  Feel free to reply to this message for any help. Happy penpalling!',
      //           createdAt: createdAt,
      //           user: {
      //             _id: '1'
      //           }
      //         });
      //       }

      //       this.setState({
      //         isLoading: false,
      //         messages: items,
      //         unreadMsgCount: (items.length > 0) ? items[0].isRead : 0,
      //         chatCount
      //       });
      //     })
      //     .catch(error => {
      //       console.log(error);
      //       items.push({
      //         _id: '1',
      //         text: 'Hello there. Welcome to Opus. You can make new penpals here by just searching them by tags!  Feel free to reply to this message for any help. Happy penpalling!',
      //         createdAt: createdAt,
      //         user: {
      //           _id: '1'
      //         }
      //       });
      //       this.setState({
      //         isLoading: false,
      //         messages: items,
      //         unreadMsgCount: (items.length > 0) ? items[0].isRead : 0,
      //         chatCount
      //       });
      //     });
      //   // console.log('items', items);
      // } else {
      //   this.setState({
      //     isLoading: false,
      //     messages: items,
      //     unreadMsgCount: (items.length > 0) ? items[0].isRead : 0,
      //     chatCount
      //   });
      // }
    });
  }

  componentWillMount() {
    console.log(DeviceInfo.getModel());
    this.props.initialtestimonialStateData();
    firebase.notifications().removeAllDeliveredNotifications();
    this.getData();
    this.props.navigation.addListener("willFocus", payload => {
      console.log("Search willfocus");
      this.setState({ isPageVisible: true })
      try {
        let id = (this.props.toUserId).toString()
        AsyncStorage.setItem('@to_user_id', id)
        AsyncStorage.getItem('@to_user_id').then((uId) => {
          console.log('uId', uId)
        })
          .catch((err) => {
            console.log('err', err)
          })
      } catch (e) {
        // saving error
        console.log('Error in store authToken');
      }

    });
    this.props.navigation.addListener("willBlur", payload => {
      console.log("Search willBlur");
      this.setState({ isPageVisible: false })
      AsyncStorage.setItem('@to_user_id', '');
      this.chatRefData.off();
    });
  }

  componentWillUnMount() {
    this.chatRefData.off();
  }

  getData() {
    toUserId = this.props.navigation.getParam('toUserId');
    fullname = this.props.navigation.getParam('fullname');
    to_fire_id = this.props.navigation.getParam('to_fire_id');
    isUserFromNotification = this.props.navigation.getParam("isUserFromNotification");

    this.setState({ fullname: fullname });
    getUser()
      .then(user => {
        if (user.length > 0) {
          this.setState({ userData: user[0] });
          userID = user[0].id;
          fire_id = user[0].fire_id;
          console.log(" Database mathi user get user ID  ", userID);
          this.props.getChatDetail(userID, toUserId, this.props.SplashauthToken, true);
          this.props.clerchatnotification(userID, toUserId, this.props.SplashauthToken);
        }
      })
      .catch(error => {
        console.log(error);
      });

  }

  onSend(messages = []) {
    if (!this.state.is_block && this.state.chat_available) {
      // this.setState({
      //     messages: GiftedChat.append(this.state.messages, messages),
      // });
      console.warn('message_onSend', messages)
      console.warn('this.chatRefData', this.chatRefData)

      messages.forEach(message => {
        var now = new Date().getTime();
        this.chatRef.push({
          _id: now,
          text: message.text,
          createdAt: now,
          uid: this.user,
          fuid: uid,
          order: -1 * now,
          isRead: this.state.unreadMsgCount + 1
        });
        this.setState({
          unreadMsgCount: this.state.unreadMsgCount + 1
        }, () => {
          this.props.getChatNotification({
            userID: userID,
            toUserId: toUserId,
            message: message.text,
            message_count: this.state.unreadMsgCount,
            token: this.props.SplashauthToken
          });
        })
        // this.props.getChatNotification({
        //   userID: userID,
        //   toUserId: toUserId,
        //   message: message.text,
        //   token: this.props.SplashauthToken
        // });
        if (this.state.messages.length === 0) {
          console.warn("chat");

          this.props.userChatHistory(userID, toUserId, this.props.SplashauthToken);
        }
      });
    } else {
      if (this.state.is_block) {
        Alert.alert('Alert', 'You have blocked this user');
      } else {
        Alert.alert('Oops', 'You have been blocked by this user');
      }
    }
  }
  //  componentDidMount () {
  //   this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
  //   this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  // }
  //
  // componentWillUnmount () {
  //   this.keyboardDidShowListener.remove();
  //   this.keyboardDidHideListener.remove();
  // }

  _keyboardDidShow() {
    // this.setState({ emojiKeyBoardVisible: false });
  }

  _keyboardDidHide() {
    // alert('Keyboard Hidden');
  }

  onTestimonialChange(text) {
    this.props.testimonialChange(text);
  }
  onratingChange(rating) {
    console.log(rating);
    this.props.ratingChange(rating);
  }

  openEmojiKeyboard() {
    Keyboard.dismiss();
    this.setState({ emojiKeyBoardVisible: !this.state.emojiKeyBoardVisible });
  }

  componentWillReceiveProps(nextProp) {
    if (nextProp.chatAuthResult === "success chat Detail") {
      this.setState({
        is_friend: (nextProp.chatDetail.is_friend === 'Y') ? true : false,
        is_block: (nextProp.chatDetail.is_block === 'Y') ? true : false,
        chat_available: (nextProp.chatDetail.chat_available === 'Y') ? true : false,
        is_dot_allow: (nextProp.chatDetail.is_dot_allow === 'Y') ? false : true,
      })
      console.log('nextProp.authResultTestimonial', nextProp.authResultTestimonial);
      console.log('nextProp.authResultTestimonial', nextProp.authResultRetting);
      if (nextProp.authResultTestimonial === "Success Testimonial") {
        setTimeout(() => {
          Alert.alert(
            'Success',
            'Testimonial successfully',
            [
              { text: 'OK', onPress: () => this.setState({ TestimonialDialogVisible: false }) },
            ],
            { cancelable: false }
          )

        }, 50);

      }
      if (nextProp.authResultTestimonial === "Testimonial should not be null.") {
        // setTimeout(() => {
        Alert.alert("Oops", "Testimonial failed");

        // }, 300);

      }

      if (nextProp.authResultRetting === "Success Rating") {
        setTimeout(() => {
          Alert.alert(
            'Success',
            'Rating added successfully',
            [
              { text: 'OK', onPress: () => this.setState({ RatingDialogVisible: false }) },
            ],
            { cancelable: false }
          )
        }, 50);
      }
      if (nextProp.authResultRetting === "You have already rated this user.") {
        setTimeout(() => {
          Alert.alert(
            'Oops',
            'You have already rated this user.',
            [
              { text: 'OK', onPress: () => this.setState({ RatingDialogVisible: false }) },
            ],

          )
        }, 50);
      }
      if (nextProp.authResultBlockFriend === "Success Block") {
        setTimeout(() => {
          Alert.alert(
            'Success',
            'Friend block successfully',
            [
              { text: 'OK', onPress: () => { cancelable: false } },
            ],
            { cancelable: false }
          )

        }, 500);

      }
      if (nextProp.authResultBlockFriend === "User has already been reported") {

        setTimeout(() => {
          Alert.alert("Oops", "User has already been reported");

        }, 500);

      }
      // else if (nextProp.authResultBlockFriend === "User id must be numeric.") {
      //   setTimeout(() => {
      //     Alert.alert("Oops", "User id must be numeric.");

      //   }, 500);

      // }

    }

  }

  onButtonPress() {
    let { testimonial } = this.props;
    let isValid = true;
    if (testimonial === "") {
      isValid = false;
      this.props.testimonialErrorChange("Enter testimonial");
    } else {
      this.props.testimonialErrorChange("");
    }
    if (isValid === true) {
      // this.props.navigation.navigate('BottomTab', { myProps: this.props });
      // if (!this.props.isLoading) {
      this.props.getTestimonial({ userID, toUserId, testimonial, token: this.props.SplashauthToken });
      // }

    }
    // this.setState({ TestimonialDialogVisible: false })
  }

  onRatingPress() {
    const { rating } = this.props;
    if (rating != "") {
      let isValid = true;
      if (rating === "") {
        isValid = false;
        this.props.ratingErrorChange("Enter rating");
      } else {
        this.props.ratingErrorChange("");
      }
      if (isValid === true) {
        // this.props.navigation.navigate('BottomTab', { myProps: this.props });
        this.props.getRating({
          userID,
          toUserId,
          rating,
          token: this.props.SplashauthToken
        });
        console.log(rating);
      }
    } else {
      Alert.alert("Alert", "Please select rating");
    }
    // this.setState({ TestimonialDialogVisible: false })
  }

  onBlockFriend() {
    console.log(userID);
    console.log(toUserId);
    this.setState({ OptionDialogVisible: false, is_block: true });
    setTimeout(() => {
      Alert.alert(
        "Alert",
        "Are you sure you want to block " + fullname + "?",
        [
          {
            text: "Yes", onPress: () => {
              this.setState({ OptionDialogVisible: false, is_block: true });
              this.props.getBlockfriend({
                userID,
                toUserId,
                token: this.props.SplashauthToken
              });
            }
          },
          {
            text: "No",
            onPress: () => {
              this.setState({ is_block: false })
              cancelable: false;
            }
          }
        ],
        { cancelable: false }
      );
    }, 500);
    // this.props.getBlockfriend({ userID, toUserId, token: this.props.SplashauthToken });
    // this.props.getChatDetail(userID, toUserId, this.props.SplashauthToken);
  }
  onAddFriend() {
    console.log(userID);
    console.log(toUserId);
    this.setState({
      OptionDialogVisible: false,
      is_friend: !this.state.is_friend
    });
    if (this.state.is_friend) {
      setTimeout(() => {
        Alert.alert(
          "Alert",
          "Are you sure you want to remove " + fullname + " as your friend?",
          [
            { text: "Yes", onPress: () => this.props.unfriendApi(userID, toUserId, this.props.SplashauthToken) },
            {
              text: "No",
              onPress: () => {
                this.setState({ is_friend: true })
                cancelable: false;
              }
            }
          ],
          { cancelable: false }
        );
      }, 500);
    } else {
      this.props.getAddfriend({
        userID: userID,
        toUserId: toUserId,
        token: this.props.SplashauthToken
      });
    }
  }



  focusNextField(nextField) {
    this.refs[nextField].focus();
  }

  VishibleDailog() {

    if (this.state.chatCount > 4 && this.state.is_dot_allow) {
      this.setState({ is_dot_allow: false });
      setTimeout(() => {
        this.props.dotDisable(userID, toUserId, this.props.SplashauthToken);
        this.props.getChatDetail(userID, toUserId, this.props.SplashauthToken, false);
      }, 50);
    }
    this.setState({ OptionDialogVisible: true })
    // }
    // else{
    //   setTimeout(() => {
    //     Alert.alert(
    //       'Opus',
    //       'To unlock the options it is required to send 5 messages',
    //       [
    //         { text: 'OK', onPress: () => { cancelable: false } },
    //       ],
    //       { cancelable: false }
    //     )

    //   }, 0);

    // }
  }

  // renderSend(props) {
  //   return (
  //       <Send
  //           {...props}
  //       >
  //           <View style={{marginRight: 10, marginBottom: 15, alignItems: 'center', justifyContent: 'center' }}>
  //               <Image source={require("../img/send_message_icon.png")} 
  //                 style={{ height: 18, width: 18 }}
  //                 resizeMode={'center'}
  //               />
  //           </View>
  //       </Send>
  //   );
  // }
  onBackPress = () => {
    console.log('pressed', isUserFromNotification)
    if (isUserFromNotification) {
      this.props.navigation.replace('BottomTabChatFirst')
      return true;
    } else {
      this.props.navigation.goBack(null);
      // this.props.navigation.replace('Chat');
      return true;
    }
  }
  render() {
    // console.warn('msgs length', this.state.messages);

    return (
      <View style={ styles.container }>
        <Header
          onLeftPressed={ () => this.onBackPress() }
          leftImage={ require('../img/back_icon.png') }
          headertext={ this.state.fullname }
          rightImage={ require('../img/dot_menu.png') }
          onPressDone={ () => this.VishibleDailog() }
          isEnableGreenDot={ this.state.chatCount > 4 && this.state.is_dot_allow }
        />
        <View style={ { marginBottom: (DeviceInfo.getModel() === 'iPhone X' || DeviceInfo.getModel() === 'iPhone XS Max' || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 20 : 0, flex: 1 } }>
          <GiftedChat
            allowFontScaling={ false }
            messages={ this.state.messages }
            // onSend={ this.onSend.bind(this) }
            alwaysShowSend={ true }
            onSend={ (messages) => this.onSend(messages) }
            // renderCustomView={this.renderCustomView}
            renderBubble={ this.renderBubble }
            // renderSend={this.renderSend}
            user={ {
              _id: this.user
            } }
          />
        </View>
        <Dialog
          allowFontScaling={ false }
          visible={ this.state.OptionDialogVisible }
          title="Option"
          onTouchOutside={ () => this.setState({ OptionDialogVisible: false }) }
        >
          <View>
            <View style={ { justifyContent: 'center', alignItems: 'center' } }>
              <TouchableOpacity style={ styles.ViewStyle }
                disabled={ this.state.chatCount < 5 }
                onPress={ () => {
                  this.setState({ TestimonialDialogVisible: true, OptionDialogVisible: false });
                  this.props.testimonialErrorChange("");
                }
                }
              >
                <Text allowFontScaling={ false } style={ [styles.TextStyleView1, { color: (this.state.chatCount < 5) ? '#7F7F7F' : '#323232' }] }>Give Testimonial</Text>
              </TouchableOpacity>
              <TouchableOpacity style={ styles.ViewStyle }
                disabled={ this.state.chatCount < 5 }
                onPress={ () => this.setState({ RatingDialogVisible: true, OptionDialogVisible: false }) }
              >
                <Text allowFontScaling={ false } style={ [styles.TextStyleView1, { color: (this.state.chatCount < 5) ? '#7F7F7F' : '#323232' }] }>Rating</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={ styles.ViewStyle }
                onPress={ () => this.onAddFriend() } >
                <Text allowFontScaling={ false } style={ styles.TextStyleView1 }>{ (this.state.is_friend) ? 'Unfriend' : 'Add Friend' }</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={ [styles.ViewStyle, { borderColor: '#fff' }] }
                onPress={ () => this.onBlockFriend() } >
                <Text allowFontScaling={ false } style={ styles.TextStyleView1 }>{ 'Block' }</Text>
              </TouchableOpacity>
            </View>

          </View>
        </Dialog>
        <Dialog
          visible={ this.state.RatingDialogVisible }
          title="Rating"
          onTouchOutside={ () => this.setState({ RatingDialogVisible: false }) }
        >
          <View>
            <View style={ { justifyContent: 'center', alignItems: 'center' } }>
              <Text allowFontScaling={ false } style={ styles.TextStyleView }>Give a rating to</Text>
              <Text allowFontScaling={ false } style={ styles.userTextStyle }>{ fullname }</Text>
              <Stars
                default={ Number(this.props.rating) }
                count={ 10 }
                update={ (val) => { this.onratingChange(val) } }
                half={ false }
                starSize={ 15 }
                fullStar={ <Image source={ require('../img/full_star.png') } style={ [styles.myStarStyle] } /> }
                emptyStar={ <Image source={ require('../img/empty_star.png') } style={ [styles.myStarStyle, styles.myEmptyStarStyle] } /> }
              />
              <TouchableOpacity
                onPress={ () => this.onRatingPress() }
                activeOpacity={ 0.7 }
              >
                <Text allowFontScaling={ false } style={ styles.tapTextStyleView }>Tap to rate</Text>
              </TouchableOpacity>

            </View>
          </View>
        </Dialog>

        <Dialog
          visible={ this.state.TestimonialDialogVisible }
          title="Testimonial"
          onTouchOutside={ () => this.setState({ TestimonialDialogVisible: false }) }
        >


          <View style={ { height: 150 } }>
            <View style={ { justifyContent: 'center', alignItems: 'center' } }>
              <Text allowFontScaling={ false } style={ styles.TextStyleView }>Give a Testimonial to</Text>
              <Text allowFontScaling={ false } style={ styles.userTextStyle }>{ fullname }</Text>
            </View>
            <TextInput
              ref="1"
              allowFontScaling={ false }
              returnKeyType="done"
              keyboardType="default"
              multiline
              placeholderTextColor='#7F7F7F'
              placeholder="Write testimonial here..."
              value={ this.props.testimonial }
              onChangeText={ this.onTestimonialChange.bind(this) }
              style={ styles.textInputStyle1 }
            // onSubmitEditing={() => this.onButtonPress()}
            />
            <View
              style={ {
                borderBottomColor: "#261d1d26",
                borderBottomWidth: 1
              } }
            />
            <Text allowFontScaling={ false } style={ styles.errorTextStyle1 }>{ this.props.testimonialError }</Text>

            <View style={ { justifyContent: 'center', alignItems: 'center', marginTop: 3 } }>
              <Button
                color="#CC181E"
                onPress={ () => this.onButtonPress() }
                // onSubmitEditing={() => this.onButtonPress()}
                style={ styles.buttonTextSTyle } title="GIVE TESTIMONIAL"
              />
            </View>

          </View>
        </Dialog>
        {/* <Loader
          loading={this.props.isLoading}
        /> */}
        <Loader
          loading={ this.state.isLoading }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  container1: {
    flex: 1,
    backgroundColor: "white",
    borderTopWidth: 1,
    borderColor: "#e2e0e0",
  },
  textInputStyle: {
    color: '#2c2c2c',
    fontFamily: 'Lato-Regular',
    paddingLeft: 25,
    paddingRight: 25,
    paddingTop: 3,
    paddingBottom: 3,
    marginLeft: 19,
    fontSize: 16,
    width: '90%'
  },
  ViewStyle: {
    borderBottomWidth: 1,
    borderColor: '#e2e0e0',
    width: '100%'
  },
  TextStyleView1: {
    color: '#323232',
    fontSize: 16,
    padding: 10,
    textAlign: 'center',
    fontFamily: 'Lato-Regular',
  },
  TextStyleView: {
    color: '#7F7F7F',
    fontSize: 14,
    fontFamily: 'Lato-Regular',
  },
  userTextStyle: {
    fontSize: 14,
    color: '#4d4e51',
    fontFamily: 'Lato-Regular',
    marginTop: 1,
    marginBottom: 10
  },
  tapTextStyleView: {
    color: '#7F7F7F',
    fontSize: 18,
    fontFamily: 'Lato-Regular',
    marginTop: 6,
    marginLeft: 3,
    marginRight: 3,
    marginBottom: 3
  },
  buttonTextSTyle: {
    color: 'white',
    fontFamily: 'Lato-Regular',
    paddingLeft: 5,
    paddingRight: 5,
    fontSize: 14,
  },
  textInputStyle1: {
    color: '#1d1d26',
    fontFamily: 'Lato-Regular',
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 14,
    height: 60,
  },
  myEmptyStarStyle: {
    color: 'white',
    width: 15,
    height: 15,
    margin: 5
  },
  myStarStyle: {
    color: '#147f7e',
    backgroundColor: 'transparent',
    textShadowColor: 'black',
    textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 2,
    width: 15,
    height: 15,
    margin: 5,
  },
  errorTextStyle1: {
    fontSize: 14,
    color: "#F00",
    alignItems: "center",
    justifyContent: "center"
  },
});
const mapStateToProps = state => {
  return {
    testimonial: state.Testimonial.testimonial,
    testimonialError: state.Testimonial.testimonialError,
    authResultTestimonial: state.Testimonial.authResultTestimonial,
    // authResultBlockFriend: state.Testimonial.authResultBlockFriend,
    authResultRetting: state.Testimonial.authResultRetting,
    isLoading: state.Chat.isLoading,
    rating: state.Testimonial.rating,
    ratingError: state.Testimonial.ratingError,
    SplashauthToken: state.Splash.authToken,
    to_fire_id: state.Chat.to_fire_id,
    toUserId: state.Chat.user_id,
    chatDetail: state.Chat.chatDetail,
    chatAuthResult: state.Chat.authResult,
  };
};

export default connect(
  mapStateToProps,
  {
    getTestimonial,
    getAddfriend,
    getRating,
    getBlockfriend,
    ratingChange,
    ratingErrorChange,
    initialtestimonialStateData,
    testimonialChange,
    testimonialErrorChange,
    getChatNotification,
    userChatHistory,
    unfriendApi,
    getChatDetail,
    dotDisable,
    clerchatnotification
  }
)(ChatMessage);
