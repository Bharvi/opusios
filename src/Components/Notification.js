/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Modal,
  Image, TouchableOpacity,
  PushNotificationIOS
} from 'react-native';
import Header from './Header';
import ItemNotification from './ItemNotification';
import { ProgressDialog, Dialog } from 'react-native-simple-dialogs';
import { connect } from "react-redux";
import {
  getFeedlist,
  getnotificationlist,
  initialgetallnotificationStateData,
  getNotification_clearAll
} from "../Actions";
import { getUser } from "../Database/allSchema";
import { POSTIMAGEPATH } from "../Actions/type";

let userId = "";

class Notification extends Component {

  constructor (props) {
    super(props);
    this.state = {
      prifilePicDialogVisible: false,
      notification: [
        { image: 'http://maya.projectdemo.website/upload/brands/ffd2b007e90af7fc1fa242b87d82cd377194a2e2.jpg', title: 'Erika Pluda', des: 'like your post', time: 'just Now' },
        { image: 'http://maya.projectdemo.website/upload/brands/ffd2b007e90af7fc1fa242b87d82cd377194a2e2.jpg', title: 'Erika Pluda', des: 'like your post', time: 'just Now' },
        { image: 'http://maya.projectdemo.website/upload/brands/ffd2b007e90af7fc1fa242b87d82cd377194a2e2.jpg', title: 'Erika Pluda', des: 'like your post', time: '1 day' },
        { image: 'http://maya.projectdemo.website/upload/brands/ffd2b007e90af7fc1fa242b87d82cd377194a2e2.jpg', title: 'Erika Pluda', des: 'like your post', time: '5 day' },
      ],
      userphoto: '',
    };
  }

  componentWillMount() {
    this.props.initialgetallnotificationStateData();
    postID = this.props.navigation.getParam("postID");
    console.log(postID);
    getUser()
      .then(user => {
        if (user.length > 0) {
          // this.setState({ userData: user[0] });
          userId = user[0].id;
          console.log(userId);
          this.CallApi();
        }
      })
      .catch(error => {
        console.log(error);
      });
    didFocusSubscription = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.props.getFeedlist({
          userId,
          page1: 1,
          token: this.props.SplashauthToken,
          isLoading: false,
          isRefreshing: false
        });
        console.log('getfeedlist called..');
      }
    );
  }
  componentWillUnMount() {
    didFocusSubscription.remove();
  }


  CallApi() {
    this.props.getnotificationlist({ userId, token: this.props.SplashauthToken });
  }

  OnPressed() {
    this.props.navigation.navigate("FeedsPage");
  }
  notificationclearAll() {
    this.props.getNotification_clearAll({ userId, token: this.props.SplashauthToken });
    this.props.getnotificationlist({ userId, token: this.props.SplashauthToken });
    // PushNotificationIOS.setApplicationIconBadgeNumber(0);
  }
  OnPressClick(item) {
    if (item.notification_type === "1") {
      return this.props.navigation.navigate("CommentPage", { postID: item.post_id });
    }
    else if (item.notification_type === "2") {
      return this.props.navigation.navigate("CommentPage", { postID: item.post_id });
    }
    else if (item.notification_type === "5") {
      return this.props.navigation.navigate("Profile", {
        user_id: item.user_id,
        form: 'Notification'
      });
    }
    else if (item.notification_type === "8") {
      return this.props.navigation.navigate("CommentPage", { postID: item.post_id });
    }
  }

  onOtherProfile(item) {
    console.log("profie click");
    getUser()
      .then(user => {
        if (user.length > 0) {
          let userId = user[0].id;
          console.log("sds", userId, item.user_id);
          if (item.user_id == userId) {
            console.log("Myprofile");
            console.log(this.props.navigation);

            this.props.navigation.navigate('MyProfile');
          }
          else {
            console.log("Profile");
            this.props.navigation.navigate('Profile', {
              user_id: item.user_id,
              form: 'Notification'
            });
          }

        }
      })
      .catch(error => {
        console.log(error);
      });
  }


  render() {
    return (
      <View style={ styles.container }>
        <Header
          onLeftPressed={ () => this.OnPressed() }
          leftImage={ require('../img/back_icon.png') }
          headertext="Notification"
          textDone="Clear All"
          onPressDone={ () => this.notificationclearAll() }
          isVisibleClearAll={ this.props.Notification_list.length > 0 }
        />
        <View>
          <View style={ { marginBottom: 50 } }>
            {
              (this.props.Notification_list != null && this.props.Notification_list.length > 0) ?
                <FlatList
                  data={ this.props.Notification_list }
                  renderItem={ ({ item, index }) => {
                    console.log(item.user_name);
                    return (
                      <ItemNotification
                        fireId={ item.firebase }
                        usericon={ { uri: POSTIMAGEPATH + item.user_image } }
                        onPressProfile={ () => this.OnPressClick(item) }
                        name={ item.user_name }
                        des={ item.notification_type }
                        onProfile={ () => this.onOtherProfile(item) }
                        timing={ item.notification_date }
                        onPressUserIcon={ () => this.setState({ prifilePicDialogVisible: true, userphoto: POSTIMAGEPATH + item.user_image }) }
                      />
                    );
                  } }
                /> :
                <View
                  style={ {
                    justifyContent: "center",
                    alignItems: "center",
                    height: '100%',
                    width: '100%',
                    backgroundColor: 'white'
                  } }
                >
                  {/* <Image style={{ height: 140, width: 140}}
            source={require('../img/notification-not-found.jpg')}/> */}
                  <Text style={ { fontSize: 13, fontFamily: "Lato-Regular", color: '#686868', } }>You have no notifications</Text>
                </View>
            }
          </View>
        </View>

        <Modal
          transparent={ true }
          visible={ this.state.prifilePicDialogVisible }
          onRequestClose={ () => this.setState({ prifilePicDialogVisible: false }) }
        >
          <View
            style={ {
              height: "100%",
              width: "100%",
              paddingTop: 130,
              paddingBottom: 100
            } }
          >
            <View style={ { backgroundColor: 'rgba(0,0,0,0.9)', padding: 5, justifyContent: 'center', alignContent: 'center' } }>
              <View style={ { alignItems: "center", justifyContent: "center" } }>
                <Image
                  style={ { width: "100%", height: "100%" } }
                  source={ { uri: this.state.userphoto } }
                  resizeMode="contain"
                />
                <TouchableOpacity
                  style={ styles.CloseViewSTyle }
                  onPress={ () =>
                    this.setState({ prifilePicDialogVisible: false })
                  }
                >
                  <Image
                    style={ { height: 18, width: 18 } }
                    source={ require("../img/close_icon.png") }
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  CloseViewSTyle: { position: "absolute", right: 2, top: 0, backgroundColor: 'rgba(255,0,0,0.5)', width: 32, height: 32, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }
});
const mapStateToProps = state => {
  return {
    Notification_list: state.Notificationlist.Notification_list,
    isLoading: state.Notificationlist.isLoading,
    isLoadingclear: state.Notificationlist.isLoadingclear,
    authResult: state.Notificationlist.authResult,
    SplashauthToken: state.Splash.authToken,
  };
};

export default connect(
  mapStateToProps,
  {
    getnotificationlist,
    getFeedlist,
    initialgetallnotificationStateData,
    getNotification_clearAll
  }
)(Notification);
