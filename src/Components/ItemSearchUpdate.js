import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, Dimensions, ImageBackground } from 'react-native';
import ImageLoad from './ImageLoad';
import UserProfileImg from './common/UserProfileImg';
let { width } = Dimensions.get('window');

let boxCount = 3;
let boxWidth = width / boxCount;
//reset route
// const navigateToScreen = (route) => () => {
//   const resetAction = StackActions.reset({
//           index: 0,
//           actions: [NavigationActions.navigate({ routeName: route })],
//       });
//   this.props.navigation.dispatch(resetAction);
// };

export default class ItemSearchUpdate extends Component {

  render() {
    // console.log('this.props.fireId', this.props.fireId)

    const { container, viewDivideStyle, cartImageStyle } = styles;
    return (
      <View style={ container }>
        <View style={ { flex: 3, flexDirection: 'row', padding: 10 } }>
          <View
            style={ styles.viewDivideStyle }
          >
            <TouchableOpacity
              style={ { flexDirection: 'row' } }
              activeOpacity={ 0.8 }
              onPress={ this.props.onPressUserIcon }
            >
              {/* <ImageLoad
                           style={{ height: 50, width: 50, borderRadius: 220 / 2 }}
                           borderRadius={50 / 2}
                           source={this.props.usericon}
                     /> */}
              <UserProfileImg
                style={ { height: 50, width: 50, borderRadius: 50 / 2 } }
                borderRadius={ 50 / 2 }
                source={ this.props.usericon }
                dotSize={ 10 }
                fireId={ this.props.fireId }
                right={ '22%' }
                top={ '60%' }
              />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={ 0.8 }
              onPress={ this.props.onPressProfile }
            >
              <View style={ { flexWrap: 'wrap', paddingRight: 100, flexDirection: 'row', marginLeft: 8 } }>
                <Text allowFontScaling={ false } style={ { color: '#CC181E', fontSize: 14, fontFamily: 'Lato-Bold' } }>{ this.props.name }</Text>
                <Text allowFontScaling={ false } style={ { color: '#686868', fontSize: 14, fontFamily: 'Lato-Regular' } }> searched for</Text>
                <Text allowFontScaling={ false } style={ { color: 'black', fontSize: 14, fontFamily: 'Lato-Bold', } }> { this.props.des } </Text>
                <Text allowFontScaling={ false } style={ { color: '#686868', fontSize: 14, fontFamily: 'Lato-Regular' } }>tag</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={ 0.8 }
              style={ styles.viewDivideStyle1 }
              onPress={ this.props.onPress }
            >
              <Image
                style={ { height: 35, width: 35, } }
                source={ this.props.commentImage }
              />

            </TouchableOpacity>
          </View>
        </View>
      </View >
    );
  }
}

const styles = {
  container: {
    backgroundColor: '#ffffff',
    width: '97%',
    flex: 3,
    borderBottomWidth: 1,
    marginLeft: 5,
    marginRight: 5,
    borderColor: '#dddee4'
  },
  viewDivideStyle: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    width: boxWidth
  },
  viewDivideStyle1: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 8
  },
  commentViewStyle: {
    height: 40,
    width: 40,
    borderWidth: 1,
    borderRadius: 20,
    justifyContent: 'center',
    borderColor: '#28969d',
    alignItems: 'center',
    marginLeft: 40
  }

};
