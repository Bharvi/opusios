/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  Platform
} from 'react-native';
import { connect } from 'react-redux';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import {getAllTags , onPressTagView, locationChange1, locationErrorChange1, profecationChange1, profecationErrorChange1 } from '../Actions';
import Header from './HeaderSeting';
import DeviceInfo from 'react-native-device-info';

const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } }};
const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } }};

 class LocationAutosuggestion extends Component {

   OnPressed() {
     this.props.navigation.goBack(null);
   }
  render() {
    return (
      <View style={styles.container}>
      {/* <StatusBar
         backgroundColor="#CC181E"
         barStyle="light-content"
       /> */}
       <Header
            onLeftPressed={() => this.OnPressed()}
            leftImage={require('../img/back_icon.png')}
            headertext="Search Location"
          />
      <GooglePlacesAutocomplete
      placeholder='Search Location'
      minLength={1}
      autoFocus={false}
      returnKeyType={'search'}
      listViewDisplayed='auto'
      fetchDetails={true}
        // renderDescription={row => row.description}
      onPress={(data, details = null) => {
        this.props.locationChange1(data.description);
        this.props.navigation.goBack(null);
        console.log(data);
      }}

      getDefaultValue={() => ''}

      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: 'AIzaSyA0ZF94MaAt6YiktchxAcrNYORBJfOcLR8',
        language: 'en', // language of the results
        types: '(cities)' // default: 'geocode'
      }}

      styles={{
        textInputContainer: {
          backgroundColor: 'white',
          borderTopWidth: 0,
        },
        description: {
       },
       predefinedPlacesDescription: {
         color: 'white',
       },
        textInput: {
          marginLeft: 10,
          marginRight: 7,
          height: 35,
          color:'black',
          backgroundColor: 'transparent',
          fontSize: 16,
          borderWidth: 1, borderColor: '#e2e0e0'
        },
        container:{
         backgroundColor:'transparent',

       },
        poweredContainer:{
          justifyContent:'center',
          backgroundColor:'transparent'
        }
        }}

        currentLocation={false}
        currentLocationLabel="Current location"
        nearbyPlacesAPI='GooglePlacesSearch'
        GoogleReverseGeocodingQuery={{
          // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
        }}

        renderRightButton={() =>
          <TouchableOpacity
          onPress={() => this.props.navigation.goBack(null)}
          style = {{ alignItems:'center',alignSelf:'center' }}
          >
          <Text allowFontScaling={false} style = {{ fontSize: 16 , color:'#3f4042', marginRight:10, fontFamily: "Lato-Regular"}}>Cancel</Text>
          </TouchableOpacity>}


    />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,    
    backgroundColor: 'white'
  },
});

const mapStateToProps = state => {
      return{
        tags:state.CompeleProfile.tags,
        professions:state.CompeleProfile.professions,
        profession:state.CompeleProfile.profession,
        professionError:state.CompeleProfile.professionError,
        location:state.CompeleProfile.location,
        locationError:state.CompeleProfile.locationError,
        authResulttags:state.CompeleProfile.authResulttags,
        isLoading:state.CompeleProfile.isLoading,
      };
  };

export default connect( mapStateToProps,{ locationChange1, locationErrorChange1 })(LocationAutosuggestion);
