/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity
} from 'react-native';
import ImageLoad from './ImageLoad';
import moment from 'moment';
import UserProfileImg from '../Components/common/UserProfileImg';
import firebase from "react-native-firebase";
import realm, { getUser, getChatListData } from "../Database/allSchema";

let userImg = '';
let userName = '';
let firebaseCurrentUser = '';
let chatId = '';
let messagesArr = []
let userId = ''


export default class ItemChat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      msgCount: 0,
      msg: '',
      messagesArr: []
      // time: null
    };
    getUser()
      .then((user) => {
        if (user && user.length > 0) {
          // console.log('user[0].id', user[0].id);
          userId = user[0].id;
          userFireId = user[0].fire_id
          if (user[0].id == 1) {
            firebaseCurrentUser = 'afdsfsdfdsfsdfsdfsdf';
          } else {
            firebaseCurrentUser = user[0].fire_id
          }
        }
      })

  }
  componentWillMount() {

    this.setItems(this.props.item)
    // this.setUserStatus(this.props.fireId)


  }
  componentWillReceiveProps(nextProps) {
    this.setItems(nextProps.item)
    // this.setUserStatus(nextProps.fireId)

  }

  componentDidMount() {

  }

  setUserStatus(fireId) {
    firebase.database().ref('friends/' + fireId)
      .on('value', (snapshot) => {
        // user Data 
        if (snapshot.val()) {
          if (snapshot.val().onlineStatus == 'true') {
            // isUserOnline = true
            this.setState({ isUserOnline: true })
          } else {
            // isUserOnline = false
            this.setState({ isUserOnline: false })
          }
        } else {
          // isUserOnline = false
          this.setState({ isUserOnline: false })
        }
      });
  }

  setItems(item) {
    let chatRefData = firebase.database().ref().child("chat/" + item.chatId).orderByChild("order").limitToFirst(1)
    let count = firebase.database().ref().child("chat/" + item.chatId).orderByKey("isRead").limitToLast(1);

    count.on("value", snap2 => {
      snap2.forEach(child2 => {
        if (firebaseCurrentUser != child2.val().uid) {
          // console.log("value", child2.val().isRead);
          //  let isReadMsg = (child2.val().isRead) ? child2.val().isRead : 0;
          this.setState({ msgCount: (child2.val().isRead) ? child2.val().isRead : 0 })
        }
      });
    });
  }



  render() {
    // console.log('index', this.props.index)
    let msgTime = '';
    if (moment(new Date()).format("DD-MM-YYYY") === moment(this.props.time).format("DD-MM-YYYY")) {

      msgTime = moment(this.props.time).format("hh:mm a");
      // console.warn('msgTime');
    }
    else {
      msgTime = moment(this.props.time).format("DD/MM/YY");
      // console.warn('msgTime');
    }
    return (
      <TouchableOpacity
        key={this.props.item.firebase}
        style={styles.container}
        onPress={this.props.onPressProfile1}
        activeOpacity={0.8}>
        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }} >
          <TouchableOpacity
            style={{}}
            activeOpacity={0.8}
            onPress={this.props.onPressUserIcon}
          >
            {/* <View style={{ position: 'absolute', width: 8, height: 8, backgroundColor: '#46D009', borderRadius: 10, left: 35, top: -10 }} /> */}
            {/* <ImageLoad
              style={{ height: 50, width: 50, borderRadius: 200 / 2 }}
              borderRadius={50 / 2}
              source={this.props.userphoto}
            /> */}
            <UserProfileImg
              style={{ height: 50, width: 50, borderRadius: 200 / 2 }}
              borderRadius={50 / 2}
              source={this.props.userphoto}
              dotSize={10}
              fireId={this.props.fireId}
              top={'60%'}
              right={'23%'}
            />
          </TouchableOpacity>
          {/* <View style={{ flex: 4, borderBottomWidth: 1, borderColor: '#e2e0e0',}}> */}
          <View style={{ marginLeft: 10, paddingRight: 50, }}>
            <View
              activeOpacity={0.8}
              style={{
                // flex: 1,
                // flexWrap: 'wrap'
              }}
            >
              <Text
                numberOfLines={1}
                onPress={this.props.Otherptofile}
                allowFontScaling={false}
                style={styles.MarcusTextStyle}>
                {this.props.name}
              </Text>
              {(this.props.msg) ?
                <Text
                  numberOfLines={1}
                  allowFontScaling={false}
                  style={styles.yesterdayTextStyle} >{this.props.msg}</Text>
                : null}
            </View>

            <View>

            </View>
          </View>
          <Text allowFontScaling={false} style={styles.timeText}>{msgTime}</Text>

          {/* {
            (this.props.msgCount > 0) ?

              <View style={ styles.countView }>
                <Text allowFontScaling={ false }
                  style={ [{ fontSize: (this.props.msgCount >= 100) ? 9 : 12 }, styles.countText] }>

                  { (this.props.msgCount >= 100) ? '99+' : this.props.msgCount }
                </Text>
              </View>
              : null
          } */}

          {
            (this.state.msgCount > 0) ?
              (this.props.msgCount > 0) ?
                <View style={styles.countView}>
                  <Text allowFontScaling={false}
                    style={[{ fontSize: (this.props.msgCount >= 100) ? 9 : 12 }, styles.countText]}>
                    {(this.props.msgCount >= 100) ? '99+' : this.props.msgCount}
                  </Text>
                </View>
                : null
              : null
          }


          <View style={{ backgroundColor: '#e2e0e0', height: 0.5, marginTop: 10 }} />
          {/* </View> */}


        </View>
        <View style={{ marginLeft: 60, marginRight: 10, borderBottomWidth: 1, borderColor: '#e2e0e0' }} />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  countView: {
    height: 22,
    width: 22,
    borderRadius: 22 / 2,
    backgroundColor: '#CC181E',
    position: 'absolute',
    right: 10,
    bottom: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  countText: {
    fontSize: 12,
    color: 'white',
    fontWeight: 'bold'
  },
  timeText: { color: '#B3B3B3', position: 'absolute', right: 10, top: 15, fontSize: 10 },
  MarcusTextStyle: {
    flex: 1, fontSize: 18, fontFamily: 'Lato-Bold', color: '#CC181E', paddingRight: 40
  },
  yesterdayTextStyle:
  {
    fontSize: 14,
    color: '#B3B3B3',
    fontFamily: 'Lato-Regular',
    marginTop: 3,
    // width: '60%',
    flex: 1,
    paddingRight: 22,
  },
  UnfriendTextStyle: { fontSize: 14, color: '#B3B3B3', fontFamily: 'Lato-Regular' },
  UnfriendViewStyle: {
    width: 70,
    height: 25,
    borderWidth: 1,
    borderColor: '#C7C7C7',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3
  }
});
