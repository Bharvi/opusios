/* @flow */

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
  Modal,
  Share,
  Alert
} from "react-native";
import ItemPost from "./ItemPost";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import { connect } from "react-redux";
import { getMYPosts, getPostsSharepost, getPostsAbusePost } from "../Actions";
import { getUser } from "../Database/allSchema";
import { POSTIMAGEPATH } from "../Actions/type";
import ImageLoad from './ImageLoad';
import Loader from './Loader';

let userId = "";
let postID = "";
let shareText = "Test 2";

class Postsprofile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prifilePicDialogVisible: false,
      userPhoto: "",
      post_photo: "",
      post_caption: "",
      page: 1,
      posts: [],
      pageCount: 1,
      PostData: [
        {
          name: "Marcus Richards",
          des: "Yesterday, Halifax, N.S",
          title: "Good friends, good food and a lot of laughs."
        },
        {
          name: "Marcus Richards",
          des: "Yesterday, Halifax, N.S",
          title: "Good friends, good food and a lot of laughs."
        },
        {
          name: "Marcus Richards",
          des: "Yesterday, Halifax, N.S",
          title: "Good friends, good food and a lot of laughs."
        }
      ]
    };
    // this.shareMessage = this.shareMessage.bind(this);
  }
  // shareMessage() {
  //   console.log(this.state.post_caption);
  //   let image = POSTIMAGEPATH + this.state.post_photo;
  //   console.log(image);
  //   Share.share({
  //     message: this.state.post_caption
  //     // url: image ,
  //   });
  // }

  componentWillReceiveProps(nextProp) {
    if(nextProp.authResult === 'posts success') {
      this.setState({ posts: nextProp.posts })
    }
    if (nextProp.authResultSharePost === "Success") {
      setTimeout(() => {
        // this.shareMessage();
       
      }, 500);
      
    } else if (nextProp.authResultSharePost === "User id must be numeric.") {
      Alert.alert("Oops", "User id must be numeric.");
    } else if (nextProp.authResultSharePost === "Post id must be numeric.") {
      Alert.alert("Oops", "Post id must be numeric.");
    }
    if (nextProp.authResultAbusePost === "Success Abuse") {
      setTimeout(() => {
        Alert.alert(
          "Success",
          "Report Abuse successfully.",
          [
            {
              text: "OK",
              onPress: () => {
                this.setState({ pageCount: 1 });
                this.fetchData();
              }
            }
          ],
          { cancelable: false }
        );
       
      }, 500);
      
    } else if (
      nextProp.authResultAbusePost === "Already Post is Abuse by same User"
    ) {
      setTimeout(() => {
        Alert.alert("Oops", "Already post is abuse by same user");
      }, 500);
      
    } else if (nextProp.authResultAbusePost === "User id must be numeric.") {
      setTimeout(() => {
        Alert.alert("Oops", "User id must be numeric.");
      }, 500);
      
    } else if (nextProp.authResultAbusePost === "Post id must be numeric.") {
      setTimeout(() => {
        Alert.alert("Oops", "Post id must be numeric.");
      }, 500);
      
    }
  }
  
  componentDidMount() {
      this.fetchData();
    this.willFocusSubscription = this.props.myNavigation.addListener(
      "willFocus",
      () => {
          this.setState({ pageCount: 1 });
          this.fetchData();
      }
    );
  }
  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  fetchData = () => {
    console.log(userId, this.state.pageCount);

    this.props.getMYPosts({userId:this.props.userId, page: this.state.pageCount, token: this.props.SplashauthToken });
  };

  handleLoadMore = () => {
    this.setState(
      {
        pageCount: this.state.pageCount + 1
      },
      () => {
        console.log(this.state.pageCount);
        if (this.props.isData) {
          this.props.getMYPosts({userId:this.props.userId, 
          page:this.state.pageCount, posts:this.state.posts, token: this.props.SplashauthToken});
        }
      }
    );
  };

  onCommentRedirt(item) {
    this.props.myNavigation.navigate("CommentPage", { postID: item.post_id });
  }

  onProfileData(item) {
    console.log("profie click");
    getUser()
      .then(user => {
        if (user.length > 0) {
         let userId = user[0].id;
          console.log("sds", userId, item.user_id );
            if(item.user_id == userId){
              console.log(this.props.navigation);
               this.props.myNavigation.navigate('MyProfile');
            }
            else {
              console.log("Profile");
               this.props.myNavigation.goBack();
               this.props.myNavigation.navigate('Profile', {
                  user_id: item.user_id,
                  form: 'MyProfile1'
                });
            }
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  onSharePost(item) {
    // console.log("on share post ", postID);
    // this.setState({
    //   post_photo: POSTIMAGEPATH + item.post_photo,
    //   post_caption: item.post_caption
    // });
    // console.log(this.state.post_photo);
    // console.log(this.state.post_caption);
    setTimeout(() => {
      Alert.alert(
        "Opus",
        "Do you want to share the post?",
        [
          {
            text: "Yes",
            onPress: () => {
              this.props.getPostsSharepost({userId:this.props.userId, 
                postID:item.post_id, 
                shareText, 
                token: this.props.SplashauthToken});
              this.props.myNavigation.navigate('FeedsPage');
            }
          },
          {
            text: "No",
            onPress: () => {
              cancelable: false;
            }
          }
        ],
      );
     
    }, 500);

    
  }
  onAbusePost(item) {
    console.log(userId);
    console.log(item.post_id);
    this.props.getPostsAbusePost({userId: this.props.userId, postID:item.post_id, token: this.props.SplashauthToken });
  }

  onPressLikeBtn(item, index) {
    console.warn("index",index);
    let tempFeedList = this.state.posts;
    tempFeedList[index] = {...tempFeedList[index], 
      is_liked: (tempFeedList[index].is_liked == "Y") ? "N" : "Y",
      like_count: (tempFeedList[index].is_liked == "Y") ? Number(tempFeedList[index].like_count) - 1 : Number(tempFeedList[index].like_count) + 1,
    };
    this.setState({ posts: tempFeedList });
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: "white"  }}>
        {
        this.props.posts != null &&
        this.props.posts.length > 0 ? (
          <FlatList
          style={{ marginBottom: 10 }}
            data={this.props.posts}
            extraData={this.props.posts}
            onEndReached={this.handleLoadMore.bind(this)}
            onEndReachedThreshold={2}
            renderItem={({ item, index }) => {
              return (
                <ItemPost
                item={item}
                name={item.fullname}
                userprofileImage={{ uri: POSTIMAGEPATH + item.user_photo }}
                des={item.time}
                postTitle={item.post_caption}
                Image={POSTIMAGEPATH + item.post_photo}
                numOfComments={item.comment_count}
                onPressComment={() => this.onCommentRedirt(item)}
                sharepost={() => this.onSharePost(item)}
                Abusepost={() => this.onAbusePost(item)}
                onPressUserIcon={() =>
                  this.setState({
                    prifilePicDialogVisible: true,
                    userPhoto: POSTIMAGEPATH + item.user_photo
                  })
                }
                openMyProfile={() => this.onProfileData(item)}
                onPressLike={() => this.onPressLikeBtn(item, index)}
                />
              );
            }}
          />
        ) : (
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              flex: 1,
              height: 500
            }}
          >
            {/* <Image
              style={{ height: 130, width: 130 }}
              source={require("../img/no-data-found.jpg")}
            /> */}
            <Text style={{ fontSize: 13, fontFamily: "Lato-Regular", color: '#686868',}}>No posts</Text>
          </View>
        )}

        <Modal
          transparent={true}
          visible={this.state.prifilePicDialogVisible}
          onRequestClose={() =>
            this.setState({ prifilePicDialogVisible: false })
          }
        >
          <View
            style={{
              height: "100%",
              width: "100%",
              paddingTop: 130,
              paddingBottom: 100
            }}
          >
            <View
              style={{
                backgroundColor: "rgba(0,0,0,0.9)",
                padding: 5,
                justifyContent: "center",
                alignContent: "center"
              }}
            >
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <ImageLoad
                  style={{ height: "100%", width: "100%" }}
                  source={{ uri: this.state.userPhoto }}
                  resizeMode="contain"
                />

                <TouchableOpacity
                  style={styles.CloseViewSTyle}
                  onPress={() =>
                    this.setState({ prifilePicDialogVisible: false })
                  }
                >
                  <Image
                    style={{ height: 18, width: 18 }}
                    source={require("../img/close_icon.png")}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        {
          <Loader
            loading={this.props.isLoading}
            
          />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  CloseViewSTyle: {
    position: "absolute",
    right: 2,
    top: 2,
    backgroundColor: "rgba(255,0,0,0.5)",
    width: 32,
    height: 32,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  }
});
const mapStateToProps = state => {
  return {
    posts: state.Posts.posts,
    authResult: state.Posts.authResult,
    authResultSharePost: state.Posts.authResultSharePost,
    authResultAbusePost: state.Posts.authResultAbusePost,
    isLoading: state.Posts.isLoading,
    isdata: state.Posts.isdata,
    SplashauthToken: state.Splash.authToken,
  };
};

export default connect(
  mapStateToProps,
  {
    getMYPosts,
    getPostsSharepost,
    getPostsAbusePost
  }
)(Postsprofile);
