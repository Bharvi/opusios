/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  ScrollView,
  Dimensions,
  Modal,
  Alert,
  Platform
} from 'react-native';
import Itemsuggestedprofile from './Itemsuggestedprofile';
import { ProgressDialog, Dialog } from 'react-native-simple-dialogs';
import { getUser } from "../Database/allSchema";
import { getSearchResult, initialgetsearchResultStateData, getAddfriend1, saveUserChat, clerchatnotification } from "../Actions";
import { connect } from "react-redux";
import { POSTIMAGEPATH, PROFILEIMAGEPATH } from "../Actions/type";
import ImageLoad from './ImageLoad';
import Loader from './Loader';

const productWidth = Dimensions.get('window').width;

let searchtag = '';
let userId = '';

class SearchResult extends Component {

  constructor (props) {
    super(props);
    this.state = {
      prifilePicDialogVisible: false,
      searchText: '',
      loading: false,
      data: [],
      page: 1,
      refreshing: false,
      userPhoto: '',
      SuggestedProfile: [
        { a: 'Morgan Seibert', b: 'Marketing', c: 'Mumbai,Maharashtra,India', d: '120 Users' },
        { a: 'Morgan Seibert', b: 'Marketing', c: 'Mumbai,Maharashtra,India', d: '12 Users' },
        { a: 'Morgan Seibert', b: 'Marketing', c: 'Mumbai,Maharashtra,India', d: '120 Users' },
        { a: 'Morgan Seibert', b: 'Marketing', c: 'Mumbai,Maharashtra,India', d: '10 Users' },
      ],
    };
  }
  componentWillReceiveProps(nextProp) {

    if (nextProp.authResultAddFriend === "Success Addfriend") {
      console.log("Success");
    } else if (nextProp.authResultAddFriend === "Already Friend") {
      console.log("Already Friend");
      // Alert.alert("Oops", "Already Friend");
    }
    else if (nextProp.authResultAddFriend === "User id must be numeric.") {
      setTimeout(() => {
        Alert.alert("Oops", "User id must be numeric.");
      }, 500);


    }
    console.log('this.props.total_results', nextProp.total_results);
  }

  componentWillMount() {
    searchtag = this.props.navigation.getParam("searchText");
    console.warn(searchtag);
    getUser()
      .then(user => {
        if (user.length > 0) {
          userId = user[0].id;
          console.log(userId);
          this.fetchData();
        }
      })
      .catch(error => {
        console.log(error);
      });
    this.setState({ searchText: searchtag })
  }
  closedata() {
    // this.setState({ searchText : '' })
  }

  fetchData = () => {

    this.props.getSearchResult({ userId, page: this.state.page, tag: searchtag, token: this.props.SplashauthToken });
  };

  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        if (this.props.isdata) {
          this.props.getSearchResult({ userId, page: this.state.page, tag: searchtag, search_result: this.props.search_result, token: this.props.SplashauthToken });
        }

      }
    );
  };

  onChatMessage(item) {
    console.log(item.user_id);
    console.log(item.fullname);
    console.log(item.firebase);
    this.props.saveUserChat(item.user_id, item.fullname, item.firebase);
    this.props.clerchatnotification(userId, item.user_id, this.props.SplashauthToken);
    setTimeout(() => {
      this.props.navigation.navigate('ChatMessage', { toUserId: item.user_id, fullname: item.fullname, to_fire_id: item.firebase });
    }, 1000);

  }
  onProfileData(item) {
    console.log(item);
    this.props.navigation.navigate('Profile', { user_id: item.user_id, fullname: item.fullname, profile_image: item.profile_image, average_rating: item.average_rating, user_give_rating_count: item.user_give_rating_count, form: 'searchResult', searchText1: searchtag })
  }

  render() {
    return (
      <View style={ styles.container } >
        <View style={ { backgroundColor: "#CC181E", height: 12 } }></View>
        <View style={ { backgroundColor: "#CC181E", paddingTop: (Platform.OS === 'ios') ? 10 : 0 } } />
        <View style={ { height: 55, backgroundColor: '#CC181E', flexDirection: 'row', alignItems: 'center' } }>
          <TouchableOpacity
            style={ { flexDirection: 'row', height: 50, alignItems: 'center' } }
            activeOpacity={ 1 }
            onPress={ () => this.props.navigation.navigate("Search") }
          >
            <Image
              source={ require('../img/back_icon.png') }
              style={ styles.backImageStyle }
            />

            <Text numberOfLines={ 1 } allowFontScaling={ false } style={ styles.searchTextStyleHeder }>Search for { this.state.searchText }</Text>
          </TouchableOpacity>
          {
            // <View style={styles.viewStyle}>
            //       <TouchableOpacity>
            //         <Image
            //           source={require('../img/search_icon.png')}
            //           style={styles.searchImageStyle}
            //         />
            //       </TouchableOpacity>
            //             <TextInput
            //                ref='1'
            //                underlineColorAndroid='transparent'
            //                returnKeyType="search"
            //                keyboardType="default"
            //                placeholder="Search"
            //                placeholderTextColor='#DDDEE4'
            //                style={styles.TextInputStyle}
            //                editable = {false}
            //                value={this.state.searchText}
            //             />
            //             <TouchableOpacity
            //              onPress={() => this.closedata()}
            //             >
            //               <Image
            //                 source={require('../img/close_icon.png')}
            //                 style={styles.closeImageStyle}
            //               />
            //             </TouchableOpacity>
            //       </View>
          }
        </View>
        <View style={ { backgroundColor: 'white', marginTop: 5, marginLeft: 10, marginRight: 10, marginBottom: 5, justifyContent: '' } }>
          <View style={ { padding: 10, borderColor: '#dddee4', borderBottomWidth: 1, marginBottom: 5 } }>
            <Text allowFontScaling={ false } style={ styles.searchTextStyle }>{ this.props.total_results } Search result </Text>
          </View>
          <View style={ { height: '100%', width: '100%' } }>
            {
              (!this.props.isLoading && this.props.search_result != null && this.props.search_result.length > 0) ?
                <FlatList
                  key={ this.props.search_result.length - 1 }
                  keyExtractor={ item => item.firebase }
                  style={ { marginTop: 5, marginBottom: 170 } }
                  data={ this.props.search_result }
                  onEndReached={ this.handleLoadMore.bind(this) }
                  onEndReachedThreshold={ 1 }
                  renderItem={ ({ item, index }) => {
                    console.log(item.fullname);
                    return (
                      <Itemsuggestedprofile
                        Morgan={ item.fullname }
                        fireId={ item.firebase }
                        Marketing={ item.profession }
                        Mumbai={ item.location }
                        average_rating={ item.average_rating }
                        Users={ item.user_give_rating_count }
                        tagArray={ item.user_tags }
                        commentImage={ require('../img/message_icon.png') }
                        userProfile={ { uri: POSTIMAGEPATH + item.profile_image } }
                        messagePress={ () => this.onChatMessage(item) }
                        onPressUserIcon={ () => this.setState({ prifilePicDialogVisible: true, userPhoto: PROFILEIMAGEPATH + item.profile_image }) }
                        onPressProfile={ () => this.onProfileData(item) }
                        onClickMore={ () => this.props.navigation.navigate('Profile', { tagtype: 'tagtype', user_id: item.user_id }) }
                      />
                    );
                  } }

                />
                :
                <View
                  style={ {
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: 'white',
                    height: 500

                  } }
                >
                  {/* <Image style={{ height: 130, width: 130}}
                                     source={require('../img/no-data-found.jpg')}/> */}
                  <Text style={ { fontSize: 13, fontFamily: "Lato-Regular", color: '#686868', } }>No search to show</Text>
                </View>

            }
          </View>

        </View>

        <Loader
          loading={ this.props.isLoading }

        />
        <Modal
          transparent={ true }
          visible={ this.state.prifilePicDialogVisible }
          onRequestClose={ () => this.setState({ prifilePicDialogVisible: false }) }
        >
          <View
            style={ {
              height: "100%",
              width: "100%",
              paddingTop: 130,
              paddingBottom: 100
            } }
          >
            <View style={ { backgroundColor: 'rgba(0,0,0,0.9)', padding: 5, justifyContent: 'center', alignContent: 'center' } }>
              <View style={ { alignItems: "center", justifyContent: "center" } }>
                <ImageLoad
                  style={ { height: '100%', width: "100%" } }
                  source={ { uri: this.state.userPhoto } }
                  resizeMode="contain"
                />
                <TouchableOpacity
                  style={ styles.CloseViewSTyle }
                  onPress={ () =>
                    this.setState({ prifilePicDialogVisible: false })
                  }
                >
                  <Image
                    style={ { height: 18, width: 18 } }
                    source={ require("../img/close_icon.png") }
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#DDDEE3'
  },
  searchTextStyleHeder: {
    fontSize: 16,
    color: 'white',
    marginLeft: 10,
    fontFamily: 'Lato-Medium',
    width: '80%'
  },
  viewStyle: {
    flexDirection: 'row',
    margin: 10,
    padding: 8,
    backgroundColor: '#b7151b',
    alignItems: 'center',
    borderRadius: 5,
    justifyContent: 'space-between',
    height: 38,
    width: '88%'
  },
  TextInputStyle: {
    color: '#DDDEE4',
    flex: 1,
    fontFamily: 'Lato-Regular',
    justifyContent: 'center',
    paddingLeft: 10,
    paddingTop: 2,
    paddingBottom: 2,
    fontSize: 15,
  },
  searchImageStyle: {
    height: 18,
    width: 18,
    alignSelf: 'center'
  },
  backImageStyle: {
    height: 20,
    width: 20,
    tintColor: 'white',
    alignSelf: 'center',
    marginLeft: 10
  },
  closeImageStyle: {
    height: 12,
    width: 12,
    alignSelf: 'center',
  },
  searchTextStyle: {
    fontSize: 16,
    color: '#4E4E4E',
    fontFamily: 'Lato-Medium'
  },
  CloseViewSTyle: { position: "absolute", right: 2, top: 2, backgroundColor: 'rgba(255,0,0,0.5)', width: 32, height: 32, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }

});
const mapStateToProps = state => {
  return {
    search_result: state.SearchResult.search_result,
    total_results: state.SearchResult.total_results,
    authResult: state.SearchResult.authResult,
    isLoading: state.SearchResult.isLoading,
    isdata: state.SearchResult.isdata,
    SplashauthToken: state.Splash.authToken,
    authResultAddFriend: state.SearchResult.authResultAddFriend,
  };
};

export default connect(
  mapStateToProps,
  {
    getSearchResult, initialgetsearchResultStateData,
    getAddfriend1,
    saveUserChat,
    clerchatnotification
  }
)(SearchResult);
