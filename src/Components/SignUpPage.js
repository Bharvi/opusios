import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar,
  Platform,
  Alert
} from "react-native";
import { connect } from "react-redux";
import ImagePicker from "react-native-image-picker";
import {
  fullNameChange,
  fullNameErrorChangeSignup,
  confirmpassChangeSignup,
  confirmpassErrorChangeSignup,
  emailChangeSignup,
  emailErrorChangeSignup,
  passwordChangeSignup,
  passwordErrorChangeSignup,
  initialSIgnupStateData,
  SigupApi
} from "../Actions";
import Header from "./Header";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import { StackActions, NavigationActions } from "react-navigation";

const device_type = Platform.OS === "ios" ? 1 : 0;
const device_token = 1234555;
const staticimage = 'https://bootdey.com/img/Content/avatar/avatar6.png';

class SignUpPage extends Component {
  state = {
    avatarSource: null
  };
  componentDidMount() {
    this.props.initialSIgnupStateData();
  }

  componentWillReceiveProps(nextProp) {
    //go to login
    if (nextProp.authResult === 'Success') {

      Alert.alert(
        'Success',
        'Sign up successful. please wait for an approval email from our admin',
        [
          { text: 'OK', onPress: () => this.props.navigation.navigate('SignInPage') },
        ],
        { cancelable: false }
      )
    } else if (nextProp.authResult === "Email ID already exists.") {
      Alert.alert("Oops", "Email id already exists.");
    } else if (nextProp.authResult === "Registration Failed.") {
      Alert.alert("Oops", "Registration failed.");
    }
  }

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled photo picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        let source = { uri: response.uri };

        console.log(source);
        this.setState({
          avatarSource: source
        });
        // // let source = { uri: response.uri };
        // // const imageData = new FormData();
        // // imageData.append('name', 'image');
        // // imageData.append('image', {
        // //     uri: response.uri,
        // //     type: response.type,
        // //     name: response.fileName,
        // //     data: response.data
        // // });
        // // console.log("Pathik image Store ", imageData);
        // //here you can call api and send the image data in data of api ,in uploadImage I called the api
        // // this.setState({
        // //   avatarSource: imageData
        // // });
        // this.uploadImage(response.fileName);
        // console.log(" Secong Store data image ", source);

        }
    });
  }
  // uploadImage(source){
  //
  //       const data = new FormData();
  //       data.append('name', 'images'); // you can append anyone.
  //       data.append('images', {
  //           uri: source.uri,
  //           type: 'image/jpeg', // or photo.type
  //           name: response.fileName
  //       });
  //
  //       fetch('https://xxx/react_image', {
  //           method: 'POST',
  //           headers: {
  //               'Accept': 'application/json',
  //               'Content-Type': 'multipart/form-data',
  //           },
  //           body: data,
  //       }).then((response) => response.json())
  //           .then((responseJson) => {
  //
  //              console.log(responseJson);
  //
  //           }).catch((error) => {
  //           //
  //       });
  //   }

  // uploadImage = source => {
  //   console.log(source);
  //
  //   var details = {source};
  //
  //   var formBody = [];
  //   for (var property in details) {
  //     var encodedKey = encodeURIComponent(property);
  //     var encodedValue = encodeURIComponent(details[property]);
  //     formBody.push(encodedKey + "=" + encodedValue);
  //   }
  //   formBody = formBody.join("&");
  //   console.log(" Hello ");
  //
  //   fetch("http://www.opus.projectdemo.website/public/uploads/", formBody, {
  //     headers: {
  //       "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
  //     }
  //   })
  //     .then(response => source)
  //     .catch(error => {
  //       console.log(error);
  //     });
  // };

  onFullNameChange(text) {
    this.props.fullNameChangeSignup(text);
  }
  onEmailChange(text) {
    this.props.emailChangeSignup(text);
  }
  onPasswordChange(text) {
    this.props.passwordChangeSignup(text);
  }
  onConfirmPassChange(text) {
    this.props.confirmpassChangeSignup(text);
  }

  onBackPress() {
    this.props.navigation.goBack(null);
  }

  onButtonPress() {
    const { fullname, email, password, confirmpass } = this.props;
    let isValid = true;
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (fullname === "") {
      isValid = false;
      this.props.fullNameErrorChangeSignup("Enter fullname");
    } else {
      this.props.fullNameErrorChangeSignup("");
    }
    if (email === "") {
      isValid = false;
      this.props.emailErrorChangeSignup("Enter email");
    } else if (reg.test(email) === false) {
      isValid = false;
      this.props.emailErrorChangeSignup("Enter valid email");
    } else {
      this.props.emailErrorChangeSignup("");
    }
    if (password === "") {
      isValid = false;
      this.props.passwordErrorChangeSignup("Enter password");
    } else {
      this.props.passwordErrorChangeSignup("");
    }
    if (confirmpass === "") {
      isValid = false;
      this.props.confirmpassErrorChangeSignup("Enter confirm password");
    } else if (confirmpass !== password) {
      isValid = false;
      this.props.confirmpassErrorChangeSignup("Password does not match");
    } else {
      this.props.confirmpassErrorChangeSignup("");
    }
    if (isValid === true) {
      console.log("successsss");
      //   this.props.navigation.navigate('BottomTab', { myProps: this.props });
      // this.props.navigation.navigate("EditProfilePage");
      console.log(device_type);
      console.log(fullname);
      console.log(email);
      console.log(password);
      // const user_photo = this.state.avatarSource.uri;
      // console.log(user_photo);
      console.log(this.state);
      this.props.SigupApi(
        {fullname,
        email,
        password,
        user_photo: this.state.avatarSource != null ? this.state.avatarSource.uri : staticimage,
        device_type,
        device_token
      }
      );
    }
  }

  focusNextField(nextField) {
    this.refs[nextField].focus();
  }
  jumpLogin() {
    this.onButtonPress();
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header
          leftImage={require("../img/cross_icon.png")}
          headertext={"Sign UP"}
          textDone={"Done"}
          onLeftPressed={() => this.onBackPress()}
          onPressDone={() => this.onButtonPress()}
        />
        <StatusBar backgroundColor="#CC181E" barStyle="light-content" />
        <ScrollView>
          <View style={{ margin: 20 }}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={{ alignSelf: "center" }}
              onPress={this.selectPhotoTapped.bind(this)}
            >
              {this.state.avatarSource === null ? (
                <Image
                  style={styles.avatar}
                  source={{ uri: staticimage  }}
                />
              ) : (
                <Image style={styles.avatar} source={this.state.avatarSource} />
              )}
              <Image
                style={{
                  width: 28,
                  height: 28,
                  position: "absolute",
                  bottom: 68,
                  left: 68
                }}
                source={require("../img/plus_bgwith_icon.png")}
              />
            </TouchableOpacity>

            <View style={{ marginTop: 20 }}>
              <View>
                <Text style={styles.labelTextStyle}> FULL NAME </Text>
                <TextInput
                  ref="2"
                  underlineColorAndroid="transparent"
                  returnKeyType="next"
                  keyboardType="default"
                  autoCapitalize="none"
                  style={styles.textInputStyle}
                  value={this.props.fullname}
                  onChangeText={text => this.props.fullNameChange(text)}
                  onSubmitEditing={() => this.focusNextField("2")}
                />
                <View
                  style={{
                    borderBottomColor: "#261d1d26",
                    borderBottomWidth: 1
                  }}
                />
              </View>
              <Text style={styles.errorTextStyle}>
                {this.props.fullNameError}
              </Text>

              <View style={{ marginTop: 8 }}>
                <Text style={styles.labelTextStyle}> EMAIL </Text>
                <TextInput
                  ref="2"
                  underlineColorAndroid="transparent"
                  returnKeyType="next"
                  keyboardType="email-address"
                  autoCapitalize="none"
                  style={styles.textInputStyle}
                  value={this.props.email}
                  onChangeText={this.onEmailChange.bind(this)}
                  onSubmitEditing={() => this.focusNextField("3")}
                />
                <View
                  style={{
                    borderBottomColor: "#261d1d26",
                    borderBottomWidth: 1
                  }}
                />
              </View>
              <Text style={styles.errorTextStyle}>{this.props.emailError}</Text>

              <View style={{ marginTop: 8 }}>
                <Text style={styles.labelTextStyle}> PASSWORD </Text>
                <TextInput
                  ref="3"
                  underlineColorAndroid="transparent"
                  returnKeyType="next"
                  secureTextEntry
                  keyboardType="default"
                  autoCapitalize="none"
                  style={styles.textInputStyle}
                  value={this.props.password}
                  onChangeText={this.onPasswordChange.bind(this)}
                  onSubmitEditing={() => this.focusNextField("4")}
                />
                <View
                  style={{
                    borderBottomColor: "#261d1d26",
                    borderBottomWidth: 1
                  }}
                />
              </View>
              <Text style={styles.errorTextStyle}>
                {this.props.passwordError}
              </Text>

              <View style={{ marginTop: 8 }}>
                <Text style={styles.labelTextStyle}> CONFIRM PASSWORD </Text>
                <TextInput
                  ref="4"
                  underlineColorAndroid="transparent"
                  returnKeyType="done"
                  secureTextEntry
                  keyboardType="default"
                  autoCapitalize="none"
                  style={styles.textInputStyle}
                  value={this.props.confirmpass}
                  onChangeText={this.onConfirmPassChange.bind(this)}
                  onSubmitEditing={() => this.jumpLogin()}
                />
                <View
                  style={{
                    borderBottomColor: "#261d1d26",
                    borderBottomWidth: 1
                  }}
                />
              </View>
              <Text style={styles.errorTextStyle}>
                {this.props.confirmpassError}
              </Text>
            </View>
          </View>

          <View
            style={{ marginTop: 60, alignSelf: "center", marginBottom: 10 }}
          >
            <Text style={styles.byCreatingTextStyle}>
              By Creating an account, you are aggreeing to our{" "}
            </Text>
            <View style={{ flexDirection: "row", alignSelf: "center" }}>
              <TouchableOpacity activeOpacity={0.8}>
                <Text style={styles.termsTextStyle}> Terms Of Use </Text>
                <View
                  style={{
                    borderBottomColor: "black",
                    borderBottomWidth: 1
                  }}
                />
              </TouchableOpacity>
              <Text style={styles.byCreatingTextStyle}> and </Text>
              <TouchableOpacity activeOpacity={0.8}>
                <Text style={styles.termsTextStyle}> Privacy Policy </Text>
                <View
                  style={{
                    borderBottomColor: "black",
                    borderBottomWidth: 1
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <ProgressDialog
          visible={this.props.isLoading}
          title="SignUp"
          message="Please, wait..."
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  labelTextStyle: {
    fontSize: 12,
    color: "#848484",
    fontFamily: "Lato-Regular"
  },
  textInputStyle: {
    color: "#1d1d26",
    fontFamily: "Lato-Regular",
    paddingLeft: 10,
    fontSize: 16,
    height: 40
  },
  byCreatingTextStyle: {
    fontSize: 12,
    color: "#848484",
    fontFamily: "Lato-Regular"
  },
  termsTextStyle: {
    fontSize: 12,
    color: "#000000",
    fontFamily: "Lato-Regular"
  },
  errorTextStyle: {
    fontSize: 14,
    color: "#F00",
    textAlign: "right",
    marginRight: 10
  },
  avatar: {
    width: 95,
    height: 95,
    borderRadius: 500 / 2
  }
});

const mapStateToProps = state => {
  return {
    fullname: state.signup.name,
    fullNameError: state.signup.fullNameError,
    email: state.signup.email,
    emailError: state.signup.emailError,
    password: state.signup.password,
    passwordError: state.signup.passwordError,
    confirmpass: state.signup.confirmPassword,
    confirmpassError: state.signup.confirmPasswordError,
    isLoading: state.signup.isLoading,
    authResult: state.signup.authResult
  };
};
export default connect(
  mapStateToProps,
  {
    emailChangeSignup,
    emailErrorChangeSignup,
    passwordChangeSignup,
    passwordErrorChangeSignup,
    fullNameChange,
    fullNameErrorChangeSignup,
    confirmpassChangeSignup,
    confirmpassErrorChangeSignup,
    initialSIgnupStateData,
    SigupApi
  }
)(SignUpPage);
