import React from 'react'
import { Text } from 'react-native'

export const Letter = (props) => {
  const { children, spacing, textStyle } = props

  const letterStyles = [
    textStyle,
    {
      paddingRight: spacing,
      fontSize: 14,
      textAlign: 'center',
      fontFamily: 'Lato-Black'
    }
  ];

  return <Text allowFontScaling={ false } style={ letterStyles }>{ children }</Text>
}
