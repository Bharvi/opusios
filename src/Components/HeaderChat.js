import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
  Platform,
  Dimensions
} from 'react-native';
import DeviceInfo from 'react-native-device-info';

export default class HeaderChat extends Component {
  constructor (props) {
    super(props);
    this.state = {
      isModalVisible: false,

    };
    this.callFunc = this.callFunc.bind(this);
  }

  callFunc() {
    if (this.state.isModalVisible) {
      this.setState({ isModalVisible: false });
      this.props.onPressClose();
    } else {
      this.setState({ isModalVisible: true });
    }
  }


  render() {
    return (
      <View>
        <View style={ { backgroundColor: "#CC181E", height: 15 } }></View>
        <View style={ { backgroundColor: "#CC181E", paddingTop: (DeviceInfo.getModel() === 'iPhone X' || DeviceInfo.getModel() === 'iPhone XS Max' || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 15 : 0 } } />
        <View style={ styles.container }>
          { (!this.state.isModalVisible) ?
            <View style={ { height: 50, width: 50 } }></View> : null
          }
          <View style={ { flex: 20, justifyContent: 'center', alignItems: 'center', } }>

            <Text allowFontScaling={ false } style={ styles.textStyle }>{ (!this.state.isModalVisible) ? this.props.headertext : null }</Text>

            {
              (this.state.isModalVisible) ?
                <View
                  style={ styles.viewStyle }
                >
                  <TouchableOpacity>
                    <Image
                      source={ require('../img/search_icon.png') }
                      style={ styles.searchImageStyle }
                    />
                  </TouchableOpacity>
                  <TextInput
                    ref='1'
                    allowFontScaling={ false }
                    underlineColorAndroid='transparent'
                    returnKeyType="search"
                    keyboardType="default"
                    placeholder="Search"
                    placeholderTextColor='#DDDEE4'
                    style={ styles.TextInputStyle }
                    onChangeText={ this.props.onChangeText }
                  />
                  {
                    (this.state.isModalVisible) ?
                      <TouchableOpacity
                        style={ { flex: 2, justifyContent: 'center', alignItems: 'center', height: 35, width: 35 } }
                        onPress={ this.callFunc }
                      >
                        <Image
                          style={ styles.backArrowStyle1 }
                          source={ require('../img/cross_icon.png') }
                        />
                      </TouchableOpacity>
                      : null
                  }
                </View>
                : null
            }
          </View>
          {
            (!this.state.isModalVisible) ?
              <TouchableOpacity
                style={ { height: 50, width: 50, alignItems: 'center', justifyContent: 'center' } }
                onPress={ this.callFunc }
              >
                <Image
                  style={ styles.backArrowStyle }
                  source={ this.props.searchImage }
                />
              </TouchableOpacity>
              : null
          }

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    width: '100%',
    backgroundColor: '#CC181E'
  },
  textStyle: {
    fontSize: 16,
    color: '#ffffff',
    fontFamily: 'Lato-Regular'
  },
  textStyleDone: {
    fontSize: 16,
    color: '#CCffffff',
    textAlign: 'center',
    fontFamily: 'Lato-Regular'
  },
  backArrowStyle: {
    height: 20,
    width: 20,
  },
  backArrowStyle1: {
    height: 20,
    width: 20,
    position: 'absolute',
    right: 8,
    padding: 8
  },
  viewStyle: {
    flexDirection: 'row',
    margin: 10,
    padding: 8,
    backgroundColor: '#b7151b',
    alignItems: 'center',
    borderRadius: 5,
    height: 35,
    width: Dimensions.get('window').width - 20,
    bottom: 5
  },
  searchImageStyle: {
    height: 18,
    width: 18,
    tintColor: 'white',
    alignSelf: 'center'
  },
  TextInputStyle: {
    color: '#DDDEE4',
    flex: 7,
    fontFamily: 'Lato-Regular',
    justifyContent: 'center',
    paddingLeft: 10,
    paddingTop: 1,
    paddingBottom: 1,
    fontSize: 14,
  },
});

