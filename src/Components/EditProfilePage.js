import React, { Component } from "react";
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  StyleSheet,
  TextInput,
  DatePickerAndroid,
  TimePickerAndroid,
  DatePickerIOS,
  TouchableHighlight,
  Platform,
  Animated,
  Keyboard,
  Button,
  Alert,
  Modal,
  Picker,
  KeyboardAvoidingView
} from "react-native";
import { connect } from "react-redux";
import ImagePicker from "react-native-image-picker";
import Tooltip from "./ToolTip/tooltip";
import {
  fullNameChangeEdit,
  fullNameErrorChangeEdit,
  emailChangeEdit,
  emailErrorChangeEdit,
  sexChangeEdit,
  sexErrorChangeEdit,
  birthDateChangeEdit,
  bdayErrorChangeEdit,
  professionChangeEdit,
  professionErrorChangeEdit,
  locationChangeEdit,
  locationErrorChangeEdit,
  briefDescriptionChangeEdit,
  // user_tagChangeEdit,
  briefDescriptionErrorChangeEdit,
  initialEditProfileStateData,
  userEdit,
  setProfileData,
  // addTag,
  // removeTag,
  // user_tag_errorChangeEdit,
  getAllTags
} from "../Actions";
import DatePicker from "react-native-datepicker";
import Style from "./style";
import Moment from "moment";
import PropTypes from "prop-types";
import Header from "./HeaderEdit";
import UserProfileImg from '../Components/common/UserProfileImg';

import { getUser } from "../Database/allSchema";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import Loader from './Loader';

let userId = '';
let gender = '';

class EditProfilePage extends Component {
  constructor (props) {
    super(props);
    this.state = {
      data: [
        { tagName: "cricket" },
        { tagName: "travelling" },
        { tagName: "music" },
        { tagName: "reading" },
        { tagName: "Reading" }
      ],
      avatarSource: null,
      date: null,
      visible: false,
      animatedHeight: new Animated.Value(0),
      allowPointerEvents: true,
      genderVisible: false,
      gender: null,
      femaleChecked: false,
      maleChecked: false,
      SelectTimeSlotState: '',
      profession: '',
      showPicker: false
    };
  }
  componentWillMount() {
    this.props.initialEditProfileStateData();
    this.props.getAllTags({ token: this.props.SplashauthToken });
  }

  componentDidMount() {
    getUser()
      .then(user => {
        if (user.length > 0) {
          this.setState({ userData: user[0] });
          userId = user[0].id;

          console.log(user[0]);
          this.setState({ gender: ((user[0].gender != 'null') ? ((user[0].gender === 'Female') ? 0 : 1) : 'null') });
          console.log("Show GEnder:", this.state.gender);
          if (user[0].birth_date !== '') {
            this.setState({ date: new Date(Moment(user[0].birth_date).format('YYYY-MM-DD')) });
          }


          console.log(this.state.date);
          this.props.setProfileData(this.state.userData);
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  componentWillReceiveProps(nextProps) {
    // if (nextProps.authResult === 'record update successfully') {
    //   Alert.alert(
    //     'Success',
    //     'Profile update successfully.',
    //     [
    //       { text: 'OK', onPress: () => this.props.navigation.goBack(null) },
    //     ],
    //     { cancelable: false }
    //   )
    // }
    if (nextProps.authResult === "record update error") {
      Alert.alert(
        "Failed",
        "Profile update failed, try again later."
        // [
        //   { text: 'OK', onPress: () => this.props.navigation.goBack(null) },
        // ],
        // { cancelable: false }
      );
    }
  }
  renderFemaleCheckBox() {
    return (
      <View style={ styles.checkedBoxStyle }>
        {
          (this.state.femaleChecked || this.state.gender == 0) ?
            <View style={ styles.checkedViewStyle } />
            : null
        }
      </View>
    );
  }
  rendermaleCheckBox() {
    return (
      <View style={ styles.checkedBoxStyle }>
        {
          (this.state.maleChecked || this.state.gender == 1) ?
            <View style={ styles.checkedViewStyle } />
            : null
        }
      </View>
    );
  }


  onNameChange(text) {
    this.props.fullNameChangeEdit(text);
  }
  onEmailChange(text) {
    this.props.emailChangeEdit(text);
  }
  onSexChange(text) {
    this.props.sexChangeEdit(text);
  }
  onBirthDateChange(text) {
    console.log(this.state.date);
    this.props.birthDateChangeEdit(text);
  }
  onProfessionChange(text) {
    this.props.professionChangeEdit(text);
  }
  onLocationChange(text) {
    this.props.locationChangeEdit(text);
  }
  onBriefDescriptionChange(text) {
    this.props.briefDescriptionChangeEdit(text);
  }

  onusrt_tagChange(text) {
    this.props.user_tagChangeEdit(text);
  }
  onBackPress() {
    this.props.navigation.goBack(null);
  }

  getGender() {
    console.log(this.state.gender);
    if (this.state.gender === 0) {
      return 'Female';
    }
    else if (this.state.gender === 1) {
      return 'Male';
    }
    else {
      return 'Select Sex';
    }
  }

  onButtonPress() {
    let {
      fullname,
      email_id,
      gender,
      birth_date,
      profession_name,
      location,
      brief_desc
    } = this.props;
    let isValid = true;
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (fullname === "") {
      isValid = false;
      this.props.fullNameErrorChangeEdit("Enter fullname");
    } else {
      this.props.fullNameErrorChangeEdit("");
    }
    // if (email_id === "") {
    //   isValid = false;
    //   this.props.emailErrorChangeEdit("Enter email");
    // } else if (reg.test(email_id) === false) {
    //   isValid = false;
    //   this.props.emailErrorChangeEdit("Enter valid email");
    // } else {
    //   this.props.emailErrorChangeEdit("");
    // }
    if (gender === "") {
      isValid = false;
      this.props.sexErrorChangeEdit("Enter gender");
    } else {
      this.props.sexErrorChangeEdit("");
    }
    // if (birth_date === "") {
    //   isValid = false;
    //   this.props.bdayErrorChangeEdit("Enter birthday");
    // } else {
    //   this.props.bdayErrorChangeEdit("");
    // }
    if (profession_name === "") {
      isValid = false;
      this.props.professionErrorChangeEdit("Enter profession");
    } else {
      this.props.professionErrorChangeEdit("");
    }
    if (location === "") {
      isValid = false;
      this.props.locationErrorChangeEdit("Enter location");
    } else {
      this.props.locationErrorChangeEdit("");
    }
    //   isValid = false;
    //   console.log(brief_desc);
    //   this.props.briefDescriptionErrorChangeEdit("Enter brief description");
    // } else {
    //   this.props.briefDescriptionErrorChangeEdit("");
    // }
    // if (this.props.tagArray === null || this.props.tagArray.length === 0) {
    //   isValid = false;
    //   this.props.user_tag_errorChangeEdit("Please add tag");
    // } else {
    //   this.props.user_tag_errorChangeEdit("");
    // }
    if (isValid === true) {
      // this.props.navigation.navigate("BottomTab", { myProps: this.props });
      //   this.props.navigation.goBack(null);
      // console.log(" Login Success");
      //  this.props.navigation.navigate('SignUpPage');
      console.log("Valid true Data");
      console.log(userId);
      console.log(fullname);
      console.log(email_id);
      console.log("Show the gender ", this.state.gender);
      console.log(Moment(this.state.date).format('MMMM Do YYYY'));
      console.log(profession_name);
      console.log(location);
      console.log(brief_desc);
      this.props.userEdit({
        id: userId,
        fullname,
        email_id,
        gender: this.state.gender,
        birth_date: Moment(this.state.date).format("DD MMMM YYYY"),
        profession_name,
        location,
        brief_desc,
        profile_picture:
          this.state.avatarSource ? this.state.avatarSource.uri : null,
        myNavigation: this.props.navigation.navigate,
        token: this.props.SplashauthToken
      });
    }
  }

  focusNextField(nextField) {
    this.refs[nextField].focus();
  }

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };
    ImagePicker.launchImageLibrary(options, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled photo picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        let source = { uri: response.uri };
        this.setState({
          avatarSource: source
        });
      }
    });
  }

  jumpLogin() {
    this.onButtonPress();
  }


  professtionDropDown(rowData) {
    return (
      <TouchableOpacity style={ { width: '100%' } }>
        <Text
          allowFontScaling={ false }
          style={ {
            fontSize: 14,
            marginBottom: 10,
            paddingLeft: 10,
            paddingTop: 10,
            paddingRight: 10,
            width: '100%'
          } }
        >
          { rowData.name }
        </Text>
      </TouchableOpacity>
    );
  }
  pickerItem() {
    return this.props.professions.map((item) => {
      return (
        <Picker.Item
          label={ item.label }
          value={ item.label }
          key={ item.key || item.label }
        />
      )
    }
    );

  }

  renderItem = ({ item, index }) => {
    return (
      <View
        style={ {
          paddingLeft: 4,
          marginLeft: 10,
          width: "29%",
          backgroundColor: "#C8C8C8",
          height: 35,
          borderRadius: 3,
          borderColor: "#e2e0e0",
          borderWidth: 1,
          justifyContent: "center",
          alignItems: "center",
          margin: 3,
          flexDirection: "row"
        } }
      >
        <Text
          allowFontScaling={ false }
          style={ {
            fontSize: 16,
            color: "#676767",
            width: "85%",
            marginLeft: 6,
            right: 7
          } }
          numberOfLines={ 2 }
        >
          { item.tagName }
        </Text>

        <TouchableOpacity
          activeOpacity={ 4 }
          style={ { position: "absolute", right: 8 } }
        >
          <Image
            style={ { width: 14, height: 14, tintColor: "#676767" } }
            source={ require("../img/close_icon.png") }
          />
        </TouchableOpacity>

      </View>
    );
  };
  render() {
    let ImgUrl = this.props.user_photo;
    let userImg = ImgUrl.substring(ImgUrl.lastIndexOf('/') + 1)
    console.log(this.props.professions);

    return (
      <View>
        <Header
          leftImage={ require("../img/back_icon.png") }
          headertext={ "Edit Profile" }
          textDone={ "Done" }
          onPressDone={ () => this.onButtonPress() }
          onLeftPressed={ () => this.onBackPress() }
        />
        <StatusBar backgroundColor="#CC181E" barStyle="light-content" />
        <ScrollView style={ { marginBottom: 60 } }>
          <KeyboardAvoidingView
            keyboardVerticalOffset={ 64 }
            contentContainerStyle={ { flex: 1 } }
            behavior='position'
            enabled>
            <TouchableOpacity
              activeOpacity={ 0.8 }
              style={ { alignSelf: "center", marginTop: 12 } }
              onPress={ this.selectPhotoTapped.bind(this) }
            >
              {/* {this.state.avatarSource === null ? (
                <Image
                  style={styles.avatar}
                  source={{ uri: this.props.user_photo }}
                />
              ) : (
                  <Image style={styles.avatar} source={this.state.avatarSource} />
                )} */}
              { (this.state.avatarSource == null) ?

                <UserProfileImg
                  style={ { height: 95, width: 95, borderRadius: 95 / 2 } }
                  borderRadius={ 95 / 2 }
                  source={ { uri: this.props.user_photo } }
                  dotSize={ 1 }
                  fireId={ null }
                  profile={ true }
                />
                :
                <Image style={ styles.avatar } source={ this.state.avatarSource } />
              }
              <Image
                style={ {
                  width: 28,
                  height: 28,
                  position: "absolute",
                  bottom: 68,
                  left: 68
                } }
                source={ require("../img/plus_bgwith_icon.png") }
              />
            </TouchableOpacity>

            <View style={ { margin: 10 } }>
              <View style={ { marginTop: 20 } }>
                <Text allowFontScaling={ false } style={ styles.labelTextStyle }> FULL NAME </Text>
                <TextInput
                  allowFontScaling={ false }
                  ref="1"
                  underlineColorAndroid="transparent"
                  returnKeyType="next"
                  keyboardType="default"
                  autoCapitalize="none"
                  style={ styles.textInputStyle }
                  value={ this.props.fullname }
                  editable={ false }
                  onChangeText={ this.onNameChange.bind(this) }
                  onSubmitEditing={ () => this.focusNextField("2") }
                />
                <View
                  style={ {
                    borderBottomColor: "#261d1d26",
                    borderBottomWidth: 1
                  } }
                />
              </View>
              {
                // <Text allowFontScaling={false}  style={styles.errorTextStyle}>
                //   {this.props.fullnameError}
                // </Text>
              }

              <View style={ { marginTop: 8 } }>
                <Text allowFontScaling={ false } style={ styles.labelTextStyle }> EMAIL </Text>
                <TextInput
                  allowFontScaling={ false }
                  ref="2"
                  underlineColorAndroid="transparent"
                  returnKeyType="next"
                  keyboardType="email-address"
                  autoCapitalize="none"
                  style={ styles.textInputStyle }
                  value={ this.props.email_id }
                  editable={ false }
                  onChangeText={ this.onEmailChange.bind(this) }
                  onSubmitEditing={ () => this.focusNextField("3") }
                />
                <View
                  style={ {
                    borderBottomColor: "#261d1d26",
                    borderBottomWidth: 1
                  } }
                />
              </View>

              {
                // <Text allowFontScaling={false}  style={styles.errorTextStyle}>
                //   {this.props.email_idError}
                // </Text>
              }

              <View style={ { flexDirection: "row", alignItems: 'center', alignSelf: 'center' } }>
                <TouchableOpacity
                  onPress={ () => this.setState({ genderVisible: true }) }
                  style={ { marginTop: 8, width: "50%" } }
                >
                  <Text allowFontScaling={ false } style={ styles.labelTextStyle }>  SEX </Text>
                  {
                    // <TextInput
                    //   ref="3"
                    //   underlineColorAndroid="transparent"
                    //   returnKeyType="next"
                    //   keyboardType="default"
                    //   autoCapitalize="none"
                    //   style={styles.textInputStyle}
                    //   value={this.props.gender}
                    //   onChangeText={this.onSexChange.bind(this)}
                    //   onSubmitEditing={() => this.focusNextField("4")}
                    // />
                    <Text allowFontScaling={ false } style={ styles.genderTextStyle } >{ this.getGender() }</Text>
                  }
                </TouchableOpacity>

                <View style={ styles.verticalViewline } />

                <TouchableOpacity
                  style={ { marginTop: 8, width: "50%", marginLeft: 8 } }
                  onPress={ this.onPressDate }
                >
                  <Text allowFontScaling={ false } style={ styles.labelTextStyle }> BIRTHDAY </Text>
                  <DatePicker
                    allowFontScaling={ false }
                    style={ { width: 200 } }
                    date={ this.state.date }
                    placeholder={ 'Select Birthdate' }
                    // style={styles.textInputStyle}
                    format="MMMM DD, YYYY"
                    maxDate={ new Date() }
                    confirmBtnText="Ok"
                    cancelBtnText="Cancel"
                    customStyles={ {
                      dateIcon: {
                        width: 0,
                        height: 0,
                      },
                      dateInput: {
                        height: 30,
                        borderColor: "transparent",
                        marginLeft: 0,
                        fontSize: 14,
                        width: '100%'
                      },
                      placeholderText: {
                        color: "#1d1d26",
                        fontFamily: "Lato-Regular",
                        fontSize: 16,
                        width: '100%'
                      },
                      dateText: {
                        color: "#1d1d26",
                        fontFamily: "Lato-Regular",
                        fontSize: 16,
                        flexWrap: 'wrap',
                        width: '100%'
                      }
                    } }
                    onDateChange={ date => {
                      this.setState({ date: date });
                    } }
                    onPressCancel={ () => this.setState({ date: '' }) }
                  />
                  {
                    // <Text style={styles.errorTextStyle}>
                    //   {this.props.birth_dateError}
                    // </Text>
                  }
                </TouchableOpacity>
              </View>
              <View
                style={ {
                  borderBottomColor: "#261d1d26",
                  borderBottomWidth: 1
                } }
              />
              {
                // <Text allowFontScaling={false}  style={styles.errorTextStyle1}>
                //   {this.props.genderError}
                // </Text>
              }
              <View style={ { marginTop: 8 } }>
                <Text allowFontScaling={ false } style={ styles.labelTextStyle }> PROFESSION </Text>
                {
                  // <RNPickerSelect
                  //     placeholder={{label: this.props.profession_name,
                  //     value: 'Select profession',
                  //     color: '#9EA0A4'}}
                  //     items={this.props.professions}
                  //     hideDoneBar={false}
                  //     onValueChange={(value) => {
                  //         this.props.professionChangeEdit(value.label);
                  //     }}
                  //     placeholderTextColor={'black'}
                  //     style={{ width: '100%',
                  //     borderBottomWidth: 0.2,
                  //     borderColor: '#261d1d26',
                  //     marginTop: 10,
                  //     height: 50,
                  //     fontSize: 16,
                  //     color: "#1d1d26",
                  //     fontFamily: "Lato-Regular",
                  //     paddingLeft: 10, }}
                  //     value={this.props.profession_name}
                  // />


                  // <TouchableOpacity
                  //     style={{ width: '100%' }}
                  //     activeOpacity={0.8}
                  //     onPress={() => this.setState({ showPicker: true })}
                  // >
                  <TextInput
                    allowFontScaling={ false }
                    ref="professionInput"
                    underlineColorAndroid="transparent"
                    style={ styles.textInputStyle }
                    value={ this.props.profession_name }
                    editable={ false }
                    onTouchStart={ () => this.setState({ showPicker: true }) }
                  />
                  //</TouchableOpacity>
                  // (!this.props.isLoading1) ?
                  // <ModalDropdown
                  //     ref='professtionDropDown'
                  //     style={{
                  //       width: '100%',
                  //       borderBottomWidth: 0.2,
                  //       borderColor: '#261d1d26',
                  //       marginTop: 10,
                  //       height: 30
                  //     }}
                  //     textStyle={{
                  //       fontSize: 16,
                  //       width: '100%',
                  //       color: "#1d1d26",
                  //       fontFamily: "Lato-Regular",
                  //       paddingLeft: 10,
                  //     }}
                  //     defaultValue={this.props.profession_name}
                  //     options={this.props.professions}
                  //     renderButtonText={rowData => rowData.name}
                  //     dropdownStyle={{ width: 300 }}
                  //     onSelect={(idx, value) => {
                  //       console.warn(value.name,'onSelect');
                  //       this.setState({ SelectTimeSlotState: value.id })
                  //       this.props.professionChangeEdit(value.name);
                  //     }}
                  //     renderRow={this.professtionDropDown.bind(this)}
                  //   />
                  // :
                  // null
                  // }

                }
                <View
                  style={ {
                    borderBottomColor: "#261d1d26",
                    borderBottomWidth: 1
                  } }
                />
              </View>
              {
                // <Text allowFontScaling={false}  style={styles.errorTextStyle}>
                //   {this.props.profession_nameError}
                // </Text>
              }

              <TouchableOpacity style={ { marginTop: 8 } }
                activeOpacity={ 0.8 }
                onPress={ () => this.props.navigation.navigate('LocationAutosuggationEdit') }
              >
                <Text allowFontScaling={ false } style={ styles.labelTextStyle }> LOCATION </Text>

                <TextInput
                  allowFontScaling={ false }
                  ref="5"
                  editable={ false }
                  underlineColorAndroid="transparent"
                  returnKeyType="next"
                  keyboardType="default"
                  placeholder='Select Location'
                  autoCapitalize="none"
                  style={ styles.textInputStyle }
                  value={ this.props.location }
                  onTouchStart={ () => this.props.navigation.navigate('LocationAutosuggationEdit') }
                  onChangeText={ this.onLocationChange.bind(this) }
                  onSubmitEditing={ () => this.focusNextField("6") }
                />

                <View
                  style={ {
                    borderBottomColor: "#261d1d26",
                    borderBottomWidth: 1
                  } }
                />
              </TouchableOpacity>
              {
                // <Text allowFontScaling={false}  style={styles.errorTextStyle}>
                //   {this.props.locationError}
                // </Text>
              }

              <View style={ { marginTop: 8 } }>
                <Text allowFontScaling={ false } style={ styles.labelTextStyle }> BRIEF DESCRIPTION </Text>
                <TextInput
                  allowFontScaling={ false }
                  ref="6"
                  underlineColorAndroid="transparent"
                  returnKeyType="default"
                  keyboardType="default"
                  autoCapitalize="none"
                  style={ styles.textInputStyleBriefDes }
                  value={ this.props.brief_desc }
                  onChangeText={ this.onBriefDescriptionChange.bind(this) }
                />
                <View
                  style={ {
                    borderBottomColor: "#261d1d26",
                    borderBottomWidth: 1,
                    marginBottom: 20
                  } }
                />
              </View>
              {
                // <Text allowFontScaling={false}  style={styles.errorTextStyle}>
                //   {this.props.brief_descError}
                // </Text>
              }
            </View>

            <View>

              {
                // <View
                //   style={{
                //     flexDirection: "row",
                //     justifyContent: "space-between"
                //   }}
                // >
                //   <View
                //     style={{
                //       flex: 1,
                //       borderBottomWidth: 1,
                //       borderColor: "#A9A9A9",
                //       marginRight: 5
                //     }}
                //   >
                //     <TextInput
                //       ref="8"
                //       underlineColorAndroid="transparent"
                //       returnKeyType="next"
                //       keyboardType="email-address"
                //       autoCapitalize="none"
                //       style={styles.textInputStyle}
                //       value={this.props.user_tag}
                //       onChangeText={this.onusrt_tagChange.bind(this)}
                //     />
                //   </View>
                //   <View style={{ width: 50 }}>
                //     <Button
                //       title="Add"
                //       color="#CC181E"
                //       style={styles.dialogokTextStyle}
                //       onPress={() =>
                //         this.props.addTag(
                //           this.props.user_tag,
                //           this.props.tagArray
                //         )
                //       }
                //     />
                //   </View>
                // </View>
                // <Text style={styles.errorTextStyle}>
                //   {this.props.user_tagError}
                // </Text>
                // {this.props.tagArray != null && this.props.tagArray.length > 0 ? (
                //   <FlatList
                //     data={this.props.tagArray}
                //     numColumns={3}
                //     style={{ marginBottom: 8 }}
                //     renderItem={({ item, index }) => {
                //       return (
                //         <View
                //           style={{
                //             backgroundColor: "#eaeaea",
                //             borderRadius: 5,
                //             borderColor: "#e2e0e0",
                //             borderWidth: 1,
                //             justifyContent: "space-between",
                //             alignItems: "center",
                //             margin: 5,
                //             padding: 3,
                //             flexDirection: "row",
                //             width: "30%"
                //           }}
                //         >
                //           <Text
                //             style={{
                //               fontSize: 14,
                //               color: "#676767",
                //               fontFamily: "Roboto-Regular",
                //               marginRight: 2,
                //               flexWrap: "wrap",
                //               width: "70%"
                //             }}
                //           >
                //             {item}
                //           </Text>
                //           <TouchableOpacity
                //             onPress={() =>
                //               this.props.removeTag(this.props.tagArray, index)
                //             }
                //           >
                //             <Image
                //               style={{
                //                 height: 12,
                //                 width: 12,
                //                 tintColor: "black"
                //               }}
                //               source={require("../img/close_icon.png")}
                //             />
                //           </TouchableOpacity>
                //         </View>
                //       );
                //     }}
                //   />
                // ) : null}
              }
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
        <Dialog
          visible={ this.state.genderVisible }
          title="Select Sex"
          onTouchOutside={ () => this.setState({ genderVisible: false }) }
        >
          <View>
            <View style={ { flexDirection: "column" } }>
              <TouchableOpacity
                onPress={ () => this.setState({ femaleChecked: true, maleChecked: false, gender: 0, genderVisible: false }) }
                style={ { flexDirection: 'row' } }
              >
                {
                  this.renderFemaleCheckBox()
                }
                <Text allowFontScaling={ false } style={ styles.DailogTextStyle }>Female</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={ () => this.setState({ femaleChecked: false, maleChecked: true, gender: 1, genderVisible: false }) }

                style={ { flexDirection: 'row', marginTop: 8 } }
              >
                {
                  this.rendermaleCheckBox()
                }
                <Text allowFontScaling={ false } style={ styles.DailogTextStyle }>Male</Text>
              </TouchableOpacity>


            </View>
          </View>
        </Dialog>

        <Modal
          visible={ this.state.showPicker }
          transparent
          animationType={ 'slide' }
          supportedOrientations={ ['portrait', 'landscape'] }
        >
          <TouchableOpacity
            style={ { flex: 1 } }
            onPress={ () => {
              this.setState({ showPicker: false })
            } }
          />
          <TouchableOpacity
            onPress={ () => {
              this.setState({
                showPicker: false,
              });
            } }
            hitSlop={ { top: 4, right: 4, bottom: 4, left: 4 } }
          >
            <View>
              <Text style={ {
                color: '#007AFE',
                fontWeight: 'bold',
                fontSize: 16,
                padding: 5,
                textAlign: 'right',
                backgroundColor: 'white'
              } }>Done</Text>
            </View>
          </TouchableOpacity>
          <View
            style={ {
              height: 215,
              justifyContent: 'center',
              backgroundColor: '#D0D4DB',
            } }>
            <Picker
              style={ {
                backgroundColor: 'white'
              } }
              testID={ 'Picker' }
              onValueChange={ (itemValue, itemIndex) => {
                console.log(itemValue);

                this.props.professionChangeEdit(this.props.professions[itemIndex].label)
              }
              }
              selectedValue={ this.props.profession_name }
            >
              { this.pickerItem() }
            </Picker>
          </View>
        </Modal>
        <Loader
          loading={ this.props.isLoading }

        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  labelTextStyle: {
    fontSize: 14,
    color: "#848484",
    fontFamily: "Lato-Regular"
  },
  textInputStyle: {
    color: "#1d1d26",
    fontFamily: "Lato-Regular",
    paddingLeft: 10,
    fontSize: 16,
    height: 40
  },
  genderTextStyle: {
    color: "#1d1d26",
    fontFamily: "Lato-Regular",
    paddingLeft: 10,
    fontSize: 16,
    height: 40,
    marginTop: 10
  },
  DailogTextStyle: {
    color: "#1d1d26",
    fontFamily: "Lato-Regular",
    paddingLeft: 10,
    fontSize: 16,
    marginTop: -2
  },
  avatar: {
    width: 95,
    height: 95,
    borderRadius: 95 / 2,
  },
  errorTextStyle: {
    fontSize: 14,
    color: "#F00",
    textAlign: "right",
    marginRight: 10,
    fontFamily: "Lato-Regular"
  },
  errorTextStyle1: {
    fontSize: 14,
    color: "#F00",
    textAlign: "left",
    marginRight: 10,
    fontFamily: "Lato-Regular"
  },
  verticalViewline: {
    height: 40,
    borderWidth: 0.6,
    justifyContent: "center",
    borderColor: "#261d1d26",
    alignItems: "center",
    marginTop: 10,
    marginBottom: 10
  },
  textInputStyleBriefDes: {
    color: "#1d1d26",
    fontFamily: "Lato-Regular",
    alignItems: 'center',
    padding: 10,
    fontSize: 16,
    height: 50
  },
  checkedBoxStyle: {
    borderWidth: 1,
    width: 15,
    height: 15,
    borderColor: 'black',
    justifyContent: 'center',
    alignItems: 'center'
  },
  checkedViewStyle: {
    backgroundColor: '#CC181E',
    width: 10,
    height: 10,
  },
});

EditProfilePage.defaultProps = {
  mode: "date",
  androidMode: "default",
  date: "",
  // component height: 216(DatePickerIOS) + 1(borderTop) + 42(marginTop), IOS only
  height: 259,

  // slide animation duration time, default to 300ms, IOS only
  duration: 300,
  confirmBtnText: "",
  cancelBtnText: "",
  iconSource: require("../img/close_icon.png"),
  customStyles: {},

  // whether or not show the icon
  showIcon: false,
  disabled: false,
  allowFontScaling: true,
  hideText: false,
  placeholder: "",
  TouchableComponent: TouchableHighlight,
  modalOnResponderTerminationRequest: e => true
};

EditProfilePage.propTypes = {
  mode: PropTypes.oneOf(["date", "datetime", "time"]),
  androidMode: PropTypes.oneOf(["clock", "calendar", "spinner", "default"]),
  date: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Date),
    PropTypes.object
  ]),
  format: PropTypes.string,
  minDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
  maxDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
  height: PropTypes.number,
  duration: PropTypes.number,
  confirmBtnText: PropTypes.string,
  cancelBtnText: PropTypes.string,
  iconSource: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  iconComponent: PropTypes.element,
  customStyles: PropTypes.object,
  showIcon: PropTypes.bool,
  disabled: PropTypes.bool,
  allowFontScaling: PropTypes.bool,
  onDateChange: PropTypes.func,
  onOpenModal: PropTypes.func,
  onCloseModal: PropTypes.func,
  onPressMask: PropTypes.func,
  placeholder: PropTypes.string,
  modalOnResponderTerminationRequest: PropTypes.func,
  is24Hour: PropTypes.bool,
  getDateStr: PropTypes.func,
  locale: PropTypes.string
};

const mapStateToProps = state => {
  return {
    fullname: state.editprofile.fullname,
    fullnameError: state.editprofile.fullnameError,
    user_photo: state.editprofile.user_photo,
    user_photoError: state.editprofile.user_photoError,
    email_id: state.editprofile.email_id,
    email_idError: state.editprofile.email_idError,
    gender: state.editprofile.gender,
    genderError: state.editprofile.genderError,
    birth_date: state.editprofile.birth_date,
    birth_dateError: state.editprofile.birth_dateError,
    profession_name: state.editprofile.profession_name,
    profession_nameError: state.editprofile.profession_nameError,
    location: state.editprofile.location,
    locationError: state.editprofile.locationError,
    brief_desc: state.editprofile.brief_desc,
    brief_descError: state.editprofile.brief_descError,
    // user_tag: state.editprofile.user_tag,
    // user_tagError: state.editprofile.user_tagError,
    isLoading: state.editprofile.isLoading,
    SplashauthToken: state.Splash.authToken,
    professions: state.CompeleProfile.professions,
    isLoading1: state.CompeleProfile.isLoading,
    // tagArray: state.editprofile.tagArray
  };
};
export default connect(
  mapStateToProps,
  {
    fullNameChangeEdit,
    fullNameErrorChangeEdit,
    emailChangeEdit,
    emailErrorChangeEdit,
    sexChangeEdit,
    sexErrorChangeEdit,
    birthDateChangeEdit,
    bdayErrorChangeEdit,
    professionChangeEdit,
    professionErrorChangeEdit,
    locationChangeEdit,
    locationErrorChangeEdit,
    briefDescriptionChangeEdit,
    briefDescriptionErrorChangeEdit,
    initialEditProfileStateData,
    // user_tagChangeEdit,
    userEdit,
    setProfileData,
    // addTag,
    // removeTag,
    // user_tag_errorChangeEdit,
    getAllTags

  }
)(EditProfilePage);
