import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  StatusBar,
  TextInput,
  Alert
} from "react-native";
import {
  emailChangeForgotPass,
  emailErrorChangeForgotPass,
  initialForgotPassStateData,
  ForgotPassword
} from "../Actions";
import { connect } from "react-redux";
import Header from "./Header";
import { StackActions, NavigationActions } from 'react-navigation';
import { ProgressDialog, Dialog } from 'react-native-simple-dialogs';

class ForgotPassPage extends Component {
  componentWillReceiveProps(nextProp) {
    //go to login
    if (nextProp.authResultForgot === "Success") {
      // Alert.alert('Login Successfully.');
      Alert.alert(
        "Success",
        "Your password send successfully on your email",
        [
          {
            text: "OK",
            onPress: () => this.gotoSignInPage()
          }
        ],
        { cancelable: false }
      );
    } else if (nextProp.authResultForgot === "Email ID does not exist.") {
      Alert.alert("Oops", "Email id does not exist.");
    }
  }

  gotoSignInPage() {
    let resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: "SignInPage"
        })
      ]
    });
    this.props.navigation.dispatch(resetAction);
  }
  componentDidMount() {
    this.props.initialForgotPassStateData();
  }

  onEmailChange(text) {
    this.props.emailChangeForgotPass(text);
  }

  onButtonPress() {
    let { email } = this.props;
    let isValid = true;
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email === "") {
      isValid = false;
      this.props.emailErrorChangeForgotPass("Enter email");
    } else if (reg.test(email) === false) {
      isValid = false;
      this.props.emailErrorChangeForgotPass("Enter valid email");
    } else {
      this.props.emailErrorChangeForgotPass("");
    }
    if (isValid === true) {
      // this.props.navigation.goBack(null);
      this.props.ForgotPassword(email);
    }
  }
  onBackPress() {
    this.props.navigation.goBack(null);
  }

  render() {
    return (
      <View>
        <Header
          leftImage={require("../img/back_icon.png")}
          headertext={"Forgot Password"}
          textDone={"Done"}
          onPressDone={() => this.onButtonPress()}
          onLeftPressed={() => this.onBackPress()}
        />

        <StatusBar backgroundColor="#CC181E" barStyle="light-content" />
        <View style={{ margin: 20 }}>
          <Text style={styles.textStyle}>
            If you forgot your password. We can send you {"\n"}an email to reset
            it.{" "}
          </Text>

          <Text style={styles.labelTextStyle}> USERNAME </Text>
          <TextInput
            ref="1"
            underlineColorAndroid="transparent"
            returnKeyType="next"
            keyboardType="email-address"
            autoCapitalize="none"
            style={styles.textInputStyle}
            value={this.props.email}
            onChangeText={this.onEmailChange.bind(this)}
          />
          <View
            style={{
              borderBottomColor: "#261d1d26",
              borderBottomWidth: 1
            }}
          />
          <Text style={styles.errorTextStyle}>{this.props.emailError}</Text>
        </View>
        <ProgressDialog
                 visible={this.props.isLoadingForgot}
                 title="ForgotPassword"
                 message="Please, wait..."
           />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 14,
    color: "#261d1d26",
    fontFamily: "Lato-Regular"
  },
  textInputStyle: {
    color: "#1d1d26",
    fontFamily: "Lato-Regular",
    paddingLeft: 10,
    fontSize: 16,
    paddingRight: 10,
    height: 40
  },
  labelTextStyle: {
    fontSize: 14,
    color: "#848484",
    fontFamily: "Lato-Regular",
    marginTop: 20
  },
  errorTextStyle: {
    fontSize: 14,
    color: "#F00",
    textAlign: "right",
    marginRight: 10
  }
});

const mapStateToProps = state => {
  return {
    email: state.forgotpass.email,
    emailError: state.forgotpass.emailError,
    isLoadingForgot: state.forgotpass.isLoading,
    authResultForgot: state.forgotpass.authResult
  };
};
export default connect(
  mapStateToProps,
  {
    emailChangeForgotPass,
    emailErrorChangeForgotPass,
    initialForgotPassStateData,
    ForgotPassword
  }
)(ForgotPassPage);
