
import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Image,
  ActivityIndicator,
  Platform,
  TouchableOpacity
} from 'react-native';
import PinchZoomView from './PhotoView';

let imageUrl = '';
export default class ZoomImage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }

  componentWillMount() {
    imageUrl = this.props.navigation.getParam('imageUrl');
  }
// onLoad={() => this.setState({ isLoading: false })}
  render() {
    return (

      <View style={styles.marker}>
      <View style={styles.container}>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <PinchZoomView
            minScale={0.5}
            maxScale={5}
            style={{
              width: 300,
              height: 200 }}
          >
                <Image
                    source={{ uri: imageUrl }}
                    style={{ flex: 1 }}
                    resizeMode='contain'
                />
          </PinchZoomView>
        </View>
        <TouchableOpacity
          style={{ position: 'absolute', right: 15, top: 15, tintColor: '#000', padding: 3 }}
          onPress={() => this.props.navigation.goBack()}
        >
            <Image
              style={{ height: 15, width: 15 }}
              source={require('../img/close_icon.png')}
            />
        </TouchableOpacity>
        { (this.state.isLoading) ?
          <ActivityIndicator style={{ flex: 1 }} size="large" color="#0000ff" />
          : null
        }
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 20 : 0,
    height: 200,
    width: 300,
    backgroundColor: 'red',
    padding: 50
  },
  marker: {
  position: 'absolute',
  top: 0,
  left: 0,
  backgroundColor: 'rgba(52, 52, 52, 0.8)',
  height: 200,
  width: 300,
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 90 / 2,
}
});
