/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity
} from 'react-native';
import UserProfileImg from './common/UserProfileImg';

export default class Itemfriend extends Component {
  render() {
    return (
      <View style={ styles.container }>
        <View style={ { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 10 } }>
          <TouchableOpacity
            style={ { flex: 1.2 } }
            activeOpacity={ 0.8 }
            onPress={ this.props.onPressUserIcon }
          >
            {/* <View style={{ position: 'absolute', width: 8, height: 8, backgroundColor: '#46D009', borderRadius: 10, left: 35, top: -10 }} /> */ }
            {/* <ImageLoad
                          style={{ height: 50, width: 50, borderRadius: 200 / 2 }}
                          borderRadius={50 / 2}
                          source={this.props.userprofileImage}
                    /> */}
            <UserProfileImg
              style={ { height: 50, width: 50, borderRadius: 50 / 2 } }
              borderRadius={ 50 / 2 }
              source={ this.props.userprofileImage }
              dotSize={ 10 }
              fireId={ this.props.fireId }
              top={ '60%' }
              right={ '20%' }
            />
          </TouchableOpacity>
          <View style={ { flex: 5, marginRight: 40 } }>
            <TouchableOpacity
              style={ { marginLeft: 8, justifyContent: 'center' } }
              onPress={ this.props.profilenameclick }
            >
              <Text allowFontScaling={ false } style={ styles.MarcusTextStyle }>{ this.props.name }</Text>
              { this.props.des ?
                <Text allowFontScaling={ false } style={ styles.yesterdayTextStyle } >{ this.props.des }</Text>
                : null }
            </TouchableOpacity>
          </View>

          <TouchableOpacity
            activeOpacity={ 0.8 }
            style={ styles.viewDivideStyle1 }
            onPress={ this.props.onPress }
          >
            <Image
              style={ { height: 35, width: 35, } }
              source={ this.props.commentImage }
            />

          </TouchableOpacity>

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderColor: '#e2e0e0',
  },
  MarcusTextStyle: { fontSize: 16, fontFamily: 'Lato-Bold', color: '#010101' },
  yesterdayTextStyle: { fontSize: 14, color: '#B3B3B3', fontFamily: 'Lato-Regular' },
  UnfriendTextStyle: { fontSize: 14, color: '#B3B3B3', fontFamily: 'Lato-Regular' },
  UnfriendViewStyle: {
    width: 70,
    height: 25,
    borderWidth: 1,
    borderColor: '#C7C7C7',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3
  },
  viewDivideStyle1: {
    position: 'absolute',
    right: 10,
    flex: 1.7

  },
});
