import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity
} from 'react-native';
import DeviceInfo from 'react-native-device-info';

export default class HeaderChatMessage extends Component {
  render() {
    return (
      <View>
                <View style={{ backgroundColor: "#CC181E", height: 15 }}></View>
                <View style={{ backgroundColor: "#CC181E", paddingTop: (DeviceInfo.getModel() === 'iPhone X' || DeviceInfo.getModel() === 'iPhone XS Max' || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 15 : 0 }}/>
                <View style={styles.container}>
                <TouchableOpacity
                 style={{ height:50, width: 50, justifyContent: 'center', alignContent: 'center'}}
                 onPress={this.props.onLeftPressed}
                 activeOpacity={0.8}
                >
                    <Image
                    style={styles.backArrowStyle}
                    source={this.props.leftImage}
                    />
                </TouchableOpacity>

            <View style={{ flex: 18, justifyContent: 'center', alignItems: 'center', }}>
                <Text allowFontScaling={false} style={styles.textStyle}>{this.props.headertext}</Text>
            </View>
                <TouchableOpacity
                activeOpacity={0.8}
                style={{ height:50, width: 50,
                  justifyContent: 'center',flexDirection: 'row',
                  alignItems: 'center', paddingLeft: 5 }}
                 onPress={this.props.onPressDone}
                >
                    <Image
                    style={styles.dotMenuStyle}
                    source={this.props.rightImage}
                    />
                    {(this.props.isEnableGreenDot) ? 
                    <View style={{ position: 'absolute', width: 9, height: 9, backgroundColor: '#46D009', borderRadius: 10, right:8, top: 7}} />
                      : 
                      null
                      }
                    </TouchableOpacity>
              </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    width: '100%',
    backgroundColor: '#CC181E',
  },
  textStyle: {
    fontSize: 16,
    color: '#ffffff',
    fontFamily: 'Lato-Regular',
  },
  textStyleDone: {
    fontSize: 16,
    color: '#CCffffff',
    textAlign: 'center',
    fontFamily: 'Lato-Regular',
    
  },
  backArrowStyle: {
    height: 20,
    width: 20,
    alignSelf: 'center',
    marginLeft: 8
  },
  dotMenuStyle: {
    height: 22,
    width: 22,
  },
  menuStyle: {
    height: 18,
    width: 18
  }
});
