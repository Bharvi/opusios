import React, { Component } from 'react';
import {
  View,
  Image,
  Platform,
  StyleSheet,
  ImageBackground,
  UIApplication,
  Text,
  StatusBar,
  Alert,
  BackHandler,
  PushNotificationIOS,
  AppState,
} from 'react-native';
import { connect } from 'react-redux';
import { getToken, checkVersion, saveUserChat } from '../Actions';
import { StackActions, NavigationActions } from 'react-navigation';
import axios from 'axios';
import { getUser } from '../Database/allSchema';
import firebase from 'react-native-firebase';
import DeviceInfo from 'react-native-device-info';
import { compose } from '../../node_modules/redux';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import RNExitApp from 'react-native-exit-app';

let fromNotification = false;
// let count = 0 ;

class SplashScreen extends Component {
  //for hide header
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      fromNotification: false,
    };
  }

  componentWillMount() {
    console.log('in method........');
    const version = DeviceInfo.getVersion();
    // UIApplication.setApplicationIconBadgeNumber(0);
    // this.setState({ fromNotification: false });

    // this.props.checkVersion(version);
    // PushNotificationIOS.setApplicationIconBadgeNumber(0);
  }

  async componentWillReceiveProps(nextProps) {
    console.log('SplashauthToken', nextProps.SplashauthToken);
    if (nextProps.SplashauthToken) {
      try {
        await AsyncStorage.setItem('@token', nextProps.SplashauthToken);
        console.log('token in async storage');
      } catch (e) {
        console.log('failed to store token in async storage');
      }
    }
  }

  componentDidMount() {
    console.log('componentDidMount');
    // NetInfo.addEventListener('connectionChange', this.handleConnectivityChange);
    NetInfo.addEventListener(state => {
      console.log('Connection type', state.type);
      console.log('Is connected?', state.isConnected);

      if (state.isConnected) {
        console.log('handleConnectivityChange');

        this.checkPermission();
        this.createNotificationListeners();

        setTimeout(() => {
          console.warn('fromNotification', this.state.fromNotification);

          this.props.getToken(
            this.props.navigation,
            StackActions,
            NavigationActions,
            this.state.fromNotification,
          );
          // this.checkPermission();
          // this.createNotificationListeners()
        }, 20);
      } else {
        setTimeout(() => {
          Alert.alert(
            'Alert',
            'Please check internet connection.',
            [
              {
                text: 'OK',
                onPress: () => RNExitApp.exitApp(),
              },
            ],
            { cancelable: false },
          );
        }, 10);
      }
    });
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  async getToken() {
    firebase
      .messaging()
      .getToken()
      .then(fcmToken => {
        if (fcmToken) {
          this.props.setFcmToken(fcmToken);
          this.setState({ sendfcmtoken: fcmToken });
          console.log('fcm tokenn', fcmToken);
          console.warn('fcm tokenn', fcmToken);
        } else {
          console.log('else fcm tokenn', fcmToken);
          console.warn('fcm tokenn', fcmToken);
        }
      });
  }

  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected' + error);
    }
  }

  //Remove listeners allocated in
  componentWillUnmount() {
    // this.notificationListener();
    // this.notificationOpenedListener();
    // this.createNotificationListeners();
  }

  async createNotificationListeners() {
    firebase.messaging().requestPermission();
    /*
     * Triggered when a particular notification has been received in foreground
     * */
    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => {
        // //     let count = 0 ;
        // console.warn('appState',this.state.appState);

        // if(this.state.appState === 'inactive') {
        // firebase.notifications().getBadge()
        //         .then( count => {
        //           console.warn('count',count);
        //           count++
        //           firebase.notifications().setBadge(count)
        //        })
        //        .then(() => {
        //          console.warn('Doing great',count);
        //        })
        //        .catch( error => {
        //          console.warn('fail to count', count)
        //        })
        //       }
        console.log(
          'notification.data',
          notification.data['gcm.notification.data'],
        );

        let to_user_id = '';
        AsyncStorage.getItem('@to_user_id').then(u_id => {
          console.log('AsyncStorage id', u_id);
          to_user_id = u_id;

          console.log(
            'notification.data',
            notification.data['gcm.notification.data'],
          );
          let data = notification.data['gcm.notification.data'].split(',');
          let id = '';
          if (data[0] === 'chat') {
            id = data[1];
          }
          console.log('type', data[0]);
          console.log('id', id);
          console.log('to_user_id', to_user_id);

          if (data[0] === 'chat') {
            if (to_user_id !== id) {
              const showNotification = new firebase.notifications.Notification()
                .setTitle(notification.title)
                .setBody(notification.body)
                .setData(notification.data);
              firebase
                .notifications()
                .displayNotification(showNotification)
                .catch(err => console.warn('error', err));
            }
          } else {
            const showNotification = new firebase.notifications.Notification()
              .setTitle(notification.title)
              .setBody(notification.body)
              .setData(notification.data);

            firebase
              .notifications()
              .displayNotification(showNotification)
              .catch(err => console.warn('error', err));
          }
        });
      });

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    //onclick of notification when app is in forgraound
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        console.warn(
          'onNotificationOpened' +
          JSON.stringify(notificationOpen.notification.data),
        );
        this.redirect(notificationOpen.notification);
      });

    /*
     * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
     * */
    //onclick of notification when app is in backgroung
    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      console.warn(' page redirect', notificationOpen);
      this.redirect(notificationOpen.notification);
    }

    /*
     * Triggered for data only payload in foreground
     * */
    this.messageListener = firebase.messaging().onMessage(message => {
      //process data message
      console.warn('message when app in forground', message);
      this.redirect(message.notification);
    });
  }

  redirect(notification) {
    firebase.notifications().removeAllDeliveredNotifications();
    var n = notification.data;
    console.warn('Pathik fromNotification', n['gcm.notification.data']);
    this.setState({ fromNotification: true });

    if (this.props.SplashauthToken) {
      try {
        let notificationData = notification.data['gcm.notification.data'];
        let notificationArray = notificationData.split(',');
        if (notificationArray[0] === 'post') {
          this.props.navigation.navigate('CommentPage', {
            postID: notificationArray[1],
            isUserFromNotification: true,
          });
        } else if (notificationArray[0] === 'friend') {
          this.props.navigation.navigate('BottomTabProfileFirst', {
            user_id: notificationArray[1],
            isUserFromNotification: true,
          });
        } else if (notificationArray[0] === 'chat') {
          // dispatch({
          //   type: SAVE_USER_DATA,
          //   payload: {
          //     user_id: notificationArray[1],
          //     fullname: notificationArray[2],
          //     firebase: notificationArray[3],
          //   },
          // });
          this.props.saveUserChat(
            notificationArray[1],
            notificationArray[2],
            notificationArray[3],
          );

          setTimeout(() => {
            this.props.navigation.navigate('ChatMessage', {
              toUserId: notificationArray[1],
              fullname: notificationArray[2],
              to_fire_id: notificationArray[3],
              isUserFromNotification: true,
            });
          }, 50);
        }
      } catch (e) {
        this.props.getToken(
          this.props.navigation,
          StackActions,
          NavigationActions,
          true,
          notification.data['gcm.notification.data'],
        );
      }
    } else {
      this.props.getToken(
        this.props.navigation,
        StackActions,
        NavigationActions,
        true,
        notification.data['gcm.notification.data'],
      );
    }
  }

  render() {
    let today = new Date();
    return (
      <ImageBackground
        source={require('../img/login_bg.png')}
        style={styles.backgroundImage}>
        <StatusBar backgroundColor="#cc181e" barStyle="light-content" />
        <View style={styles.bgColor}>
          <Image
            source={require('../img/opus_logo.png')}
            style={styles.image1}
          />
          <Text
            allowFontScaling={false}
            style={{
              color: '#ffff',
              fontSize: 20,
              alignSelf: 'center',
              fontFamily: 'Lato-Italic',
            }}>
            {' '}
            Uniting penpals worldwide{' '}
          </Text>
          <View style={{ position: 'absolute', bottom: 30 }}>
            <Text allowFontScaling={false} style={styles.textStyle}>
              © {today.getFullYear()} all rights reserved{' '}
            </Text>
          </View>
        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: 'red',
  },
  image: {
    width: 90,
    height: 85,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  image1: {
    width: 200,
    height: 150,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    marginBottom: 20,
  },
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    color: '#ffffff',
    fontFamily: 'Lato-Regular',
    fontSize: 14,
  },
  bgColor: {
    position: 'absolute',
    backgroundColor: '#cc181e',
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
const mapStateToProps = state => {
  return {
    isUpdated: state.Splash.isUpdated,
    isLoading: state.Splash.isLoading,
    to_user_id: state.Chat.user_id,
    SplashauthToken: state.Splash.authToken,
  };
};
export default connect(
  mapStateToProps,
  {
    getToken,
    checkVersion,
    saveUserChat,
  },
)(SplashScreen);
