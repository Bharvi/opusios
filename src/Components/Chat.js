/* @flow */

import React, { Component } from "react";
import {
  View,
  Text,
  AsyncStorage,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  Modal,
  StatusBar,
  ActivityIndicator
} from "react-native";
import HeaderChat from "./HeaderChat";
import ItemChat from "./ItemChat";
import { connect } from "react-redux";
import { onBottomTabChange, getMyChatList, saveUserChat, clerchatnotification, initialOtherProfileStateData } from "../Actions";
import ImageLoad from "./ImageLoad";
import realm, { getUser, getChatListData } from "../Database/allSchema";
import { POSTIMAGEPATH } from "../Actions/type";
import Loader from './Loader';
import firebase from "react-native-firebase";
// import AsyncStorage from '@react-native-community/async-storage';

let userId = "";
let firebaseCurrentUser = ''
let userFireId = ''
class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      prifilePicDialogVisible: false,
      pageNo: 2,
      userPhoto: "",
      chatList: [],
      searchResult: [],
      searchText: '',
      isSearching: false,
      chat: [
        { name: "Erika Pluda", des: "@ErikaPluda", time: "10m" },
        { name: "Danial Kernel", des: "@ErikaPluda", time: "20m" },
        { name: "Twanda Khanna", des: "like you post", time: "10h" }
      ],
      pageVisible: false
    };
    this.componentWillMount = this.componentWillMount.bind(this);
    getUser()
      .then((user) => {
        if (user && user.length > 0) {
          console.log('user[0].id', user[0].id);
          userId = user[0].id;
          userFireId = user[0].fire_id
          if (user[0].id == 1) {
            firebaseCurrentUser = 'afdsfsdfdsfsdfsdfsdf';
          } else {
            firebaseCurrentUser = user[0].fire_id
          }
        }
      })
  }

  componentWillReceiveProps(nextProps) {
    console.log('chatList.props')
    //get data
    let myChatList = nextProps.chatList
    this.state.searchResult = []
    if (this.state.searchResult != nextProps.chatList) {

      if ((myChatList.length >= this.state.searchResult.length) && !this.state.isSearching) {
        this.setState({
          searchResult: myChatList,
          chatList: myChatList
        }, () => {
          // console.log('this.state.searchResult', this.state.searchResult);
          this.setState({ isLoading: false })
        });
      }
      setTimeout(() => {
        if (myChatList.length == 0) {
          this.setState({ isLoading: false })
        }
      }, 2000);
    }
  }
  componentDidMount() {
    this.setState({ searchResult: this.props.chatList }, () => {
      this.fetchData();
    })
  }
  componentWillMount() {



    this.props.navigation.addListener("didFocus", payload => {
      setTimeout(() => {
        console.log("Search didFocus");
        // this.props.initialOtherProfileStateData();
        this.props.onBottomTabChange({ prop: "issearchseleted", value: false });
        this.props.onBottomTabChange({ prop: "isuserseleted", value: false });
        this.props.onBottomTabChange({ prop: "isfeedseleted", value: false });
        this.props.onBottomTabChange({ prop: "ischatseleted", value: true });
      }, 0);
    });
    this.props.navigation.addListener("willFocus", payload => {
      console.log("Search willfocus");
      this.setState({ pageVisible: true });
      getUser()
        .then(user => {
          if (user.length > 0) {
            this.setState({ pageCount: 1 });
            userId = user[0].id;
            console.log(userId);
            // this.fetchData();
          }
        })
        .catch(error => {
          console.warn(error);
        });
    });
    this.props.navigation.addListener("willBlur", payload => {
      console.log("Search willBlur");
      this.setState({ pageVisible: false })
      this.props.onBottomTabChange({ prop: "issearchseleted", value: false });
      this.props.onBottomTabChange({ prop: "isuserseleted", value: false });
      this.props.onBottomTabChange({ prop: "isfeedseleted", value: false });
      this.props.onBottomTabChange({ prop: "ischatseleted", value: false });
    });
  }

  getChatList() {
    let myChatList = this.props.chatList
    console.log('myChatList', myChatList)
    this.setState({
      searchResult: myChatList,
      chatList: myChatList
    }, () => {
      console.log('this.state.searchResult', this.state.searchResult);
      // this.setState({ isLoading: false })
    });
    // if (this.props.chatList.length == 0) {
    //   // this.setState({ isLoading: false })
    //   this.props.getMyChatList(
    //     userId,
    //     this.props.SplashauthToken,
    //     userFireId,
    //     1,
    //     [],
    //     false
    //   );
    // }
  }

  fetchData = () => {
    console.log('fetchData searchResult', this.state.searchResult);
    if (!this.state.searchResult || this.state.searchResult.length === 0) {
      this.setState({ isLoading: true });
      //get chatList
      this.getChatList();
    }
    else {
      this.props.getMyChatList(
        userId,
        this.props.SplashauthToken,
        firebaseCurrentUser,
        this.state.pageNo,
        this.state.searchResult,
        false
      );
    }
    var allChat = firebase.database().ref('/chat');
    allChat.on("value", snap => {
      getUser()
        .then(user => {
          if (user.length > 0) {
            userId = user[0].id;
            console.log(userId);
            // get chatList
            this.props.getMyChatList(
              userId,
              this.props.SplashauthToken,
              firebaseCurrentUser,
              1,
              this.state.searchResult,
              true
            );
          }
        })
        .catch(error => {
          console.log(error);
        });
    });
  };

  onPressItem(item, index, id) {

    this.props.saveUserChat(item.to_user_id, item.fullname, item.firebase);
    this.props.clerchatnotification(userId, item.to_user_id, this.props.SplashauthToken);
    this.props.navigation.navigate("ChatMessage", {
      toUserId: item.to_user_id,
      fullname: item.fullname,
      to_fire_id: item.firebase
    });
    setTimeout(() => {


      let tempChatList = this.state.searchResult;
      let tempArr = [];

      tempChatList.forEach(element => {
        if (element.to_user_id == id) {
          element = { ...element, isReadMsg: 0 }
          console.log('element', element)
        }
        tempArr.push(element);
      });
      console.log('tempArr', tempArr)

      this.setState({ searchResult: tempArr }, () => {
        // this.props.navigation.navigate("ChatMessage", {
        //   toUserId: item.to_user_id,
        //   fullname: item.fullname,
        //   to_fire_id: item.firebase
        // });
        console.log('searchResult', this.state.searchResult);
      });
    }, 0);


  }

  onSearchText(text) {
    this.setState({ isSearching: true })
    let temp = this.state.chatList.filter((item) => item.fullname.toLowerCase().includes(text.toLowerCase()));
    this.setState({
      searchResult: temp,
      searchText: text
    })
  }

  onOtherProfile(item) {



    console.log('profile pressed,', item)

    this.props.navigation.navigate("Profile", {
      user_id: item.to_user_id,
      fullname: item.fullname,
      profile_image: POSTIMAGEPATH + item.profile_image,
      form: 'chat'
    });


  }
  // handleLoadMore = () => {
  //   if (!this.props.isLoadMore) {
  //     this.setState(
  //       {
  //         pageNo: this.state.pageNo + 1
  //       },
  //       () => {
  //         console.log('in handle more..reach the end', this.state.pageNo)
  //         // console.log('Arr passed', this.state.searchResult)
  //         this.props.getMyChatList(
  //           userId,
  //           this.props.SplashauthToken,
  //           userFireId,
  //           this.state.pageNo,
  //           this.state.searchResult,
  //           false
  //         );
  //       }
  //     );
  //   }
  // };

  componentWillUnmount() {
    // this.setState({ searchResult: this.state.chatList, searchText: '', isSearching: false })
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log('nextProps', nextProps.chatList.length);
  //   console.log('nextState', nextState.chatList.length)
  //   console.log('nextProps.chatList != nextState.chatList', nextProps.chatList != nextState.searchResult)
  //   if (nextProps.chatList.length != 0) {
  //     return nextProps.chatList != nextState.searchResult
  //   }

  //   return true

  // }

  render() {
    let newArray = [];
    this.state.searchResult.forEach(obj => {
      if (!newArray.some(o => o.firebase === obj.firebase)) {
        newArray.push({ ...obj })
      }
    });
    let chatList = newArray;
    chatList.sort((d1, d2) => (new Date(d2.recent_time)) - (new Date(d1.recent_time)));
    console.log('data', chatList.length)
    return (
      <View style={styles.container}>
        <HeaderChat
          headertext="Chat"
          onChangeText={(text) => this.onSearchText(text)}
          searchImage={require('../img/search_icon.png')}
          onPressClose={() => this.setState({ searchResult: this.state.chatList, searchText: '', isSearching: false })}
        />
        <StatusBar backgroundColor="#CC181E" barStyle="light-content" />
        {!this.state.isLoading &&
          this.state.searchResult != null &&
          this.state.searchResult.length > 0 ? (
            <FlatList
              data={chatList}
              extraData={this.state}
              style={{ flex: 1 }}
              // keyExtractor={(item, index) => index}            
              // onEndReached={() => this.handleLoadMore()}
              onEndReachedThreshold={0.01}

              showsHorizontalScrollIndicator={false}
              renderItem={({ item, index }) => {
                return (
                  <ItemChat
                    key={item.firebase}
                    userphoto={{
                      uri: POSTIMAGEPATH + item.profile_image
                    }}
                    item={item}
                    fireId={item.firebase}
                    name={item.fullname}
                    Otherptofile={() => this.onOtherProfile(item)}
                    // msg={ item.lst_msg }
                    // time={ item.lst_msg_time }
                    // msgCount={ item.isReadMsg }
                    index={index}
                    msg={item.recent_msg}
                    time={item.recent_time}
                    msgCount={item.isReadMsg}
                    onPressProfile1={() => this.onPressItem(item, index, item.to_user_id)}
                    onPressUserIcon={() =>
                      this.setState({
                        prifilePicDialogVisible: true,
                        userPhoto: POSTIMAGEPATH + item.profile_image
                      })
                    }
                  // ListFooterComponent={
                  //   // console.log('this.props.isData', this.props.isData)
                  //   (this.props.isData && userId == 1) ?
                  //     <ActivityIndicator size="small" color={'#CC181E'} />
                  //     :
                  //     null
                  // }
                  />
                );
              }}
              keyExtractor={item => item.firebase}
              key={chatList.length - 1}
            // initialNumToRender={3000}
            />
          ) :
          (!this.state.isLoading ?
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
              <Text allowFontScaling={false} style={{ fontSize: 16 }}>No chat found</Text>
            </View>
            : null)
        }

        <Modal
          transparent={true}
          visible={this.state.prifilePicDialogVisible}
          onRequestClose={() =>
            this.setState({ prifilePicDialogVisible: false })
          }
        >
          <View
            style={{
              height: "100%",
              width: "100%",
              paddingTop: 130,
              paddingBottom: 100
            }}
          >
            <View
              style={{
                backgroundColor: "rgba(0,0,0,0.9)",
                padding: 5,
                justifyContent: "center",
                alignContent: "center"
              }}
            >
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <ImageLoad
                  style={{ height: "100%", width: "100%" }}
                  source={{ uri: this.state.userPhoto }}
                  resizeMode="contain"
                />
                <TouchableOpacity
                  style={styles.CloseViewSTyle}
                  onPress={() =>
                    this.setState({ prifilePicDialogVisible: false })
                  }
                >
                  <Image
                    style={{ height: 18, width: 18 }}
                    source={require("../img/close_icon.png")}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        <Loader
          loading={this.state.isLoading}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 60,
    backgroundColor: "white"
  },
  CloseViewSTyle: {
    position: "absolute",
    right: 10,
    top: 10,
    backgroundColor: "rgba(255,0,0,0.5)",
    width: 32,
    height: 32,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  }
});
const mapStateToProps = state => {
  return {
    friend: state.Chat.chatList,
    authResult: state.Chat.authResult,
    isLoading: state.Chat.isLoading,
    chatList: state.Chat.chatList,
    isdata: state.Chat.isdata,
    SplashauthToken: state.Splash.authToken,
    isData: state.Chat.isDataInChat,
    isLoadMore: state.Chat.isLoadMore
  };
};

export default connect(
  mapStateToProps,
  {
    onBottomTabChange,
    getMyChatList,
    saveUserChat,
    clerchatnotification,
    initialOtherProfileStateData
  }
)(Chat);