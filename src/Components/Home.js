/* @flow */
import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native';
import Header from './Header';

export default class Home extends Component {

      constructor(props) {
        super(props);
        this.state = {
          searchText: '',
        };
      }
  render() {
    return (
      <View style={styles.container} >
      <View style={{ height: 35, backgroundColor: '#149A99' }}>
          <View style={styles.viewStyle}>
                        <TextInput
                           ref='1'
                           underlineColorAndroid='transparent'
                           returnKeyType="search"
                           keyboardType="default"
                           placeholder="Search"
                           style={styles.TextInputStyle}
                           value={this.state.searchText}
                           onChangeText={(text) => this.setState({ searchText: text })}
                           onSubmitEditing={() => this.onPressSearch()}
                        />
                        <TouchableOpacity onPress={() => this.onPressSearch()}>
                          {
                            // <Image
                            //   source={require('../img/search_icon.png')}
                            //   style={styles.searchImageStyle}
                            // />
                          }
                        </TouchableOpacity>

                    </View>
                </View>
            </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  TextInputStyle: {
    color: 'white',
    flex: 1,
    borderColor: '#000',
    justifyContent: 'center',
    paddingLeft: 5,
    paddingTop: 3,
    paddingBottom: 3,
    fontSize: 16,
  },
  searchImageStyle: {
    height: 25,
    width: 25,
    tintColor: 'black',
    alignSelf: 'center'
  },
  SearchViewStyle: {
    flex: 1,
    marginBottom: 10,
    padding: 10,
    borderRadius: 5,
    marginHorizontal: 5

  },
});
