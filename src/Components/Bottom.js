import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, Dimensions, ImageBackground, Platform, SafeAreaView } from 'react-native';
import { connect } from "react-redux";
import { NavigationActions, StackActions } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';
import { getAppHintData, updateAppHintData } from "../Database/allSchema";
// #149a99
let { width, height } = Dimensions.get('window');

let boxCount = 4;
let boxWidth = width / boxCount;


// const navigateToScreen = (route) => () => {
//   const resetAction = StackActions.reset({
//           index: 0,
//           actions: [NavigationActions.navigate({ routeName: route })],
//       });
//   this.props.navigation.dispatch(resetAction);
// };
let myProps = {};
let deviceId = '';
let majorVersionIOS = '';

class Bottom extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isChatDotVisible: false
    };
  }

  componentWillMount() {
    let from = this.props.navigation.getParam('from')



    getAppHintData()
      .then(data => {
        if (data && data.length > 0) {
          if (data[0].isChatDotVisible == 'true') {
            this.setState({ isChatDotVisible: true });
          } else {
            this.setState({ isChatDotVisible: false });
          }
        } else {
          this.setState({ isChatDotVisible: true });
        }
      })
      .catch(error => {
        console.log(error);
      });
    // myProps = this.props.navigation.getParam('myProps');
    deviceId = DeviceInfo.getDeviceId();
    majorVersionIOS = parseInt(Platform.Version, DeviceInfo.getDeviceId());
  }


  onPressTimeView() {
    this.props.navigation.navigate('Search');
  }
  onChat() {
    this.setState({ isChatDotVisible: false });
    getAppHintData()
      .then(data => {
        if (data && data.length > 0) {
          if (data[0].isChatDotVisible == 'true') {
            const appHintData = {
              id: 1,
              isChatDotVisible: 'false',
            }
            updateAppHintData(appHintData).then(() => {
              console.log('Insert Suceess appHintData');
            }).catch((error) => {
              console.log(`Insert error${error}`);
            });
          } else {
          }
        }
      })
      .catch(error => {
        console.log(error);
      });
    this.props.navigation.navigate('Chat');
  }
  onfeed() {
    this.props.navigation.navigate('FeedsPage');
  }
  onUser() {
    this.props.navigation.navigate('MyProfile', { myProps, Formchat: 'MyProfile' });
  }

  SelectedSize(menu) {
    return (
      {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: boxWidth,
        backgroundColor: menu ? '#f28d90' : '#fff'
      }
    )
  }
  SelectedSizeImage(menu) {
    return (
      {

        height: 25, width: 25, tintColor: menu ? '#CC181E' : '#b9bdc0',
      }
    )

  }
  SelectedSizeImage1(menu) {
    return (
      {

        height: 25, width: 25, tintColor: menu ? '#CC181E' : '#b9bdc0',
      }
    )

  }
  SelectedSizeImage2(menu) {
    return (
      {

        height: 25, width: 25, tintColor: menu ? '#CC181E' : '#b9bdc0',
      }
    )

  }



  render() {
    const { container, containerIphoneX, verticalViewline, viewDivideStyle, cartImageStyle, SelectedviewDivideStyle } = styles;
    return (

      <View style={containerIphoneX}>
        <View style={{ flex: 5, flexDirection: 'row' }}>

          <TouchableOpacity
            style={this.SelectedSize(this.props.issearchseleted)}
            onPress={() => this.onPressTimeView()}
          >
            <Image
              style={this.SelectedSizeImage(this.props.issearchseleted)}
              source={require('../img/search_icon.png')}
            />
          </TouchableOpacity>



          <TouchableOpacity
            style={this.SelectedSize(this.props.isuserseleted)}
            onPress={() => this.onUser()}
          >
            <Image
              style={this.SelectedSizeImage(this.props.isuserseleted)}
              source={require('../img/user_new_icon.png')} />

          </TouchableOpacity>



          <TouchableOpacity
            style={this.SelectedSize(this.props.isfeedseleted)}
            onPress={() => this.onfeed()}
          >
            <Image
              style={this.SelectedSizeImage(this.props.isfeedseleted)}
              source={require('../img/note_icon.png')}
            />
          </TouchableOpacity>


          <TouchableOpacity
            style={this.SelectedSize(this.props.ischatseleted)}
            onPress={() => this.onChat()}
          >
            <Image
              style={this.SelectedSizeImage(this.props.ischatseleted)}
              source={require('../img/comment_icon.png')}
            />
            {(this.state.isChatDotVisible) ?
              <View style={styles.commentCount} /> : null
            }
          </TouchableOpacity>

        </View>
      </View>

    );
  }
}

const styles = {
  containerIphoneX: {
    backgroundColor: '#ffffff',
    position: 'absolute',
    bottom: ((Platform.OS === 'ios' && (DeviceInfo.getModel() === 'iPhone X' || DeviceInfo.getModel() === 'iPhone XS Max' || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR')) ? 15 : 0),
    width: '100%',
    height: 50,
    flex: 5
  },
  container: {
    backgroundColor: '#ffffff',
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 50,
    flex: 5
  },
  viewDivideStyle: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: boxWidth,
  },
  SelectedviewDivideStyle: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: boxWidth,
    backgroundColor: '#f28d90'

  },
  verticalViewline: {
    height: 28,
    borderWidth: 0.3,
    justifyContent: 'center',
    borderColor: '#fff',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10
  },

  notiBadgeStyle: {
    position: 'absolute',
    right: 26,
    top: 13,
    backgroundColor: 'red',
    borderRadius: 2,
    width: 12,
    height: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cartImageStyle: {
    height: 25,
    width: 25,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'flex-end',
    tintColor: '#CC181E'
  },
  commentCount: {
    height: 11,
    width: 11,
    borderRadius: 11 / 2,
    backgroundColor: '#cc181e',
    position: 'absolute',
    top: 9,
    right: 29
  }
};
const mapStateToProps = state => {
  return {
    issearchseleted: state.bottom.issearchseleted,
    isuserseleted: state.bottom.isuserseleted,
    isfeedseleted: state.bottom.isfeedseleted,
    ischatseleted: state.bottom.ischatseleted,
  };
};

export default connect(
  mapStateToProps,
)(Bottom);

