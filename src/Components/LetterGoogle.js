import React from 'react'
import { Text } from 'react-native'

export const LetterGoogle = (props) => {
  const { children, spacing, textStyle } = props

  const letterStyles = [
    textStyle,
    { paddingRight: spacing,
      fontSize: 14,
      color: '#af1313',
      fontFamily: 'Lato-Black' }
  ]

  return <Text style={letterStyles}>{children}</Text>
}
