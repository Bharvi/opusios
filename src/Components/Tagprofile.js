/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image
} from 'react-native';

import ItemTags from './ItemTags';
import { connect } from 'react-redux';
import { getProfileTag, initialTAGProfileStateData } from '../Actions';
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import { getUser } from "../Database/allSchema";
import Loader from './Loader';

let userId = "";
let numColumns = 3;


let formatData = (data, numColumns) => {
   const numberOfFullRows = Math.floor(data.length / numColumns);
   let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
   while (numberOfElementsLastRow != numColumns && numberOfElementsLastRow != 0) {
    data.push({ key: 'blank-${numberOfElementsLastRow}', empty: true });
    numberOfElementsLastRow++;
   }
   return data;
 }


class Tagprofile extends Component {
  constructor(props) {
      super(props);
      this.state = {
        TagsData: [
           { name: 'music' },
           { name: 'MBA' },
           { name: 'acting' },
         ]
      };
      }


  componentDidMount() {
    // this.props.initialTAGProfileStateData();
    // this.props.getProfileTag({userId:this.props.userId, token: this.props.SplashauthToken });
        
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
          // this.props.getProfileTag({userId:this.props.userId, token: this.props.SplashauthToken });
      }
    );
    }
    componentWillUnmount() {
      this.willFocusSubscription.remove();
  }



      // componentWillMount() {
      //   didFocusSubscription = this.props.navigation.addListener(
      //      'willFocus',
      //      payload => {
      //        this.getData();
      //      }
      //    );
      // }

      // componentWillUnMount() {
      //   didFocusSubscription.remove();
      // }
      //
      // getData() {
      //   console.log(this.props.userId);
      //   this.props.getProfileTag(this.props.userId );
      //
      // }
      //
      // componentDidMount() {
      //   console.log(this.props.userId);
      //   // getUser()
      //   //   .then(user => {
      //   //     if (user.length > 0) {
      //   //       // this.setState({ userData: user[0] });
      //   //       userId = user[0].id;
      //   //       console.log(userId);
      //   //     this.props.getProfileTag(userId );
      //   //     }
      //   //   })
      //   //   .catch(error => {
      //   //     console.log(error);
      //   //   });
      //   this.props.getProfileTag(this.props.userId );
      //
      // }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white', }}>
          {
            (this.props.ProfileTag_list != null && this.props.ProfileTag_list.length > 0 )?
            <FlatList
              contentContainerStyle={{ flexDirection:'row',flexWrap: 'wrap' , padding:5 }}
              style={{ marginBottom: 10 }}
              data={this.props.ProfileTag_list}
              showsHorizontalScrollIndicator={false}
              // numColumns={numColumns}
              removeClippedSubviews={true}
              renderItem={({ item, index }) => {
              if (item.empty === true) {
              return <View style={[styles.item, styles.itemInvisible]} />;
             }
              return (
                <ItemTags
                  tagName={item.user_tag}

                />
                );
            }}
            />
            :
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                flex: 1
              }}
            >
            {/* <Image style={{ height: 130, width: 130}}
              source={require('../img/no-data-found.jpg')}/> */}
              <Text style={{ fontSize: 13, fontFamily: "Lato-Regular", color: '#686868',}}>No tag to show</Text>
            </View>
          }
          <Loader
            loading={this.props.isLoading}
            
          />
    </View>
    );
  }
}

const styles = {
  item: {
  alignItems: 'center',
  justifyContent: 'center',
  flex: 1,
  marginLeft: 5,
  marginRight: 5
  },

  itemInvisible: {
  backgroundColor: '#fff'
  },
};



const mapStateToProps = state => {
    return {
      ProfileTag_list: state.OtherProfile.ProfileTag_list,
      authResult: state.OtherProfile.authResult,
      isLoading: state.OtherProfile.isLoading,
      SplashauthToken: state.Splash.authToken,
    };
};

export default connect(mapStateToProps, {
  // getProfileTag,
  // initialTAGProfileStateData
})(Tagprofile);
