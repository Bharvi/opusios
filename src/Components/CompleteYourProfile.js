/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  FlatList,
  StatusBar,
  Alert,
  Dimensions,
  Modal,
  Picker

} from 'react-native';
import  CompleteYourProfileLayout  from './CompleteYourProfileLayout';
import { connect } from 'react-redux';
import {getAllTags , onPressTagView, locationChange1, locationErrorChange1, profecationChange1, profecationErrorChange1, getcompleteProfile } from '../Actions';
import Header from './HeaderCompeleteprofile'
import ModalDropdown from './ModalDropdown';
import { getUser } from "../Database/allSchema";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import Loader from './Loader';

let formatData = (data, numColumns) => {
  let numberOfFullRows = Math.floor(data.length / numColumns);
   let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
   while (numberOfElementsLastRow != numColumns && numberOfElementsLastRow != 0) {
    data.push({ key: 'blank-${numberOfElementsLastRow}', empty: true });
    numberOfElementsLastRow++;
   }
   return data;
 }
 let numColumns = 3;

var { height } = Dimensions.get('window');
 var box_count = 3;
 var box_height = height / box_count;

 let Location = '';
 let userId= '';

 class CompleteYourProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
        tags: [
          {tags:'animals', seleted: false},
          {tags:'animals', seleted: false},
       ],
       professions: [],
       SelectTimeSlotState: '',
       showPicker: false,
       professionName: ''

    }
  }
  componentWillMount() {
    this.props.getAllTags({ token:this.props.SplashauthToken });
    userId = this.props.navigation.getParam('userId');
    //
    // this.props.getAllTags({ token:'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsRYNJdBNZbhTsycIn3N-1q7Dw3JFw9oNfGUaccbzQ6SReb5A6R2kwNEiWPJIkxkbXfWFOdTrV3CMp5zvyK5jVZm7tWQhHfeEbicAqo3WCVP7_Coz5-2iVGwZjm_T4DXu-g8IHVUxX7nADgLO1IhGHB6ZiVl8LG8Td6ZZ05tRXCWevETqq9D9Yy2Z1kmxGk-0idlu4vn5-gnLfsyXhpz4XUrixtKagGxi2TDSLjsvwBihi7z3rC8FGD6BA8T9VJyuECUGmIFUm36UEyoeHjsCWevu-hqn9i0cKaD5F3xrNB1nGoeat9IuQ1ithpJ1d8k4Z0IlwXkmksN0BMa18AEtmI6Hm-9DmeLhDTHGghRIDwdbemnmiH95AJUVNQtnJD4-y4P9RrhHL5ZR_HcrDoEy3EceUlmAapSyeVA1bISmud-UJo6RPmdXLyGlx5trtmXSQlPduShTtiWHOMYBpX8l9m4Fvr7yn-ZBc0yWNihUd2oDl-SPzpeRZd25Og0CkcA9M8IHVUxX7nADgLO1IhGHB6ZiVl8LG8Td6ZZ05tRXCWevETqq9D9Yy2Z1kmxGk-0idlu4vn5-gnLfsyXhpz4XUrixtKagGxi2TDSLjsvwBihi7z3rC8FGD6BA8T9VJyuECUGmIFUm36UEyoeHjsCWevu-hqn9i0cKaD5F3xrNB1nGoeat9IuQ1ithpJ1d8k4Z0IlwXkmksN0BMa18AEtmI6Hm-9DmeLhDTHGghRIDwdbemnmiH95AJUVNQtnJD4-y4P9RrhHL5ZR_HcrDoEy3EceUlmAapSyeVA1bISmud-UJo6RPmdXLyGlx5trtmXSQlPduShTtiWHOMYBpX8l9m4Fvr7yn-ZBc0yWNihUd2oDl-SPzpeRZd25Og0CkcA9MLf9YEsY2h0' });
    // userId = this.props.navigation.getParam('userId');
  }
  componentWillReceiveProps(nextProps){
    console.warn('profession', this.state.professionName);
  }

  onAddProfessionChange(text) {
     this.props.profecationChange1(text);
   }
  onAddLocationChange(text) {
      this.props.locationChange1(text);

   }

   onButtonPress() {

     console.log('onButtonPress');
       let temp = [];
       for(let i = 0; i < this.props.tags.length; i++) {
         if(this.props.tags[i].seleted) {
           temp.push(this.props.tags[i].id);
         }
       }
       if(temp.length> 0 ){



       console.log(temp.toString());
       let {
       profession,
       location,
     } = this.props;
     // this.props.getcompleteProfile({
     //   userID: userId,
     //   tagID:temp.toString(),
     //   professions: this.state.SelectTimeSlotState,
     //   location,
     //   token: this.props.SplashauthToken,
     //   myNavigation: this.props.navigation
     // });
     let isValid = true;
     if (profession === "") {
       isValid = false;
       this.props.profecationErrorChange1("Enter profession");
     } else {
       this.props.profecationErrorChange1("");
     }
     if (location === "") {
       isValid = false;
       this.props.locationErrorChange1("Enter location");
     } else {
       this.props.locationErrorChange1("");
     }
     if (isValid === true) {
       console.log(userId);
       this.props.getcompleteProfile({
         userID: userId,
         tagID:temp.toString(),
         professions: this.state.SelectTimeSlotState,
         location,
         token: this.props.SplashauthToken,
         myNavigation: this.props.navigation
       });
     }
     }
     else{
       Alert.alert(
         "Alert",
         "Please select at least tag"
         )
     }
   }



   professtionDropDown(rowData) {
     console.log(rowData.name)
     return (
       <TouchableOpacity style={{ width: '100%' }}>
         <Text
          allowFontScaling={false} 
           style={{
             fontSize: 14,
             marginBottom: 10,
             paddingLeft: 10,
             paddingTop: 10,
             paddingRight: 10,
             width: '100%'
           }}
         >
           {rowData.name}
         </Text>
       </TouchableOpacity>
     );
   }

   pickerItem(){
    return this.props.professions.map((item) => {
      return (
              <Picker.Item
                  label={item.label}
                  value={item.value}
                  key={item.key || item.label}
              />
      )
            }
        );
    
  }


  render()
   {

    return (
      <View style={styles.container}>
      <StatusBar
         backgroundColor="#CC181E"
         barStyle="light-content"
       />
      <Header
        headertext={'Complete your profile'}
        textDone={'Next'}
         //onPressDone={this.props.navigation.navigate('BottomTab', { myProps: this.props })}
        onPressDone={() => this.onButtonPress()}
      />
      <View style={styles.container1}>
        <Text allowFontScaling={false}  style = {styles.text}>Select 'Tags' (Keyword/Topic) for penpals to Search you.</Text>
        <View allowFontScaling={false}  style = {styles.list}>
        <FlatList
              contentContainerStyle={{ flexDirection:'row',flexWrap: 'wrap' }}
              data={this.props.tags}
              // numColumns={numColumns}
              renderItem={({ item, index }) => {

                      return (
               <CompleteYourProfileLayout
                  item = {item}
                  onPress={() => this.props.onPressTagView(index,this.props.tags)}
               />
            );
          }}
        />
        </View>
        <View style = {styles.list}>
        <Text allowFontScaling={false}  style = {styles.text}>Profession</Text>
        <View style = {{paddingLeft:10}}>
        <TextInput
                  ref="professionInput"
                  underlineColorAndroid="transparent"
                  style={styles.textInputStyle}
                  value={this.state.professionName}
                  editable={false}
                  placeholder={'Select Profession'}
                  onTouchStart={() => this.setState({ showPicker: true })}
                />
      {
        // <TouchableOpacity
        //     style={{ width: '100%' }}
        //     activeOpacity={0.8}
        //     onPress={() => this.refs.professtionDropDown.show()}
        // >

      
        // <ModalDropdown
        //     ref='professtionDropDown'
        //     style={{
        //       width: '100%',
        //       borderBottomWidth: 1,
        //       borderColor: '#261d1d26',
        //       height: 30
        //     }}
        //     textStyle={{
        //       fontSize: 16,
        //       width: '100%',
        //       color: "#1d1d26",
        //       fontFamily: "Lato-Regular",
        //       paddingLeft: 3,
        //     }}
        //     defaultValue={"Select profession"}
        //     options={this.props.professions}
        //     renderButtonText={rowData => rowData.name}
        //     dropdownStyle={{ width: 300 }}
        //     onSelect={(idx, value) =>
        //       {
        //       this.setState({ SelectTimeSlotState: value.id });
        //       this.props.profecationChange1(value.id);
        //     }
        //     }
        //     renderRow={this.professtionDropDown.bind(this)}
        //   />
        
          // </TouchableOpacity>

      }
    {
        <Text allowFontScaling={false}  style={styles.errorTextStyle}>
        {this.props.professionError}
      </Text>
    }
        </View>
        
        <TouchableOpacity
        style={{ 
          height: 45 }}
             onPress={()=> this.props.navigation.navigate('LocationAutosuggestion')}
            >

            <Text allowFontScaling={false}  style = {styles.text}>Location</Text>
        <TextInput
            editable = {false}
            style = {{
            fontSize: 16,
            color: "#1d1d26",
            fontFamily: "Lato-Regular",
            paddingLeft: 10,
             }}
            returnKeyType="done"
            value={this.props.location}
            onTouchStart={()=>  this.props.navigation.navigate('LocationAutosuggestion')}
            placeholder = {'Enter your location'}
            underlineColorAndroid="black"
        />
        </TouchableOpacity>
    {
      <Text style={styles.errorTextStyle1}>
          {this.props.locationError}
        </Text>
      }
      </View>
        </View>

        <Modal
          visible={this.state.showPicker}
          transparent
          animationType={'slide'}
          supportedOrientations={['portrait', 'landscape']}
        >
            <TouchableOpacity
              style={{ flex: 1 }}
              onPress={() => {
                this.setState({ showPicker: false })
              }}
            />
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          showPicker: false,
                        });
                      }}
                      hitSlop={{ top: 4, right: 4, bottom: 4, left: 4 }}
                    >
                    <View>
                        <Text style={{
                          color: '#007AFE',
                          fontWeight: 'bold',
                          fontSize: 16,
                          padding: 5,
                          textAlign: 'right',
                          backgroundColor: 'white'
                        }}>Done</Text>
                    </View>
                </TouchableOpacity>
                    <View 
                      style={{ height: 215,
                        justifyContent: 'center',
                        backgroundColor: '#D0D4DB',
                      }}>
                        <Picker
                        style={{
                          backgroundColor: 'white'
                        }}
                            testID={'Picker'}
                            onValueChange={(itemValue, itemIndex) =>
                              { console.log(itemValue);
                                this.setState({ SelectTimeSlotState: this.props.professions[itemIndex].value, 
                                  professionName: this.props.professions[itemIndex].label });
                                this.props.profecationChange1(this.props.professions[itemIndex].label )}
                            }
                            selectedValue={this.state.SelectTimeSlotState}
                        >
                          {this.pickerItem()}
                        </Picker> 
                   </View>
                </Modal>
        <Loader
          loading={this.props.isLoading}
          
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  container1: {
    flex: 1,
    backgroundColor: 'white',
    padding: 20
  },
  text:{
    color: 'black',
    fontSize: 17,
    fontWeight: 'bold',
    marginVertical:12
  },
  list:{
    height: box_height,
   },
   errorTextStyle: {
     fontSize: 14,
     color: "#F00",
     textAlign: "right",
     marginRight: 10,
     fontFamily: "Lato-Regular"
   },
   errorTextStyle1: {
    fontSize: 14,
    color: "#F00",
    textAlign: "right",
    marginRight: 10,
    marginTop: 25,
    fontFamily: "Lato-Regular"
  }
});

const mapStateToProps = state => {
      return{
        tags:state.CompeleProfile.tags,
        professions:state.CompeleProfile.professions,
        profession:state.CompeleProfile.profession,
        professionError:state.CompeleProfile.professionError,
        location:state.CompeleProfile.location,
        locationError:state.CompeleProfile.locationError,
        authResulttags:state.CompeleProfile.authResulttags,
        isLoading:state.CompeleProfile.isLoading,
        SplashauthToken: state.Splash.authToken,
      };
  };

export default connect( mapStateToProps,{getAllTags,onPressTagView, locationChange1, locationErrorChange1, profecationChange1, profecationErrorChange1, getcompleteProfile })(CompleteYourProfile);
