
import React, { Component } from 'react';

import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions,
  StatusBar,
  Modal,
  Alert
} from "react-native";
import GestureRecognizer, {
  swipeDirections
} from "react-native-swipe-gestures";
import Testimonials from "./Testimonialsprofile";
import Friend from "./Friendprofile";
import Posts from "./Postsprofile";
import About from "./Aboutprofile";
import Tag from "./Tagprofile";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import { POSTIMAGEPATH, PROFILEIMAGEPATH } from "../Actions/type";
import { connect } from "react-redux";
import {
  onBottomTabChange,
  getOtherProfile,
  getOtherProfileTag,
  getOtherProfileAbout,
  initialOtherProfileStateData,
  saveUserChat,
  initialTAGProfileStateData,
  getBlockfriend,
  getUnfriend,
  getAddfriend,
  initialMyprofileStateData
} from "../Actions";
import { getUser } from "../Database/allSchema";
import ImageLoad from './ImageLoad';
import UserProfileImg from '../Components/common/UserProfileImg';
import Loader from "./Loader";
import DeviceInfo from 'react-native-device-info';
import { NavigationEvents } from 'react-navigation';

let progress = 38;
let average_rating = 0;

let myProps = {};

let config = {
  velocityThreshold: 0.3,
  directionalOffsetThreshold: 80
};
let user_id = "";
let userID = '';
let Formchat = '';
let searchtag = '';
let count = 0
class Profile extends Component {
  static title = "Scrollable top bar";
  static backgroundColor = "#ffffff";
  static appbarElevation = 0;

  constructor(props) {
    super(props);
    this.componentWillMount()
    // this.props.initialOtherProfileStateData();
  }

  state = {
    type: 0,
    prifilePicDialogVisible: false,
    fullname: "",
    profileimage: "",
    averagerating: 0,
    usergiverating_count: "",
    user_id: "",
    isFriend: false,
    isFirstTime: false

  };
  componentWillMount() {
    this.props.navigation.addListener("willFocus", payload => {
      this.props.initialOtherProfileStateData();

      this.getData();
    });
    this.props.navigation.addListener("didFocus", payload => {
      setTimeout(() => {
        console.log("search didFocus");
        this.props.onBottomTabChange({ prop: "issearchseleted", value: false });
        this.props.onBottomTabChange({ prop: "isuserseleted", value: true });
        this.props.onBottomTabChange({ prop: "isfeedseleted", value: false });
        this.props.onBottomTabChange({ prop: "ischatseleted", value: false });
      }, 0);
    });
    this.props.navigation.addListener("willBlur", payload => {
      // 
      this.props.initialOtherProfileStateData();
      this.props.onBottomTabChange({ prop: "issearchseleted", value: false });
      this.props.onBottomTabChange({ prop: "isuserseleted", value: false });
      this.props.onBottomTabChange({ prop: "isfeedseleted", value: false });
      this.props.onBottomTabChange({ prop: "ischatseleted", value: false });

    });
  }
  componentWillUnMount() {
    console.log("profile willBlur");
  }

  getData() {

    this.props.initialOtherProfileStateData();

    console.log('in get data')
    this.setState({ type: 0 });
    searchtag = this.props.navigation.getParam("searchText1");
    user_id = this.props.navigation.getParam("user_id") + '';
    Formchat = this.props.navigation.getParam("form");
    // this.props = myProps;  
    let tag_type = this.props.navigation.getParam("tagtype");
    if (tag_type === "tagtype") {
      this.setState({ type: 1 });
    }
    console.warn("user_id" + user_id);
    // console.warn("SplashauthToken"+this.props.SplashauthToken);
    getUser()
      .then(user => {
        if (user.length > 0) {
          this.setState({ userData: user[0] });
          userID = user[0].id;
          console.log(" Database mathi user get user ID  ", userID);

          this.props.getOtherProfile({
            userId: user_id,
            toUserID: userID,
            token: this.props.SplashauthToken
          });
          this.props.getOtherProfileAbout({
            userId: user_id,
            token: this.props.SplashauthToken
          });
          this.props.getOtherProfileTag({
            userId: user_id,
            token: this.props.SplashauthToken
          });
        }
      })
      .catch(error => {
        console.warn('err', error);
      });

  }
  componentDidMount() {
    getUser()
      .then(user => {
        if (user.length > 0) {
          this.setState({ userData: user[0] });
          userID = user[0].id;
          console.log(" Database mathi user get user ID  ", userID);
        }
      })
      .catch(error => {
        console.log(error);
      });

    // this.props.initialOtherProfileStateData();

    // this.getData();

  }
  componentWillReceiveProps(nextProp) {


    if (nextProp.authResult === "my profile data success") {
      console.warn("nextProp.Data.is_friend", nextProp.Data.is_friend);
      this.setState({ isFriend: (nextProp.Data.is_friend == "Y") ? true : false })
    }

  }

  //   componentWillReceiveProps(nextProp) {

  //     if (nextProp.authResultAddFriend === "Success Addfriend") {
  //       console.log("Success");
  //     }else if (nextProp.authResultAddFriend === "Already Friend") {
  //       // Alert.alert("Oops", "Already Friend");
  //       console.log("Already Friend");
  //     }
  //     else if(nextProp.authResultAddFriend === "User id must be numeric.") {
  //       setTimeout(() => {
  //         Alert.alert("Oops", "User id must be numeric.");
  //       }, 500);


  //     }
  // }

  onBlockFriend() {
    console.log(userID);
    console.log(user_id);
    // this.setState({ OptionDialogVisible: false });
    Alert.alert(
      "Alert",
      "Are you sure you want to block " + this.props.Data.user_name + "?",
      [
        {
          text: "Yes", onPress: () => {
            this.setState({ OptionDialogVisible: false });
            this.props.getBlockfriend({
              userID,
              toUserId: user_id,
              token: this.props.SplashauthToken
            });
          }
        },
        {
          text: "No",
          onPress: () => {
            cancelable: false;
          }
        }
      ],
      { cancelable: false }
    );
    // this.props.getBlockfriend({ userID, toUserId: user_id, token: this.props.SplashauthToken });

    // this.UnFriendApi(false);
  }


  onUnfriend() {
    Alert.alert(
      "Alert",
      "Are you sure you want to remove " + this.props.Data.user_name + " as your friend?",
      [
        { text: "Yes", onPress: () => this.UnFriendApi(true) },
        {
          text: "No",
          onPress: () => {
            cancelable: false;
          }
        }
      ],
      { cancelable: false }
    );

  }
  UnFriendApi(isShowAlert) {
    this.setState({ isFriend: false });
    this.props.getUnfriend({
      userId: userID,
      toUserId: user_id,
      token: this.props.SplashauthToken,
      isShowAlert
    });

  }

  onAddfriend() {
    this.setState({ isFriend: true });
    this.props.getAddfriend({
      userID: userID,
      toUserId: user_id,
      token: this.props.SplashauthToken
    });

  }

  onChatMessage() {
    this.props.saveUserChat(user_id, this.props.Data.user_name, this.props.Data.firebase);
    setTimeout(() => {
      this.props.navigation.navigate("ChatMessage", {
        toUserId: user_id,
        fullname: this.props.Data.user_name,
        to_fire_id: this.props.Data.firebase
      });
    }, 500);
  }

  onSwipeLeft(gestureState) {
    console.log("onSwipeLeft" + this.state.type);
    // if (this.state.type >= 0 && this.state.type < 4) {
    //   this.setState({ type: this.state.type + 1 });
    // }
  }

  onSwipeRight(gestureState) {
    console.log("onSwipeRight" + this.state.type);
    // if (this.state.type > 0 && this.state.type <= 4) {
    //   this.setState({ type: this.state.type - 1 });
    // }
  }

  // renderProgess() {
  //   const progressArray = [];
  //   for (let i = 0; i < (this.props.Data.avg_user_rating*3); i++) {
  //     progressArray.push(i);
  //   }
  //   return progressArray.map(number => (
  //       <View
  //         style={{
  //           backgroundColor: '#1bc713',
  //           width: 5.1,
  //           height: 8,
  //           borderRadius: 2,
  //           marginRight: 1
  //         }}
  //       />
  //   ));
  // }





  renderProgess() {
    let backgroundColor = "#1bc713";
    if ((this.props.Data.avg_user_rating * 3) < (4 * 3)) {
      backgroundColor = "red";
    } else if (
      (this.props.Data.avg_user_rating * 3) >= (4 * 3) &&
      (this.props.Data.avg_user_rating * 3) < (6 * 3)
    ) {
      backgroundColor = "yellow";
    }
    let progressArray = [];
    for (let i = 0; i < (this.props.Data.avg_user_rating * 3); i++) {
      progressArray.push(i);
    }
    return progressArray.map(number => (
      <View
        style={{
          backgroundColor: backgroundColor,
          width: 5.1,
          height: 8,
          borderRadius: 2,
          marginRight: 1
        }}
      />
    ));
  }

  callApi() {
    console.log('count', count)
    setTimeout(() => {

      if (this.props.navigation.state.routeName == 'Profile') {
        if (!this.props.isLoading && (this.props.Data.length == 0) && count == 0) {

          console.log('this.props.navigation.state.routeName', this.props.navigation.state.routeName, this.props.isLoading, this.props.Data.length)
          this.setState({ isFirstTime: true })
          this.getData()
          count = 1
        }
      }
    }, 1000);

  }
  renderPage() {
    if (this.props.Data.length == 0) {
      count == 0
    }
    // this.callApi()


    if (user_id != "") {
      if (this.state.type === 0) {
        console.log("About");
        console.log(user_id);
        return <About userId={user_id} navigation={this.props.navigation} />;
      } else if (this.state.type === 1) {
        console.log("Tag");
        return <Tag userId={user_id} navigation={this.props.navigation} />;
      } else if (this.state.type === 2) {
        console.log("Testimonials");
        return (
          <Testimonials userId={user_id} navigation={this.props.navigation} />
        );
      } else if (this.state.type === 3) {
        console.log("Posts");
        return <Posts userId={user_id} myNavigation={this.props.navigation} />;
      } else if (this.state.type === 4) {
        console.log("Friend");
        return <Friend userId={user_id} navigation={this.props.navigation} />;
      }
    }
    return null;
  }
  redirectPage() {
    console.warn(Formchat);
    if (Formchat === "chat") {
      console.warn("Wel come Chat Page", Formchat);
      this.props.navigation.navigate("Chat");
    } else if (Formchat === "MyProfile") {
      console.warn("Wel come MyProfile1", Formchat);
      this.props.navigation.navigate("Search");
    } else if (Formchat === "FeedsPage") {
      console.warn("Wel come FeedsPage", Formchat);
      this.props.navigation.navigate("FeedsPage");
    } else if (Formchat === "MyProfile1") {
      console.warn("Wel come MyProfile", Formchat);
      this.props.navigation.navigate("MyProfile", { myProps });
    } else if (Formchat === "searchResult") {
      console.warn("Wel come searchResult", Formchat);
      this.props.navigation.navigate("SearchResult", { searchText: searchtag });
    } else if (Formchat === "searchUpdate") {
      console.warn("Wel come searchUpdate", Formchat);
      this.props.navigation.navigate("SearchUpdates");
    } else if (Formchat === "Commentpage") {
      console.warn("Wel come searchUpdate", Formchat);
      this.props.navigation.navigate("FeedsPage");
    }
    else if (Formchat === "Notification") {
      console.warn("Wel come searchUpdate", Formchat);
      this.props.navigation.navigate("Notification");
    }
  }


  render() {
    console.log(this.props.Data)
    return (
      <View style={styles.container}>
        
        <View style={{ backgroundColor: "white", height: 15 }}></View>
        <View style={{ backgroundColor: "white", paddingTop: (DeviceInfo.getModel() === 'iPhone X' || DeviceInfo.getModel() === 'iPhone XS Max' || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 15 : 0 }} />
        <View style={styles.hedarStyleView}>
          <TouchableOpacity
            activeOpacity={1}
            style={{ height: 50, width: 50, alignItems: 'center', justifyContent: 'center', alignContent: 'center' }}
            onPress={() => this.redirectPage()}>
            <Image
              style={styles.backArrowStyle}
              source={require("../img/back_icon.png")}
            />
          </TouchableOpacity>
          <Text allowFontScaling={false} style={styles.textStyle}>Profile</Text>
          <View
            style={{ height: 50, width: 50, padding: 5, alignItems: 'center', alignSelf: 'center', justifyContent: 'center' }}
          />
        </View>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <View
          style={{
            flexDirection: "row",
            backgroundColor: "white",
            padding: 10,
            marginBottom: 15
          }}
        >
          <View
            style={{
              flex: 2.7,
              flexDirection: 'row',
              justifyContent: "center",
              alignItems: "center"
            }}

          >
            <TouchableOpacity
              activeOpacity={0.8}
              style={{ flexDirection: 'row' }}
              onPress={() => this.setState({ prifilePicDialogVisible: true })}
            >
              {(this.props.isLoading) ?
                <Image
                  style={{ height: 90, width: 90, borderRadius: 90 / 2, marginRight: 10 }}
                  source={require('../img/Unknown.jpg')}
                />
                :
                <UserProfileImg
                  style={{ height: 90, width: 90, borderRadius: 90 / 2 }}
                  borderRadius={90 / 2}
                  source={{ uri: POSTIMAGEPATH + this.props.Data.user_image }}
                  dotSize={12}
                  fireId={this.props.Data.firebase}
                  profile={true}
                />
              }
              {/* <UserProfileImg
                style={ { height: 90, width: 90, borderRadius: 90 / 2 } }
                borderRadius={ 90 / 2 }
                source={ { uri: POSTIMAGEPATH + this.props.Data.user_image } }
                dotSize={ 12 }
                fireId={ this.props.Data.firebase }
                profile={ true }
              /> */}
              <TouchableOpacity
                onPress={() => this.onChatMessage()}
                style={{
                  position: 'absolute',
                  height: 29,
                  width: 29,
                  right: '2%',
                  top: '45%',
                  borderRadius: 30 / 2,
                  backgroundColor: '#CC181E',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderWidth: 1,
                  borderColor: 'white'
                }}>
                <Image
                  style={styles.chatIconStyle}
                  source={require("../img/message_icon.png")}
                />
              </TouchableOpacity>
            </TouchableOpacity>
            {/* <TouchableOpacity
              onPress={ () => this.onChatMessage() }
              style={ { height: 29, width: 29, borderRadius: 30 / 2, backgroundColor: '#CC181E', right: 25, top: 20, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: 'white' } }>
              <Image
                style={ styles.chatIconStyle }
                source={ require("../img/message_icon.png") }
              />
            </TouchableOpacity> */}
          </View>
          <View
            style={{
              flex: 5,
              paddingLeft: 10,
              paddingRight: 10,
              paddingTop: 10,
              justifyContent: "center"
            }}
          >
            <Text allowFontScaling={false} style={styles.nameTextStyle}>
              {this.props.Data.user_name}
            </Text>
            <View style={{ flex: 7, marginTop: 5, marginBottom: 1 }}>
              <View
                style={{
                  backgroundColor: "#DBDBDB",
                  width: 180,
                  height: 8,
                  borderRadius: 3
                }}
              />
              <View
                style={{
                  width: this.props.Data.avg_user_rating,
                  position: "absolute",
                  height: 8,
                  flexDirection: "row"
                }}
              >
                {this.renderProgess()}
              </View>
              {this.props.Data.user_rating ?
                <Text allowFontScaling={false} style={styles.userTextStyle}>
                  {(this.props.Data.user_rating !== 0) ? (this.props.Data.user_rating + ' Users') : 'No ratings'}
                </Text> : null
              }
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 6, }} >
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => this.onBlockFriend()}
                  style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', borderColor: '#CC181E', borderWidth: 1, backgroundColor: 'white', borderRadius: 25 / 2, width: 25, height: 25, justifyContent: 'center', alignItems: 'center' }}>
                    <Image
                      style={{ height: 15, width: 12, tintColor: '#CC181E' }}
                      source={require('../img/user_report.png')} />

                  </View>
                  <Text allowFontScaling={false} style={styles.userTextStyle1}>Block</Text>
                </TouchableOpacity>
                {
                  (this.state.isFriend) ?
                    <TouchableOpacity
                      activeOpacity={0.8}
                      onPress={() => this.onUnfriend()}
                      style={{ flexDirection: 'row', marginLeft: 5, alignItems: 'center' }}>
                      <View style={{ backgroundColor: '#CC181E', borderRadius: 25 / 2, width: 25, height: 25, justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                          style={{ height: 15, width: 12, tintColor: 'white', }}
                          source={require('../img/user_addfriend.png')} />
                      </View>

                      <Text allowFontScaling={false} style={styles.userTextStyle1}>Unfriend</Text>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity
                      activeOpacity={0.8}
                      onPress={() => this.onAddfriend()}
                      style={{ flexDirection: 'row', marginLeft: 5, alignItems: 'center' }}>
                      <View style={{ backgroundColor: '#CC181E', borderRadius: 25 / 2, width: 25, height: 25, justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                          style={{ height: 15, width: 12, tintColor: 'white', }}
                          source={require('../img/user_addfriend.png')} />
                      </View>

                      <Text allowFontScaling={false} style={styles.userTextStyle1}>Friend</Text>
                    </TouchableOpacity>
                }
              </View>
            </View>
          </View>
        </View>
        <View style={{ marginBottom: 40, flex: 1 }}>
          <View style={{ height: 45, backgroundColor: "white", borderBottomWidth: 1, borderColor: '#e2e0e0' }}>
            <ScrollView
              ref={(ref) => this.scrollView = ref}
              horizontal showsHorizontalScrollIndicator={false}>
              <TouchableOpacity
                style={[
                  styles.tabbarStyleView,
                  {
                    borderBottomWidth: this.state.type === 0 ? 1 : 0,
                    borderBottomColor: "#ff0000"
                  }
                ]}
                onPress={() => {
                  this.scrollView.scrollTo({ x: 0, y: 0, animated: true });
                  this.setState({ type: 0 });
                }}

              >
                <Text
                  allowFontScaling={false}
                  style={[
                    styles.label,
                    { color: this.state.type === 0 ? "#ff0000" : "#7F7F7F" }
                  ]}
                >
                  About
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.tabbarStyleView,
                  {
                    borderBottomWidth: this.state.type === 1 ? 1 : 0,
                    borderBottomColor: "#ff0000"
                  }
                ]}
                onPress={() => {
                  this.scrollView.scrollTo({ x: 0, y: 0, animated: true });
                  this.setState({ type: 1 });
                }}
              >
                <Text
                  allowFontScaling={false}
                  style={[
                    styles.label,
                    { color: this.state.type === 1 ? "#ff0000" : "#7F7F7F" }
                  ]}
                >
                  Tags
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.tabbarStyleView,
                  {
                    borderBottomWidth: this.state.type === 2 ? 1 : 0,
                    borderBottomColor: "#ff0000"
                  }
                ]}
                onPress={() => this.setState({ type: 2 })}
              >
                <Text
                  allowFontScaling={false}
                  style={[
                    styles.label,
                    { color: this.state.type === 2 ? "#ff0000" : "#7F7F7F" }
                  ]}
                >
                  Testimonials
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.tabbarStyleView,
                  {
                    borderBottomWidth: this.state.type === 3 ? 1 : 0,
                    borderBottomColor: "#ff0000"
                  }
                ]}
                onPress={() => {
                  this.setState({ type: 3 })
                  this.scrollView.scrollToEnd()
                }}
              >
                <Text
                  allowFontScaling={false}
                  style={[
                    styles.label,
                    { color: this.state.type === 3 ? "#ff0000" : "#7F7F7F" }
                  ]}
                >
                  Posts
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.tabbarStyleView,
                  {
                    borderBottomWidth: this.state.type === 4 ? 1 : 0,
                    borderBottomColor: "#ff0000"
                  }
                ]}
                onPress={() => {
                  this.setState({ type: 4 })
                  this.scrollView.scrollToEnd()
                }}
              >
                <Text
                  allowFontScaling={false}
                  style={[
                    styles.label,
                    { color: this.state.type === 4 ? "#ff0000" : "#7F7F7F" }
                  ]}
                >
                  Friends
                </Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <View
            onSwipeLeft={state => this.onSwipeLeft(state)}
            onSwipeRight={state => this.onSwipeRight(state)}
            config={config}
            style={{
              flex: 1
            }}
          >
            {this.renderPage()}
          </View>
          <Modal
            transparent={true}
            visible={this.state.prifilePicDialogVisible}
            onRequestClose={() =>
              this.setState({ prifilePicDialogVisible: false })
            }
          >
            <View
              style={{
                height: "100%",
                width: "100%",
                paddingTop: 130,
                paddingBottom: 100
              }}
            >
              <View
                style={{
                  backgroundColor: "rgba(0,0,0,0.9)",
                  padding: 5,
                  justifyContent: "center",
                  alignContent: "center"
                }}
              >
                <View
                  style={{ alignItems: "center", justifyContent: "center" }}
                >
                  <ImageLoad
                    style={{ height: "100%", width: "100%" }}
                    source={{ uri: POSTIMAGEPATH + this.props.Data.user_image }}
                    resizeMode="contain"
                  />
                  <TouchableOpacity
                    style={styles.CloseViewSTyle}
                    onPress={() =>
                      this.setState({ prifilePicDialogVisible: false })
                    }
                  >
                    <Image
                      style={{ height: 18, width: 18 }}
                      source={require("../img/close_icon.png")}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </View>
        <Loader
          loading={this.props.isLoading}
        />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#DBDBDB"
  },
  tabbar: {
    backgroundColor: "#ffffff"
  },
  tab: {
    width: 120
  },
  indicator: {
    backgroundColor: "#ff000b"
  },
  label: {
    color: "#7F7F7F",
    fontSize: 14,
    fontFamily: "Lato-Heavy"
  },
  hedarStyleView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    height: 50,
    backgroundColor: "white"
  },
  textStyle: {
    fontSize: 16,
    color: "#4d4e51",
    fontFamily: "Lato-Bold"
  },
  nameTextStyle: {
    fontSize: 16,
    color: "#CC181E",
    fontFamily: "Lato-Medium"
  },
  backArrowStyle: {
    height: 18,
    width: 18,
    tintColor: "#4d4e51",
    padding: 8
  },
  setingStyle: {
    height: 25,
    width: 25,
    tintColor: "white",
  },
  userTextStyle: {
    color: "#828282",
    fontFamily: "Lato-Medium",
    fontSize: 14,
    marginLeft: 3
  },
  userTextStyle1: {
    color: "#CC181E",
    fontFamily: "Lato-Medium",
    fontSize: 14,
    marginLeft: 3
  },
  tabbarStyleView: {
    height: 40,
    width: 100,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center"
  },
  CloseViewSTyle: {
    position: "absolute",
    right: 2,
    top: 2,
    backgroundColor: "rgba(255,0,0,0.5)",
    width: 32,
    height: 32,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  chatIconStyle: {
    height: 31,
    width: 31,
    tintColor: "white",
  },
});

const mapStateToProps = state => {
  return {
    Data: state.OtherProfile.Data,
    authResult: state.OtherProfile.authResult,
    isLoading: state.OtherProfile.isLoading,
    SplashauthToken: state.Splash.authToken,
    authResultBlockFriend: state.Testimonial.authResultBlockFriend,
    authResultFriend: state.Friend.authResultFriend,
    authResultAddFriend: state.Testimonial.authResultAddFriend,
    // isLoading: state.Friend.isLoading,
  };
};
export default connect(
  mapStateToProps,
  {
    onBottomTabChange,
    getBlockfriend,
    getAddfriend,
    getUnfriend,
    initialMyprofileStateData,
    saveUserChat,
    initialTAGProfileStateData,
    getOtherProfile,
    getOtherProfileTag,
    getOtherProfileAbout,
    initialOtherProfileStateData,
  }
)(Profile);
