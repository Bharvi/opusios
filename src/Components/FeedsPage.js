import React, { Component } from "react";
import {
  View,
  Text,
  StatusBar,
  FlatList,
  Image,
  TouchableOpacity,
  Alert,
  Share,
  StyleSheet,
  Dimensions,
  Modal,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import { NavigationActions, StackActions } from "react-navigation";
import HomeHeader from "./HomeHeader";
import FeedsItemView from "./FeedsItemView";
import ItemUserComment from "./ItemUserComment";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import { connect } from "react-redux";
import {
  getFeedlist,
  getSharepost,
  getAbusePost,
  onBottomTabChange,
  onPressLike
} from "../Actions";
import { getUser } from "../Database/allSchema";
import { POSTIMAGEPATH, PROFILEIMAGEPATH } from "../Actions/type";
import ImageLoad from './ImageLoad';
import Loader from './Loader';
import DeviceInfo from 'react-native-device-info';

let ImagePath = "";
let userId = "";
let postID = "";
let shareText = "Test 2";
let post_caption = "";
let post_photo = "";

let { width, height } = Dimensions.get("window");

class FeedsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prifilePicDialogVisible: false,
      loading: false,
      data: [],
      page: 1,
      pageCount: 1,
      refreshing: false,
      post_photo: "",
      post_caption: "",
      userphoto: "",
      feedListState: []
    };
    // this.shareMessage = this.shareMessage.bind(this);
  }

  // shareMessage() {
  //   console.log(this.state.post_caption);
  //   let image = POSTIMAGEPATH + this.state.post_photo;
  //   console.log(JSON.stringify(image));
  //   Share.share({
  //     message: this.state.post_caption,
  //     url: image
  //   });
  // }
  componentWillReceiveProps(nextProp) {
    if (nextProp.authResult === 'feed list success') {
      this.setState({ feedListState: nextProp.Feed_list })
    }
    if (nextProp.authResultSharePost === "Success") {
      setTimeout(() => {
        // this.shareMessage();

      }, 500);

    } else if (nextProp.authResultSharePost === "User id must be numeric.") {
      setTimeout(() => {
        Alert.alert("Oops", "User id must be numeric.");

      }, 500);

    } else if (nextProp.authResultSharePost === "Post id must be numeric.") {
      setTimeout(() => {
        Alert.alert("Oops", "Post id must be numeric.");

      }, 500);

    }
    if (nextProp.authResultFeedAbusePost === "Success Abuse") {
      setTimeout(() => {
        Alert.alert(
          "Success",
          "Report abuse successfully.",
          [
            {
              text: "OK",
              onPress: () => {
                this.setState({ pageCount: 1 });
                this.getData();
              }
            }
          ],
          { cancelable: false }
        );

      }, 500);

    } else if (
      nextProp.authResultFeedAbusePost === "Already Post is Abuse by same User"
    ) {
      setTimeout(() => {
        Alert.alert("Oops", "Already post is abuse by same user");

      }, 500);

    } else if (
      nextProp.authResultFeedAbusePost === "User id must be numeric."
    ) {
      setTimeout(() => {
        Alert.alert("Oops", "User id must be numeric.");

      }, 500);

    } else if (
      nextProp.authResultFeedAbusePost === "Post id must be numeric."
    ) {
      setTimeout(() => {
        Alert.alert("Oops", "Post id must be numeric.");

      }, 500);

    }
  }

  componentWillMount() {
    this.setState({ pageCount: 1 });
    this.getData();
    this.props.navigation.addListener("didFocus", payload => {
      setTimeout(() => {
        console.log("Search didFocus");
        this.props.onBottomTabChange({ prop: "issearchseleted", value: false });
        this.props.onBottomTabChange({ prop: "isuserseleted", value: false });
        this.props.onBottomTabChange({ prop: "isfeedseleted", value: true });
        this.props.onBottomTabChange({ prop: "ischatseleted", value: false });
      }, 0);
    });
    this.props.navigation.addListener("willBlur", payload => {
      console.log("Search willBlur");
      this.props.onBottomTabChange({ prop: "issearchseleted", value: false });
      this.props.onBottomTabChange({ prop: "isuserseleted", value: false });
      this.props.onBottomTabChange({ prop: "isfeedseleted", value: false });
      this.props.onBottomTabChange({ prop: "ischatseleted", value: false });
    });
    // this.willFocusSubscription = this.props.navigation.addListener(
    //   "willFocus",
    //   () => {
    //     this.setState({ pageCount: 1 });
    //     this.getData();
    //   }
    // );
  }
  _onRefresh = () => {

    this.setState({ pageCount: 1 });
    getUser()
      .then(user => {

        if (user.length > 0) {
          // this.setState({ userData: user[0] });
          userId = user[0].id;
          console.log(userId);
          this.fetchData(false, true);
        }
      })
      .catch(error => {

        console.log(error);
      });
  }

  componentWillUnmount() {
  }

  getData() {
    getUser()
      .then(user => {
        if (user.length > 0) {
          // this.setState({ userData: user[0] });
          userId = user[0].id;
          console.log(userId);
          this.fetchData(true, false);
        }
      })
      .catch(error => {
        console.log(error);
      });
  }
  componentDidMount() { }

  CommentPostID(item) {
    this.props.navigation.navigate("CommentPage", { postID: item.post_id });
  }
  onSharePost(item) {
    setTimeout(() => {
      Alert.alert(
        "Opus",
        "Do you want to share the post?",
        [
          {
            text: "Yes",
            onPress: () => {
              this.props.getSharepost({
                userId,
                postID: item.post_id,
                shareText,
                token: this.props.SplashauthToken
              });
              this.setState({ pageCount: 1 });
              this.props.getFeedlist({
                userId,
                page1: 1,
                token: this.props.SplashauthToken,
                isLoading: false,
                isRefreshing: false
              });


            }
          },
          {
            text: "No",
            onPress: () => {
              cancelable: false;
            }
          }
        ],
      );

    }, 500);




  }
  onAbusePost(item) {
    console.log(userId);
    console.log(item.post_id);
    this.props.getAbusePost({
      userId,
      postID: item.post_id,
      token: this.props.SplashauthToken
    });
  }

  onProfileData(item) {
    console.log(item);

    this.props.navigation.navigate("Profile", {
      user_id: item.user_id,
      fullname: item.fullname,
      profile_image: item.profile_image,
      average_rating: item.average_rating,
      user_give_rating_count: item.user_give_rating_count,
      form: 'FeedsPage'
    });
  }

  fetchData = (isLoading, isRefreshing) => {
    console.log(userId, this.state.pageCount);
    this.props.getFeedlist({
      userId,
      page1: this.state.pageCount,
      token: this.props.SplashauthToken,
      isLoading,
      isRefreshing
    });
  };

  handleLoadMore = () => {

    this.setState(
      {
        pageCount: this.state.pageCount + 1
      },
      () => {
        console.log("handleLoadMore", this.state.pageCount, this.props.isdata);

        if (this.props.isdata) {
          console.log("REad morew Api ");
          this.props.getFeedlist({
            userId,
            page1: this.state.pageCount,
            Feed_list: this.state.feedListState,
            token: this.props.SplashauthToken,
            isLoading: false,
            isRefreshing: false
          });
        }
      }
    );
  };

  ActivityIndicator = () => {
    return (this.props.isdata) ? <ActivityIndicator size="small" /> : null
  };

  onPressLikeBtn(item, index) {
    console.warn("index", index);
    let tempFeedList = this.state.feedListState;
    tempFeedList[index] = {
      ...tempFeedList[index],
      is_liked: (tempFeedList[index].is_liked == "Y") ? "N" : "Y",
      like_count: (tempFeedList[index].is_liked == "Y") ? Number(tempFeedList[index].like_count) - 1 : Number(tempFeedList[index].like_count) + 1,
    };
    this.setState({ feedListState: tempFeedList })
  }

  render() {
    console.warn('render', this.state.feedListState);

    return (
      <View style={{ backgroundColor: "#e5eaea", flex: 1 }}>
        <HomeHeader
          headertext={"Feeds"}
          notificationCount={this.props.notificationCount}
          onPressDone={() => this.onButtonPress()}
          notification={require("../img/notification_padding.png")}
          onPressNotification={() =>
            this.props.navigation.navigate("Notification")
          }
        />
        <StatusBar backgroundColor="#CC181E" barStyle="light-content" />
        <View style={{ marginBottom: (DeviceInfo.getModel() === 'iPhone X' || DeviceInfo.getModel() === 'iPhone XS Max' || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 70 : 55, flex: 1 }}>
          {
            this.state.feedListState &&
              this.state.feedListState.length > 0 ? (
                <FlatList
                  style={{}}
                  refreshControl={
                    <RefreshControl
                      tintColor={['#CC181E']}
                      refreshing={this.props.isRefreshing}
                      onRefresh={this._onRefresh.bind(this)}
                    />
                  }
                  data={this.state.feedListState}
                  extraData={this.state}
                  onEndReached={this.handleLoadMore.bind(this)}
                  onEndReachedThreshold={1}
                  ListFooterComponent={this.ActivityIndicator.bind(this)}
                  // renderItem={this._renderItem}
                  renderItem={({ item, index }) => {
                    return (
                      <FeedsItemView
                        item={item}
                        name={item.fullname}
                        userprofileImage={{ uri: POSTIMAGEPATH + item.user_photo }}
                        des={item.time}
                        postTitle={item.post_caption}
                        numOfComments={item.comment_count}
                        numOflike={item.like_count}
                        Image={POSTIMAGEPATH + item.post_photo}
                        CommentArray={item.latest_comment}
                        onPressUserIcon={() =>
                          this.setState({
                            prifilePicDialogVisible: true,
                            userphoto: POSTIMAGEPATH + item.user_photo
                          })
                        }
                        onPressComment={() => this.CommentPostID(item)}
                        sharepost={() => this.onSharePost(item)}
                        Abusepost={() => this.onAbusePost(item)}
                        onPressProfile={() => this.onProfileData(item)}
                        onPressLike={() => this.onPressLikeBtn(item, index)}
                      />
                    );
                  }}
                />
              ) : (
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    flex: 1,
                    backgroundColor: "white"
                  }}
                >
                  {/* <Image
                  style={{ height: 140, width: 140 }}
                  source={require("../img/no-data-found.jpg")}
                /> */}
                  <Text style={{ fontSize: 13, fontFamily: "Lato-Regular", color: '#686868', }}>No Feeds to show</Text>
                </View>
              )}

          {
            <TouchableOpacity
              activeOpacity={0.8}
              style={{ bottom: 5, position: "absolute", right: 10 }}
              // onPress={() => this.props.navigation.navigate("CreatePostPage", { userId: userId} )}
              onPress={() =>
                this.props.navigation.navigate("CreatePostPage", {
                  userId: userId
                })
              }
            >
              <Image
                style={{ height: 75, width: 75 }}
                source={require("../img/plus_withshadow.png")}
              />
            </TouchableOpacity>
          }
        </View>
        <Loader
          loading={this.props.isLoading}
        />

        <Modal
          transparent={true}
          visible={this.state.prifilePicDialogVisible}
          onRequestClose={() =>
            this.setState({ prifilePicDialogVisible: false })
          }
        >
          <View
            style={{
              height: "100%",
              width: "100%",
              paddingTop: 130,
              paddingBottom: 100
            }}
          >
            <View
              style={{
                backgroundColor: "rgba(0,0,0,0.9)",
                padding: 5,
                justifyContent: "center",
                alignContent: "center"
              }}
            >
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <ImageLoad
                  style={{ width: "100%", height: "100%" }}
                  source={{ uri: this.state.userphoto }}
                  resizeMode="contain"
                />
                <TouchableOpacity
                  style={styles.CloseViewSTyle}
                  onPress={() =>
                    this.setState({ prifilePicDialogVisible: false })
                  }
                >
                  <Image
                    style={{ height: 18, width: 18 }}
                    source={require("../img/close_icon.png")}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  CloseViewSTyle: {
    position: "absolute",
    right: 2,
    top: 0,
    backgroundColor: "rgba(255,0,0,0.5)",
    width: 32,
    height: 32,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  containerItem: {
    flex: 1,
    backgroundColor: 'white',
    borderTopWidth: 1,
    borderColor: '#e2e0e0',
    marginTop: 10
  },
  MarcusTextStyle: {
    fontSize: 15,
    fontFamily: 'Lato-Bold',
    color: '#2c2c2c',
    textAlign: 'left'
  },
  MarcusTextStyle2: {
    fontSize: 15,
    fontFamily: 'Lato-Bold',
    color: '#2c2c2c',
    textAlign: 'left'
  },
  MarcusTextStyle3: {
    fontSize: 15,
    fontFamily: 'Lato-Medium',
    color: '#848484',
    textAlign: 'left',

  },
  yesterdayTextStyle: {
    fontSize: 13,
    color: '#989898',
    fontFamily: 'Lato-Medium'
  },
  TextStyle: {
    fontSize: 14,
    paddingLeft: 10,
    paddingRight: 10,
    fontFamily: 'Lato-Semibold',
    color: '#2c2c2c'
  },
  CommentTextStyle: {
    color: '#A4A4A4',
    fontSize: 13,
    fontFamily: 'Lato-Bold',

    alignItems: 'center',
    marginLeft: 5
  },
  commentIconStyle: {
    width: 15,
    height: 15
  },
  commentIconStyle1: {
    width: 15,
    height: 15,
    tintColor: '#848484'
  },
  nameTextStyle: {
    fontSize: 14,
    fontFamily: 'Lato-Semibold',
    color: '#2c2c2c'
  },
  commentTextStyle: {
    fontSize: 14,
    color: '#000000',
    fontFamily: 'Lato-Regular'
  },
});

const mapStateToProps = state => {
  return {
    Feed_list: state.FeedList.Feed_list,
    notificationCount: state.FeedList.notificationCount,
    latest_comment: state.FeedList.latest_comment,
    authResult: state.FeedList.authResult,
    authResultSharePost: state.FeedList.authResultSharePost,
    authResultFeedAbusePost: state.FeedList.authResultAbusePost,
    isLoading: state.FeedList.isLoading,
    isdata: state.FeedList.isdata,
    SplashauthToken: state.Splash.authToken,
    isRefreshing: state.FeedList.isRefreshing
  };
};

export default connect(
  mapStateToProps,
  {
    getFeedlist,
    getSharepost,
    getAbusePost,
    onBottomTabChange,
    onPressLike
  }
)(FeedsPage);
