/* @flow */
import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  ScrollView,
  StatusBar,
  Modal,
  Alert,
  Platform,
  ActivityIndicator,

} from "react-native";
import GestureRecognizer, {
  swipeDirections
} from "react-native-swipe-gestures";
import { connect } from "react-redux";
import {
  getSearchUpdateList,
  initialgetSearchListStateData,
  onBottomTabChange,
  getAddfriend,
  saveUserChat,
  clerchatnotification
} from "../Actions";
import { getUser } from "../Database/allSchema";
import { POSTIMAGEPATH, PROFILEIMAGEPATH } from "../Actions/type";
import ItemSearchUpdate from "./ItemSearchUpdate";
import ImageLoad from "./ImageLoad";
import Loader from './Loader';
import DeviceInfo from 'react-native-device-info';

let userId = "";


class SearchUpdates extends Component {
  constructor (props) {
    super(props);
    this.state = {
      prifilePicDialogVisible: false,
      searchText: "",
      page: 1,
      userPhoto: ""
    };
  }

  componentWillMount() {
    this.setState({ searchText: '' });
    // this.props.initialgetSearchListStateData();
    getUser()
      .then(user => {
        if (user.length > 0) {
          this.setState({ page: 1 });
          userId = user[0].id;
          console.log(userId);
          this.fetchData();
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  componentWillReceiveProps(nextProp) {
  }

  onSearchResult(text) {
    this.setState({ searchText: text });
  }

  onSearchResultRedirect() {
    if (/^(\w+\s?)*\s*$/.test(this.state.searchText)) {
      if (this.state.searchText.replace(/(^\s+|\s+$)/g, '') !== '') {
        this.setState({ searchText: '' });
        console.log('search State', this.state.searchText);
        this.props.navigation.navigate('SearchResult', {
          searchText: this.state.searchText
        });
      }
    } else {
      setTimeout(() => {
        Alert.alert('Alert', 'Please enter valid character to search.');
      }, 500);
    }
  }

  fetchData = () => {
    console.log("Splash Login token", this.props.SplashauthToken);
    this.props.getSearchUpdateList(
      userId,
      this.state.page,
      [],
      this.props.SplashauthToken
    );
  };

  handleLoadMore = () => {
    if (this.props.isdata) {
      this.setState(
        {
          page: this.state.page + 1
        },
        () => {
          console.log(this.state.page);
          this.props.getSearchUpdateList(
            userId,
            this.state.page,
            this.props.search_update_list,
            this.props.SplashauthToken
          );
        }
      );
    }
  };

  onProfileData(item) {
    console.log(item);
    this.props.navigation.navigate("Profile", {
      user_id: item.user_id,
      fullname: item.fullname,
      profile_image: item.profile_image,
      average_rating: item.average_rating,
      user_give_rating_count: item.user_give_rating_count,
      form: 'searchUpdate'
    });
  }

  onChatMessage(item) {
    console.log(item.user_id);
    console.log(item.fullname);
    console.log(item.firebase, 'search');
    this.props.saveUserChat(item.user_id, item.fullname, item.firebase);
    this.props.clerchatnotification(userId, item.user_id, this.props.SplashauthToken);

    setTimeout(() => {
      this.props.navigation.navigate("ChatMessage", {
        toUserId: item.user_id,
        fullname: item.fullname,
        to_fire_id: item.firebase
      });
    }, 500);
  }


  render() {
    return (
      <View style={ styles.container }>
        <View style={ { backgroundColor: "#CC181E", height: 15 } } />
        <View style={ { backgroundColor: "#CC181E", paddingTop: (DeviceInfo.getModel() === 'iPhone X' || DeviceInfo.getModel() === 'iPhone XS Max' || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 15 : 0 } } />
        <View style={ { height: 55, backgroundColor: '#CC181E', flexDirection: 'row', alignItems: 'center' } }>
          <TouchableOpacity
            onPress={ () => this.props.navigation.navigate("Search") }
          >
            <Image
              source={ require('../img/back_icon.png') }
              style={ styles.backImageStyle }
            />
          </TouchableOpacity>
          <Text
            allowFontScaling={ false }
            style={ styles.searchTextStyleHeder }>Search Updates
        </Text>
        </View>


        <View style={ { backgroundColor: "white", margin: 10, flex: 1 } }>

          { !this.props.isLoading &&
            this.props.search_update_list != null &&
            this.props.search_update_list.length > 0 ? (
              <ScrollView>
                <FlatList
                  data={ this.props.search_update_list }
                  onEndReached={ this.handleLoadMore.bind(this) }
                  onEndReachedThreshold={ 1 }
                  renderItem={ ({ item, index }) => {
                    return (
                      <ItemSearchUpdate
                        usericon={ { uri: PROFILEIMAGEPATH + item.profile_image } }
                        name={ item.fullname }
                        des={ item.tag }
                        fireId={ item.firebase }
                        commentImage={ require("../img/message_icon.png") }
                        onPress={ () => this.onChatMessage(item) }
                        onPressProfile={ () => this.onProfileData(item) }
                        onPressUserIcon={ () =>
                          this.setState({
                            prifilePicDialogVisible: true,
                            userPhoto: PROFILEIMAGEPATH + item.profile_image
                          })
                        }
                      />
                    );
                  } }
                />
                {
                  (this.props.isdata) ? <ActivityIndicator size="small" /> : null
                }
              </ScrollView>
            ) : (
              <View
                style={ {
                  justifyContent: "center",
                  alignItems: "center",
                  height: '100%',
                  marginBottom: 5
                } }
              >
                {/* <Image
                  style={{ height: 130, width: 130 }}
                  source={require("../img/no-data-found.jpg")}
                /> */}
                <Text allowFontScaling={ false } style={ { fontSize: 13, fontFamily: "Lato-Regular", color: '#686868', } }>No search updates to show</Text>
              </View>
            ) }
        </View>

        <Loader
          loading={ this.props.isLoading }
        />

        <Modal
          transparent={ true }
          visible={ this.state.prifilePicDialogVisible }
          onRequestClose={ () =>
            this.setState({ prifilePicDialogVisible: false })
          }
        >
          <View
            style={ {
              height: "100%",
              width: "100%",
              paddingTop: 130,
              paddingBottom: 100
            } }
          >
            <View
              style={ {
                backgroundColor: "rgba(0,0,0,0.9)",
                padding: 5,
                justifyContent: "center",
                alignContent: "center"
              } }
            >
              <View style={ { alignItems: "center", justifyContent: "center" } }>
                <ImageLoad

                  style={ { height: "100%", width: "100%" } }
                  source={ { uri: this.state.userPhoto } }
                  resizeMode="contain"
                />
                <TouchableOpacity
                  style={ styles.CloseViewSTyle }
                  onPress={ () =>
                    this.setState({ prifilePicDialogVisible: false })
                  }
                >
                  <Image
                    style={ { height: 18, width: 18 } }
                    source={ require("../img/close_icon.png") }
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#DDDEE3"
  },
  viewStyle: {
    flexDirection: "row",
    margin: 10,
    padding: 8,
    backgroundColor: "#b7151b",
    alignItems: "center",
    borderRadius: 5,
    justifyContent: "space-between",
    height: 38
  },
  TextInputStyle: {
    color: "#DDDEE4",
    flex: 1,
    fontFamily: "Lato-Regular",
    justifyContent: "center",
    paddingLeft: 10,
    paddingTop: 2,
    paddingBottom: 2,
    fontSize: 15
  },
  searchImageStyle: {
    height: 13,
    width: 13,
    tintColor: "white",
    alignSelf: "center"
  },
  searchTextStyle: {
    fontSize: 16,
    color: "#4E4E4E",
    fontFamily: "Lato-Medium"
  },
  CloseViewSTyle: {
    position: "absolute",
    right: 2,
    top: 2,
    backgroundColor: "rgba(255,0,0,0.5)",
    width: 32,
    height: 32,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  backImageStyle: {
    height: 17,
    width: 17,
    tintColor: 'white',
    alignSelf: 'center',
    marginLeft: 8
  },
  searchTextStyleHeder: {
    fontSize: 16,
    color: 'white',
    marginLeft: 10,
    fontFamily: 'Lato-Medium'
  }
});

const mapStateToProps = state => {
  return {
    search_update_list: state.SearchList.search_update_list,
    authResult: state.SearchList.authResult,
    isLoading: state.SearchList.isLoading,
    isdata: state.SearchList.isdata,
    SplashauthToken: state.Splash.authToken,
    authResultAddFriend: state.Testimonial.authResultAddFriend,
  };
};

export default connect(
  mapStateToProps,
  {
    getSearchUpdateList,
    initialgetSearchListStateData,
    getAddfriend,
    saveUserChat,
    clerchatnotification
  }
)(SearchUpdates);
