/* @flow */

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList
} from "react-native";
import ItemViewComment from "./ItemViewComment";
import ImageLoad from './ImageLoad';

export default class ItemUserComment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [{ name: "Peter Jackson", descritiiiion: "Awsome Photography" }]
    };
  }

  render() {
    return (
      <View style={styles.container}>
      <View style={{ backgroundColor: "#e5eaea" }}>
        <View style={{ flexDirection: "row", marginLeft: 10, marginTop: 10 }}>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={this.props.onPressUserIcon}
          >
            <ImageLoad
              style={{ height: 30, width: 30, borderRadius: 200 / 2 }}
              borderRadius={30 / 2}
              source={this.props.commentUserprofile}
            />
          </TouchableOpacity>
          <View style={{ marginLeft: 15 }}>
            <Text allowFontScaling={false} style={styles.nameTextStyle}>{this.props.commentusername}</Text>
            <Text allowFontScaling={false} style={styles.commentTextStyle}>{this.props.comment}</Text>
          </View>
        </View>
      </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    borderTopWidth: 1,
    borderColor: "#e2e0e0",
    marginTop: 10
  },
  MarcusTextStyle: {
    fontSize: 16,
    fontFamily: "Lato-Bold",
    color: "#2c2c2c"
  },
  yesterdayTextStyle: {
    fontSize: 14,
    color: "#989898",
    fontFamily: "Lato-Medium"
  },
  TextStyle: {
    fontSize: 14,
    paddingLeft: 10,
    paddingRight: 10,
    fontFamily: "Lato-Semibold",
    color: "#2c2c2c"
  },
  CommentTextStyle: {
    color: "#A4A4A4",
    fontSize: 14,
    fontFamily: "Lato-Bold",
    marginLeft: 5
  },
  commentIconStyle: {
    width: 15,
    height: 15
  },
  nameTextStyle: {
    fontSize: 14,
    fontFamily: "Lato-Semibold",
    color: "#2c2c2c"
  },
  commentTextStyle: {
    fontSize: 14,
    color: "#000000",
    fontFamily: "Lato-Regular"
  }
});
