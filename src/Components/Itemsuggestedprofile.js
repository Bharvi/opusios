/* @flow */

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList
} from "react-native";
import ImageLoad from './ImageLoad';
import UserProfileImg from '../Components/common/UserProfileImg';

let Total = 38;

export default class Itemsuggestedprofile extends Component {



  // renderProgess() {
  //   const progressArray = [];
  //   for (let i = 0; i < (this.props.average_rating*1.2); i++) {
  //     progressArray.push(i);
  //   }
  //   return progressArray.map(number => (
  //     <View
  //       style={{
  //         backgroundColor: "#1bc713",
  //         width: 5.1,
  //         height: 8,
  //         borderRadius: 2,
  //         marginRight: 1
  //       }}
  //     />
  //   ));
  // }


  renderProgess() {
    let backgroundColor = '#1bc713';
    if ((this.props.average_rating * 3.3) < (4 * 3.3)) {
      backgroundColor = 'red';
    } else if ((this.props.average_rating * 3.3) >= (4 * 3.3) && (this.props.average_rating * 3.3) < (6 * 3.3)) {
      backgroundColor = 'yellow';
    }
    let progressArray = [];
    for (let i = 0; i < (this.props.average_rating * 3.3); i++) {
      progressArray.push(i);
    }
    return progressArray.map(number => (
      <View
        style={ {
          backgroundColor: backgroundColor,
          width: 5.1,
          height: 8,
          borderRadius: 2,
          marginRight: 1
        } }
      />
    ));
  }



  render() {
    return (
      <View style={ styles.container }>
        <View style={ { flexDirection: 'row', borderBottomWidth: 1, borderColor: '#e2e0e0' } }>
          <TouchableOpacity
            style={ { flex: 2.5, padding: 10 } }
            activeOpacity={ 0.8 }
            onPress={ this.props.onPressUserIcon }>
            {/* <ImageLoad
                      style={{ height: 70, width: 70, borderRadius: 250 / 2 }}
                      borderRadius={70 / 2}
                      source={this.props.userProfile}
                /> */}
            <UserProfileImg
              style={ { height: 70, width: 70, borderRadius: 70 / 2 } }
              borderRadius={ 70 / 2 }
              source={ this.props.userProfile }
              dotSize={ 14 }
              fireId={ this.props.fireId }
              top={ '70%' }
              right={ '18%' }
            />
          </TouchableOpacity>
          <View style={ { flex: 7.5, padding: 8, justifyContent: 'center', alignItems: 'center' } }>
            <TouchableOpacity
              onPress={ this.props.onPressProfile }
              activeOpacity={ 0.8 }>
              <Text allowFontScaling={ false } style={ styles.largeTextstyle }>{ (this.props.Morgan != '') && (this.props.Morgan != null) ? this.props.Morgan : '---' }</Text>
              <Text allowFontScaling={ false } style={ styles.marketTextStyle }>{ (this.props.Marketing != '') && (this.props.Marketing != null) ? this.props.Marketing : '---' }</Text>
              <Text allowFontScaling={ false } style={ styles.mumbaiTextStyle }>{ (this.props.Mumbai != '') && (this.props.Mumbai != null) ? this.props.Mumbai : '--- ' }</Text>
            </TouchableOpacity>
            <View style={ { flexDirection: 'row', padding: 10 } }>
              {
                (this.props.tagArray.length > 0 && this.props.tagArray[0] !== '') ?
                  <View style={ styles.sportsViewstyle }>
                    <Text allowFontScaling={ false } numberOfLines={ 1 } style={ styles.sporttextStyle }>{ this.props.tagArray[0] }</Text>
                  </View> : null
              }
              {
                (this.props.tagArray.length > 1 && this.props.tagArray[1] !== '') ?
                  <View style={ styles.marketingViewstyle }>
                    <Text allowFontScaling={ false } style={ styles.sporttextStyle }>{ this.props.tagArray[1] }</Text>
                  </View> : null
              }

              {
                (this.props.tagArray.length > 2 && this.props.tagArray[2] !== '') ?
                  <TouchableOpacity style={ styles.songsViewstyle }
                    activeOpacity={ 0.7 }
                    onPress={ this.props.onClickMore }>
                    <Text allowFontScaling={ false } style={ styles.sporttextStyle }>More...</Text>
                  </TouchableOpacity> :
                  null
              }
            </View>
          </View>
        </View>

        <View style={ { flexDirection: 'row', padding: 8, justifyContent: 'center', alignItems: 'center' } }>
          <View style={ { flex: 7, marginTop: 5, marginBottom: 1 } }>
            <View style={ { backgroundColor: '#dbdbdb', width: 200, height: 8, borderRadius: 3 } } />
            <View style={ { width: this.props.average_rating, position: 'absolute', height: 8, flexDirection: 'row' } }>
              { this.renderProgess() }
            </View>
            <Text allowFontScaling={ false } style={ styles.userTextStyle }>{ (this.props.Users !== 0) ? (this.props.Users + ' Users') : 'No ratings' }</Text>
          </View>
          <View style={ { marginLeft: 20, backgroundColor: '#e2e0e0', width: 1, height: 35 } } />
          <TouchableOpacity
            style={ { flex: 1.5, justifyContent: 'center', alignItems: 'center' } }
            onPress={ this.props.messagePress }
          >
            <Image
              style={ { height: 35, width: 35, marginLeft: 10 } }
              source={ this.props.commentImage }
            />
          </TouchableOpacity>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    borderColor: '#dddee4',
    borderWidth: 1,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 8,
    borderRadius: 3
  },
  sportsViewstyle:
  {
    width: 65,
    height: 25,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e2e0e0',
    borderRadius: 3,
    borderColor: '#eaeaea',
    padding: 2,
  },
  marketingViewstyle:
  {
    width: 85,
    height: 25,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e2e0e0',
    borderRadius: 3,
    marginLeft: 5,
    borderColor: '#eaeaea',
    padding: 2,
  },
  songsViewstyle:
  {
    width: 55,
    height: 25,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e2e0e0',
    borderRadius: 3,
    marginLeft: 5,
    borderColor: '#eaeaea'
  },
  largeTextstyle: {
    color: '#414141', fontSize: 20, fontFamily: 'Lato-Bold', alignItems: 'center', justifyContent: 'center', textAlign: 'center'
  },
  marketTextStyle: { color: '#848484', fontFamily: 'Lato-Bold', fontSize: 14, alignItems: 'center', textAlign: 'center' },
  mumbaiTextStyle: { color: '#848484', fontFamily: 'Lato-Regular', fontSize: 14, textAlign: 'center' },
  sporttextStyle: { color: '#2c2c2c', fontFamily: 'Lato-Regular', fontSize: 12, flexWrap: 'wrap', },
  commentViewStyle: {
    height: 40,
    width: 40,
    borderWidth: 1,
    borderRadius: 20,
    justifyContent: 'center',
    borderColor: '#28969d',
    alignItems: 'center'
  },
  userTextStyle: { color: '#848484', fontFamily: 'Lato-Medium', fontSize: 14, marginLeft: 3 }
});
