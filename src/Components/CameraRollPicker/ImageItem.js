import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  View,
  NativeModules
} from 'react-native';
import PropTypes from 'prop-types';
//import Video from 'react-native-video';

// let videoThumbBase64String = '';

class ImageItem extends Component {
  constructor(props) {
    super(props)
    this.state = { videoThumbBase64String: '' }
  }

  componentWillMount() {
    var { width } = Dimensions.get('window');
    var { imageMargin, imagesPerRow, containerWidth } = this.props;


    if (typeof containerWidth != "undefined") {
      width = containerWidth;
    }
    this._imageSize = (width - (imagesPerRow + 1) * imageMargin) / imagesPerRow;
    if(!this.props.item.node.type.includes('image')){
      console.log(this.props.item.node.image.uri);
      NativeModules.GetVideoThumbModule.getVideoThumb(this.props.item.node.image.uri,(msg) => {
        this.setState({ videoThumbBase64String : `data:image/png;base64,${msg}` });
        // videoThumbBase64String = `data:image/png;base64,${msg}`;
        // console.log(msg);
      });
  }
  }

  renderVideoThumb() {
    return (
      <View>
      <Image
        source={{ uri: this.state.videoThumbBase64String }}
        style={{ height: this._imageSize, width: this._imageSize }}
      />
        <View style={styles.videoIcon}>
          <Image
            style={{ width: 25, height: 25 }}
            source={require('./video_icon.png')}
          />
        </View>
      </View>
    );
  }

  render() {
    var { item, selected, selectedMarker, imageMargin } = this.props;

    var marker = selectedMarker ? selectedMarker :
      (
        <View style={styles.marker}>
          <Image
            style={{ width: 25, height: 25 }}
            source={require('./check_icon.png')}
          />
        </View>
    );

    var image = item.node.image;
    console.log(item.node.type.includes('image'));
    return (
      <TouchableOpacity
        style={{ marginBottom: imageMargin, marginRight: imageMargin }}
        onPress={() => this._handleClick(image)}>
        {(item.node.type.includes('image')) ?
          <Image
            source={{ uri: image.uri }}
            style={{ height: this._imageSize, width: this._imageSize }} />
          :
          this.renderVideoThumb()

          // <Video
          //    source={{ uri: image.uri }}
          //    style={{ height: this._imageSize, width: this._imageSize }}
          //    paused={true}
          //    resizeMode={'cover'}
          // />
        }
        {(selected) ? marker : null}
      </TouchableOpacity>
    );
  }

  _handleClick(item) {
    this.props.onClick(item);
  }
}

const styles = StyleSheet.create({
  marker: {
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  videoIcon: {
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: 'rgba(52, 52, 52, 0.4)',
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

ImageItem.defaultProps = {
  item: {},
  selected: false,
};
ImageItem.propTypes = {
  item: PropTypes.object,
  selected: PropTypes.bool,
  selectedMarker: PropTypes.element,
  imageMargin: PropTypes.number,
  imagesPerRow: PropTypes.number,
  onClick: PropTypes.func,
};

export default ImageItem;
