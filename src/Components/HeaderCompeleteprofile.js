import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform
} from 'react-native';

export default class HeaderCompeleteprofile extends Component {
  render() {
    return (
      <View>
        <View style={{ backgroundColor: "#CC181E", height: 10 }}></View>
        <View style={{ backgroundColor: "#CC181E", paddingTop: (Platform.OS === 'ios') ? 10 : 0 }}/>
      
      <View style={styles.container}>

                <TouchableOpacity
                 onPress={this.props.onLeftPressed}
                 activeOpacity={0.2}
                >
                    <Image
                    style={styles.backArrowStyle}
                    source={this.props.leftImage}
                    />
                </TouchableOpacity>

            <View style={{ flex: 11, justifyContent: 'center', alignItems: 'center', }}>
                <Text allowFontScaling={false}  style={styles.textStyle}> {this.props.headertext} </Text>
            </View>
                <TouchableOpacity
                 onPress={this.props.onPressDone}
                style={{ flexDirection: 'row', marginRight: 8 }}
                >
                  <Text allowFontScaling={false}  style={styles.textStyleDone}>{this.props.textDone} </Text>
                    {/* <Image
                    style={styles.backArrowStyle}
                    source={this.props.rightImage}
                    /> */}
                    
              </TouchableOpacity>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    backgroundColor: '#CC181E'
  },
  textStyle: {
    fontSize: 16,
    color: '#ffffff',
    fontFamily: 'Lato-Regular',
    marginLeft: 20
  },
  textStyleDone: {
    fontSize: 16,
    color: '#CCffffff',
    textAlign: 'center',
    fontFamily: 'Lato-Regular',

  },
  backArrowStyle: {
    height: 18,
    width: 18,
    marginLeft: 10,
    padding: 8
  }
});
