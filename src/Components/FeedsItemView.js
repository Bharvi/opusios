/* @flow */

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
  Dimensions,
  ScrollView
} from "react-native";
import ItemViewComment from "./ItemViewComment";
import ItemUserComment from "./ItemUserComment";
import { POSTIMAGEPATH } from "../Actions/type";
import ImageLoad from './ImageLoad';
import { connect } from "react-redux";
import {
  likeUnlikeApi,
  getFeedlist
} from "../Actions";
import { getUser } from "../Database/allSchema";

let dimensions = Dimensions.get("window");
let imageHeight = Math.round((dimensions.width * 9) / 16);
let imageWidth = dimensions.width;

class FeedsItemView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [{ name: "Peter Jackson", descritiiiion: "Awsome Photography" }],
      imgWidth: 0,
      imgHeight: 0,
      isLike: false,
      numOflike: 0.
    };
  }

  componentDidMount() {
    Image.getSize(this.props.Image, (width, height) => {
      // calculate image width and height
      let screenWidth = Dimensions.get("window").width;
      let scaleFactor = width / screenWidth;
      let imageHeight = height / scaleFactor;
      this.setState({ 
        imgWidth: screenWidth,
        imgHeight: imageHeight,
        // isLike: (this.props.item.is_liked === 'Y') ? true : false,
        // numOflike: Number(this.props.item.like_count)
      });
    });
  }

  onPressLike() {
    
    this.props.onPressLike();
    // this.setState({ numOflike: (this.state.isLike) ? this.state.numOflike - 1 : this.state.numOflike + 1, 
    //   isLike: !this.state.isLike },
    //   () => {
    //     console.log('onPressLike',this.state.isLike);
        getUser()
          .then(user => {
            if (user.length > 0) {
              this.props.likeUnlikeApi(
                user[0].id, 
                this.props.item.post_id,
                this.props.SplashauthToken,
                (this.props.item.is_liked === 'Y') ? 'like' : 'unlike'
              );
            }
          })
          .catch(error => {
            console.log(error);
          });
      // }
    // );
  }

  render() {
    console.log('render',this.props.item);
    return (
      <View style={styles.containerItem}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            padding: 10,
          }}
        >
          <TouchableOpacity
            style={{ flex: 1.3 }}
            activeOpacity={0.8}
            onPress={this.props.onPressUserIcon}
          >
            <ImageLoad
              style={{ height: 40, width: 40, borderRadius: 200 / 2 }}
              borderRadius={40 / 2}
              source={this.props.userprofileImage}
            />
          </TouchableOpacity>
          <View style={{ flex: 7 }}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={this.props.onPressProfile}
            >  
            <View style={{ flexDirection: 'row'}}>
                {
               (this.props.item.share_post_user_fullname )?

               <View style={{ flexWrap: 'wrap', marginRight: 10, textAlign: 'left', flexDirection: 'row'}}>
               <Text allowFontScaling={false}  style={styles.MarcusTextStyle}>{this.props.name}</Text>
                <Text  allowFontScaling={false}  style={styles.MarcusTextStyle3}> shared </Text>
               <Text numberOfLines={1}  allowFontScaling={false}  style={styles.MarcusTextStyle2}>{this.props.item.share_post_user_fullname + "'s "}</Text>
               <Text  allowFontScaling={false}  style={styles.MarcusTextStyle3}>post</Text>
                </View>
               : <Text allowFontScaling={false}  style={styles.MarcusTextStyle}>{this.props.name}</Text>
             }
              
              </View>
              


              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text allowFontScaling={false}  style={styles.yesterdayTextStyle}>{this.props.des}.</Text>
                <Image
                  source={require("../img/earth_icon.png")}
                  style={{ width: 10, height: 10, marginLeft: 5,marginTop: 3}}
                />
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 0.7 }}>
            
          </View>
        </View>
        <Text allowFontScaling={false}  style={styles.TextStyle}>{this.props.postTitle}</Text>
        <ImageLoad
          source={{uri: this.props.Image}}
          style={{
            width: this.state.imgWidth, height: this.state.imgHeight,
            marginTop: 5,
            alignSelf: "center",

          }}
          resizeMode="contain"
        />
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            paddingLeft: 10,
            paddingRight: 10,
            paddingTop: 15,
            paddingBottom: 15

          }}
        >
          <TouchableOpacity
            activeOpacity={0.8}
            style={{ position: 'absolute', left: 15 , top: 8, bottom: 8}}
            onPress={() => this.onPressLike()}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Image
                source={require("../img/like.png")}
                style={[styles.commentIconStyle1, { tintColor: (this.props.item.is_liked === 'Y') ? '#CC181E' : '#848484'}]}
              />
              <Text allowFontScaling={false}  style={styles.CommentTextStyle}>
                {this.props.item.like_count}
              </Text>
              {/* <Text allowFontScaling={false}  style={styles.CommentTextStyle}>Comment</Text> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.8}
            style={{ position: 'absolute', left: 75, top: 8, bottom: 8}}
            onPress={this.props.onPressComment}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Image
                source={require("../img/fill_comment.png")}
                style={styles.commentIconStyle1}
              />
              <Text allowFontScaling={false}  style={styles.CommentTextStyle}>
                {this.props.numOfComments}
              </Text>
              {/* <Text allowFontScaling={false}  style={styles.CommentTextStyle}>Comment</Text> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.8}
            style={{ position: 'absolute', left: 140, top: 8, bottom: 8}}
            onPress={this.props.sharepost}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Image
                source={require("../img/share_icon.png")}
                style={styles.commentIconStyle}
              />
              {/* <Text allowFontScaling={false}  style={styles.CommentTextStyle}>Share</Text> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.8}
            style={{ position: 'absolute', right: 10, top: 8 }}
            onPress={this.props.Abusepost}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Image
                source={require("../img/flag_icon.png")}
                style={styles.commentIconStyle}
              />
              {/* <Text allowFontScaling={false}  style={styles.CommentTextStyle}>Report Abuse</Text> */}
            </View>
          </TouchableOpacity>
        </View>

        <FlatList
          data={this.props.CommentArray}
          renderItem={({ item, index }) => {
            console.log(item.user_fullname);
            return (
              <ItemUserComment
                comment={item.comment}
                commentusername={item.user_fullname}
                commentUserprofile={{ uri: POSTIMAGEPATH + item.user_photo }}
                onPressUserIcon={this.props.onPressUserIcon}
              />
            );
          }}
        />      
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerItem: {
    flex: 1,
    backgroundColor: 'white',
    borderTopWidth: 1,
    borderColor: '#e2e0e0',
    marginTop: 10
  },
  MarcusTextStyle: {
    fontSize: 15,
    fontFamily: 'Lato-Bold',
    color: '#2c2c2c',
    textAlign: 'left'
  },
  MarcusTextStyle2: {
    fontSize: 15,
    fontFamily: 'Lato-Bold',
    color: '#2c2c2c',
    textAlign: 'left'
  },
  MarcusTextStyle3: {
    fontSize: 15,
    fontFamily: 'Lato-Medium',
    color: '#848484',
     textAlign: 'left',

  },
  yesterdayTextStyle: {
    fontSize: 13,
    color: '#989898',
    fontFamily: 'Lato-Medium'
 },
  TextStyle: {
     fontSize: 14,
      paddingLeft: 10,
       paddingRight: 10,
       fontFamily: 'Lato-Semibold',
       color: '#2c2c2c'
  },
  CommentTextStyle: {
    color: '#A4A4A4',
    fontSize: 13,
    fontFamily: 'Lato-Bold',
    
   alignItems: 'center',
     marginLeft: 5
   },
  commentIconStyle: {
    width: 15,
    height: 15
  },
  commentIconStyle1: {
    width: 15,
    height: 15,
    tintColor: '#848484'
  },
  nameTextStyle: {
   fontSize: 14,
   fontFamily: 'Lato-Semibold',
   color: '#2c2c2c'
 },
  commentTextStyle: {
    fontSize: 14,
   color: '#000000',
    fontFamily: 'Lato-Regular'
  },
});

const mapStateToProps = state => {
  return {
    SplashauthToken: state.Splash.authToken
  };
};

export default connect(
  mapStateToProps,
  {
    likeUnlikeApi,
    getFeedlist
  }
)(FeedsItemView);