/* @flow */

import React, { Component } from "react";
import { View, Text, StyleSheet, Image, FlatList } from "react-native";
import ItemTestimonials from "./ItemTestimonials";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import { connect } from "react-redux";
import { getProfileTestimonial } from "../Actions";
import { getUser } from "../Database/allSchema";
import { POSTIMAGEPATH } from "../Actions/type";
import Loader from './Loader';

let userId = "";

class Testimonialsprofile extends Component {
  constructor (props) {
    super(props);
    this.state = {
      pageCount: 1,
      list: []
    };
    // this.onEndReached = this.onEndReached.bind(this);
  }

  componentDidMount() {
    this.fetchData();
    console.log(this.props.userId);
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.setState({ pageCount: 1 });
        this.fetchData();
      }
    );
  }
  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  fetchData = () => {
    this.props.getProfileTestimonial({
      userId: this.props.userId,
      page: this.state.pageCount,
      token: this.props.SplashauthToken,
      isLoading: true,
      isMyProfile: false
    });
  };

  handleLoadMore = () => {
    this.setState(
      {
        pageCount: this.state.pageCount + 1
      },
      () => {
        this.props.getProfileTestimonial({
          userId: this.props.userId,
          page: this.state.pageCount,
          Testimonials: this.props.ProfileTestimonial_list,
          token: this.props.SplashauthToken,
          isMyProfile: false,
          isLoading: false,
        });
      }
    );
  };


  onOtherProfile(item) {
    console.log("profie click");
    getUser()
      .then(user => {
        if (user.length > 0) {
          let userId = user[0].id;
          console.log("sds", userId, item.user_id);
          if (item.user_id == userId) {
            console.log("Myprofile");
            console.log(this.props.navigation);

            this.props.navigation.navigate('MyProfile');
          }
          else {
            this.props.navigation.goBack();
            this.props.navigation.navigate('Profile', {
              user_id: item.user_id,
              form: 'MyProfile1'
            });
          }
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <View style={ { flex: 1, backgroundColor: "white" } }>
        { !this.props.isLoading &&
          this.props.ProfileTestimonial_list != null &&
          this.props.ProfileTestimonial_list.length > 0 ? (
            <FlatList
              key={ this.props.ProfileTestimonial_list.length - 1 }
              keyExtractor={ item => item.testimonial_id }
              maxHeight={ null }
              style={ { backgroundColor: "white" } }
              data={ this.props.ProfileTestimonial_list }
              showsHorizontalScrollIndicator={ false }
              onEndReached={ this.handleLoadMore.bind(this) }
              onEndReachedThreshold={ 2 }
              renderItem={ ({ item, index }) => {
                return (
                  <ItemTestimonials
                    fireId={ item.firebase }
                    Name={ item.fullname }
                    userprofileImage={ { uri: POSTIMAGEPATH + item.user_photo } }
                    Date={ item.date }
                    Des={ item.description }
                    openMyProfile={ () => this.onOtherProfile(item) }
                    modaluserphoto={ { uri: POSTIMAGEPATH + item.user_photo } }
                  />
                );
              } }
            />
          ) : (
            <View
              style={ {
                justifyContent: "center",
                alignItems: "center",
                flex: 1
              } }
            >
              {/* <Image
              style={{ height: 130, width: 130 }}
              source={require("../img/no-data-found.jpg")}
            /> */}
              <Text style={ { fontSize: 13, fontFamily: "Lato-Regular", color: '#686868', } }>No testimonials to show</Text>

            </View>
          ) }
        <Loader loading={ this.props.isLoading } />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    ProfileTestimonial_list: state.ProfileTestimonial.testimonialsProfile,
    authResult: state.ProfileTestimonial.authResult,
    isLoading: state.ProfileTestimonial.isLoading,
    SplashauthToken: state.Splash.authToken
  };
};

export default connect(
  mapStateToProps,
  {
    getProfileTestimonial
  }
)(Testimonialsprofile);
