/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Button,
  FlatList,
  CameraRoll
} from 'react-native';
import ItemViewGallery from './ItemViewGallery';

export default class All extends Component {
  state = {
    photos: []
  };

  componentWillMount() {
    CameraRoll.getPhotos({
        first: 300,
        assetType: 'All',
        // mimeTypes: ['video/mp4']
      })
      .then(data => {
        this.setState({ photos: data.edges });
        console.log(data.edges);
      })
      .catch((err) => {
         //Error Loading Images
         console.log(err);
      });
  }

  render() {
    return (
      <View style={styles.container}>
      <FlatList
        data={this.state.photos}
        numColumns={3}
        showsVerticalScrollIndicator={false}
        renderItem={(p) => {
          // if(p.item.node.type.includes('video')){
          console.log(p.item.node);
        // }
        return (
          <ItemViewGallery
            item={p.item}
          />
          );
      }}
      />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
