/* @flow */

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
  Modal,
  Share,
  Alert,
  Platform,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import ItemPost from "./ItemPost";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import { connect } from "react-redux";
import { getMYPosts, getPostsSharepost1, getPostsAbusePost } from "../Actions";
import { getUser } from "../Database/allSchema";
import { POSTIMAGEPATH } from "../Actions/type";
import ImageLoad from "./ImageLoad";
import Loader from './Loader';

let userId = "";
let postID = "";
let shareText = "Test 2";

class Posts extends Component {
  constructor (props) {
    super(props);
    this.state = {
      refreshing: false,
      prifilePicDialogVisible: false,
      userPhoto: "",
      post_photo: "",
      post_caption: "",
      page: 1,
      data: [],
      loading: false,
      error: null,
      posts: [],
      fetching_from_server: false,
      PostData: [
        {
          name: "Marcus Richards",
          des: "Yesterday, Halifax, N.S",
          title: "Good friends, good food and a lot of laughs."
        },
        {
          name: "Marcus Richards",
          des: "Yesterday, Halifax, N.S",
          title: "Good friends, good food and a lot of laughs."
        },
        {
          name: "Marcus Richards",
          des: "Yesterday, Halifax, N.S",
          title: "Good friends, good food and a lot of laughs."
        }
      ],
      pageCount: 1
    };
    // this.shareMessage = this.shareMessage.bind(this);
  }
  // shareMessage() {
  //   console.log(this.state.post_caption);
  //   const image = POSTIMAGEPATH + this.state.post_photo;
  //   console.log(image);
  //   Share.share({
  //     message: this.state.post_caption
  //     // url: image ,
  //   });
  // }

  _onRefresh = () => {
    // this.setState({refreshing: true});
    getUser()
      .then(user => {

        if (user.length > 0) {
          this.setState({ pageCount: 1 });
          userId = user[0].id;
          this.setState({ pageCount: 1 });
          this.dataLoad(true, false)
          console.warn('in then..')
        }
        // this.setState({refreshing: false});
      })
      .catch(error => {
        console.warn("error", error);
        // this.setState({refreshing: false});
      });
  }

  componentWillReceiveProps(nextProp) {
    console.warn("authResult", nextProp.authResult);
    if (nextProp.authResult === 'posts success') {


      this.setState({ posts: nextProp.posts })
    }
    console.warn('nextProps', nextProp.isLoading);
    nextProp.isLoding = true
    if (nextProp.authResultSharePost === "Success") {
      setTimeout(() => {
        // this.shareMessage(); 
      }, 500);

    } else if (nextProp.authResultSharePost === "User id must be numeric.") {
      Alert.alert("Oops", "User id must be numeric.");
    } else if (nextProp.authResultSharePost === "Post id must be numeric.") {
      Alert.alert("Oops", "Post id must be numeric.");
    }
    if (nextProp.authResultAbusePost === "Success Abuse") {

      setTimeout(() => {
        Alert.alert(
          "Success",
          "Report abuse successfully.",
          [
            {
              text: "OK",
              onPress: () => {
                this.setState({ pageCount: 1 });
                this.dataLoad(false, true);
              }
            }
          ],
          { cancelable: false }
        );

      }, 500);

    } else if (
      nextProp.authResultAbusePost === "Already Post is Abuse by same User"
    ) {
      setTimeout(() => {
        Alert.alert("Oops", "Already post is abuse by same user");

      }, 500);

    } else if (nextProp.authResultAbusePost === "User id must be numeric.") {
      setTimeout(() => {
        Alert.alert("Oops", "User id must be numeric.");

      }, 500);

    } else if (nextProp.authResultAbusePost === "Post id must be numeric.") {
      setTimeout(() => {
        Alert.alert("Oops", "Post id must be numeric.");
      }, 500);

    }
  }
  componentWillMount() {
    if (!this.state.posts || this.state.posts.length == 0) {
      getUser()
        .then(user => {
          if (user.length > 0) {
            this.setState({ pageCount: 1 });
            userId = user[0].id;
            this.setState({ pageCount: 1 });
            this.dataLoad(false, true);
          }
        })
        .catch(error => {
          console.log(error);
        });
    }



    this.willFocusSubscription = this.props.myNavigation.addListener(
      "willFocus",
      () => {

      }
    );
  }
  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  dataLoad(isLoading, isLoadingInit) {
    console.log(userId, this.state.pageCount);
    this.props.getMYPosts({
      userId,
      page: this.state.pageCount,
      token: this.props.SplashauthToken,
      isLoading,
      isLoadingInit
    });
  }

  onCommentRedirt(item) {
    this.props.myNavigation.navigate("CommentPage", { postID: item.post_id });
  }
  onProfileData(item) {
    console.log(item.user_photo);
    this.props.myNavigation.navigate("Profile", {
      user_id: item.user_id,
      fullname: item.fullname,
      profile_image: POSTIMAGEPATH + item.user_photo,
      form: 'MyProfile1'
    });
  }

  onSharePost(item) {
    // console.log("on share post ", postID);
    // this.setState({
    //   post_photo: POSTIMAGEPATH + item.post_photo,
    //   post_caption: item.post_caption
    // });
    // console.log(this.state.post_photo);
    // console.log(this.state.post_caption);

    setTimeout(() => {
      Alert.alert(
        "Opus",
        "Do you want to share the post?",
        [
          {
            text: "Yes",
            onPress: () => {
              this.props.getPostsSharepost1({
                userId,
                postID: item.post_id,
                shareText,
                token: this.props.SplashauthToken
              });
              this.props.myNavigation.navigate('FeedsPage');
            }
          },
          {
            text: "No",
            onPress: () => {
              cancelable: false;
            }
          }
        ],
      );

    }, 500);
  }
  onAbusePost(item) {
    console.log(userId);
    console.log(item.post_id);
    this.props.getPostsAbusePost({
      userId,
      postID: item.post_id,
      token: this.props.SplashauthToken
    });
  }

  handleLoadMore = () => {
    this.setState(
      {
        pageCount: this.state.pageCount + 1
      },
      () => {
        console.log(this.state.pageCount);
        if (this.props.isData) {
          this.props.getMYPosts({
            userId,
            page: this.state.pageCount,
            posts: this.state.posts,
            token: this.props.SplashauthToken,
            isLoading: false,
            isLoadingInit: false
          });
        }
      }
    );
  };

  onPressLikeBtn(item, index) {
    console.warn("index", index);
    let tempFeedList = this.state.posts;
    tempFeedList[index] = {
      ...tempFeedList[index],
      is_liked: (tempFeedList[index].is_liked == "Y") ? "N" : "Y",
      like_count: (tempFeedList[index].is_liked == "Y") ? Number(tempFeedList[index].like_count) - 1 : Number(tempFeedList[index].like_count) + 1,
    };
    this.setState({ posts: tempFeedList });
  }

  render() {
    return (
      <View style={ { flex: 1, backgroundColor: "white", marginBottom: 10 } }>
        {
          this.state.posts != null &&
            this.state.posts.length > 0 ? (
              <FlatList
                refreshControl={
                  <RefreshControl
                    tintColor={ ['#CC181E'] }
                    refreshing={ (this.props.isLoading) }
                    onRefresh={ this._onRefresh.bind(this) }
                  />
                }
                style={ { marginBottom: 10 } }
                data={ this.state.posts }
                extraData={ this.state.posts }
                keyExtractor={ (item, index) => index }

                onEndReached={ this.handleLoadMore.bind(this) }
                onEndReachedThreshold={ 1 }
                renderItem={ ({ item, index }) => {
                  return (
                    <ItemPost
                      item={ item }
                      name={ item.fullname }
                      userprofileImage={ { uri: POSTIMAGEPATH + item.user_photo } }
                      des={ item.time }
                      postTitle={ item.post_caption }
                      Image={ POSTIMAGEPATH + item.post_photo }
                      numOfComments={ item.comment_count }
                      onPressComment={ () => this.onCommentRedirt(item) }
                      sharepost={ () => this.onSharePost(item) }
                      Abusepost={ () => this.onAbusePost(item) }
                      onPressUserIcon={ () =>
                        this.setState({
                          prifilePicDialogVisible: true,
                          userPhoto: POSTIMAGEPATH + item.user_photo
                        })
                      }
                      openMyProfile={ () => this.onProfileData(item) }
                      onPressLike={ () => this.onPressLikeBtn(item, index) }
                    />
                  );
                } }
              />
            ) : (
              null
            ) }

        { (this.state.posts == null && !this.state.posts.length > 0) ?
          <View
            style={ {
              justifyContent: "center",
              alignItems: "center",
              flex: 1,
              height: 500
            } }
          >
            {/* <Image
              style={{ height: 130, width: 130 }}
              source={require("../img/no-data-found.jpg")}
            /> */}
            <Text allowFontScaling={ false } style={ { fontSize: 13, fontFamily: "Lato-Regular", color: '#686868', } }>No posts</Text>
          </View>
          : null
        }

        <Modal
          transparent={ true }
          visible={ this.state.prifilePicDialogVisible }
          onRequestClose={ () =>
            this.setState({ prifilePicDialogVisible: false })
          }
        >
          <View
            style={ {
              height: "100%",
              width: "100%",
              paddingTop: 130,
              paddingBottom: 100
            } }
          >
            <View
              style={ {
                backgroundColor: "rgba(0,0,0,0.9)",
                padding: 5,
                justifyContent: "center",
                alignContent: "center"
              } }
            >
              <View style={ { alignItems: "center", justifyContent: "center" } }>
                <ImageLoad
                  style={ { height: "100%", width: "100%" } }
                  source={ { uri: this.state.userPhoto } }
                  resizeMode="contain"
                />

                <TouchableOpacity
                  style={ styles.CloseViewSTyle }
                  onPress={ () =>
                    this.setState({ prifilePicDialogVisible: false })
                  }
                >
                  <Image
                    style={ { height: 18, width: 18 } }
                    source={ require("../img/close_icon.png") }
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
        {
          <Loader
            loading={ this.props.isLoadingInit }
          />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  CloseViewSTyle: {
    position: "absolute",
    right: 2,
    top: 2,
    backgroundColor: "rgba(255,0,0,0.5)",
    width: 32,
    height: 32,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  item: {
    padding: 10
  },
  separator: {
    height: 0.5,
    backgroundColor: "rgba(0,0,0,0.4)"
  },
  text: {
    fontSize: 15,
    color: "black"
  },
  footer: {
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: "#800000",
    borderRadius: 4,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  btnText: {
    color: "white",
    fontSize: 15,
    textAlign: "center"
  }
});
const mapStateToProps = state => {
  return {
    posts: state.Posts.posts,
    authResult: state.Posts.authResult,
    authResultSharePost: state.Posts.authResultSharePost1,
    authResultAbusePost: state.Posts.authResultAbusePost,
    isLoading: state.Posts.isLoading,
    isdata: state.Posts.isdata,
    SplashauthToken: state.Splash.authToken,
    isLoadingInit: state.Posts.isLoadingInit
  };
};

export default connect(
  mapStateToProps,
  {
    getMYPosts,
    getPostsSharepost1,
    getPostsAbusePost
  }
)(Posts);
