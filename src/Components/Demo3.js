import React, { Component } from "react";
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  StyleSheet,
  TextInput,
  DatePickerAndroid,
  TimePickerAndroid,
  DatePickerIOS,
  TouchableHighlight,
  Platform,
  Animated,
  Keyboard,
  Button,
  Alert
} from "react-native";
import { connect } from "react-redux";
import ImagePicker from "react-native-image-picker";
import Tooltip from "./ToolTip/tooltip";
import {
  fullNameChangeEdit,
  fullNameErrorChangeEdit,
  emailChangeEdit,
  emailErrorChangeEdit,
  sexChangeEdit,
  sexErrorChangeEdit,
  birthDateChangeEdit,
  bdayErrorChangeEdit,
  professionChangeEdit,
  professionErrorChangeEdit,
  locationChangeEdit,
  locationErrorChangeEdit,
  briefDescriptionChangeEdit,
  user_tagChangeEdit,
  briefDescriptionErrorChangeEdit,
  initialEditProfileStateData,
  userEdit,
  setProfileData,
  addTag,
  removeTag,
  user_tag_errorChangeEdit
} from "../Actions";
import DatePicker from "react-native-datepicker";
import Style from "./style";
import Moment from "moment";
import PropTypes from "prop-types";
import Header from "./Header";
import { getUser } from "../Database/allSchema";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";

const FORMATS = {
  date: "DD-MM-YYYY",
  datetime: "DD-MM-YYYY HH:mm",
  time: "HH:mm"
};

let userId = 0;
const gender = '';

class EditProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        { tagName: "cricket" },
        { tagName: "travelling" },
        { tagName: "music" },
        { tagName: "reading" },
        { tagName: "Reading" }
      ],
      avatarSource: null,
      date: this.getDate(),
      visible: false,
      animatedHeight: new Animated.Value(0),
      allowPointerEvents: true,
      genderVisible: false,
      gender: 0,
      femaleChecked: false,
      maleChecked: false,
    };

    this.getDate = this.getDate.bind(this);
    this.getDateStr = this.getDateStr.bind(this);
    this.datePicked = this.datePicked.bind(this);
    this.onPressDate = this.onPressDate.bind(this);
    this.onPressCancel = this.onPressCancel.bind(this);
    this.onPressConfirm = this.onPressConfirm.bind(this);
    this.onDateChange = this.onDateChange.bind(this);
    this.onPressMask = this.onPressMask.bind(this);
    this.onDatePicked = this.onDatePicked.bind(this);
    // this.onTimePicked = this.onTimePicked.bind(this);
    // this.onDatetimePicked = this.onDatetimePicked.bind(this);
    // this.onDatetimeTimePicked = this.onDatetimeTimePicked.bind(this);
    this.setModalVisible = this.setModalVisible.bind(this);
  }
  componentDidMount() {
    this.props.initialEditProfileStateData();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.date !== this.props.date) {
      this.setState({ date: this.getDate(nextProps.date) });
    }
  }

  setModalVisible(visible) {
    const { height, duration } = this.props;

    if (visible) {
      this.setState({ modalVisible: visible });
      return Animated.timing(this.setState.animatedHeight, {
        toValue: height,
        duration: duration
      }).start();
    }
    // else {
    //   return Animated.timing(
    //       this.start.animatedHeight,
    //       {
    //         toValue: 0,
    //         duration: duration,
    //       }
    //
    //     ).start(() => {
    //    this.setState({ modalVisible: visible });
    //  });
    // }
  }

  onStartShouldSetResponder(e) {
    return true;
  }

  onMoveShouldSetResponder(e) {
    return true;
  }

  onPressMask() {
    if (typeof this.props.onPressMask === "function") {
      this.props.onPressMask();
    } else {
      this.onPressCancel();
    }
  }

  onPressCancel() {
    this.setModalVisible(false);

    if (typeof this.props.onCloseModal === "function") {
      this.props.onCloseModal();
    }
  }

  onPressConfirm() {
    this.datePicked();
    this.setModalVisible(false);

    if (typeof this.props.onCloseModal === "function") {
      this.props.onCloseModal();
    }
  }

  getDate(date = this.props.date) {
    const { mode, minDate, maxDate, format = FORMATS[mode] } = this.props;

    if (!date) {
      let now = new Date();
      if (minDate) {
        let minDate = this.getDate(minDate);
        if (now < _minDate) {
          return _minDate;
        }
      }

      if (maxDate) {
        let _maxDate = this.getDate(maxDate);
        if (now > _maxDate) {
          return _maxDate;
        }
      }
      return now;
    }

    if (date instanceof Date) {
      return date;
    }
    return Moment(date, format).toDate();
  }

  getDateStr(date = this.props.birth_date) {
    const { mode, format = FORMATS[mode] } = this.props;

    const dateInstance = date instanceof Date ? date : this.getDate(date);

    if (typeof this.props.getDateStr === "function") {
      return this.props.getDateStr(dateInstance);
    }

    return Moment(dateInstance).format(format);
  }

  datePicked() {
    if (typeof this.props.onDateChange === "function") {
      this.props.onDateChange(
        this.getDateStr(this.state.date),
        this.state.date
      );
    }
  }

  getTitleElement() {
    const { date, placeholder, customStyles, allowFontScaling } = this.props;

    if (!date && placeholder) {
      return (
        <Text
          allowFontScaling={allowFontScaling}
          style={[Style.placeholderText, customStyles.placeholderText]}
        >
          {placeholder}
        </Text>
      );
    }
    return (
      <Text
        allowFontScaling={allowFontScaling}
        style={[Style.dateText, customStyles.dateText]}
      >
        {this.getDateStr()}
      </Text>
    );
  }

  onDateChange(date) {
    this.setState({
      allowPointerEvents: false,
      date: date
    });
    const timeoutId = setTimeout(() => {
      this.setState({
        allowPointerEvents: true
      });
      clearTimeout(timeoutId);
    }, 200);
  }

  onDatePicked({ action, year, month, day }) {
    if (action !== DatePickerAndroid.dismissedAction) {
      this.setState({
        date: new Date(year, month, day)
      });
      this.datePicked();
    } else {
      this.onPressCancel();
    }
  }

  onPressDate() {
    if (this.props.disabled) {
      return true;
    }

    Keyboard.dismiss();

    // reset state
    this.setState({
      date: this.getDate()
    });

    if (Platform.OS === "ios") {
      this.setModalVisible(true);
    } else {
      const {
        mode,
        androidMode,
        format = FORMATS[mode],
        minDate,
        maxDate,
        is24Hour = !format.match(/h|a/)
      } = this.props;
      // 选日期
      if (mode === "date") {
        DatePickerAndroid.open({
          date: this.state.date,
          minDate: minDate && this.getDate(minDate),
          maxDate: maxDate && this.getDate(maxDate),
          mode: androidMode
        }).then(this.onDatePicked);
      } else if (mode === "time") {
        // 选时间

        let timeMoment = Moment(this.state.date);

        TimePickerAndroid.open({
          hour: timeMoment.hour(),
          minute: timeMoment.minutes(),
          is24Hour: is24Hour,
          mode: androidMode
        }).then(this.onTimePicked);
      } else if (mode === "datetime") {
        // 选日期和时间

        DatePickerAndroid.open({
          date: this.state.date,
          minDate: minDate && this.getDate(minDate),
          maxDate: maxDate && this.getDate(maxDate),
          mode: androidMode
        }).then(this.onDatetimePicked);
      }
    }

    if (typeof this.props.onOpenModal === "function") {
      this.props.onOpenModal();
    }
  }

  componentDidMount() {
    getUser()
      .then(user => {
        if (user.length > 0) {
          this.setState({ userData: user[0] });
          userId = user[0].id;
          console.log(userId);
          console.log(gender);
          this.setState({ gender: (user[0].gender === 'Female') ? 0 : 1});
          this.setState({ date: user[0].birth_date});
          console.log(this.state.date);
          this.props.setProfileData(this.state.userData);
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.date !== this.props.birth_date) {
      this.setState({ date: this.getDate(nextProps.date) });
    }
    // if (nextProps.authResult === 'record update successfully') {
    //   Alert.alert(
    //     'Success',
    //     'Profile update successfully.',
    //     [
    //       { text: 'OK', onPress: () => this.props.navigation.goBack(null) },
    //     ],
    //     { cancelable: false }
    //   )
    // }
    if (nextProps.authResult === "record update error") {
      Alert.alert(
        "Failed",
        "Profile update failed, try again later."
        // [
        //   { text: 'OK', onPress: () => this.props.navigation.goBack(null) },
        // ],
        // { cancelable: false }
      );
    }
  }
  renderFemaleCheckBox() {
    return (
      <View style={styles.checkedBoxStyle}>
        {
          (this.state.femaleChecked || this.state.gender== 0) ?
          <View style={styles.checkedViewStyle} />
          : null
        }
      </View>
    );
  }
  rendermaleCheckBox() {
      return (
        <View style={styles.checkedBoxStyle}>
          {
            (this.state.maleChecked|| this.state.gender== 1) ?
              <View style={styles.checkedViewStyle} />
            : null
          }
        </View>
      );
    }


  onNameChange(text) {
    this.props.fullNameChangeEdit(text);
  }
  onEmailChange(text) {
    this.props.emailChangeEdit(text);
  }
  onSexChange(text) {
    this.props.sexChangeEdit(text);
  }
  onBirthDateChange(text) {
    console.log(this.state.date);
    this.props.birthDateChangeEdit(text);
  }
  onProfessionChange(text) {
    this.props.professionChangeEdit(text);
  }
  onLocationChange(text) {
    this.props.locationChangeEdit(text);
  }
  onBriefDescriptionChange(text) {
    this.props.briefDescriptionChangeEdit(text);
  }

  onusrt_tagChange(text) {
    this.props.user_tagChangeEdit(text);
  }
  onBackPress() {
    this.props.navigation.goBack(null);
  }

  onButtonPress() {
    const {
      fullname,
      email_id,
      gender,
      birth_date,
      profession_name,
      location,
      brief_desc
    } = this.props;
    let isValid = true;
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (fullname === "") {
      isValid = false;
      this.props.fullNameErrorChangeEdit("Enter fullname");
    } else {
      this.props.fullNameErrorChangeEdit("");
    }
    if (email_id === "") {
      isValid = false;
      this.props.emailErrorChangeEdit("Enter email");
    } else if (reg.test(email_id) === false) {
      isValid = false;
      this.props.emailErrorChangeEdit("Enter valid email");
    } else {
      this.props.emailErrorChangeEdit("");
    }
    if (gender === "") {
      isValid = false;
      this.props.sexErrorChangeEdit("Enter gender");
    } else {
      this.props.sexErrorChangeEdit("");
    }
    if (birth_date === "") {
      isValid = false;
      this.props.bdayErrorChangeEdit("Enter birthday");
    } else {
      this.props.bdayErrorChangeEdit("");
    }
    if (profession_name === "") {
      isValid = false;
      this.props.professionErrorChangeEdit("Enter profession");
    } else {
      this.props.professionErrorChangeEdit("");
    }
    if (location === "") {
      isValid = false;
      this.props.locationErrorChangeEdit("Enter location");
    } else {
      this.props.locationErrorChangeEdit("");
    }
    // if (brief_desc === "") {
    //   isValid = false;
    //   console.log(brief_desc);
    //   this.props.briefDescriptionErrorChangeEdit("Enter brief description");
    // } else {
    //   this.props.briefDescriptionErrorChangeEdit("");
    // }
    if (this.props.tagArray === null || this.props.tagArray.length === 0) {
      isValid = false;
      this.props.user_tag_errorChangeEdit("Please add tag");
    } else {
      this.props.user_tag_errorChangeEdit("");
    }
    if (isValid === true) {
      // this.props.navigation.navigate("BottomTab", { myProps: this.props });
      //   this.props.navigation.goBack(null);
      // console.log(" Login Success");
      //  this.props.navigation.navigate('SignUpPage');
      console.log(userId);
      console.log(fullname);
      console.log(email_id);
      console.log(gender);
      console.log(this.state.date);
      console.log(profession_name);
      console.log(location);
      console.log(brief_desc);
      this.props.userEdit({
        id: userId,
        fullname,
        email_id,
        gender: this.state.gender,
        birth_date,
        profession_name,
        location,
        brief_desc,
        profile_picture:
          this.state.avatarSource != null ? this.state.avatarSource.uri : this.props.user_photo,
        tag:
          this.props.tagArray.length > 0 ? this.props.tagArray.toString() : "",
        myNavigation: this.props.navigation.navigate
      });
    }
  }

  focusNextField(nextField) {
    this.refs[nextField].focus();
  }

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };
    ImagePicker.showImagePicker(options, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled photo picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        let source = { uri: response.uri };
        this.setState({
          avatarSource: source
        });
      }
    });
  }

  jumpLogin() {
    this.onButtonPress();
  }

  renderItem = ({ item, index }) => {
    return (
      <View
        style={{
          paddingLeft: 4,
          marginLeft: 10,
          width: "29%",
          backgroundColor: "#C8C8C8",
          height: 35,
          borderRadius: 3,
          borderColor: "#e2e0e0",
          borderWidth: 1,
          justifyContent: "center",
          alignItems: "center",
          margin: 3,
          flexDirection: "row"
        }}
      >
        <Text
          style={{
            fontSize: 16,
            color: "#676767",
            width: "85%",
            marginLeft: 6,
            right: 7
          }}
          numberOfLines={2}
        >
          {item.tagName}
        </Text>

        <TouchableOpacity
          activeOpacity={4}
          style={{ position: "absolute", right: 8 }}
        >
          <Image
            style={{ width: 14, height: 14, tintColor: "#676767" }}
            source={require("../img/close_icon.png")}
          />
        </TouchableOpacity>
      </View>
    );
  };
  render() {
    return (
      <View>
        <Header
          leftImage={require("../img/back_icon.png")}
          headertext={"Edit Profile"}
          textDone={"Done"}
          onPressDone={() => this.onButtonPress()}
          onLeftPressed={() => this.onBackPress()}
        />
        <StatusBar backgroundColor="#CC181E" barStyle="light-content" />
        <ScrollView style={{ marginBottom: 50 }}>
          <View style={{ margin: 10 }}>








            <View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <View
                  style={{
                    flex: 1,
                    borderBottomWidth: 1,
                    borderColor: "#A9A9A9",
                    marginRight: 5
                  }}
                >
                  <TextInput
                    ref="8"
                    underlineColorAndroid="transparent"
                    returnKeyType="next"
                    keyboardType="email-address"
                    autoCapitalize="none"
                    style={styles.textInputStyle}
                    value={this.props.user_tag}
                    onChangeText={this.onusrt_tagChange.bind(this)}
                  />
                </View>
                <View style={{ width: 50 }}>
                  <Button
                    title="Add"
                    color="#CC181E"
                    style={styles.dialogokTextStyle}
                    onPress={() =>
                      this.props.addTag(
                        this.props.user_tag,
                        this.props.tagArray
                      )
                    }
                  />
                </View>
              </View>
              <Text style={styles.errorTextStyle}>
                {this.props.user_tagError}
              </Text>
              {this.props.tagArray != null && this.props.tagArray.length > 0 ? (
                <FlatList
                  data={this.props.tagArray}
                  numColumns={3}
                  style={{ marginBottom: 8 }}
                  renderItem={({ item, index }) => {
                    return (
                      <View
                        style={{
                          backgroundColor: "#eaeaea",
                          borderRadius: 5,
                          borderColor: "#e2e0e0",
                          borderWidth: 1,
                          justifyContent: "space-between",
                          alignItems: "center",
                          margin: 5,
                          padding: 3,
                          flexDirection: "row",
                          width: "30%"
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 14,
                            color: "#676767",
                            fontFamily: "Lato-Regular",
                            marginRight: 2,
                            flexWrap: "wrap",
                            width: "70%"
                          }}
                        >
                          {item}
                        </Text>
                        <TouchableOpacity
                          onPress={() =>
                            this.props.removeTag(this.props.tagArray, index)
                          }
                        >
                          <Image
                            style={{
                              height: 12,
                              width: 12,
                              tintColor: "black"
                            }}
                            source={require("../img/close_icon.png")}
                          />
                        </TouchableOpacity>
                      </View>
                    );
                  }}
                />
              ) : null}
            </View>
          </View>
        </ScrollView>
        <Dialog
          visible={this.state.genderVisible}
          title="Select gender"
          onTouchOutside={() => this.setState({ genderVisible: false })}
        >
          <View>
            <View style={{ flexDirection: "column" }}>
            <TouchableOpacity
                    onPress={() => this.setState({ femaleChecked: true, maleChecked: false, gender: 0, genderVisible: false })}
                             style={{ flexDirection: 'row'  }}
                           >
                               {
                                 this.renderFemaleCheckBox()
                               }
                                <Text style={styles.DailogTextStyle}>Female</Text>
                           </TouchableOpacity>
                           <TouchableOpacity
                           onPress={() => this.setState({ femaleChecked: false, maleChecked: true, gender: 1, genderVisible: false })}

                             style={{ flexDirection: 'row', marginTop: 8 }}
                           >
                                 {
                                   this.rendermaleCheckBox()
                                 }
                                  <Text style={styles.DailogTextStyle}>Male</Text>
                           </TouchableOpacity>


            </View>
          </View>
        </Dialog>

        <ProgressDialog
          visible={this.props.isLoading}
          title="Loading"
          message="Please, wait..."
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  labelTextStyle: {
    fontSize: 14,
    color: "#848484",
    fontFamily: "Lato-Regular"
  },
  textInputStyle: {
    color: "#1d1d26",
    fontFamily: "Lato-Regular",
    paddingLeft: 10,
    fontSize: 16,
    height: 40
  },
  genderTextStyle: {
    color: "#1d1d26",
    fontFamily: "Lato-Regular",
    paddingLeft: 10,
    fontSize: 16,
    height: 40,
    marginTop: 5
  },
  DailogTextStyle: {
    color: "#1d1d26",
    fontFamily: "Lato-Regular",
    paddingLeft: 10,
    fontSize: 16,
    marginTop: -2
  },
  avatar: {
    width: 95,
    height: 95,
    borderRadius: 500 / 2
  },
  errorTextStyle: {
    fontSize: 14,
    color: "#F00",
    textAlign: "right",
    marginRight: 10,
    fontFamily: "Lato-Regular"
  },
  errorTextStyle1: {
    fontSize: 14,
    color: "#F00",
    textAlign: "left",
    marginRight: 10,
    fontFamily: "Lato-Regular"
  },
  verticalViewline: {
    height: 40,
    borderWidth: 0.6,
    justifyContent: "center",
    borderColor: "#261d1d26",
    alignItems: "center",
    marginTop: 10,
    marginBottom: 10
  },
  textInputStyleBriefDes: {
    color: "#1d1d26",
    fontFamily: "Lato-Regular",
    padding: 10,
    fontSize: 16,
    height: 50
  },
  checkedBoxStyle: {
    borderWidth: 1,
    width: 15,
    height: 15,
    borderColor: 'black',
    justifyContent: 'center',
    alignItems: 'center'
  },
  checkedViewStyle: {
    backgroundColor: '#CC181E',
    width: 10,
    height: 10,
  },
});

EditProfilePage.defaultProps = {
  mode: "date",
  androidMode: "default",
  date: "",
  // component height: 216(DatePickerIOS) + 1(borderTop) + 42(marginTop), IOS only
  height: 259,

  // slide animation duration time, default to 300ms, IOS only
  duration: 300,
  confirmBtnText: "",
  cancelBtnText: "",
  iconSource: require("../img/close_icon.png"),
  customStyles: {},

  // whether or not show the icon
  showIcon: false,
  disabled: false,
  allowFontScaling: true,
  hideText: false,
  placeholder: "",
  TouchableComponent: TouchableHighlight,
  modalOnResponderTerminationRequest: e => true
};

EditProfilePage.propTypes = {
  mode: PropTypes.oneOf(["date", "datetime", "time"]),
  androidMode: PropTypes.oneOf(["clock", "calendar", "spinner", "default"]),
  date: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Date),
    PropTypes.object
  ]),
  format: PropTypes.string,
  minDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
  maxDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
  height: PropTypes.number,
  duration: PropTypes.number,
  confirmBtnText: PropTypes.string,
  cancelBtnText: PropTypes.string,
  iconSource: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  iconComponent: PropTypes.element,
  customStyles: PropTypes.object,
  showIcon: PropTypes.bool,
  disabled: PropTypes.bool,
  allowFontScaling: PropTypes.bool,
  onDateChange: PropTypes.func,
  onOpenModal: PropTypes.func,
  onCloseModal: PropTypes.func,
  onPressMask: PropTypes.func,
  placeholder: PropTypes.string,
  modalOnResponderTerminationRequest: PropTypes.func,
  is24Hour: PropTypes.bool,
  getDateStr: PropTypes.func,
  locale: PropTypes.string
};

const mapStateToProps = state => {
  return {
    fullname: state.editprofile.fullname,
    fullnameError: state.editprofile.fullnameError,
    user_photo: state.editprofile.user_photo,
    user_photoError: state.editprofile.user_photoError,
    email_id: state.editprofile.email_id,
    email_idError: state.editprofile.email_idError,
    gender: state.editprofile.gender,
    genderError: state.editprofile.genderError,
    birth_date: state.editprofile.birth_date,
    birth_dateError: state.editprofile.birth_dateError,
    profession_name: state.editprofile.profession_name,
    profession_nameError: state.editprofile.profession_nameError,
    location: state.editprofile.location,
    locationError: state.editprofile.locationError,
    brief_desc: state.editprofile.brief_desc,
    brief_descError: state.editprofile.brief_descError,
    user_tag: state.editprofile.user_tag,
    user_tagError: state.editprofile.user_tagError,
    isLoading: state.editprofile.isLoading,
    tagArray: state.editprofile.tagArray
  };
};
export default connect(
  mapStateToProps,
  {
    fullNameChangeEdit,
    fullNameErrorChangeEdit,
    emailChangeEdit,
    emailErrorChangeEdit,
    sexChangeEdit,
    sexErrorChangeEdit,
    birthDateChangeEdit,
    bdayErrorChangeEdit,
    professionChangeEdit,
    professionErrorChangeEdit,
    locationChangeEdit,
    locationErrorChangeEdit,
    briefDescriptionChangeEdit,
    briefDescriptionErrorChangeEdit,
    initialEditProfileStateData,
    user_tagChangeEdit,
    userEdit,
    setProfileData,
    addTag,
    removeTag,
    user_tag_errorChangeEdit
  }
)(EditProfilePage);
