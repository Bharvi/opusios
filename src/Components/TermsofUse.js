/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,

} from 'react-native';
import Header from './HeaderEdit';
import { connect } from "react-redux";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import { getUser } from "../Database/allSchema";
import { getMYTerms } from "../Actions";
import Loader from './Loader';
import WebView from 'react-native-webview';

class TermsofUse extends Component {

  componentDidMount() {
    this.props.getMYTerms({ token: this.props.SplashauthToken });
  }

  OnPressed() {
    this.props.navigation.goBack(null);
  }
  render() {
    return (
      <View style={ styles.container }>
        {/* <Header
            onLeftPressed={() => this.OnPressed()}
            leftImage={require('../img/back_icon.png')}
            headertext="Terms of Use"
          /> */}
        <Header
          leftImage={ require("../img/back_icon.png") }
          headertext={ "Terms of Use" }
          onLeftPressed={ () => this.OnPressed() }
        />
        <WebView
          originWhitelist={ ['*'] }
          source={ { html: this.props.Data } }
          style={ { marginTop: 10, marginLeft: 10, marginRight: 10 } }
        />
        <Loader
          loading={ this.props.isLoading }

        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
});
const mapStateToProps = state => {
  return {
    Data: state.Terms.Data,
    authResult: state.Terms.authResult,
    isLoading: state.Terms.isLoading,
    SplashauthToken: state.Splash.authToken,
  };
};

export default connect(
  mapStateToProps,
  {
    getMYTerms
  }
)(TermsofUse);
