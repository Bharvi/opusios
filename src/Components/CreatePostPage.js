import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  Alert
} from "react-native";
import Headericon from "./Headericon";
import { connect } from "react-redux";
import {
  getCratePost,
  captionChange,
  captionErrorChange,
  imagepostChange,
  imagepostErrorChange,
  initialCreatePostStateData
} from "../Actions";
import { getUser } from "../Database/allSchema";
import ImagePicker from 'react-native-image-picker';
import { StackActions, NavigationActions } from 'react-navigation';
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import Loader from './Loader';
// <Image
//       style={{ height: 20, width: 20, tintColor: '#848484', bottom: 10, right: 10 }}
//       source={require('../img/photo_icon.png')}
// />
let Windowheight = Dimensions.get("window").height - 170;

let userId = '';
let staticImage = 'https://ibb.co/sPkgB8N';
let staticimage = 'https://bootdey.com/img/Content/avatar/avatar6.png';

class CreatePostPage extends Component {

  state = {
    avatarSource: null,
  };


  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
        console.log(source);

        this.setState({
          avatarSource: source
        });
      }
    });
  }

  onBackPress() {
    this.props.navigation.goBack(null);
  }
  componentWillMount() {
    // userId = this.props.navigation.getParam("userId");
    // console.log(userId);
  }
  componentDidMount() {
    this.props.initialCreatePostStateData();
    getUser()
      .then(user => {
        if (user.length > 0) {
          this.setState({ userData: user[0] });
          userId = user[0].id;
          console.log(userId);
        }
      })
      .catch(error => {
        console.log(error);
      });

  }

  oncaptionChange(text) {
    this.props.captionChange(text);
  }
  // onimagePostChange(image) {
  //   this.props.imagepostChange(image);
  // }

  componentWillReceiveProps(nextProp) {
    if (nextProp.authResult === "Success") {
      setTimeout(() => {
        Alert.alert(
          'Success',
          'Create post successfully.',
          [
            { text: 'OK', onPress: () => this.props.navigation.navigate('FeedsPage') },
          ],
          { cancelable: false }
        )

      }, 1500);

    } else if (nextProp.authResult === "User id must be numeric.") {
      setTimeout(() => {
        Alert.alert("Oops", "create post failed");

      }, 500);

    } else if (nextProp.authResult === "select image") {
      setTimeout(() => {
        Alert.alert("Oops", "Select image");
      }, 500);

    }
  }

  onButtonPress() {
    let { caption, imagepost } = this.props;
    let isValid = true;
    if (caption === "") {
      isValid = false;
      this.props.captionErrorChange("Enter caption");
    } else {
      this.props.captionErrorChange("");
    }
    // if (imagepost === "") {
    //   isValid = false;
    //   this.props.imagepostErrorChange("Enter image");
    // } else {
    //   this.props.imagepostErrorChange("");
    // }
    if (isValid === true) {
      // this.props.navigation.navigate('BottomTab', { myProps: this.props });
      console.log(staticimage);
      this.props.getCratePost({
        userId,
        caption,
        media: this.state.avatarSource != null ? this.state.avatarSource.uri : staticimage,
        // media: this.state.avatarSource != null ? this.state.avatarSource.uri : '',
        token: this.props.SplashauthToken
      });

    }
  }

  render() {
    return (
      <View>
        <Headericon
          leftImage={ require("../img/back_icon.png") }
          headertext={ "Create Post" }
          doneIcon={ require("../img/check_icon.png") }
          onLeftPressed={ () => this.onBackPress() }
          onPressDone={ () => this.onButtonPress() }
        />

        <View>
          <View style={ { backgroundColor: "#fff", padding: 10 } }>
            <Text allowFontScaling={ false } style={ styles.labelTextStyle }> Add a Caption </Text>
            <TextInput
              allowFontScaling={ false }
              multiline
              underlineColorAndroid="transparent"
              returnKeyType="next"
              keyboardType="default"
              autoCapitalize="none"
              style={ styles.textInputStyle }
              value={ this.props.caption }
              onChangeText={ this.oncaptionChange.bind(this) }
            />
          </View>
          <Text allowFontScaling={ false } style={ styles.errorTextStyle }>{ this.props.captionError }</Text>

          <View style={ styles.mainViewStyle }>
            <TouchableOpacity
              activeOpacity={ 0.8 }
              style={ styles.uploadImageBoxStyle }
              onPress={ this.selectPhotoTapped.bind(this) }
            >
              { this.state.avatarSource === null ? <Image style={ styles.avatar } source={ require("../img/upload_image.png") } /> :
                <Image style={ styles.avatar1 } source={ this.state.avatarSource } />
              }

            </TouchableOpacity>
          </View>
        </View>
        <Loader
          loading={ this.props.isLoading }

        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  labelTextStyle: {
    fontSize: 14,
    color: "#848484",
    fontFamily: "Lato-Regular"
  },
  textInputStyle: {
    color: "#2c2c2c",
    fontFamily: "Lato-Regular",
    fontSize: 16,
    height: 70,
    textAlignVertical: "top"
  },
  errorTextStyle: {
    fontSize: 14,
    color: "#F00",
    textAlign: "left",
    marginLeft: 10
  },
  errorTextStyle1: {
    fontSize: 14,
    color: "#F00",
    alignItems: "center",
    justifyContent: "center"
  },
  mainViewStyle: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#dddee4",
    height: Windowheight
  },
  uploadImageBoxStyle: {
    height: 300,
    width: 300,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: "grey"
  },
  uploadTextStyle: {
    fontSize: 12,
    color: "#848484",
    textAlign: "center",
    textAlignVertical: "center"
  },
  avatar: {
    height: 130,
    width: 130
  },
  avatar1: {
    height: 300,
    width: 300
  },

});

const mapStateToProps = state => {
  return {
    caption: state.CreatePost.caption,
    captionError: state.CreatePost.captionError,
    imagepost: state.CreatePost.imagepost,
    imagepostError: state.CreatePost.imagepostError,
    authResult: state.CreatePost.authResult,
    isLoading: state.CreatePost.isLoading,
    SplashauthToken: state.Splash.authToken,
  };
};

export default connect(
  mapStateToProps,
  {
    getCratePost,
    captionChange,
    captionErrorChange,
    imagepostChange,
    imagepostErrorChange,
    initialCreatePostStateData
  }
)(CreatePostPage);
