import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Switch,
  TouchableOpacity,
  TextInput,
  Button,
  StatusBar,
  Alert
} from "react-native";
// import Header from "./HeaderSeting";
import { Dialog } from "react-native-simple-dialogs";
import { connect } from "react-redux";
import { getLogout, getNotificationSeting, setUserStatus, initialSocialLogin, initialChatList } from "../Actions";
import { ProgressDialog } from "react-native-simple-dialogs";
import { StackActions, NavigationActions } from "react-navigation";
import { getUser, deleteUser, deleteAppHintData, deleteChatList } from "../Database/allSchema";
import Loader from './Loader';
import { ShareDialog } from 'react-native-fbsdk';
import Header from "./HeaderEdit";
import firebase from "react-native-firebase";


let userId = "";
let is_notify = "";
let fireId;
let onlineUserID = "";


class Settings extends Component {
  constructor () {
    super();
    this.state = {
      toggled: null,
      shareLinkContent: {
        contentType: 'link',
        contentUrl: "http://www.khesed.com/",
        contentDescription: '',
      }
    };
  }

  componentWillMount() {
    getUser()
      .then(user => {
        if (user.length > 0) {
          userId = user[0].id;
          is_notify = user[0].is_notify;
          fireId = user[0].fire_id;
          console.log(userId);
          console.log(is_notify);
          this.setState({ toggled: user[0].is_notify == "Y" ? true : false });
          console.log(this.state.toggled);
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  OnPressed() {
    this.props.navigation.goBack(null);
  }
  logout() {
    Alert.alert(
      "Logout",
      "Are you sure, you want to Logout?",
      [
        // { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
        // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: "Yes", onPress: () => this.ApiCAll() },
        {
          text: "No",
          onPress: () => {
            cancelable: false;
          }
        }
      ],
      { cancelable: false }
    );
  }
  NotificationSetionApi(value) {
    console.warn('value', value);
    let trunon = (value == true) ? "Y" : "N";
    console.warn('trunon', trunon);
    this.props.getNotificationSeting({
      userID: userId,
      is_notify: trunon,
      token: this.props.SplashauthToken
    });
    this.setState({ toggled: value });
  }
  ApiCAll() {
    console.log('userId logout', userId + '=' + fireId);

    this.props.setUserStatus(userId, '0', this.props.SplashauthToken);
    this.props.getLogout({ userID: userId, token: this.props.SplashauthToken });
    this.props.initialSocialLogin();
    this.props.initialChatList();
    try {
      if (userId == 1) {
        onlineUserID = 'afdsfsdfdsfsdfsdfsdf';
      } else {
        onlineUserID = fireId;
      }
      firebase.database().ref()
        .child("friends/" + onlineUserID)
        .update({
          onlineStatus: 'false'
        });
      firebase.auth().signOut();
    } catch (err) {
      console.log('err_userOnline', err)
    }
    deleteAppHintData().then(() => {
    })
      .catch(error => {
        console.log(error);
      });
    deleteUser()
      .then(() => {
        deleteChatList()
          .then(() => {
            console.log('Success in delete chatlist')
          })
          .catch((err) => {
            console.log('Error in delete chatlist', err)
          })
        console.log("logout database");
        let resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              routeName: "SocialLoginPage"
            })
          ]
        });
        this.props.navigation.dispatch(resetAction);
      })
      .catch(error => {
        console.log(error);
      });
  }

  shareLinkWithShareDialog() {
    console.warn("Hello")
    let tmp = this;
    ShareDialog.canShow(this.state.shareLinkContent).then(
      (canShow) => {
        console.warn(canShow);
        if (canShow) {
          return ShareDialog.show(tmp.state.shareLinkContent);
        }
      }
    ).then(
      (result) => {
        console.log(result);
        if (result.isCancelled) {
          console.log('Share cancelled');
          // Alert.alert('Oops','Post shared Successfully');
        } else {
          // Alert.alert('Success','Post shared Successfully');
          console.log('Share success with postId: '
            + result.postId);
        }
      },
      (error) => {
        console.log('Share fail with error: ' + error);
      }
    );
  }
  render() {
    return (
      <View style={ styles.container }>
        <Header
          leftImage={ require("../img/back_icon.png") }
          headertext={ "Settings" }
          onLeftPressed={ () => this.OnPressed() }
        />
        {/* <Header
          onLeftPressed= {() => this.OnPressed()}
          leftImage={require("../img/back_icon.png")}
          headertext="Settings"
        /> */}
        <StatusBar backgroundColor="#CC181E" barStyle="light-content" />
        <View>
          <TouchableOpacity
            style={ styles.ViewStyle }
            onPress={ () => this.props.navigation.navigate("EditProfilePage") }
          >
            <Text allowFontScaling={ false } style={ styles.TextStyle }>Edit Profile</Text>
            <Image
              source={ require("../img/right_arroe.png") }
              style={ { width: 15, height: 15 } }
            />
          </TouchableOpacity>
          {/* <View style={styles.ViewStyle}>
            <Text allowFontScaling={false} style={styles.TextStyle}>Notifications</Text>
            <View>
              <Switch
                style={{ transform: [{ scaleX: 0.9 }, { scaleY: 1 }] }}
                onValueChange={value => this.NotificationSetionApi(value)}
                value={this.state.toggled}
                thumbTintColor="#F6F6F6"
                onTintColor="#008000"

                circleActiveColor={'#30a566'}
                circleInActiveColor={'#000000'}
              />
            </View>
          </View> */}
          <TouchableOpacity
            onPress={ () => this.props.navigation.navigate("AboutUs") }
            style={ styles.ViewStyle }
          >
            <Text allowFontScaling={ false } style={ styles.TextStyle }>About Us</Text>
            <Image
              source={ require("../img/right_arroe.png") }
              style={ { width: 15, height: 15 } }
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={ () => this.shareLinkWithShareDialog() }
            style={ styles.ViewStyle }>
            <Text allowFontScaling={ false } style={ styles.TextStyle }>Share it on Facebook</Text>
            <Image
              source={ require("../img/right_arroe.png") }
              style={ { width: 15, height: 15 } }
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={ () => this.props.navigation.navigate("TermsofUse") }
            style={ styles.ViewStyle }
          >
            <Text allowFontScaling={ false } style={ styles.TextStyle }>Terms of Use</Text>
            <Image
              source={ require("../img/right_arroe.png") }
              style={ { width: 15, height: 15 } }
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={ () => this.props.navigation.navigate("PrivacyPolicy") }
            style={ styles.ViewStyle }
          >
            <Text allowFontScaling={ false } style={ styles.TextStyle }>Privacy Policy</Text>
            <Image
              source={ require("../img/right_arroe.png") }
              style={ { width: 15, height: 15 } }
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={ () => this.logout() }
            style={ styles.ViewStyle }
          >
            <Text allowFontScaling={ false } style={ styles.TextStyle }>Logout</Text>
          </TouchableOpacity>
        </View>
        {
          //   <Loader
          //   loading={this.props.isLoading}

          // />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  ViewStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 15,
    borderBottomWidth: 0.7,
    borderColor: "#c5c3c8"
  },
  TextStyle: { fontSize: 16, fontFamily: "Lato-Medium", color: "#2a2c30" },
  myEmptyStarStyle: {
    color: "white",
    width: 15,
    height: 15,
    margin: 5
  },
  myStarStyle: {
    color: "#147f7e",
    backgroundColor: "transparent",
    textShadowColor: "black",
    textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 2,
    width: 15,
    height: 15,
    margin: 5
  },
  TextStyleView: {
    color: "#7F7F7F",
    fontSize: 14,
    fontFamily: "Lato-Regular"
  },
  userTextStyle: {
    fontSize: 14,
    color: "#4d4e51",
    fontFamily: "Lato-Regular",
    marginTop: 1,
    marginBottom: 10
  },
  tapTextStyleView: {
    color: "#7F7F7F",
    fontSize: 12,
    fontFamily: "Lato-Regular",
    margin: 3
  },
  textInputStyle: {
    color: "#1d1d26",
    fontFamily: "Lato-Regular",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 14,
    height: 100
  },
  buttonTextSTyle: {
    color: "white",
    fontFamily: "Lato-Regular",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 14
  }
});
const mapStateToProps = state => {
  return {
    authResult: state.Setting.authResult,
    authResultNotification: state.Setting.authResultNotification,
    isLoading: state.Setting.isLoading,
    SplashauthToken: state.Splash.authToken
  };
};

export default connect(
  mapStateToProps,
  {
    getLogout,
    setUserStatus,
    getNotificationSeting,
    initialSocialLogin,
    initialChatList
  }
)(Settings);
