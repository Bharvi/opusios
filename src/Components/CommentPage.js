import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  StatusBar,
  Image,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Alert,
  Dimensions,
  Share,
  Modal,
  ScrollView,
} from "react-native";
import Headericon from "./Headericon";
import { connect } from "react-redux";
import {
  commentChange,
  commentErrorChange,
  initialCommentStateData,
  getComment,
  getCommentlist,
  initialgetallcommentlistStateData,
  getCommentSharepost,
  getCommentAbusePost,
  likeUnlikeApi
} from "../Actions";
import { getUser } from "../Database/allSchema";
import ItemViewComment from "./ItemViewComment";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import { POSTIMAGEPATH } from "../Actions/type";
import ImageLoad from './ImageLoad';
import Loader from './Loader';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import DeviceInfo from 'react-native-device-info';


let postID = "";
let userId = "";
let shareText = "Test 2";
let isUserFromNotification = false;

let height = Dimensions.get('window') - 150;

var { height1 } = Dimensions.get('window') - 300;
var box_count = 1;
var box_height = height / box_count;

class CommentPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prifilePicDialogVisible: false,
      userPhoto: '',
      post_photo: "",
      post_caption: "",
      page: 1,
      pageCount: 1,
      data: [],
      loading: false,
      error: null,
      refreshing: false,
      fetching_from_server: false,
      isLike: false,
      numOflike: 0.

    };
    // this.shareMessage = this.shareMessage.bind(this);

  }
  // shareMessage() {
  //   console.log(this.state.post_caption);
  //   let image = this.state.post_photo;
  //   console.log(image);
  //   Share.share({
  //     message: this.state.post_caption
  //     // url: image ,
  //   });
  // }

  componentWillMount() {
    isUserFromNotification = this.props.navigation.getParam('isUserFromNotification')

    this.props.navigation.addListener(
      "willFocus",
      payload => {
        getUser()
          .then(user => {
            if (user.length > 0) {
              this.setState({ userData: user[0] });
              userId = user[0].id;
              console.log(userId);
              console.log('Hello Comment page ');
              this.MethodCall();
              // this.onButtonPress();
              this.fetchData();
            }
          })
          .catch(error => {
            console.log(error);
          });


      }
    );
  }
  componentWillUnMount() {
    didFocusSubscription.remove();
  }
  fetchData = () => {
    console.log(userId, this.state.pageCount, postID)
    this.props.getCommentlist({ userID: userId, page: this.state.pageCount, postID, token: this.props.SplashauthToken });


  };

  onPressLike() {

    this.setState({
      numOflike: (this.state.isLike) ? this.state.numOflike - 1 : this.state.numOflike + 1,
      isLike: !this.state.isLike
    },
      () => {
        console.log('onPressLike', this.state.isLike);

        getUser()
          .then(user => {
            if (user.length > 0) {
              this.props.likeUnlikeApi(
                user[0].id,
                this.props.postdata.post_id,
                this.props.SplashauthToken,
                (this.state.isLike) ? 'like' : 'unlike'
              );
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    );
  }

  handleLoadMore = () => {
    this.setState(
      {
        pageCount: this.state.pageCount + 1
      },
      () => {
        console.log(this.state.pageCount);
        if (this.props.isdata) {
          this.props.getCommentlist({ userID: userId, page: this.state.pageCount, postID, comments_list: this.props.comments_list, token: this.props.SplashauthToken });
        }
      }
    );
  };


  MethodCall() {
    postID = this.props.navigation.getParam("postID");
    console.log(postID);
    this.props.initialCommentStateData();
  }

  componentWillReceiveProps(nextProp) {
    if (nextProp.authResult === "success list") {
      console.warn(nextProp.postdata.is_liked);
      this.setState({
        isLike: (nextProp.postdata.is_liked == 'Y') ? true : false,
        numOflike: Number(nextProp.postdata.like_count)
      });
    }


    if (nextProp.authResult === "Success") {

      // setTimeout(() => {
      this.props.getCommentlist({ userID: userId, page: this.state.pageCount, postID, token: this.props.SplashauthToken });

      // }, 50);



      // setTimeout(() => {
      //   Alert.alert(
      //     "Success",
      //     "Comment add successfully.",
      //     [
      //       {
      //         text: "OK",
      //         onPress: () => {
      //           this.props.getCommentlist({ userID: userId, page: this.state.pageCount, postID, token: this.props.SplashauthToken });
      //         }
      //       }
      //     ],
      //     { cancelable: false }
      //   );

      // }, 500);

      // console.log(nextProp.authResult);
      // console.log(" Refresh Comment list ");

    } else if (nextProp.authResult === "User id must be numeric.") {
      console.log(nextProp.authResult);
    }
    if (nextProp.authResultSharePost === "Success") {
      setTimeout(() => {
        // this.shareMessage();

      }, 500);

    } else if (nextProp.authResultSharePost === "User id must be numeric.") {
      setTimeout(() => {
        Alert.alert("Oops", "User id must be numeric.");

      }, 500);

    } else if (nextProp.authResultSharePost === "Post id must be numeric.") {
      setTimeout(() => {
        Alert.alert("Oops", "Post id must be numeric.");

      }, 500);

    }
    if (nextProp.authResultAbu === "Success Abuse") {
      setTimeout(() => {
        Alert.alert(
          'Success',
          'Report abuse successfully.',
          [
            { text: 'OK', onPress: () => { cancelable: false } },
          ],
          { cancelable: false }
        )

      }, 500);


    } else if (
      nextProp.authResultAbu === "Already Post is Abuse by same User"
    ) {
      setTimeout(() => {
        Alert.alert("Oops", "Already post is abuse by same User");

      }, 500);

    } else if (nextProp.authResultAbu === "User id must be numeric.") {
      setTimeout(() => {
        Alert.alert("Oops", "User id must be numeric.");

      }, 500);

    } else if (nextProp.authResultAbu === "Post id must be numeric.") {
      setTimeout(() => {
        Alert.alert("Oops", "Post id must be numeric.");
      }, 500);

    }
  }
  onSharePost() {
    // this.setState({
    //   post_photo: POSTIMAGEPATH + this.props.postdata.post_photo,
    //   post_caption: this.props.postdata.post_caption
    // });
    // console.log(this.state.post_photo);
    // console.log(this.state.post_caption);
    setTimeout(() => {
      Alert.alert(
        "Opus",
        "Do you want to share the post?",
        [
          {
            text: "Yes",
            onPress: () => {
              this.props.getCommentSharepost({ userID: userId, postID: this.props.postdata.post_id, shareText, token: this.props.SplashauthToken });
              this.props.navigation.navigate('FeedsPage');
            }
          },
          {
            text: "No",
            onPress: () => {
              cancelable: false;
            }
          }
        ],
      );

    }, 500);

  }
  onAbusePost() {
    console.log(userId);
    console.log(this.props.postdata.post_id);
    this.props.getCommentAbusePost({ userID: userId, postID: this.props.postdata.post_id, token: this.props.SplashauthToken });
  }


  onCommentChange(text) {
    this.props.commentChange(text);
  }


  onBackPress = () => {
    console.log('pressed', isUserFromNotification)
    if (isUserFromNotification) {
      this.props.navigation.replace('BottomTabFeedFirst')
      return true;
    } else {
      this.props.navigation.goBack(null);
      // this.props.navigation.replace('Chat');
      return true;
    }
  }

  commentPost() {
    this.onButtonPress();
  }

  onButtonPress() {
    let { comment } = this.props;
    let isValid = true;
    if (comment === "") {
      isValid = false;
      // this.props.commentErrorChange("Enter comment");
      Alert.alert("Alert", "Please enter comment")
    } else {
      this.props.commentErrorChange("");
    }
    if (isValid === true) {
      this.props.getComment({ userID: userId, postID, comment, token: this.props.SplashauthToken });

    }
  }

  onProfileData(item) {
    console.log(item);
    // this.props.navigation.navigate('MyProfile', { fullname: item.fullname, profile_image: POSTIMAGEPATH + item.user_photo })

    this.props.navigation.navigate('Profile', {
      user_id: item.user_id,
      form: 'Commentpage'
    });

  }
  onMyProfiledata() {
    console.log("commet click");
    getUser()
      .then(user => {
        if (user.length > 0) {
          let USERID = user[0].id;
          console.log("sds", userId, this.props.postdata.user_id);
          if (this.props.postdata.user_id == USERID) {
            console.log("Myprofile", this.props.postdata.user_id, USERID);
            console.log(this.props.navigation);

            this.props.navigation.navigate("MyProfile", { fullname: this.props.postdata.fullname, profile_image: POSTIMAGEPATH + this.props.postdata.user_photo })
          }
          else {
            console.log("Profile");

            this.props.navigation.navigate('Profile', {
              user_id: this.props.postdata.user_id,
              form: 'Commentpage'
            });
            // this.props.navigation.push('Profile', {
            //     user_id: item.friend_id
            //   })
          }

        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    console.log('render', this.state.isLike);
    return (
      <View style={{ flex: 1, backgroundColor: "#e5eaea" }}>

        <Headericon
          leftImage={require("../img/back_icon.png")}
          headertext={"Comments"}
          onLeftPressed={() => this.onBackPress()}
        />
        <StatusBar backgroundColor="#CC181E" barStyle="light-content" />
        <KeyboardAwareScrollView
          style={{ marginBottom: (DeviceInfo.getModel() === 'iPhone X' || DeviceInfo.getModel() === 'iPhone XS Max' || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 20 : 0 }}
          resetScrollToCoords={{ x: 0, y: 0 }}
          contentContainerStyle={styles.container1}
          scrollEnabled={true}
        >

          {(!this.props.isLoading) ?
            <View style={{ flex: 1, backgroundColor: "#e5eaea" }}>

              <View style={{ backgroundColor: 'white' }}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                    padding: 10
                  }}
                >
                  <TouchableOpacity
                    style={{ flex: 1.3 }}
                    onPress={() => this.setState({ prifilePicDialogVisible: true, userPhoto: POSTIMAGEPATH + this.props.postdata.user_photo })}
                  >
                    <ImageLoad
                      style={{ height: 40, width: 40, borderRadius: 200 / 2 }}
                      borderRadius={40 / 2}
                      source={{ uri: POSTIMAGEPATH + this.props.postdata.user_photo }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{ flex: 7 }}
                    activeOpacity={0.8}
                    onPress={() => this.onMyProfiledata()}
                  >
                    <View>
                      <Text allowFontScaling={false} style={styles.MarcusTextStyle}>
                        {this.props.postdata.fullname}
                      </Text>
                    </View>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                      <Text allowFontScaling={false} style={styles.yesterdayTextStyle}>
                        {this.props.postdata.time}.
              </Text>
                      <Image
                        source={require("../img/earth_icon.png")}
                        style={{ width: 10, height: 10, marginLeft: 5, marginTop: 3 }}
                      />
                    </View>
                  </TouchableOpacity>

                </View>
                <Text allowFontScaling={false} style={styles.TextStyle}>{this.props.postdata.post_caption}</Text>
                <ImageLoad
                  source={{ uri: POSTIMAGEPATH + this.props.postdata.post_photo }}
                  style={{
                    width: "100%",
                    height: 280,
                    marginTop: 5,
                  }}
                />
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingTop: 15,
                    paddingBottom: 15
                  }}
                >
                  <TouchableOpacity
                    activeOpacity={0.8}
                    style={{ position: 'absolute', left: 15, top: 8, bottom: 8 }}
                    onPress={() => this.onPressLike()}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Image
                        source={require("../img/like.png")}
                        style={[styles.commentIconStyle1, { tintColor: (this.state.isLike) ? '#CC181E' : '#848484' }]}
                      />
                      <Text allowFontScaling={false} style={styles.CommentTextStyle}>
                        {this.state.numOflike}
                      </Text>
                      {/* <Text allowFontScaling={false}  style={styles.CommentTextStyle}>Comment</Text> */}
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    activeOpacity={0.8}
                    style={{ position: 'absolute', left: 75, top: 8, bottom: 8 }}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Image
                        source={require("../img/fill_comment.png")}
                        style={styles.commentIconStyle1}
                      />
                      <Text allowFontScaling={false} style={styles.CommentTextStyle}>
                        {this.props.postdata.comment_count}
                      </Text>
                      {/* <Text allowFontScaling={false}  style={styles.CommentTextStyle}>Comment</Text> */}
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    activeOpacity={0.8}
                    style={{ position: 'absolute', left: 140, top: 8, bottom: 8 }}
                    onPress={() => this.onSharePost()}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Image
                        source={require("../img/share_icon.png")}
                        style={styles.commentIconStyle}
                      />
                      {/* <Text allowFontScaling={false}  style={styles.CommentTextStyle}>Share</Text> */}
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    activeOpacity={0.8}
                    style={{ position: 'absolute', right: 10, top: 8 }}
                    onPress={() => this.onAbusePost()}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Image
                        source={require("../img/flag_icon.png")}
                        style={styles.commentIconStyle}
                      />
                      {/* <Text allowFontScaling={false}  style={styles.CommentTextStyle}>Report Abuse</Text> */}
                    </View>
                  </TouchableOpacity>
                </View>
              </View>

              <ScrollView style={{ backgroundColor: "#fff", flex: 1, marginBottom: 40 }}>
                {
                  (!this.props.isLoading && this.props.comments_list != null && this.props.comments_list.length > 0 ? (
                    <FlatList
                      // maxHeight={90}
                      data={this.props.comments_list}
                      onEndReached={this.handleLoadMore.bind(this)}
                      onEndReachedThreshold={1}
                      renderItem={({ item, index }) => {
                        console.log(item.user_fullname);
                        return (
                          <ItemViewComment
                            name={item.user_fullname}
                            des={item.comment}
                            userprofileimage={{ uri: POSTIMAGEPATH + item.user_photo }}
                            onPressUserIcon={() =>
                              this.setState({ prifilePicDialogVisible: true, userPhoto: POSTIMAGEPATH + item.user_photo })
                            }
                            onPressProfile={() =>
                              this.onProfileData(item)
                            }
                          />
                        );
                      }}
                    />) :
                    <View
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                        flex: 1,
                        marginTop: 20
                      }}
                    >
                      <Text style={{ fontSize: 13, fontFamily: "Lato-Regular", color: '#686868', }}>
                        Comments not available
           </Text>
                    </View>
                  )
                }
              </ScrollView>

              <View
                style={{
                  backgroundColor: "white",
                  height: 40,
                  width: "100%",
                  borderWidth: 1,
                  borderColor: "#bababa",
                  position: "absolute",
                  bottom: 0,
                  justifyContent: "center"
                }}
              >
                <TextInput
                  underlineColorAndroid="transparent"
                  returnKeyType="done"
                  placeholder="Add your comments"
                  keyboardType="default"
                  autoCapitalize="none"
                  style={styles.textInputStyle}
                  value={this.props.comment}
                  onChangeText={this.onCommentChange.bind(this)}
                />
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={{ position: "absolute", right: 10 }}
                  onPress={this.onButtonPress.bind(this)}
                >
                  {
                    // <Image
                    //   source={require("../img/send_message_icon.png")}
                    //   style={{ width: 20, height: 20, tintColor: "#CC181E" }}
                    // />
                    <Text allowFontScaling={false} style={{
                      color: "#CC181E",
                      fontSize: 14,
                      fontFamily: "Lato-Bold",
                      marginLeft: 5
                    }}>Comment</Text>
                  }
                </TouchableOpacity>
              </View>
              <Text style={styles.errorTextStyle}>{this.props.commentError}</Text>
              <Modal
                transparent={true}
                visible={this.state.prifilePicDialogVisible}
                onRequestClose={() => this.setState({ prifilePicDialogVisible: false })}
              >

                <View
                  style={{
                    height: "100%",
                    width: "100%",
                    paddingTop: 130,
                    paddingBottom: 100
                  }}
                >
                  <View style={{ backgroundColor: 'rgba(0,0,0,0.3)', padding: 5, justifyContent: 'center', alignContent: 'center' }}>
                    <View style={{ alignItems: "center", justifyContent: "center" }}>
                      <ImageLoad
                        style={{ width: "100%", height: "100%" }}
                        source={{ uri: this.state.userPhoto }}
                      />
                      <TouchableOpacity
                        style={styles.CloseViewSTyle}
                        onPress={() =>
                          this.setState({ prifilePicDialogVisible: false })
                        }
                      >
                        <Image
                          style={{ height: 18, width: 18 }}
                          source={require("../img/close_icon.png")}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </Modal>

            </View> : null
          }
          <Loader
            loading={this.props.isLoading}
          />
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    borderTopWidth: 1,
    borderColor: "#e2e0e0",
    marginTop: 10,

  },
  container1: {
    flex: 1,
    backgroundColor: "white",
    borderTopWidth: 1,
    borderColor: "#e2e0e0",
  },
  MarcusTextStyle: {
    fontSize: 16,
    fontFamily: "Lato-Bold",
    color: "#010101"
  },
  yesterdayTextStyle: {
    fontSize: 14,
    color: "#989898",
    fontFamily: "Lato-Regular"
  },
  TextStyle: {
    fontSize: 14,
    paddingLeft: 10,
    paddingRight: 10,
    fontFamily: "Lato-Semibold",
    color: "#2c2c2c"
  },
  CommentTextStyle: {
    color: "#A4A4A4",
    fontSize: 14,
    fontFamily: "Lato-Bold",
    marginLeft: 5
  },
  commentIconStyle: {
    width: 15,
    height: 15
  },
  commentIconStyle1: {
    width: 15,
    height: 15,
    tintColor: '#848484'
  },
  textInputStyle: {
    color: "#2c2c2c",
    fontFamily: "Lato-Regular",
    paddingLeft: 10,
    paddingTop: 3,
    paddingBottom: 3,
    fontSize: 16,
    width: "80%"
  },
  errorTextStyle: {
    fontSize: 14,
    color: "#F00",
    textAlign: "left",
    marginRight: 10,
    position: "absolute",
    bottom: 0,
    right: 8,
    marginTop: 10
  },
  CloseViewSTyle: { position: "absolute", right: 2, top: 0, backgroundColor: 'rgba(255,0,0,0.5)', width: 32, height: 32, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }
});

const mapStateToProps = state => {
  return {
    comment: state.comment.comment,
    commentError: state.comment.commentError,
    isLoading: state.comment.isLoading,
    authResult: state.comment.authResult,
    authResultSharePost: state.comment.authResultSharePost,
    authResultAbu: state.comment.authResultAbusePost,
    comments_list: state.comment.comments_list,
    postdata: state.comment.postdata,
    isdata: state.comment.isdata,
    SplashauthToken: state.Splash.authToken,

  };
};

export default connect(
  mapStateToProps,
  {
    commentChange,
    commentErrorChange,
    initialCommentStateData,
    getComment,
    getCommentlist,
    initialgetallcommentlistStateData,
    getCommentSharepost,
    getCommentAbusePost,
    likeUnlikeApi
  }
)(CommentPage);
