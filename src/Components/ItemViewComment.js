import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity, FlatList
} from 'react-native';
import ImageLoad from './ImageLoad';
class ItemViewComment extends Component {
  render() {
    return (
      <View>
      <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 10, }}>
      <TouchableOpacity
      activeOpacity={0.8}
      onPress={this.props.onPressUserIcon}
      >
      <ImageLoad
            style={{ height: 35, width: 35, borderRadius: 200 / 2 }}
            borderRadius={35 / 2}
            source={this.props.userprofileimage}
      />
      </TouchableOpacity>
      <TouchableOpacity
      style={{ marginLeft: 15 }}
      activeOpacity={0.8}
      onPress={this.props.onPressProfile}
      >
      <Text allowFontScaling={false}  style={styles.MarcusTextStyle}>{this.props.name}</Text>
      <Text allowFontScaling={false} style={styles.yesterdayTextStyle} >{this.props.des}</Text>
      </TouchableOpacity>
      </View>
      <View
      style={{
      borderBottomColor: '#bababa',
      borderBottomWidth: 0.5,
      marginTop: 6
     }}
     />
    </View>
    );
  }
}

const styles = StyleSheet.create({
  MarcusTextStyle: {
   fontSize: 14,
   fontFamily: 'Lato-Semibold',
   color: '#2c2c2c'
 },
  yesterdayTextStyle: {
    fontSize: 14,
   color: '#000000',
    fontFamily: 'Lato-Regular'
  },
});
export default ItemViewComment;
