import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform
} from 'react-native';
import DeviceInfo from 'react-native-device-info';

export default class Headericon extends Component {
  render() {
    return (
      <View>
        <View style={{ backgroundColor: "#CC181E", height: 10 }}></View>
        <View style={{ backgroundColor: "#CC181E", paddingTop: (DeviceInfo.getModel() === 'iPhone X' || DeviceInfo.getModel() === 'iPhone XS Max' || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 15 : 0, alignContent: 'center', justifyContent: 'center' }}/>
      
      <View style={styles.container}>
              
                <TouchableOpacity
                activeOpacity={0.8}
                style={{ height:50, width: 50, justifyContent: 'center',
                position: 'absolute', alignItems: 'center', left: 0}}
                 onPress={this.props.onLeftPressed}
                >
                    <Image
                    style={styles.backArrowStyle}
                    source={this.props.leftImage}
                    />
                </TouchableOpacity>

                <Text allowFontScaling={false} style={styles.textStyle}>{this.props.headertext}</Text>
           
                <TouchableOpacity
                 onPress={this.props.onPressDone}
                 activeOpacity={0.8}
                  style={{ height:50, width: 50, alignItems: 'center',justifyContent: 'center',alignContent: 'center',
                position: 'absolute', right: 0}}
                >
                <Image
                style={styles.doneIconStyle}
                source={this.props.doneIcon}
                />
              </TouchableOpacity>

      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    width: '100%',
    backgroundColor: '#CC181E',
    justifyContent: 'center'
  },
  textStyle: {
    fontSize: 16,
    color: '#ffffff',
    fontFamily: 'Lato-Regular',
  },
  textStyleDone: {
    fontSize: 16,
    color: '#CCffffff',
    textAlign: 'center',
    fontFamily: 'Lato-Regular'
  },
  backArrowStyle: {
    height: 18,
    width: 18,
    padding: 8
  },
  doneIconStyle: {
    height: 20,
    width: 20,
    alignSelf: 'center'
  },

});
