import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ImageBackground,
  StatusBar,
  Platform,
  Alert
} from "react-native";
import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager
} from "react-native-fbsdk";
import { TextWithLetterSpacing } from "./TextWithLetterSpacing";
import { connect } from "react-redux";
import { Socaillogin, setLoading, initialSocialLogin, checkUserExists } from "../Actions";
import firebase from "react-native-firebase";
import Loader from './Loader';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';


let Token;

let firebaseid = '';

// import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
// const user = GoogleSignin.currentUser;

let device_type = Platform.OS === 'ios' ? 1 : 0;
let device_token = 1234555;

class SocialLoginPage extends Component {
  constructor (props) {
    super(props);
    firebase
      .messaging()
      .getToken()
      .then(token => {
        console.warn("Device firebase Token: ", token);
        Token = token;
      });
    this.state = {
      email: "",
      name: "",
      username: "",
      isCameFirst: true
    };
  }

  getRef() {
    return firebase.database().ref();
  }
  componentWillReceiveProps(nextProps) {
    console.log('in recive props out')
    if (this.state.isCameFirst) {
      if (nextProps.authResultEmail == 'User Exist.') {
        this.setState({ isCameFirst: false })
        console.log('in recive props out User Exist.')
        // console.log('data', nextProps.userSocialData)
        this.socialLogin(nextProps.userSocialData)
      }
      else if (nextProps.authResultEmail == 'User Not Exist') {
        console.log('in recive props out User Not Exist.')
        console.log('user already exist', nextProps.userSocialData);
        let data = nextProps.userSocialData;
        // console.log('data', data)
        if (nextProps.userSocialData) {
          this.socialLogin(nextProps.userSocialData)
        }
        // this.props.setLoading(false);
      }
      else if (nextProps.authResultEmail == 'error in checkEmail') {
        // this.props.setLoading(false);
        console.log('in recive props error in checkEmail')
        Alert.alert('Something went wrong please try again later.')
      }
    }
  }
  async componentDidMount() {
    //1.
    GoogleSignin.configure();
    await GoogleSignin.hasPlayServices();
  }
  socialLogin(userData) {
    //if user already exist then st there data otherwise new created data
    this.setState({ isCameFirst: false })
    console.log('userDatasociallogin', userData)
    let userSocialId = userData.social_id

    if (userSocialId === '2674643699428785') {
      this.props.Socaillogin(
        userSocialId,
        'afdsfsdfdsfsdfsdfsdf',
        userData.fullname,
        userData.email_id,
        userData.user_photo,
        device_type,
        Token,
        this.props.SplashauthToken,
        this.props.navigation,
        userData.loginType);
      this.props.setLoading(false);
    } else {
      // console.log('userDatasociallogin', userData)
      var main_fire_id = userSocialId + '@opus.com';
      firebase
        .auth()
        .signInWithEmailAndPassword(main_fire_id, main_fire_id)
        .then((user) => {
          console.log('in then part', user)
          var unsubscribe = firebase.auth().onAuthStateChanged(user => {
            if (user) {
              console.log(user.uid, user.email);
              fire_id = user.uid;
              console.log(userData, '1');
              this.props.Socaillogin(
                userData.social_id,
                fire_id,
                userData.fullname,
                userData.email_id,
                userData.user_photo,
                device_type,
                Token,
                this.props.SplashauthToken,
                this.props.navigation,
                userData.loginType);
            }
            this.props.setLoading(false);

          });
          unsubscribe();
        })
        .catch(error => {
          var errorCode = error.code;
          // var errorMessage = error.message;
          let userInfo = userData
          // console.log('userData', userData)
          console.log('errorCode in catch', error)
          if (errorCode == 'auth/user-not-found') {
            console.log('userInfo.main_fire_id', userInfo.main_fire_id)
            firebase
              .auth()
              .createUserWithEmailAndPassword(userInfo.main_fire_id, userInfo.main_fire_id)
              .then(() => {
                this.setState({ loading: false });
                var unsubscribe = firebase.auth().onAuthStateChanged(user => {
                  if (user) {
                    console.log(user.uid, user.email);
                    console.log('user', userInfo)
                    let fire_id = user.uid;
                    this.props.Socaillogin(
                      userInfo.social_id,
                      user.uid,
                      userInfo.fullname,
                      userInfo.email_id,
                      userInfo.user_photo,
                      device_type,
                      Token,
                      this.props.SplashauthToken,
                      this.props.navigation,
                      userInfo.loginType);
                  }
                  this.props.setLoading(false);
                });
                unsubscribe();
              })
              .catch(error => {
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(errorMessage);
              });
          } else {
            this.props.setLoading(false);
          }
          console.log(errorCode, 'firebase');
        });
    }
  }
  async facebookLogin() {
    console.log("facebookLogin");
    LoginManager.logOut();
    let result;
    // try {
    this.setState({ showLoadingModal: true });
    // LoginManager.setLoginBehavior('native')
    result = await LoginManager.logInWithPermissions();
    console.warn(result);
    if (result.isCancelled) {
      console.warn("Login cancelled");
    } else {
      AccessToken.getCurrentAccessToken().then(data => {
        const { accessToken } = data;
        this.initUser(data.accessToken);
      });
    }

    try {
      if (result.isCancelled) {
        this.setState({
          showLoadingModal: false,
        });
      } else {
        // this.FBGraphRequest(
        //   "id, email, picture.type(large)",
        //   this.FBLoginCallback
        // );
      }

    } catch (e) {
      console.warn(e);
    }
  }
  _responseInfoCallback = (error, result) => {
    // this.props.setLoading(true);
    if (error) {
      this.props.setLoading(false);
      Alert.alert('Oops', 'Somthing went wrong please try again later.')
      console.log(error);
    } else {
      console.log('fbresult', result);
      this.props.setLoading(true);
      this.facebookDataSet(result);
    }
  }
  initUser(token) {
    console.log('facebook accesstoken', token);
    const infoRequest = new GraphRequest(
      '/me',
      {
        token,
        auth_type: 'reauthenticate',
        parameters: {
          'fields': {
            'string': 'email,name,picture.type(large),friends'
          }
        }
      },
      this._responseInfoCallback,
    );

    new GraphRequestManager().addRequest(infoRequest).start();
  }
  facebookDataSet(result) {
    let social_id = result.id;
    let fullname = result.name;
    let email = result.email;
    let profile_picture = result.picture.data.url;

    var main_fire_id = social_id + '@opus.com';
    var fire_id = '';
    console.log(main_fire_id, 'test');

    let userInfo = {
      social_id: result.id,
      email_id: result.email,
      fullname: result.name,
      user_photo: result.picture.data.url,
      main_fire_id: result.id + '@opus.com'
    };

    let userInfoExist = {
      loginType: 'facebook',
      social_id: result.id,
      email_id: result.email,
      fullname: result.name,
      user_photo: result.picture.data.url,
      main_fire_id: result.id + '@opus.com'
    };

    let userLoginData = [];
    userLoginData.push(userInfo)

    if (email) {
      this.props.checkUserExists(
        email,
        this.props.SplashauthToken,
        userLoginData,
        'facebook'
      )
    } else {
      //if not email then login
      this.socialLogin(userInfoExist)
    }

  }
  setUser(user) {
    this.props.setLoading(true);
    let social_id = user.id;
    let fullname = user.name;
    let email = user.email;
    let profile_picture = user.photo;
    var main_fire_id = social_id + '@opus.com';
    var fire_id = '';

    let userInfo = {
      social_id: user.id,
      email_id: user.email,
      fullname: user.name,
      user_photo: user.photo,
      main_fire_id: user.id + '@opus.com'
    };

    let userLoginData = [];
    userLoginData.push(userInfo)

    this.props.checkUserExists(
      email,
      this.props.SplashauthToken,
      userLoginData,
      'google'
    )

  }
  async googleLogin() {
    console.log('in Google Login');
    try {
      // GoogleSignin.configure({ forceConsentPrompt: true });
      // await GoogleSignin.hasPlayServices();
      await GoogleSignin.signOut();
      let userInfo = await GoogleSignin.signIn();
      console.log('userInfo', userInfo)

      if (userInfo.user.name == null) {
        // await GoogleSignin.signOut();
        // setTimeout(async () => {
        //   let userInfosecond = await GoogleSignin.signIn();

        //   console.log('userInfosecond', userInfosecond);
        //   let checkEmail = (userInfosecond.user.name.includes('.com') && userInfosecond.user.name.includes('.'))
        //   if (userInfosecond.user.name != null && (checkEmail == false)) {
        //     this.setUser(userInfosecond.user);
        //   } else {
        // await GoogleSignin.signOut();
        // let userInfothird = await GoogleSignin.signIn();
        // console.log('userInfothird', userInfothird)
        Alert.alert('Oops', 'Somthing went wrong please try again later.');
        // }
        // }, 200);

      } else {
        this.setUser(userInfo.user);
      }
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('error.code', error.code + error);
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
        console.log('error.code', error.code + error);
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.log('error.code', error.code + error);
      } else {
        // some other error happened
        Alert.alert('Oops', 'Somthing went wrong please try again later.')
        console.log('error.code', error.code + error);
      }
    }
  }
  render() {
    return (
      <ImageBackground
        style={ { width: null, height: null, flex: 1 } }
        source={ require("../img/login_bg.png") }
      >
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <View style={ { marginTop: 70 } }>
          <Image
            style={ {
              width: 100,
              height: 100,
              alignSelf: "center",
              borderRadius: 100 / 2
            } }
            source={ require("../img/launch_logo.png") }
          />
        </View>

        {/* <ScrollView> */ }
        <View>
          <View style={ { marginTop: 70, width: "100%", alignSelf: "center" } }>
            {
              <TouchableOpacity
                activeOpacity={ 0.8 }
                style={ styles.fbViewStyle }
                onPress={ () => this.facebookLogin() }
              >
                <View style={ styles.fbTextStyle }>
                  <Image
                    style={ { width: 15, height: 15, alignSelf: "center" } }
                    source={ require("../img/facebook_icon.png") }
                  />
                  <TextWithLetterSpacing allowFontScaling={ false } spacing={ 3 } textStyle={ { color: '#3c579e' } }> LOGIN WITH FACEBOOK </TextWithLetterSpacing>
                </View>
              </TouchableOpacity>
            }
            <TouchableOpacity
              activeOpacity={ 0.8 }
              style={ [styles.fbViewStyle, { marginTop: 30 }] }
              onPress={ () => this.googleLogin() }
            >
              <View style={ styles.fbTextStyle }>
                <Image
                  style={ { width: 25, height: 25, alignSelf: "center", marginRight: 2 } }
                  source={ require("../img/google_plus_icon.png") }
                />
                <TextWithLetterSpacing allowFontScaling={ false } spacing={ 3 } textStyle={ { color: '#AF1313' } }> LOGIN WITH GOOGLE </TextWithLetterSpacing>
              </View>
            </TouchableOpacity>

            {
              // <View style={styles.loginfbMainViewStyle}>
              //    <TouchableOpacity
              //      activeOpacity={0.8}
              //      style={styles.loginViewStyle}
              //      onPress={() => this.props.navigation.navigate("SignInPage")}
              //    >
              //      <TextWithLetterSpacing
              //        spacing={3}
              //        style={{
              //          fontSize: 14,
              //          color: "#149a99",
              //          fontFamily: "Lato-Bold"
              //        }}
              //      >
              //        SIGN IN
              //      </TextWithLetterSpacing>
              //    </TouchableOpacity>
              //    <TouchableOpacity
              //      activeOpacity={0.8}
              //      style={styles.signUpViewStyle}
              //      onPress={() => this.props.navigation.navigate("SignUpPage")}
              //    >
              //      <TextWithLetterSpacing
              //        spacing={3}
              //        style={{
              //          fontSize: 14,
              //          color: "#149a99",
              //          fontFamily: "Lato-Bold"
              //        }}
              //      >
              //        SIGN UP
              //      </TextWithLetterSpacing>
              //    </TouchableOpacity>
              //  </View>
            }
          </View>
        </View>
        {/* </ScrollView> */ }
        <View style={ { position: "absolute", bottom: 25, alignSelf: "center" } }>
          <Text allowFontScaling={ false } style={ styles.byCreatingTextStyle }>
            By Creating an account, you are aggreeing to our
          </Text>
          <View style={ { flexDirection: "row", alignSelf: "center" } }>
            <TouchableOpacity activeOpacity={ 0.8 }
              onPress={ () => this.props.navigation.navigate("TermsofUse") }
            >
              <Text allowFontScaling={ false } style={ styles.termsTextStyle }> Terms Of Use </Text>
              <View
                style={ {
                  borderBottomColor: "#393939",
                  borderBottomWidth: 1,
                  marginLeft: 4,
                  marginRight: 4
                } }
              />
            </TouchableOpacity>
            <Text allowFontScaling={ false } style={ styles.byCreatingTextStyle }> and </Text>
            <TouchableOpacity activeOpacity={ 0.8 }
              onPress={ () => this.props.navigation.navigate("PrivacyPolicy") }
            >
              <Text allowFontScaling={ false } style={ styles.termsTextStyle }> Privacy Policy </Text>
              <View
                style={ {
                  borderBottomColor: "#393939",
                  borderBottomWidth: 1,
                  Width: "10%",
                  marginLeft: 4,
                  marginRight: 4
                } }
              />
            </TouchableOpacity>
          </View>
          <Loader
            loading={ this.props.isLoading }

          />
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  fbViewStyle: {
    borderColor: "#393939",
    borderWidth: 1.5,
    borderRadius: 6,
    height: 45,
    width: "80%",
    alignSelf: "center",
    justifyContent: "center",
    marginTop: 15
  },
  fbTextStyle: {
    flexDirection: "row",
    alignSelf: "center",
    justifyContent: "center",
    alignItems: 'center',
    color: "#3c579e"
  },
  loginWithFbTextStyle: {
    fontSize: 14,
    color: "#3c579e",
    fontFamily: "Lato-Black"
  },
  loginWithGoogleStyle: {
    fontSize: 14,
    color: "#af1313",
    fontFamily: "Lato-Black"
  },
  loginfbMainViewStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 15,
    width: "80%",
    alignSelf: "center"
  },
  loginViewStyle: {
    borderColor: "#149a99",
    borderRadius: 6,
    height: 40,
    width: "48%",
    borderWidth: 1.5,
    alignItems: "center",
    justifyContent: "center"
  },
  signUpViewStyle: {
    borderColor: "#149a99",
    borderRadius: 6,
    height: 40,
    width: "48%",
    borderWidth: 1.5,
    alignItems: "center",
    justifyContent: "center"
  },
  byCreatingTextStyle: {
    fontSize: 12,
    color: "#848484",
    fontFamily: "Lato-Regular"
  },
  termsTextStyle: {
    fontSize: 12,
    color: "#000000",
    fontFamily: "Lato-Regular"
  }
});

const mapStateToProps = state => {
  return {
    isLoading: state.Socaillogin.isLoading,
    authResult: state.Socaillogin.authResult,
    userSocialData: state.Socaillogin.userSocialData,
    authResultEmail: state.Socaillogin.authResultEmail,
    SplashauthToken: state.Splash.authToken,
  };
};
export default connect(mapStateToProps,
  {
    Socaillogin, setLoading, initialSocialLogin, checkUserExists
  })(SocialLoginPage);
