/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ProgressDialog, Dialog } from 'react-native-simple-dialogs';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  ScrollView,
  Dimensions
} from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import Bottom from './BottomNew';
import Header from './HeaderNew';
import ItemViewCategory from './ItemViewCategory';
import { getCategotyData, initialCatStateData } from '../Actions';


let brand_id = '';
let height = Dimensions.get('window').width * 1.25;
class CategoryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      header: '',
      isDownArrowVisible: true,
    };
  }

  componentWillMount() {
    this.props.initialCatStateData();
    brand_id = this.props.navigation.getParam('id');
    let title = this.props.navigation.getParam('title');
    this.setState({ header: (`${title} - category`).toUpperCase() });
  }
  componentDidMount() {
      this.props.getCategotyData({ brand_id });
  }
  onBackPress() {
    this.props.navigation.goBack(null);
  }
  BottomDown() {
        this.refs.scrollView.scrollToEnd(this.setState({ isDownArrowVisible: false }))
  }
  onPositionChanged(position) {
    if (position === (this.props.category.length - 1)) {
      console.log(position);
      this.setState({ isDownArrowVisible: false });
    } else {
      this.setState({ isDownArrowVisible: true });
    }
  }

  renderList() {
  return (
      <FlatList
            style={{ marginBottom: 50 }}
            data={this.props.category}
            ref="scrollView"
            onMomentumScrollEnd={(event) => {
              let position = Math.round(event.nativeEvent.contentOffset.y / height); //for vertical case
             console.log(position);
             this.onPositionChanged(position);
           }}
            renderItem={({ item: category }) => {
              return (
                <ItemViewCategory
                  onPress={() => this.props.navigation.navigate('ProductList', { id: category.id, title: category.name })}
                  category={category}

                />
              );
            }}
            keyExtractor={(item, index) => item.title}
      />
    );
}

  render() {
    return (
      <View style={styles.container} >
      <Header
          headerText={this.state.header}
          leftImage={require('../img/back_icon.png')}
          onLeftPressed={() => this.onBackPress()}
          searchImage={require('../img/search_icon.png')}
          storeImage={require('../img/bag_icon.png')}
          myNavigation={this.props.navigation}
      />

          {
            this.renderList()
          }

        {(this.state.isDownArrowVisible && this.props.category.length !== 1) ?
          <TouchableOpacity style={styles.downarrowstyle}
            onPress={() => this.BottomDown()}
          >
              <Image style={{width: 15, height: 15, }}
                      source={require('../img/download_arrow.png')}
              />
          </TouchableOpacity> : null
        }

        <Bottom
         HomeImage={require('../img/home_icon.png')}
         BagImage={require('../img/bag_icon.png')}
         UserImage={require('../img/user_icon.png')}
         NotificationImage={require('../img/notification1.png')}
         SettingImage={require('../img/setting_icon.png')}
         myNavigation={this.props.navigation}
         NavigationActions={NavigationActions}
         StackActions={StackActions}
        />
        <ProgressDialog
              visible={this.props.isLoading}
              title="Loading"
              message="Please, wait..."
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
downarrowstyle: { position: 'absolute',
  right: 10,
  bottom: 60,
   backgroundColor: 'white',
   borderRadius: 5,
    width: 40,
     height: 40,
     justifyContent: 'center',
     alignItems: 'center' }
});

const mapStateToProps = state => {
    return {
      category: state.category.category,
      authResult: state.category.authResult,
      isLoading: state.category.isLoading
    };
};

export default connect(mapStateToProps, { getCategotyData, initialCatStateData })(CategoryList);
