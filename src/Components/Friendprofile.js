/* @flow */

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  Modal,
  Alert
} from "react-native";
import Itemfriend from "./ItemfriendProfile";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import { connect } from "react-redux";
import { getMyFriend, getUnfriend, initialGetFriendsList } from "../Actions";
import { getUser } from "../Database/allSchema";
import { POSTIMAGEPATH, PROFILEIMAGEPATH } from "../Actions/type";
import ImageLoad from './ImageLoad';
import Loader from './Loader';

let userId = "";
let postID = "";

class Friendprofile extends Component {
  constructor (props) {
    super(props);
    this.state = {
      prifilePicDialogVisible: false,
      userPhoto: "",
      pageCount: 1,
      FriendData: [
        { name: "Erika Pluda", des: "@ErikaPluda" },
        { name: "Twnada Kernel", des: "@ErikaPluda" },
        { name: "Danial Pluda", des: "like you post" }
      ]
    };
  }

  componentWillReceiveProps(nextProp) {
    if (nextProp.authResultFriend === "UnFriend successfully") {
      // Alert.alert(
      //   "Success",
      //   "Unfriend successfully",
      //   [
      //     {
      //       text: "OK",
      //       onPress: () => {
      //         cancelable: false,
      //           this.props.getMyFriend(userId, this.state.page)
      //       }
      //     }
      //   ],
      //   { cancelable: false }
      // );
    } else if (nextProp.authResultFriend === "Not a Friend") {
      setTimeout(() => {
        Alert.alert("Oops", "Not a friend");

      }, 500);

    } else if (nextProp.authResultFriend === "User id must be numeric.") {
      setTimeout(() => {
        Alert.alert("Oops", "User id must be numeric.");

      }, 500);


    }
  }
  componentWillMount() {
    console.log(this.props.userId);
    this.fetchData();

    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.setState({ pageCount: 1 });
        this.fetchData();
      }
    );
  }
  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }


  fetchData = () => {
    // this.props.initialGetFriendsList();
    console.log(this.state.pageCount);
    this.props.getMyFriend({
      userId: this.props.userId,
      page: this.state.pageCount,
      token: this.props.SplashauthToken,
      isLoading: true,
      isMyProfile: false
    });
  };
  handleLoadMore = () => {
    this.setState(
      {
        pageCount: this.state.pageCount + 1
      },
      () => {
        console.log(this.state.pageCount);
        if (this.props.isdata) {
          this.props.getMyFriend({
            userId: this.props.userId,
            page: this.state.pageCount,
            friend: this.props.friend,
            token: this.props.SplashauthToken,
            isLoading: false,
            isMyProfile: false
          });
        }
      }
    );
  };


  // onUnfriend(item) {
  //   console.log(userId);
  //   console.log(item.friend_id);
  //   Alert.alert(
  //       'Alert',
  //       'Are you sure, you want to unfriend?',
  //       [
  //         // { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
  //         // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
  //         { text: 'Yes', onPress: () => this.props.getUnfriend(userId, item.friend_id)},
  //         { text: 'No', onPress: () => { cancelable: false } },
  //       ],
  //       { cancelable: false }
  //     )
  //
  // }

  ProfileRedirct(item) {
    console.log("profie click");
    getUser()
      .then(user => {
        if (user.length > 0) {
          let userId = user[0].id;
          console.log("sds", userId, item.friend_id);
          if (item.friend_id == userId) {
            console.log("Myprofile");
            console.log(this.props.navigation);

            this.props.navigation.navigate('MyProfile');
          }
          else {
            console.log("Profile");
            //  this.props.navigation.navigate("Aboutprofile", {
            //   user_id: item.friend_id
            // });
            this.props.navigation.goBack();
            this.props.navigation.navigate('Profile', {
              user_id: item.friend_id,
              form: 'MyProfile1'
            });
            // this.props.navigation.push('Profile', {
            //     user_id: item.friend_id
            //   })
          }

        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <View style={ { flex: 1, backgroundColor: "white" } }>
        { !this.props.isLoading && this.props.friend != null && this.props.friend.length > 0 ? (
          <FlatList
            style={ { marginBottom: 10 } }
            key={ this.props.friend.length - 1 }
            keyExtractor={ item => item.firebase }
            data={ this.props.friend }
            showsHorizontalScrollIndicator={ false }
            onEndReached={ this.handleLoadMore.bind(this) }
            onEndReachedThreshold={ 2 }
            renderItem={ ({ item, index }) => {
              return (
                <Itemfriend
                  fireId={ item.firebase }
                  name={ item.fullname }
                  userprofileImage={ {
                    uri: PROFILEIMAGEPATH + item.profile_image
                  } }
                  profilenameclick={ () => this.ProfileRedirct(item) }
                  onPressUserIcon={ () =>
                    this.setState({
                      prifilePicDialogVisible: true,
                      userPhoto: PROFILEIMAGEPATH + item.profile_image
                    })
                  }

                />
              );
            } }
          />
        ) : (
            <View
              style={ {
                justifyContent: "center",
                alignItems: "center",
                flex: 1
              } }
            >
              {/* <Image style={{ height: 130, width: 130}}
            source={require('../img/friends-not-available.jpg')} /> */}
              <Text style={ { fontSize: 13, fontFamily: "Lato-Regular", color: '#686868', } }>No friends to show</Text>
            </View>
          ) }

        <Modal transparent={ true }
          visible={ this.state.prifilePicDialogVisible }
          onRequestClose={ () => this.setState({ prifilePicDialogVisible: false }) }
        >
          <View
            style={ {
              height: "100%",
              width: "100%",
              paddingTop: 130,
              paddingBottom: 100
            } }
          >
            <View style={ { backgroundColor: 'rgba(0,0,0,0.9)', padding: 5, justifyContent: 'center', alignContent: 'center' } }>
              <View style={ { alignItems: "center", justifyContent: "center" } }>
                <ImageLoad
                  style={ { height: '100%', width: "100%" } }
                  source={ { uri: this.state.userPhoto } }
                  resizeMode="contain"
                />
                <TouchableOpacity
                  style={ styles.CloseViewSTyle }
                  onPress={ () =>
                    this.setState({ prifilePicDialogVisible: false })
                  }
                >
                  <Image
                    style={ { height: 18, width: 18 } }
                    source={ require("../img/close_icon.png") }
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
        <Loader
          loading={ this.props.isLoading } />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  CloseViewSTyle: {
    position: "absolute",
    right: 10,
    top: 10,
    backgroundColor: "rgba(255,0,0,0.5)",
    width: 32,
    height: 32,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  }
});
const mapStateToProps = state => {
  return {
    friend: state.Friend.profileFriend,
    authResult: state.Friend.authResult,
    authResultFriend: state.Friend.authResultFriend,
    isLoading: state.Friend.isLoading,
    isdata: state.Friend.isdata,
    SplashauthToken: state.Splash.authToken,
  };
};

export default connect(
  mapStateToProps,
  {
    getMyFriend,
    getUnfriend,
    initialGetFriendsList
  }
)(Friendprofile);
