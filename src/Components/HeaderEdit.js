import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform
} from 'react-native';
import DeviceInfo from 'react-native-device-info';

export default class HeaderEdit extends Component {
  render() {
    console.log(DeviceInfo.getModel());
    return (
      <View > 
        <View style={{ backgroundColor: "#CC181E", height: 15 }}></View>
        <View style={{ backgroundColor: "#CC181E", paddingTop: (DeviceInfo.getModel() === 'iPhone X' || DeviceInfo.getModel() === 'iPhone XS Max' || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 15 : 0 }}/>
      
      <View style={styles.container}>

                <TouchableOpacity
                  style={{ height: 50, width: 100, position: 'absolute',
                  justifyContent: 'center', left: 0 }}
                 onPress={this.props.onLeftPressed}
                 activeOpacity={0.2}
                >
                    <Image
                    style={styles.backArrowStyle}
                    source={this.props.leftImage}
                    />
                </TouchableOpacity>

                <Text allowFontScaling={false}  style={styles.textStyle}> {this.props.headertext} </Text>
           
                <TouchableOpacity
                 onPress={this.props.onPressDone}
                style={{ flexDirection: 'row', position: 'absolute', right: 0, height: 50, 
                justifyContent: 'center', alignItems: 'center', paddingRight: 10, paddingLeft: 10 }}
                >
                  <Text allowFontScaling={false}  style={styles.textStyleDone}>{this.props.textDone} </Text>
              </TouchableOpacity>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    backgroundColor: '#CC181E'
  },
  textStyle: {
    fontSize: 16,
    color: '#ffffff',
    fontFamily: 'Lato-Regular'
  },
  textStyleDone: {
    fontSize: 16,
    color: '#CCffffff',
    textAlign: 'center',
    fontFamily: 'Lato-Regular',

  },
  backArrowStyle: {
    height: 20,
    width: 20,
    marginLeft: 10
  }
});