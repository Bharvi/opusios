/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
//import Header from './Header';
import Header from './HeaderEdit';
import { connect } from "react-redux";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import { getUser } from "../Database/allSchema";
import { getMYAboutus } from "../Actions";
import Loader from './Loader';
import WebView from 'react-native-webview';

class AboutUs extends Component {

  componentDidMount() {
    this.props.getMYAboutus({ token: this.props.SplashauthToken });
  }

  OnPressed() {
    this.props.navigation.goBack(null);
  }

  render() {
    return (
      <View style={ styles.container }>
        {/* <Header
            onLeftPressed={() => this.OnPressed()}
            leftImage={require('../img/back_icon.png')}
            headertext="About Us"
          /> */}
        <Header
          leftImage={ require("../img/back_icon.png") }
          headertext={ "About Us" }
          onLeftPressed={ () => this.OnPressed() }
        />
        <WebView
          originWhitelist={ ['*'] }
          source={ { html: this.props.Data } }
          style={ { marginTop: 10, marginLeft: 10, marginRight: 10 } }
        />

        <Loader
          loading={ this.props.isLoading }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
});
const mapStateToProps = state => {
  return {
    Data: state.Aboutsus.Data,
    authResult: state.Aboutsus.authResult,
    isLoading: state.Aboutsus.isLoading,
    SplashauthToken: state.Splash.authToken,
  };
};

export default connect(
  mapStateToProps,
  {
    getMYAboutus
  }
)(AboutUs);

