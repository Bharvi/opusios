import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Alert,
  Platform
} from 'react-native';
import { connect } from 'react-redux';
import { emailChangeLogin,
  emailErrorChangeLogin,
  passwordChangeLogin,
  passwordErrorChangeLogin, initialLoginStateData,
  login
 } from '../Actions';
import Header from './Header'

import { ProgressDialog, Dialog } from 'react-native-simple-dialogs';
import { StackActions, NavigationActions } from 'react-navigation';


// const device_type = 0;
let device_type = Platform.OS === 'ios' ? 1 : 0;
let device_token = 1234555;

class SignInPage extends Component {

  componentDidMount() {
      this.props.initialLoginStateData();
      console.log(this.props);
  }
onEmailChange(text) {
 this.props.emailChangeLogin(text);
}
onPasswordChange(text) {
 this.props.passwordChangeLogin(text);
}
onBackPress() {
    this.props.navigation.goBack(null);
}

componentWillReceiveProps(nextProp) {
    //go to login
    if (nextProp.authResult === 'Success') {
      // Alert.alert('Login Successfully.');
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({
          routeName: 'BottomTab'
        })],
      });
      this.props.navigation.dispatch(resetAction);
    } else if (nextProp.authResult === 'Invalid Email or Password' || nextProp.authResult === 'Invalid Email or Password' || nextProp.authResult === 'Invalid Email or Password') {
     
      setTimeout(() => {
        Alert.alert(
          'Oops',
          'Invalid email or password');
      }, 500);
      
    }
  }


onButtonPress() {
    const { email, password } = this.props;
    let isValid = true;
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email === '') {
     isValid = false;
     this.props.emailErrorChangeLogin('Enter email');
   } else if (reg.test(email) === false) {
     isValid = false;
     this.props.emailErrorChangeLogin('Enter valid email');
   } else {
     this.props.emailErrorChangeLogin('');
   }
   if (password === '') {
     isValid = false;
     this.props.passwordErrorChangeLogin('Enter password');
   } else {
     this.props.passwordErrorChangeLogin('');
   }
   if (isValid === true) {
     // console.log(" Login Success");
      // this.props.navigation.navigate('BottomTab', { myProps: this.props });
      console.log(device_type);
      this.props.login(email, password, device_type);
   }
  }
  focusNextField(nextField) {
    this.refs[nextField].focus();
  }
  jumpLogin() {
  this.onButtonPress();
  }
  render() {
    return (
      <View>
      <Header
        leftImage={require('../img/back_icon.png')}
        headertext={'Sign In'}
        textDone={'Done'}
         //onPressDone={this.props.navigation.navigate('BottomTab', { myProps: this.props })}
        onPressDone={() => this.onButtonPress()}
        onLeftPressed={() => this.onBackPress()}
      />
      <StatusBar
       backgroundColor="#CC181E"
       barStyle="light-content"
      />
     <View style={{ margin: 20 }}>

         <View style={{ borderBottomWidth: 0.5, borderColor: '#261d1d26', marginBottom: 8 }}>
         <Text style={styles.labelTextStyle}> USERNAME </Text>
         <TextInput
              ref='1'
              underlineColorAndroid='transparent'
              returnKeyType="next"
              keyboardType="email-address"
              autoCapitalize="none"
              style={styles.textInputStyle}
              value={this.props.email}
              onChangeText={this.onEmailChange.bind(this)}
              onSubmitEditing={() => this.focusNextField('2')}
         />
        </View>

        <Text style={styles.errorTextStyle}>{ this.props.emailError }</Text>

        <View style={{ borderBottomWidth: 0.5, borderColor: '#261d1d26', marginTop: 8 }}>
         <Text style={styles.labelTextStyle}> PASSWORD </Text>
         <View style={{ flexDirection: 'row', width: '100%' }}>
         <TextInput
              ref='2'
              numberOfLines={1}
              underlineColorAndroid='transparent'
              returnKeyType='done'
              secureTextEntry
              keyboardType="default"
              autoCapitalize="none"
              style={styles.textInputPassStyle}
              value={this.props.password}
              onChangeText={this.onPasswordChange.bind(this)}
              onSubmitEditing={() => this.jumpLogin()}
         />
         <TouchableOpacity
         style={{ position: 'absolute', right: 10, top: 10 }}
         onPress={() => this.props.navigation.navigate('ForgotPassPage')}
         >
        <Text style={styles.forgotPassTextStyle}> Forgot Password </Text>
        </TouchableOpacity>
       </View>
      </View>
        <Text style={styles.errorTextStyle}>{ this.props.passwordError }</Text>

      <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 50 }}>
        <Text style={{ color: '#848484', fontSize: 14, fontFamily: 'Lato-Regular' }}> DON{"'"}T HAVE AN ACCOUNT? </Text>
       <View>
       <TouchableOpacity
        onPress={() => this.props.navigation.navigate('SignUpPage')}
         >
       <Text style={{ color: '#2c2c2c', fontSize: 14 }}>SIGN UP</Text>

       </TouchableOpacity>
       </View>
       </View>
     </View>
     <ProgressDialog
              visible={this.props.isLoading}
              title="Login"
              message="Please, wait..."
        />
    </View>
    );
  }
}
const styles = StyleSheet.create({
labelTextStyle: {
  fontSize: 14,
  color: '#848484',
  fontFamily: 'Lato-Regular',
},
textInputStyle: {
  color: '#2c2c2c',
  fontFamily: 'Lato-Regular',
  paddingLeft: 10,
  fontSize: 16,
  height: 40
},
textInputPassStyle: {
  color: '#2c2c2c',
  fontFamily: 'Lato-Regular',
  paddingLeft: 10,
  fontSize: 16,
  width: '60%',
  height: 40
},
forgotPassTextStyle: {
  fontSize: 14,
  color: '#CC181E',
  fontFamily: 'Lato-Regular',
},
errorTextStyle: {
 fontSize: 14,
 color: '#F00',
 textAlign: 'right',
 marginRight: 10
},
});

const mapStateToProps = state => {
    return {
      email: state.login.email,
      emailError: state.login.emailError,
      password: state.login.password,
      passwordError: state.login.passwordError,
      isLoading: state.login.isLoading,
      authResult: state.login.authResult,
    };
};
export default connect(mapStateToProps,
  {
    emailChangeLogin,
    emailErrorChangeLogin,
    passwordChangeLogin,
    passwordErrorChangeLogin,
   initialLoginStateData,
   login
  })(SignInPage);
