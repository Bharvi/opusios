import * as React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions,
  StatusBar,
  Modal,
  Platform
} from "react-native";
import Testimonials from "./Testimonials";
import Friend from "./Friend";
import Posts from "./Posts";
import About from "./About";
import Tag from "./Tag";
import { getUser } from "../Database/allSchema";
import { connect } from "react-redux";
import { onBottomTabChange, getProfile, initialMyprofileStateData, getMyFriend } from "../Actions";
import { POSTIMAGEPATH } from "../Actions/type";
import ImageLoad from './ImageLoad';
import UserProfileImg from '../Components/common/UserProfileImg';

import DeviceInfo from 'react-native-device-info';

let tag_type = "";
let userId = "";

class MyProfile extends React.Component {
  renderProgess() {
    let backgroundColor = "#1bc713";
    if ((this.props.Data.avg_user_rating * 3) < (4 * 3)) {
      backgroundColor = "red";
    } else if (
      (this.props.Data.avg_user_rating * 3) >= (4 * 3) &&
      (this.props.Data.avg_user_rating * 3) < (6 * 3)
    ) {
      backgroundColor = "yellow";
    }
    let progressArray = [];
    for (let i = 0; i < (this.props.Data.avg_user_rating * 3); i++) {
      progressArray.push(i);
    }
    return progressArray.map(number => (
      <View
        style={{
          backgroundColor: backgroundColor,
          width: 5.1,
          height: 8,
          borderRadius: 2,
          marginRight: 1
        }}
      />
    ));
  }

  static title = "Scrollable top bar";
  static backgroundColor = "#ffffff";
  static appbarElevation = 0;

  state = {
    type: 0,
    prifilePicDialogVisible: false,
    username: "",
    userphoto: ""
  };

  componentWillMount() {
    this.props.initialMyprofileStateData()
    getUser()
      .then(user => {
        if (user.length > 0) {
          userId = user[0].id;
          this.props.getProfile({ userId, toUserID: '', token: this.props.SplashauthToken });
          this.props.getMyFriend({
            userId,
            page: 1,
            token: this.props.SplashauthToken,
            isLoading: false,
            isMyProfile: true
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
    this.props.navigation.addListener("willFocus", payload => {
      this.getData();
    });
    this.props.navigation.addListener("didFocus", payload => {

      setTimeout(() => {
        console.log("search didFocus");
        this.props.onBottomTabChange({ prop: "issearchseleted", value: false });
        this.props.onBottomTabChange({ prop: "isuserseleted", value: true });
        this.props.onBottomTabChange({ prop: "isfeedseleted", value: false });
        this.props.onBottomTabChange({ prop: "ischatseleted", value: false });
      }, 0);
    });

    this.props.navigation.addListener("willBlur", payload => {
      console.log("search willBlur");
      this.props.onBottomTabChange({ prop: "issearchseleted", value: false });
      this.props.onBottomTabChange({ prop: "isuserseleted", value: false });
      this.props.onBottomTabChange({ prop: "isfeedseleted", value: false });
      this.props.onBottomTabChange({ prop: "ischatseleted", value: false });
    });
  }

  componentWillUnMount() {
  }

  getData() {
    // this.setState({ type: 0 });
    fullname = this.props.navigation.getParam("fullname");
    user_photo = this.props.navigation.getParam("profile_image");
    post_type = this.props.navigation.getParam("posttype");
    friend_type = this.props.navigation.getParam("friendtype");
    tag_type = this.props.navigation.getParam("tagtype");
    if (tag_type === "tagtype") {
      this.setState({ type: 1 });
    } else if (post_type === "posttype") {
      this.setState({ type: 3 });
    } else if (friend_type === "friendtype") {
      this.setState({ type: 4 });
    }

  }

  renderPage() {
    if (this.state.type === 0) {
      console.log("About");
      return <About navigation={this.props.navigation} />;
    } else if (this.state.type === 1) {
      console.log("Tag");
      return <Tag navigation={this.props.navigation} />;
    } else if (this.state.type === 2) {
      console.log("Testimonials");
      return <Testimonials navigation={this.props.navigation} />;
    } else if (this.state.type === 3) {
      console.log("Posts");
      return <Posts myNavigation={this.props.navigation} />;
    } else if (this.state.type === 4) {
      console.log("Friend");
      return <Friend navigation={this.props.navigation} />;
    }
    return null;
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ backgroundColor: "white", height: 15 }}></View>
        <View style={{ backgroundColor: "white", paddingTop: (DeviceInfo.getModel() === 'iPhone X' || DeviceInfo.getModel() === 'iPhone XS Max' || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 15 : 0 }} />
        <View style={styles.hedarStyleView}>
          <View style={{ height: 50, width: 50, alignItems: 'center', alignSelf: 'center', justifyContent: 'center' }} />

          <Text allowFontScaling={false} style={styles.textStyle}>My Profile</Text>
          <TouchableOpacity
            activeOpacity={0.8}
            style={{ height: 50, width: 50, alignItems: 'center', justifyContent: 'center', alignContent: 'center' }}
            onPress={() => this.props.navigation.navigate("Settings")}
          >
            <Image
              style={styles.setingStyle}
              source={require("../img/paddin_setting.png")}
            />
          </TouchableOpacity>
        </View>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <View
          style={{
            flexDirection: "row",
            backgroundColor: "white",
            padding: 10,
            marginBottom: 10
          }}
        >
          <TouchableOpacity
            style={{
              flex: 2.7,
              justifyContent: "center",
              alignItems: "center"
            }}
            activeOpacity={0.8}
            onPress={() => this.setState({ prifilePicDialogVisible: true })}
          >
            {/* {(this.props.isLoading) ? 
            <ImageLoad
              style={{ height: 90, width: 90, borderRadius: 250 / 2}}
              borderRadius={90 / 2} 
              source={{ uri: POSTIMAGEPATH + this.props.Data.user_image }} /> 
            :
            <Image
              style={{ height: 90, width: 90, borderRadius: 250 / 2}}
              borderRadius={90 / 2} 
              source={{ uri: POSTIMAGEPATH + this.props.Data.user_image }} /> 
            } */}
            <UserProfileImg
              style={{ height: 90, width: 90, borderRadius: 90 / 2 }}
              borderRadius={90 / 2}
              source={{ uri: POSTIMAGEPATH + this.props.Data.user_image }}
              dotSize={1}
              fireId={null}
              profile={true}
            />

          </TouchableOpacity>
          <View
            style={{
              flex: 5,
              paddingLeft: 10,
              paddingRight: 10,
              paddingTop: 20,
              justifyContent: "center"
            }}
          >
            <Text allowFontScaling={false} style={styles.nameTextStyle}>
              {this.props.Data.user_name}
            </Text>
            <View style={{ flex: 7, marginTop: 5, marginBottom: 1 }}>
              <View
                style={{
                  backgroundColor: "#DBDBDB",
                  width: 180,
                  height: 8,
                  borderRadius: 3
                }}
              />
              <View
                style={{
                  width: this.props.Data.avg_user_rating,
                  position: "absolute",
                  height: 8,
                  flexDirection: "row"
                }}
              >
                {this.renderProgess()}
              </View>
              <Text allowFontScaling={false} style={styles.userTextStyle}>
                {(this.props.Data.user_rating !== 0) ? (this.props.Data.user_rating + ' Users') : 'No ratings'}
              </Text>
            </View>
          </View>
        </View>
        <View style={{ marginBottom: 40, flex: 1 }}>
          <View style={{ height: 45, backgroundColor: "white", borderBottomWidth: 1, borderColor: '#e2e0e0' }}>
            <ScrollView
              ref={(ref) => this.scrollView = ref}
              horizontal
              showsHorizontalScrollIndicator={false}
            >
              <TouchableOpacity
                style={[
                  styles.tabbarStyleView,
                  {
                    borderBottomWidth: this.state.type === 0 ? 1 : 0,
                    borderBottomColor: "#ff0000"
                  }
                ]}
                onPress={() => {
                  this.scrollView.scrollTo({ x: 0, y: 0, animated: true });
                  this.setState({ type: 0 });
                }}
              >
                <Text
                  allowFontScaling={false}
                  style={[
                    styles.label,
                    { color: this.state.type === 0 ? "#ff0000" : "#7F7F7F" }
                  ]}
                >
                  About
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.tabbarStyleView,
                  {
                    borderBottomWidth: this.state.type === 1 ? 1 : 0,
                    borderBottomColor: "#ff0000"
                  }
                ]}
                onPress={() => {
                  this.scrollView.scrollTo({ x: 0, y: 0, animated: true });
                  this.setState({ type: 1 });
                }}
              >
                <Text
                  allowFontScaling={false}
                  style={[
                    styles.label,
                    { color: this.state.type === 1 ? "#ff0000" : "#7F7F7F" }
                  ]}
                >
                  Tags
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.tabbarStyleView,
                  {
                    borderBottomWidth: this.state.type === 2 ? 1 : 0,
                    borderBottomColor: "#ff0000"
                  }
                ]}
                onPress={() => this.setState({ type: 2 })}
              >
                <Text
                  allowFontScaling={false}
                  style={[
                    styles.label,
                    { color: this.state.type === 2 ? "#ff0000" : "#7F7F7F" }
                  ]}
                >
                  Testimonials
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.tabbarStyleView,
                  {
                    borderBottomWidth: this.state.type === 3 ? 1 : 0,
                    borderBottomColor: "#ff0000"
                  }
                ]}
                onPress={() => {
                  this.setState({ type: 3 })
                  this.scrollView.scrollToEnd()
                }}
              >
                <Text
                  allowFontScaling={false}
                  style={[
                    styles.label,
                    { color: this.state.type === 3 ? "#ff0000" : "#7F7F7F" }
                  ]}
                >
                  Posts
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.tabbarStyleView,
                  {
                    borderBottomWidth: this.state.type === 4 ? 1 : 0,
                    borderBottomColor: "#ff0000"
                  }
                ]}
                onPress={() => {
                  this.setState({ type: 4 })
                  this.scrollView.scrollToEnd()
                }}
              >
                <Text
                  allowFontScaling={false}
                  style={[
                    styles.label,
                    { color: this.state.type === 4 ? "#ff0000" : "#7F7F7F" }
                  ]}
                >
                  Friends
                </Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <View
            style={{
              flex: 1
            }}
          >
            {this.renderPage()}
          </View>
          <Modal
            transparent={true}
            visible={this.state.prifilePicDialogVisible}
            onRequestClose={() =>
              this.setState({ prifilePicDialogVisible: false })
            }
          >
            <View
              style={{
                height: "100%",
                width: "100%",
                paddingTop: 130,
                paddingBottom: 100
              }}
            >
              <View
                style={{
                  backgroundColor: "rgba(0,0,0,0.9)",
                  padding: 5,
                  justifyContent: "center",
                  alignContent: "center"
                }}
              >
                <View
                  style={{ alignItems: "center", justifyContent: "center" }}
                >
                  <ImageLoad
                    style={{ width: "100%", height: "100%" }}
                    source={{ uri: POSTIMAGEPATH + this.props.Data.user_image }}
                    resizeMode="contain"
                  />
                  <TouchableOpacity
                    style={styles.CloseViewSTyle}
                    onPress={() =>
                      this.setState({ prifilePicDialogVisible: false })
                    }
                  >
                    <Image
                      style={{ height: 18, width: 18 }}
                      source={require("../img/close_icon.png")}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#DBDBDB"
  },
  tabbar: {
    backgroundColor: "#ffffff"
  },
  tab: {
    width: 120
  },
  indicator: {
    backgroundColor: "#ff000b"
  },
  label: {
    color: "#7F7F7F",
    fontSize: 14,
    fontFamily: "Lato-Heavy"
  },
  hedarStyleView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    height: 50,
    backgroundColor: "white"
  },
  textStyle: {
    fontSize: 16,
    color: "#4d4e51",
    fontFamily: "Lato-Bold"
  },
  nameTextStyle: {
    fontSize: 16,
    color: "#CC181E",
    fontFamily: "Lato-Medium"
  },
  backArrowStyle: {
    height: 18,
    width: 18,
    tintColor: "white",
    padding: 8
  },
  setingStyle: {
    height: 25,
    width: 25,
    tintColor: "#4d4e51",
  },
  userTextStyle: {
    color: "#828282",
    fontFamily: "Lato-Medium",
    fontSize: 14,
    marginLeft: 3
  },
  tabbarStyleView: {
    height: 40,
    width: 100,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center"
  },
  CloseViewSTyle: {
    position: "absolute",
    right: 2,
    top: 0,
    backgroundColor: "rgba(255,0,0,0.5)",
    width: 32,
    height: 32,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  }
});

const mapStateToProps = state => {
  return {
    Data: state.About.Data,
    authResult: state.About.authResult,
    isLoading: state.About.isLoading,
    SplashauthToken: state.Splash.authToken
  };
};
export default connect(
  mapStateToProps,
  {
    onBottomTabChange,
    getProfile,
    getMyFriend,
    initialMyprofileStateData
  }
)(MyProfile);
