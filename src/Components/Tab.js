import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, Dimensions, ImageBackground } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';

let { width } = Dimensions.get('window');

let boxCount = 4;
let boxWidth = width / boxCount;

const navigateToScreen = (route) => () => {
  let resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: route })],
      });
  this.props.navigation.dispatch(resetAction);
};

export default class Bottom extends Component {

  constructor(props) {
    super(props);
    this.state = {
          issearchseleted: false,
          isuserseleted: false,
          isfeedseleted: false,
          ischatseleted: false,
    };
  }
  onPressTimeView() {
    this.setState({ issearchseleted: true });
    this.setState({ ischatseleted: false });
    this.setState({ isfeedseleted: false });
    this.setState({ isuserseleted: false });
  }
  onChat() {
    this.setState({ ischatseleted: true });
    this.setState({ issearchseleted: false });
    this.setState({ isfeedseleted: false });
    this.setState({ isuserseleted: false });
  }
  onfeed() {
    this.setState({ isfeedseleted: true });
    this.setState({ ischatseleted: false });
    this.setState({ issearchseleted: false });
    this.setState({ isuserseleted: false });
  }
  onUser() {
    this.setState({ isuserseleted: true });
    this.setState({ isfeedseleted: false });
    this.setState({ ischatseleted: false });
    this.setState({ issearchseleted: false });
  }
  render() {
   const { container, verticalViewline, viewDivideStyle, cartImageStyle, SelectedviewDivideStyle } = styles;
		return (
        <View style={container}>
            <View style={{ flex: 5, flexDirection: 'row' }}>

                {(this.state.issearchseleted) ?
                    <TouchableOpacity
                      style={SelectedviewDivideStyle}
                    >
                     <Image
                           style={{ height: 30, width: 30, tintColor: '#b9bdc0', }}
                           source={this.props.SearchImage}
                     />
                     </TouchableOpacity> :
                     <TouchableOpacity
                       style={viewDivideStyle}
                       onPress={() => this.onPressTimeView()}
                     >
                      <Image
                            style={{ height: 30, width: 30, tintColor: '#b9bdc0', }}
                            source={this.props.SearchImage}
                      />
                  </TouchableOpacity>
                }

                {(this.state.isuserseleted) ?
                  <TouchableOpacity
                  style={SelectedviewDivideStyle}
                  >
                       <Image style={cartImageStyle} source={this.props.UserImage} />

                  </TouchableOpacity> :
                  <TouchableOpacity
                  style={viewDivideStyle}
                  onPress={() => this.onUser()}
                  >
                       <Image style={cartImageStyle} source={this.props.UserImage} />

                  </TouchableOpacity>
                }

                {(this.state.isfeedseleted) ?
                  <TouchableOpacity
                   style={SelectedviewDivideStyle}
                  >
                          <Image
                              style={{ height: 30, width: 30, tintColor: '#b9bdc0' }}
                              source={this.props.feedImage}
                          />
                 </TouchableOpacity> :
                 <TouchableOpacity
                  style={viewDivideStyle}
                  onPress={() => this.onfeed()}
                 >
                         <Image
                             style={{ height: 30, width: 30, tintColor: '#b9bdc0' }}
                             source={this.props.feedImage}
                         />
                </TouchableOpacity>
                }
                {(this.state.ischatseleted) ?
                  <TouchableOpacity
                   style={SelectedviewDivideStyle}
                  >
                         <Image
                         style={{ height: 30, width: 30, tintColor: '#b9bdc0' }}
                         source={this.props.commentImage}
                         />
                  </TouchableOpacity> :
                  <TouchableOpacity
                   style={viewDivideStyle}
                   onPress={() => this.onChat()}
                  >
                         <Image
                         style={{ height: 30, width: 30, tintColor: '#b9bdc0' }}
                         source={this.props.commentImage}
                         />
                  </TouchableOpacity>
                }
            </View>
     </View>
			);
    }
}

const styles = {
  container: {
    backgroundColor: '#ffffff',
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 50,
    flex: 5
  },
 viewDivideStyle: {
     flexDirection: 'row',
     flex: 1,
     alignItems: 'center',
     justifyContent: 'center',
     width: boxWidth,
 },
 SelectedviewDivideStyle: {
   flexDirection: 'row',
   flex: 1,
   alignItems: 'center',
   justifyContent: 'center',
   width: boxWidth,
   backgroundColor: '#149a99'
 },
verticalViewline: {
	height: 28,
    borderWidth: 0.3,
    justifyContent: 'center',
    borderColor: '#fff',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10
   },

   notiBadgeStyle: {
     position: 'absolute',
     right: 26,
     top: 13,
     backgroundColor: 'red',
     borderRadius: 2,
     width: 12,
     height: 12,
     justifyContent: 'center',
     alignItems: 'center',
   },
   cartImageStyle: {
     height: 30,
     width: 30,
     alignSelf: 'center',
     alignItems: 'center',
     justifyContent: 'flex-end',
     tintColor: '#b9bdc0'
   }
};
