/* @flow */
import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  ScrollView,
  StatusBar,
  Modal,
  Alert,
  Platform,
  ActivityIndicator,
  Dimensions,
  SafeAreaView,
  RefreshControl
} from "react-native";
import { ProgressDialog, Dialog } from 'react-native-simple-dialogs';
import GestureRecognizer, {
  swipeDirections
} from "react-native-swipe-gestures";
import { connect } from "react-redux";
import {
  getSearchList,
  initialgetSearchListStateData,
  onBottomTabChange,
  getAddfriend,
  saveUserChat,
  clerchatnotification,
  getMyChatList
} from "../Actions";
import { getUser, getAppHintData, insertAppHint } from "../Database/allSchema";
import { POSTIMAGEPATH, PROFILEIMAGEPATH } from "../Actions/type";
import ItemSearchUpdate from "./ItemSearchUpdate";
import Itemsuggestedprofile from "./Itemsuggestedprofile";
import ImageLoad from "./ImageLoad";
import Loader from './Loader';
import Autocomplete from 'react-native-autocomplete-input';
let ImagePath = "";
let userId = "";
let userFireId = "";
import DeviceInfo from 'react-native-device-info';
// import { SafeAreaView } from "react-navigation";

const config = {
  velocityThreshold: 0.3,
  directionalOffsetThreshold: 80
};

class Search extends Component {
  constructor(props) {
    super(props);
    this.getappIntro()
    this.state = {
      refreshing: false,
      modalVisible: false,
      prifilePicDialogVisible: false,
      isHeaderVisible: true,
      isScrollUp: false,
      isScrollDown: false,
      searchText: "",
      loading: false,
      data: [],
      page: 1,
      isBottomVisible: false,
      userPhoto: "",
      isFirst: true
    };
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });

    this.setState({ searchText: '' });
    // this.props.initialgetSearchListStateData();
    getUser()
      .then(user => {
        this.setState({ refreshing: false });

        if (user.length > 0) {
          this.setState({ page: 1 });
          userId = user[0].id;
          console.log('id', userId);
          console.log('SplashauthToken', this.props.SplashauthToken);
          this.fetchData(false, true);
        }
      })
      .catch(error => {
        this.setState({ refreshing: false });
        console.log(error);
      });
  }
  onPositionChanged(position) {
    // if (position === (this.state.SuggestedProfile.length - 1 )) {
    //   console.log(position);
    // this.setState({ isHeaderVisible: false });
    // } else {
    // this.setState({ isHeaderVisible: true });
    // }
  }

  componentDidMount() {

    // getUser()
    //   .then(user => {
    //     if (user.length > 0) {
    //       this.setState({ page: 1 });
    //       userId = user[0].id;
    //       userFireId = user[0].fire_id;
    //       console.log(userId);
    //       this.fetchData(true, false);

    //       this.props.getMyChatList(
    //         userId,
    //         this.props.SplashauthToken,
    //         userFireId,
    //         1,
    //         [],
    //         false
    //       );
    //     }
    //   })
    //   .catch(error => {

    //     console.log(error);
    //   });


  }

  getappIntro = () => {
    //open app introduction model if user login

    getAppHintData()
      .then(data => {
        if (data && data.length > 0) {
          this.apiCall()
          if (data[0].isVisibleHint == 'true') {
            this.setState({ modalVisible: false }, () => {

            });
          }
        } else {
          this.setState({ modalVisible: true }, () => {
            const appHintData = {
              id: 1,
              isVisibleHint: 'false',
              isChatDotVisible: 'true',
            }
            insertAppHint(appHintData).then(() => {
              console.log('Insert Suceess appHintData');
            }).catch((error) => {
              // console.warn('err', error)
              console.log(`Insert error${error}`);
            });
          });

        }
      })
      .catch(error => {
        console.warn('main', error);
        console.log('modal open err', error);
        this.apiCall()
      });

    this.setState({ isFirst: false })
  }

  apiCall = () => {
    getUser()
      .then(user => {
        if (user.length > 0) {
          this.setState({ page: 1 });
          userId = user[0].id;
          userFireId = user[0].fire_id;
          console.log(userId);

          this.fetchData(true, false);

          this.props.getMyChatList(
            userId,
            this.props.SplashauthToken,
            userFireId,
            1,
            [],
            false
          );

        }
      })
      .catch(error => {

        console.log(error);
      });
  }

  // componentWillReceiveProps(nextProp) {
  //   console.log('this.state.isFirst', this.state.isFirst)
  //   if (nextProp.suggested_profiles.length >= 1) {
  //     if (this.state.isFirst) {
  //       this.getappIntro()
  //     }
  //   }
  // }

  componentWillMount() {




    this.setState({ searchText: '' });
    // this.props.initialgetSearchListStateData();


    this.props.navigation.addListener("didFocus", payload => {
      setTimeout(() => {
        console.log("search didFocus");
        this.props.onBottomTabChange({ prop: "issearchseleted", value: true });
        this.props.onBottomTabChange({ prop: "isuserseleted", value: false });
        this.props.onBottomTabChange({ prop: "isfeedseleted", value: false });
        this.props.onBottomTabChange({ prop: "ischatseleted", value: false });
      }, 0);
    });
    this.props.navigation.addListener("willBlur", payload => {
      // console.log("search willBlur");
      this.props.onBottomTabChange({ prop: "issearchseleted", value: false });
      this.props.onBottomTabChange({ prop: "isuserseleted", value: false });
      this.props.onBottomTabChange({ prop: "isfeedseleted", value: false });
      this.props.onBottomTabChange({ prop: "ischatseleted", value: false });
    });

  }

  // componentWillReceiveProps(nextProp) {

  //   if (nextProp.authResultAddFriend === "Success Addfriend") {
  //     console.log("Success");
  //   } else if (nextProp.authResultAddFriend === "Already Friend") {
  //     // Alert.alert("Oops", "Already Friend");
  //     console.log("Already Friend");
  //   }
  //   else if (nextProp.authResultAddFriend === "User id must be numeric.") {
  //     setTimeout(() => {
  //       Alert.alert("Oops", "User id must be numeric.");
  //     }, 500);


  //   }

  // }

  find(searchText) {
    // console.log('Array get', this.props.searchArray);
    if (searchText === '') {
      return [];
    }
    const regex = new RegExp('^' + searchText, 'i');
    let filterData = this.props.searchArray.filter((item) => item.search(regex) >= 0)
    return filterData;
  }

  onSearchResult(item) {
    this.setState({ searchText: item });
  }
  onSearchResultRedirect(item) {
    this.setState({ searchText: item });

    if (/^(\w+\s?)*\s*$/.test(this.state.searchText)) {
      if (this.state.searchText.replace(/(^\s+|\s+$)/g, '') !== '') {
        this.setState({ searchText: '' });
        console.log('search State', this.state.searchText);
        this.props.navigation.navigate('SearchResult', {
          searchText: item
        });
      }
    } else {
      // Alert.alert('Alert', 'Please enter valid character to search.');
      setTimeout(() => {
        Alert.alert('Alert', 'Please enter valid character to search.');
      }, 500);

    }
  }
  onSearchRedirect() {
    if (/^(\w+\s?)*\s*$/.test(this.state.searchText)) {
      if (this.state.searchText.replace(/(^\s+|\s+$)/g, '') !== '') {
        this.setState({ searchText: '' });
        console.log('search State', this.state.searchText);
        this.props.navigation.navigate('SearchResult', {
          searchText: this.state.searchText
        });
      }
    } else {
      // Alert.alert('Alert', 'Please enter valid character to search.');
      setTimeout(() => {
        Alert.alert('Alert', 'Please enter valid character to search.');
      }, 500);

    }
  }



  fetchData = (isLoading, isRefreshing) => {
    console.log("Splash Login token", this.props.SplashauthToken);
    this.props.getSearchList({
      userId,
      page: this.state.page,
      token: this.props.SplashauthToken,
      suggested_profiles: this.props.suggested_profiles,
      search_updates: this.props.search_updates,
      isLoading,
      isRefreshing
    });
  };

  handleLoadMore = () => {
    if (this.props.isdata) {
      this.setState(
        {
          page: this.state.page + 1
        },
        () => {
          console.log(this.state.page);
          this.props.getSearchList({
            userId,
            page: this.state.page,
            suggested_profiles: this.props.suggested_profiles,
            search_updates: this.props.search_updates,
            token: this.props.SplashauthToken,
            isLoading: false,
            isRefreshing: false
          });
        }
      );
    }
  };

  // BottomDown() {
  //   this.refs.scrollView.scrollToEnd(this.setState({ isHeaderVisible: false }))
  //   console.log('isHeaderVisible: false');
  // }

  onSwipeDown(gestureState) {
    console.log("onSwipeLeft" + this.state.type);
    this.setState({ isHeaderVisible: true });
  }

  onSwipeUp(gestureState) {
    console.log("onSwipeRight" + this.state.type);
    this.setState({ isHeaderVisible: false });
  }
  onProfileData(item) {
    console.log(item);
    this.props.navigation.navigate("Profile", {
      user_id: item.user_id,
      fullname: item.fullname,
      profile_image: item.profile_image,
      average_rating: item.average_rating,
      user_give_rating_count: item.user_give_rating_count,
      form: 'MyProfile'
    });
  }

  onChatMessage(item) {
    this.props.saveUserChat(item.user_id, item.fullname, item.firebase);
    this.props.clerchatnotification(userId, item.user_id, this.props.SplashauthToken);
    setTimeout(() => {
      this.props.navigation.navigate("ChatMessage", {
        toUserId: item.user_id,
        fullname: item.fullname,
        to_fire_id: item.firebase
      });
    }, 500);
  }


  render() {
    const { searchText } = this.state;
    const filterSearch = this.find(searchText);
    var uniqueArray = [...new Set(filterSearch)]
    const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
    // const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    //   const paddingToBottom = 20;
    //   return layoutMeasurement.height + contentOffset.y >=
    //     contentSize.height - paddingToBottom;
    // };
    let isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
      const paddingToBottom = 50
      return layoutMeasurement.height + contentOffset.y >=
        contentSize.height - paddingToBottom
    }
    return (
      <View style={styles.container}>
        <View style={{ backgroundColor: "#CC181E", height: 14 }}></View>
        <View style={{ backgroundColor: "#CC181E", paddingTop: (DeviceInfo.getModel() === 'iPhone X' || DeviceInfo.getModel() === 'iPhone XS Max' || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 15 : 0 }} />

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setState({ modalVisible: false }, () => {
              if (this.props.suggested_profiles.length == 0) {
                this.apiCall()
              }
            })
          }}>
          <TouchableOpacity
            onPress={() => {
              this.setState({ modalVisible: false }, () => {
                if (this.props.suggested_profiles.length == 0) {
                  this.apiCall()
                }
              })
            }}
            style={styles.containerModal}>
            <View style={styles.mainView}>

              <View style={styles.txtView}>
                <Text allowFontScaling={false} style={styles.textStyle}>Search & Connect to Penpals</Text>
                <Text allowFontScaling={false} style={styles.textStyle2}>Create profile for Penpals to search you</Text>
                <Text allowFontScaling={false} style={styles.textStyle3}>Build everlasting friendships by staying updated</Text>
                <Text allowFontScaling={false} style={styles.textStyle4}>Chat,Rate & Testify your Penpals</Text>
              </View>


              <View style={styles.lineView}>

                <View style={styles.line1}></View>
                <View style={styles.line2}></View>
                <View style={styles.line3}></View>
                <View style={styles.line4}></View>

              </View>

              <View style={styles.lineView}>
                <Image
                  style={styles.arrowStyle}
                  source={require('../img/up-arrow.png')}
                />
                <Image
                  style={styles.arrowStyle1}
                  source={require('../img/up-arrow.png')}
                />
                <Image
                  style={styles.arrowStyle2}
                  source={require('../img/up-arrow.png')}
                />
                <Image
                  style={styles.arrowStyle3}
                  source={require('../img/up-arrow.png')}
                />
              </View>



              <View style={styles.bottomView}>

                <View style={styles.bigRoundView}>
                  <View style={styles.roundView}>
                    <Image
                      style={styles.iconImg}
                      source={require('../img/search_icon.png')}
                    />
                  </View>
                </View>

                <View style={styles.bigRoundView}>
                  <View style={styles.roundView}>
                    <Image
                      style={styles.iconImg}
                      source={require('../img/user_icon.png')}
                    />
                  </View>
                </View>

                <View style={styles.bigRoundView}>
                  <View style={styles.roundView}>
                    <Image
                      style={styles.iconImg}
                      source={require('../img/note_icon.png')}
                    />
                  </View>
                </View>

                <View style={styles.bigRoundView}>
                  <View style={styles.roundView}>
                    <Image
                      style={styles.iconImg}
                      source={require('../img/comment_icon.png')}
                    />
                  </View>
                </View>
              </View>
            </View>
          </TouchableOpacity>

        </Modal>



        {
          this.state.isHeaderVisible &&
            this.props.suggested_profiles.length !== 1 ? (
              <View style={{ backgroundColor: "#CC181E", }}>
                <View style={styles.viewStyle}>
                  <TouchableOpacity
                    style={{ height: 40, width: 20, backgroundColor: '#b7151b', marginTop: 1, borderTopLeftRadius: 3, borderBottomLeftRadius: 3, left: 1, }}
                  >
                    <Image
                      source={require("../img/search_icon.png")}
                      style={styles.searchImageStyle}
                    />
                  </TouchableOpacity>

                  <Autocomplete
                    allowFontScaling={false}
                    fontSize={14}
                    autoCapitalize="none"
                    autoCorrect={false}
                    underlineColorAndroid="transparent"
                    style={styles.autocompleteStyle}
                    containerStyle={{ zIndex: 1000 }}
                    inputContainerStyle={styles.autoInputContainerStyle}
                    data={uniqueArray.length === 1 && comp(searchText, uniqueArray[0]) ? [] : uniqueArray}
                    defaultValue={searchText}
                    width={Dimensions.get('window').width - 40}
                    onChangeText={(item) => this.setState({ searchText: item })}
                    onSubmitEditing={() => this.onSearchRedirect()}
                    placeholder="Search penpals by tags/profession/locations"
                    placeholderTextColor="#DDDEE4"
                    placeholderTextAlign="center"
                    paddingLeft={10}
                    listContainerStyle={{}}
                    listStyle={styles.autoListStyle}
                    renderItem={(item) => {
                      console.log('item', item);
                      return (
                        <TouchableOpacity
                          activeOpacity={1}
                          style={{ backgroundColor: '#CC181E' }}
                          onPress={() => this.onSearchResultRedirect(item)}>
                          <Text allowFontScaling={false} style={styles.autoTextStyle}>
                            {item}
                          </Text>
                          {/* <View style={{ borderBottomWidth: 0.5, borderBottomColor: '#DDDEE4' }} /> */}
                        </TouchableOpacity>
                      )
                    }}
                  />
                </View>
              </View>
            ) : null
        }

        <StatusBar backgroundColor="#CC181E" barStyle="light-content" />
        {
          //  onScroll={(event) => {
          //   // this.setState({ isHeaderVisible: false });
          //   var currentOffset = event.nativeEvent.contentOffset.y;
          //   var direction = currentOffset > this.state.offset ? 'down' : 'up';
          //   this.setState({ offset : currentOffset, isHeaderVisible : !(currentOffset > this.state.offset), isBottomVisible : !(currentOffset > this.state.offset) })
          // }}
        }
        <ScrollView
          style={{ marginBottom: 50, flex: 1 }}
          refreshControl={
            <RefreshControl
              tintColor={['#CC181E']}
              refreshing={this.props.isRefreshing}
              onRefresh={this._onRefresh.bind(this)}
            />
          }
          // onScroll={ ({ nativeEvent }) => {

          //   if (isCloseToBottom(nativeEvent)) {
          //     console.log('isCloseToBottom', this.state.page);
          //     this.handleLoadMore();
          //   }

          // } }
          // scrollEventThrottle={ 400 }
          scrollEventThrottle={400}

          onMomentumScrollEnd={({ nativeEvent }) => {
            console.log('isCloseToBottom');

            if (isCloseToBottom(nativeEvent)) {
              this.handleLoadMore();
            }
          }}
        >
          <View style={{ backgroundColor: "white", margin: 10, }}>
            <View
              style={{
                padding: 10,
                borderColor: "#dddee4",
                borderBottomWidth: 1,
                marginBottom: 5,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',

              }}
            >
              <Text allowFontScaling={false} style={styles.searchTextStyle}>Search Updates</Text>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('SearchUpdates')}>
                <Image
                  style={{ tintColor: "#CC181E", height: 20, width: 20 }}
                  source={require("../img/list_icon.png")}
                />
              </TouchableOpacity>
            </View>
            {!this.props.isLoading &&
              this.props.search_updates != null &&
              this.props.search_updates.length > 0 ? (
                <FlatList
                  // key={ this.props.search_updates.length - 1 }
                  keyExtractor={(item, index) => index}
                  data={this.props.search_updates}
                  // extraData={ this.props.search_updates }
                  //  ref="scrollView"
                  //  onMomentumScrollEnd={(event) => {
                  //   const position = Math.round(event.nativeEvent.contentOffset.y / height); //for vertical case
                  //   console.log(position);
                  //   this.onPositionChanged(position);
                  // }}

                  renderItem={({ item, index }) => {
                    // console.log('item serach', item)
                    return (
                      <ItemSearchUpdate
                        fireId={item.firebase}
                        usericon={{ uri: PROFILEIMAGEPATH + item.profile_image }}
                        name={item.fullname}
                        des={item.tag}
                        commentImage={require("../img/message_icon.png")}
                        onPress={() => this.onChatMessage(item)}
                        onPressProfile={() => this.onProfileData(item)}
                        onPressUserIcon={() =>
                          this.setState({
                            prifilePicDialogVisible: true,
                            userPhoto: PROFILEIMAGEPATH + item.profile_image
                          })
                        }
                      //  onPressUserIcon={() => this.props.navigation.navigate('ZoomImage', { imageUrl: item.image })}
                      />
                    );
                  }}
                />
              ) : (
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    flex: 1,
                    height: 150,
                    marginBottom: 5
                  }}
                >
                  {/* <Image
                  style={{ height: 130, width: 130 }}
                  source={require("../img/no-data-found.jpg")}
                /> */}
                  <Text allowFontScaling={false} style={{ fontSize: 13, fontFamily: "Lato-Regular", color: '#686868', }}>No search update to show</Text>
                </View>
              )}
          </View>

          <View
            style={{
              backgroundColor: "white",
              marginTop: 5,
              marginLeft: 10,
              marginRight: 10,
              marginBottom: 5,
              flex: 1
            }}
          >
            <View
              style={{
                padding: 10,
                borderColor: "#dddee4",
                borderBottomWidth: 1,
                marginBottom: 5
              }}
            >
              <Text allowFontScaling={false} style={styles.searchTextStyle}>Suggested Profile</Text>
            </View>
            {!this.props.isLoading &&
              this.props.suggested_profiles != null &&
              this.props.suggested_profiles.length > 0 ? (
                <FlatList
                  style={{ marginTop: 5 }}
                  // key={ this.props.suggested_profiles.length - 1 }
                  keyExtractor={(item, index) => index}
                  data={this.props.suggested_profiles}
                  // extraData={ this.props }
                  // onEndReached={this.handleLoadMore.bind(this)}
                  // onEndThreshold={2}
                  // ref="scrollView"
                  //  onMomentumScrollEnd={(event) => {
                  //   const position = Math.round(event.nativeEvent.contentOffset.y / height); //for vertical case
                  //   console.log(position);
                  //   this.onPositionChanged(position);
                  // }}
                  renderItem={({ item, index }) => {
                    // console.log('item serach12', item)
                    return (
                      <Itemsuggestedprofile
                        fireId={item.firebase}
                        Morgan={item.fullname}
                        Marketing={item.profession}
                        Mumbai={item.location}
                        Users={item.user_give_rating_count}
                        average_rating={item.average_rating}
                        tagArray={item.user_tags}
                        commentImage={require("../img/message_icon.png")}
                        userProfile={{
                          uri: PROFILEIMAGEPATH + item.profile_image
                        }}
                        messagePress={() => this.onChatMessage(item)}
                        // onClickProfilePic={() => this.setState({ prifilePicDialogVisible: true, userPhoto: PROFILEIMAGEPATH + item.profile_image})}
                        onPressProfile={() => this.onProfileData(item)}
                        onPressUserIcon={() =>
                          this.setState({
                            prifilePicDialogVisible: true,
                            userPhoto: PROFILEIMAGEPATH + item.profile_image
                          })
                        }
                        onClickMore={() =>
                          this.props.navigation.navigate("Profile", {
                            tagtype: "tagtype",
                            user_id: item.user_id
                          })
                        }
                      />
                    );
                  }}
                />
              ) : (
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    flex: 1,
                    height: 150,
                    marginBottom: 5
                  }}
                >
                  {/* <Image
                  style={{ height: 130, width: 130 }}
                  source={require("../img/no-data-found.jpg")}
                /> */}
                  <Text allowFontScaling={false} style={{ fontSize: 13, fontFamily: "Lato-Regular", color: '#686868', }}>No suggested profiles to show</Text>
                </View>
              )}
            {
              (this.props.isdata) ? <ActivityIndicator size="small" /> : null
            }
          </View>
        </ScrollView>
        <Loader
          loading={this.props.isLoading}
        />

        <Modal
          transparent={true}
          visible={this.state.prifilePicDialogVisible}
          onRequestClose={() =>
            this.setState({ prifilePicDialogVisible: false })
          }
        >
          <View
            style={{
              height: "100%",
              width: "100%",
              paddingTop: 130,
              paddingBottom: 100
            }}
          >
            <View
              style={{
                backgroundColor: "rgba(0,0,0,0.9)",
                padding: 5,
                justifyContent: "center",
                alignContent: "center"
              }}
            >
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <ImageLoad

                  style={{ height: "100%", width: "100%" }}
                  source={{ uri: this.state.userPhoto }}
                  resizeMode="contain"
                />
                <TouchableOpacity
                  style={styles.CloseViewSTyle}
                  onPress={() =>
                    this.setState({ prifilePicDialogVisible: false })
                  }
                >
                  <Image
                    style={{ height: 18, width: 18 }}
                    source={require("../img/close_icon.png")}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#DDDEE3",
  },
  viewStyle: {
    flexDirection: "row",
    margin: 10,
    backgroundColor: 'transparent',
    // justifyContent: "space-between",
  },
  TextInputStyle: {
    color: "#DDDEE4",
    flex: 1,
    fontFamily: "Lato-Regular",
    justifyContent: "center",
    paddingLeft: 10,
    paddingTop: 2,
    paddingBottom: 2,
    fontSize: 15,
    width: '100%'
  },
  searchImageStyle: {
    height: 13,
    width: 13,
    tintColor: "white",
    alignSelf: "center",
    marginTop: 13,
    left: 3
  },
  searchTextStyle: {
    fontSize: 16,
    color: "#4E4E4E",
    fontFamily: "Lato-Medium"
  },
  CloseViewSTyle: {
    position: "absolute",
    right: 2,
    top: 2,
    backgroundColor: "rgba(255,0,0,0.5)",
    width: 32,
    height: 32,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  autocompleteStyle: {
    backgroundColor: '#b7151b',
    // backgroundColor:'#b7151b',
    height: 40,
    color: '#DDDEE4',
    borderTopRightRadius: 3,
    borderBottomRightRadius: 3
  },
  autoInputContainerStyle: {
    borderColor: 'transparent',
    // fontFamily: "Lato-Regular",

  },
  autoListStyle: {
    borderWidth: 0,
    color: '#b7151b',
    maxHeight: 180,

  },
  autoTextStyle: {
    color: '#DDDEE4',
    fontFamily: "Lato-Regular",
    fontSize: 15,
    marginTop: 5,
    marginBottom: 5,
    width: '100%'
  },
  containerModal: {
    flex: 1,
    backgroundColor: 'rgba(40,40,40,0.9)',
    paddingHorizontal: 15,
    paddingBottom: (DeviceInfo.getModel() === 'iPhone X'
      || DeviceInfo.getModel() === 'iPhone XS Max'
      || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 15 : 0
  },
  mainView: {
    flex: 1,
    justifyContent: 'space-between',
  },
  iconImg: {
    height: 30,
    width: 30,
    alignSelf: 'center',
    tintColor: 'gray'
  },
  roundView: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
    backgroundColor: 'white',
    justifyContent: 'center',
    borderColor: 'rgba(40,40,40,0.9)',
    borderWidth: 1.5
  },
  bottomView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    bottom: 2,
    position: 'absolute',
    flex: 1,
    width: '100%'
  },
  bigRoundView: {
    height: 52,
    width: 52,
    borderRadius: 52 / 2,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textStyle: {
    width: '35%',
    color: 'white',
    top: '16%',
    fontFamily: "Lato-Regular",
    fontSize: 15
  },
  textStyle2: {
    width: '30%',
    color: 'white',
    top: '30%',
    position: 'absolute',
    left: '22%',
    fontFamily: "Lato-Regular",
    fontSize: 15
  },
  textStyle3: {
    width: '40%',
    color: 'white',
    top: '45%',
    position: 'absolute',
    left: '48%',
    fontFamily: "Lato-Regular",
    fontSize: 15
  },
  textStyle4: {
    width: '23%',
    color: 'white',
    position: 'absolute',
    right: -2,
    top: '18%',
    fontFamily: "Lato-Regular",
    fontSize: 15
  },
  txtView: {
    flex: 1,
    width: '100%',
    // flexDirection:'row'
  },
  lineView: {
    position: 'absolute',
    height: '100%',
    width: '100%'
  },
  line1: {
    borderRightWidth: 1,
    borderColor: 'white',
    height: '75%',
    top: '23%',
    left: '6.9%',
    position: 'absolute',
  },
  line2: {
    borderRightWidth: 1,
    borderColor: 'white',
    height: '55%',
    top: '40%',
    left: '35.5%',
    position: 'absolute',
  },
  line3: {
    borderRightWidth: 1,
    borderColor: 'white',
    height: '40%',
    top: '55%',
    left: '63.7%',
    position: 'absolute',
  },
  line4: {
    borderRightWidth: 1,
    borderColor: 'white',
    height: '69%',
    top: '28%',
    left: '92.5%',
    position: 'absolute',
  },
  arrowStyle: {
    height: 15,
    width: 15,
    top: '22.2%',
    left: '4.9%',
    position: 'absolute',
    tintColor: 'white'
  },
  arrowStyle1: {
    height: 15,
    width: 15,
    top: '39.3%',
    left: '33.5%',
    position: 'absolute',
    tintColor: 'white'
  },
  arrowStyle2: {
    height: 15,
    width: 15,
    top: '54.3%',
    left: '61.8%',
    position: 'absolute',
    tintColor: 'white'
  },
  arrowStyle3: {
    height: 15,
    width: 15,
    top: '27.3%',
    left: '90.6%',
    position: 'absolute',
    tintColor: 'white'
  }

});

const mapStateToProps = state => {
  return {
    search_updates: state.SearchList.search_updates,
    suggested_profiles: state.SearchList.suggested_profiles,
    authResult: state.SearchList.authResult,
    isLoading: false,
    isLoading: state.SearchList.isLoading,
    isRefreshing: state.SearchList.isRefreshing,
    isdata: state.SearchList.isdata,
    SplashauthToken: state.Splash.authToken,
    authResultAddFriend: state.Testimonial.authResultAddFriend,
    searchArray: state.Splash.searchArray,
    friend: state.Chat.chatList,
  };
};

export default connect(
  mapStateToProps,
  {
    getSearchList,
    initialgetSearchListStateData,
    onBottomTabChange,
    getAddfriend,
    saveUserChat,
    clerchatnotification,
    getMyChatList
  }
)(Search);

// /* @flow */
// import React, { Component } from "react";
// import {
//   View,
//   Text,
//   StyleSheet,
//   TextInput,
//   TouchableOpacity,
//   Image,
//   FlatList,
//   ScrollView,
//   StatusBar,
//   Modal,
//   Alert,
//   Platform,
//   ActivityIndicator,
//   Dimensions,
//   SafeAreaView,
//   RefreshControl
// } from "react-native";
// import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
// import GestureRecognizer, {
//   swipeDirections
// } from "react-native-swipe-gestures";
// import { connect } from "react-redux";
// import {
//   getSearchList,
//   initialgetSearchListStateData,
//   onBottomTabChange,
//   getAddfriend,
//   saveUserChat,
//   clerchatnotification,
//   getMyChatList
// } from "../Actions";
// import { getUser, getAppHintData, insertAppHint } from "../Database/allSchema";
// import { POSTIMAGEPATH, PROFILEIMAGEPATH } from "../Actions/type";
// import ItemSearchUpdate from "./ItemSearchUpdate";
// import Itemsuggestedprofile from "./Itemsuggestedprofile";
// import ImageLoad from "./ImageLoad";
// import Loader from './Loader';
// import Autocomplete from 'react-native-autocomplete-input';
// let ImagePath = "";
// let userId = "";
// import DeviceInfo from 'react-native-device-info';

// const config = {
//   velocityThreshold: 0.3,
//   directionalOffsetThreshold: 80
// };

// class Search extends Component {
//   constructor(props) {
//     super(props);
//     this.componentWillMount = this.componentWillMount.bind(this);
//     this.state = {
//       refreshing: false,
//       modalVisible: false,
//       prifilePicDialogVisible: false,
//       isHeaderVisible: true,
//       isScrollUp: false,
//       isScrollDown: false,
//       searchText: "",
//       loading: false,
//       data: [],
//       page: 1,
//       searchResult: [],
//       isBottomVisible: false,
//       userPhoto: ""
//     };
//   }


//   _onRefresh = () => {
//     // this.setState({ refreshing: true });

//     this.setState({ searchText: '' });
//     // this.props.initialgetSearchListStateData();
//     getUser()
//       .then(user => {
//         this.setState({ refreshing: false });

//         if (user.length > 0) {
//           this.setState({ page: 1 });
//           userId = user[0].id;
//           console.log(userId);
//           this.fetchData(false, true);
//         }
//       })
//       .catch(error => {
//         // this.setState({ refreshing: false });
//         console.log(error);
//       });
//   }


//   componentWillMount() {

//     this.setState({ searchText: '' });
//     // this.props.initialgetSearchListStateData();
//     getAppHintData()
//       .then(data => {
//         if (data && data.length > 0) {
//           if (data[0].isVisibleHint == 'true') {
//             this.setState({ modalVisible: false });
//           }
//         } else {
//           this.setState({ modalVisible: true }, () => {
//             const appHintData = {
//               id: 1,
//               isVisibleHint: 'false',
//               isChatDotVisible: 'true',
//             }
//             insertAppHint(appHintData).then(() => {
//               console.log('Insert Suceess appHintData');
//             }).catch((error) => {
//               console.log(`Insert error${error}`);
//             });
//           });

//         }
//       })
//       .catch(error => {
//         console.log(error);
//       });
//     getUser()
//       .then(user => {
//         if (user.length > 0) {
//           this.setState({ page: 1 });
//           userId = user[0].id;
//           console.log(userId);
//           this.fetchData(true, false);
//           //get chatList
//           this.props.getMyChatList({
//             userId,
//             token: this.props.SplashauthToken,
//             oldList: []
//           })
//         }
//       })
//       .catch(error => {
//         console.log(error);
//       });

//     this.props.navigation.addListener("didFocus", payload => {
//       setTimeout(() => {
//         console.log("search didFocus");
//         this.props.onBottomTabChange({ prop: "issearchseleted", value: true });
//         this.props.onBottomTabChange({ prop: "isuserseleted", value: false });
//         this.props.onBottomTabChange({ prop: "isfeedseleted", value: false });
//         this.props.onBottomTabChange({ prop: "ischatseleted", value: false });
//       }, 10);
//     });
//     this.props.navigation.addListener("willBlur", payload => {
//       console.log("search willBlur");
//       this.props.onBottomTabChange({ prop: "issearchseleted", value: false });
//       this.props.onBottomTabChange({ prop: "isuserseleted", value: false });
//       this.props.onBottomTabChange({ prop: "isfeedseleted", value: false });
//       this.props.onBottomTabChange({ prop: "ischatseleted", value: false });
//     });

//   }

//   componentWillReceiveProps(nextProp) {

//     if (nextProp.authResultAddFriend === "Success Addfriend") {
//       console.log("Success");
//     } else if (nextProp.authResultAddFriend === "Already Friend") {
//       // Alert.alert("Oops", "Already Friend");
//       console.log("Already Friend");
//     }
//     // else if (nextProp.authResultAddFriend === "User id must be numeric.") {
//     //   setTimeout(() => {
//     //     Alert.alert("Oops", "User id must be numeric.");
//     //   }, 500);


//     // }

//     console.log('this.props.isRefreshing', this.props.isRefreshing)

//   }

//   find(searchText) {
//     // console.log('Array get', this.props.searchArray);
//     if (searchText === '') {
//       return [];
//     }
//     const regex = new RegExp('^' + searchText, 'i');
//     let filterData = this.props.searchArray.filter((item) => item.search(regex) >= 0)
//     return filterData;
//   }

//   onSearchResult(item) {
//     this.setState({ searchText: item });
//   }
//   onSearchResultRedirect(item) {
//     this.setState({ searchText: item });

//     if (/^(\w+\s?)*\s*$/.test(this.state.searchText)) {
//       if (this.state.searchText.replace(/(^\s+|\s+$)/g, '') !== '') {
//         this.setState({ searchText: '' });
//         console.log('search State', this.state.searchText);
//         this.props.navigation.navigate('SearchResult', {
//           searchText: item
//         });
//       }
//     } else {
//       // Alert.alert('Alert', 'Please enter valid character to search.');
//       setTimeout(() => {
//         Alert.alert('Alert', 'Please enter valid character to search.');
//       }, 500);

//     }
//   }
//   onSearchRedirect() {
//     if (/^(\w+\s?)*\s*$/.test(this.state.searchText)) {
//       if (this.state.searchText.replace(/(^\s+|\s+$)/g, '') !== '') {
//         this.setState({ searchText: '' });
//         console.log('search State', this.state.searchText);
//         this.props.navigation.navigate('SearchResult', {
//           searchText: this.state.searchText
//         });
//       }
//     } else {
//       // Alert.alert('Alert', 'Please enter valid character to search.');
//       setTimeout(() => {
//         Alert.alert('Alert', 'Please enter valid character to search.');
//       }, 500);

//     }
//   }


//   fetchData = (isLoading, isRefreshing) => {
//     // console.log("Splash Login token", this.props.SplashauthToken);
//     this.props.getSearchList({
//       userId,
//       page: this.state.page,
//       token: this.props.SplashauthToken,
//       suggested_profiles: this.props.suggested_profiles,
//       search_updates: this.props.search_updates,
//       isLoading,
//       isRefreshing
//     });
//     console.log('fetchdata calleed')
//   };

//   handleLoadMore = () => {
//     if (this.props.isdata) {
//       this.setState(
//         {
//           page: this.state.page + 1
//         },
//         () => {
//           console.log(this.state.page);
//           this.props.getSearchList({
//             userId,
//             page: this.state.page,
//             suggested_profiles: this.props.suggested_profiles,
//             search_updates: this.props.search_updates,
//             token: this.props.SplashauthToken,
//             isLoading: false,
//             isRefreshing: false
//           });
//         }
//       );
//     }
//   };

//   // BottomDown() {
//   //   this.refs.scrollView.scrollToEnd(this.setState({ isHeaderVisible: false }))
//   //   console.log('isHeaderVisible: false');
//   // }

//   onSwipeDown(gestureState) {
//     console.log("onSwipeLeft" + this.state.type);
//     this.setState({ isHeaderVisible: true });
//   }

//   onSwipeUp(gestureState) {
//     console.log("onSwipeRight" + this.state.type);
//     this.setState({ isHeaderVisible: false });
//   }
//   onProfileData(item) {
//     console.log(item);
//     this.props.navigation.navigate("Profile", {
//       user_id: item.user_id,
//       fullname: item.fullname,
//       profile_image: item.profile_image,
//       average_rating: item.average_rating,
//       user_give_rating_count: item.user_give_rating_count,
//       form: 'MyProfile'
//     });
//   }

//   onChatMessage(item) {
//     this.props.saveUserChat(item.user_id, item.fullname, item.firebase);
//     this.props.clerchatnotification(userId, item.user_id, this.props.SplashauthToken);
//     setTimeout(() => {
//       this.props.navigation.navigate("ChatMessage", {
//         toUserId: item.user_id,
//         fullname: item.fullname,
//         to_fire_id: item.firebase
//       });
//     }, 500);
//   }


//   render() {
//     const { searchText } = this.state;
//     const filterSearch = this.find(searchText);
//     var uniqueArray = [...new Set(filterSearch)]
//     const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
//     const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
//       const paddingToBottom = 20;
//       return layoutMeasurement.height + contentOffset.y >=
//         contentSize.height - paddingToBottom;
//     };
//     return (
//       <View style={styles.container}>
//         <View style={{ backgroundColor: "#CC181E", height: 14 }}></View>
//         <View style={{ backgroundColor: "#CC181E", paddingTop: (DeviceInfo.getModel() === 'iPhone X' || DeviceInfo.getModel() === 'iPhone XS Max' || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 15 : 0 }} />

//         <Modal
//           animationType="slide"
//           transparent={true}
//           visible={this.state.modalVisible}
//           onRequestClose={() => {
//             this.setState({ modalVisible: false })
//           }}>
//           <TouchableOpacity
//             onPress={() => {
//               this.setState({ modalVisible: false })
//             }}
//             style={styles.containerModal}>
//             <View style={styles.mainView}>

//               <View style={styles.txtView}>
//                 <Text allowFontScaling={false} style={styles.textStyle}>Search & Connect to Penpals</Text>
//                 <Text allowFontScaling={false} style={styles.textStyle2}>Create profile for Penpals to search you</Text>
//                 <Text allowFontScaling={false} style={styles.textStyle3}>Build everlasting friendships by staying updated</Text>
//                 <Text allowFontScaling={false} style={styles.textStyle4}>Chat,Rate & Testify your Penpals</Text>
//               </View>


//               <View style={styles.lineView}>

//                 <View style={styles.line1}></View>
//                 <View style={styles.line2}></View>
//                 <View style={styles.line3}></View>
//                 <View style={styles.line4}></View>

//               </View>

//               <View style={styles.lineView}>
//                 <Image
//                   style={styles.arrowStyle}
//                   source={require('../img/up-arrow.png')}
//                 />
//                 <Image
//                   style={styles.arrowStyle1}
//                   source={require('../img/up-arrow.png')}
//                 />
//                 <Image
//                   style={styles.arrowStyle2}
//                   source={require('../img/up-arrow.png')}
//                 />
//                 <Image
//                   style={styles.arrowStyle3}
//                   source={require('../img/up-arrow.png')}
//                 />
//               </View>



//               <View style={styles.bottomView}>

//                 <View style={styles.bigRoundView}>
//                   <View style={styles.roundView}>
//                     <Image
//                       style={styles.iconImg}
//                       source={require('../img/search_icon.png')}
//                     />
//                   </View>
//                 </View>

//                 <View style={styles.bigRoundView}>
//                   <View style={styles.roundView}>
//                     <Image
//                       style={styles.iconImg}
//                       source={require('../img/user_icon.png')}
//                     />
//                   </View>
//                 </View>

//                 <View style={styles.bigRoundView}>
//                   <View style={styles.roundView}>
//                     <Image
//                       style={styles.iconImg}
//                       source={require('../img/note_icon.png')}
//                     />
//                   </View>
//                 </View>

//                 <View style={styles.bigRoundView}>
//                   <View style={styles.roundView}>
//                     <Image
//                       style={styles.iconImg}
//                       source={require('../img/comment_icon.png')}
//                     />
//                   </View>
//                 </View>
//               </View>
//             </View>
//           </TouchableOpacity>

//         </Modal>



//         {
//           this.state.isHeaderVisible &&
//             this.props.suggested_profiles.length !== 1 ? (
//               <View style={{ backgroundColor: "#CC181E", }}>
//                 <View style={styles.viewStyle}>
//                   <TouchableOpacity
//                     style={{ height: 40, width: 20, backgroundColor: '#b7151b', marginTop: 1, borderTopLeftRadius: 3, borderBottomLeftRadius: 3, left: 1, }}
//                   >
//                     <Image
//                       source={require("../img/search_icon.png")}
//                       style={styles.searchImageStyle}
//                     />
//                   </TouchableOpacity>

//                   <Autocomplete
//                     allowFontScaling={false}
//                     fontSize={14}
//                     autoCapitalize="none"
//                     autoCorrect={false}
//                     underlineColorAndroid="transparent"
//                     style={styles.autocompleteStyle}
//                     containerStyle={{}}
//                     inputContainerStyle={styles.autoInputContainerStyle}
//                     data={uniqueArray.length === 1 && comp(searchText, uniqueArray[0]) ? [] : uniqueArray}
//                     defaultValue={searchText}
//                     width={Dimensions.get('window').width - 40}
//                     onChangeText={(item) => this.setState({ searchText: item })}
//                     onSubmitEditing={() => this.onSearchRedirect()}
//                     placeholder="Search penpals by tags/profession/locations"
//                     placeholderTextColor="#DDDEE4"
//                     placeholderTextAlign="center"
//                     paddingLeft={10}
//                     listContainerStyle={{}}
//                     listStyle={styles.autoListStyle}
//                     renderItem={(item) => (

//                       <TouchableOpacity
//                         activeOpacity={1}
//                         style={{ backgroundColor: '#CC181E' }}
//                         onPress={() => this.onSearchResultRedirect(item)}>
//                         <Text style={styles.autoTextStyle}>
//                           {item}
//                         </Text>
//                         <View style={{ borderBottomWidth: 0.5, borderBottomColor: '#DDDEE4' }} />
//                       </TouchableOpacity>

//                     )}
//                   />
//                 </View>
//               </View>
//             ) : null
//         }

//         <StatusBar backgroundColor="#CC181E" barStyle="light-content" />
//         {
//           //  onScroll={(event) => {
//           //   // this.setState({ isHeaderVisible: false });
//           //   var currentOffset = event.nativeEvent.contentOffset.y;
//           //   var direction = currentOffset > this.state.offset ? 'down' : 'up';
//           //   this.setState({ offset : currentOffset, isHeaderVisible : !(currentOffset > this.state.offset), isBottomVisible : !(currentOffset > this.state.offset) })
//           // }}
//         }
//         <ScrollView
//           style={{ marginBottom: 50 }}
//           refreshControl={
//             <RefreshControl
//               tintColor={['#CC181E']}
//               refreshing={this.props.isRefreshing}
//               onRefresh={this._onRefresh.bind(this)}
//             />
//           }

//           onScroll={({ nativeEvent }) => {
//             if (isCloseToBottom(nativeEvent)) {
//               console.log(this.state.page);
//               this.handleLoadMore();
//             }
//           }}
//           scrollEventThrottle={400}
//         >
//           <View style={{ backgroundColor: "white", margin: 10, }}>
//             <View
//               style={{
//                 padding: 10,
//                 borderColor: "#dddee4",
//                 borderBottomWidth: 1,
//                 marginBottom: 5,
//                 flexDirection: 'row',
//                 justifyContent: 'space-between',
//                 alignItems: 'center',

//               }}
//             >
//               <Text allowFontScaling={false} style={styles.searchTextStyle}>Search Updates</Text>
//               <TouchableOpacity onPress={() => this.props.navigation.navigate('SearchUpdates')}>
//                 <Image
//                   style={{ tintColor: "#CC181E", height: 20, width: 20 }}
//                   source={require("../img/list_icon.png")}
//                 />
//               </TouchableOpacity>
//             </View>
//             {!this.props.isLoading &&
//               this.props.search_updates != null &&
//               this.props.search_updates.length > 0 ? (
//                 <FlatList
//                   data={this.props.search_updates}
//                   extraData={this.props}
//                   //  ref="scrollView"
//                   //  onMomentumScrollEnd={(event) => {
//                   //   const position = Math.round(event.nativeEvent.contentOffset.y / height); //for vertical case
//                   //   console.log(position);
//                   //   this.onPositionChanged(position);
//                   // }}

//                   renderItem={({ item, index }) => {
//                     return (
//                       <ItemSearchUpdate
//                         usericon={{ uri: PROFILEIMAGEPATH + item.profile_image }}
//                         name={item.fullname}
//                         des={item.tag}
//                         commentImage={require("../img/message_icon.png")}
//                         onPress={() => this.onChatMessage(item)}
//                         onPressProfile={() => this.onProfileData(item)}
//                         onPressUserIcon={() =>
//                           this.setState({
//                             prifilePicDialogVisible: true,
//                             userPhoto: PROFILEIMAGEPATH + item.profile_image
//                           })
//                         }
//                       //  onPressUserIcon={() => this.props.navigation.navigate('ZoomImage', { imageUrl: item.image })}
//                       />
//                     );
//                   }}
//                 />
//               ) : (
//                 <View
//                   style={{
//                     justifyContent: "center",
//                     alignItems: "center",
//                     flex: 1,
//                     height: 150,
//                     marginBottom: 5
//                   }}
//                 >
//                   {/* <Image
//                   style={{ height: 130, width: 130 }}
//                   source={require("../img/no-data-found.jpg")}
//                 /> */}
//                   <Text style={{ fontSize: 13, fontFamily: "Lato-Regular", color: '#686868', }}>No search update to show</Text>
//                 </View>
//               )}
//           </View>

//           <View
//             style={{
//               backgroundColor: "white",
//               marginTop: 5,
//               marginLeft: 10,
//               marginRight: 10,
//               marginBottom: 5,
//               flex: 1
//             }}
//           >
//             <View
//               style={{
//                 padding: 10,
//                 borderColor: "#dddee4",
//                 borderBottomWidth: 1,
//                 marginBottom: 5
//               }}
//             >
//               <Text allowFontScaling={false} style={styles.searchTextStyle}>Suggested Profile</Text>
//             </View>
//             {!this.props.isLoading &&
//               this.props.suggested_profiles != null &&
//               this.props.suggested_profiles.length > 0 ? (
//                 <FlatList
//                   style={{ marginTop: 5 }}
//                   data={this.props.suggested_profiles}
//                   extraData={this.props}
//                   // onEndReached={this.handleLoadMore.bind(this)}
//                   // onEndThreshold={2}
//                   // ref="scrollView"
//                   //  onMomentumScrollEnd={(event) => {
//                   //   const position = Math.round(event.nativeEvent.contentOffset.y / height); //for vertical case
//                   //   console.log(position);
//                   //   this.onPositionChanged(position);
//                   // }}
//                   renderItem={({ item, index }) => {
//                     return (
//                       <Itemsuggestedprofile
//                         Morgan={item.fullname}
//                         Marketing={item.profession}
//                         Mumbai={item.location}
//                         Users={item.user_give_rating_count}
//                         average_rating={item.average_rating}
//                         tagArray={item.user_tags}
//                         commentImage={require("../img/message_icon.png")}
//                         userProfile={{
//                           uri: PROFILEIMAGEPATH + item.profile_image
//                         }}
//                         messagePress={() => this.onChatMessage(item)}
//                         // onClickProfilePic={() => this.setState({ prifilePicDialogVisible: true, userPhoto: PROFILEIMAGEPATH + item.profile_image})}
//                         onPressProfile={() => this.onProfileData(item)}
//                         onPressUserIcon={() =>
//                           this.setState({
//                             prifilePicDialogVisible: true,
//                             userPhoto: PROFILEIMAGEPATH + item.profile_image
//                           })
//                         }
//                         onClickMore={() =>
//                           this.props.navigation.navigate("Profile", {
//                             tagtype: "tagtype",
//                             user_id: item.user_id
//                           })
//                         }
//                       />
//                     );
//                   }}
//                 />
//               ) : (
//                 <View
//                   style={{
//                     justifyContent: "center",
//                     alignItems: "center",
//                     flex: 1,
//                     height: 150,
//                     marginBottom: 5
//                   }}
//                 >
//                   {/* <Image
//                   style={{ height: 130, width: 130 }}
//                   source={require("../img/no-data-found.jpg")}
//                 /> */}
//                   <Text style={{ fontSize: 13, fontFamily: "Lato-Regular", color: '#686868', }}>No suggested profiles to show</Text>
//                 </View>
//               )}
//             {
//               (this.props.isdata) ? <ActivityIndicator size="small" /> : null
//             }
//           </View>
//         </ScrollView>

//         <Loader loading={this.props.isLoading} />

//         <Modal
//           transparent={true}
//           visible={this.state.prifilePicDialogVisible}
//           onRequestClose={() =>
//             this.setState({ prifilePicDialogVisible: false })
//           }
//         >
//           <View
//             style={{
//               height: "100%",
//               width: "100%",
//               paddingTop: 130,
//               paddingBottom: 100
//             }}
//           >
//             <View
//               style={{
//                 backgroundColor: "rgba(0,0,0,0.9)",
//                 padding: 5,
//                 justifyContent: "center",
//                 alignContent: "center"
//               }}
//             >
//               <View style={{ alignItems: "center", justifyContent: "center" }}>
//                 <ImageLoad

//                   style={{ height: "100%", width: "100%" }}
//                   source={{ uri: this.state.userPhoto }}
//                   resizeMode="contain"
//                 />
//                 <TouchableOpacity
//                   style={styles.CloseViewSTyle}
//                   onPress={() =>
//                     this.setState({ prifilePicDialogVisible: false })
//                   }
//                 >
//                   <Image
//                     style={{ height: 18, width: 18 }}
//                     source={require("../img/close_icon.png")}
//                   />
//                 </TouchableOpacity>
//               </View>
//             </View>
//           </View>
//         </Modal>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: "#DDDEE3",
//   },
//   viewStyle: {
//     flexDirection: "row",
//     margin: 10,
//     backgroundColor: 'transparent',
//     // justifyContent: "space-between",
//   },
//   TextInputStyle: {
//     color: "#DDDEE4",
//     flex: 1,
//     fontFamily: "Lato-Regular",
//     justifyContent: "center",
//     paddingLeft: 10,
//     paddingTop: 2,
//     paddingBottom: 2,
//     fontSize: 15,
//     width: '100%'
//   },
//   searchImageStyle: {
//     height: 13,
//     width: 13,
//     tintColor: "white",
//     alignSelf: "center",
//     marginTop: 13,
//     left: 3
//   },
//   searchTextStyle: {
//     fontSize: 16,
//     color: "#4E4E4E",
//     fontFamily: "Lato-Medium"
//   },
//   CloseViewSTyle: {
//     position: "absolute",
//     right: 2,
//     top: 2,
//     backgroundColor: "rgba(255,0,0,0.5)",
//     width: 32,
//     height: 32,
//     borderRadius: 5,
//     alignItems: "center",
//     justifyContent: "center"
//   },
//   autocompleteStyle: {
//     backgroundColor: '#b7151b',
//     // backgroundColor:'#b7151b',
//     height: 40,
//     color: '#DDDEE4',
//     borderTopRightRadius: 3,
//     borderBottomRightRadius: 3,
//     width: '100%',

//   },
//   autoInputContainerStyle: {
//     borderColor: 'transparent',
//     fontFamily: "Lato-Regular",

//   },
//   autoListStyle: {
//     borderWidth: 0,
//     color: '#b7151b',
//     maxHeight: 180,

//   },
//   autoTextStyle: {
//     color: '#DDDEE4',
//     fontFamily: "Lato-Regular",
//     fontSize: 15,
//     marginTop: 5,
//     marginBottom: 5,
//     width: '100%'
//   },
//   containerModal: {
//     flex: 1,
//     backgroundColor: 'rgba(40,40,40,0.9)',
//     paddingHorizontal: 15,
//     paddingBottom: (DeviceInfo.getModel() === 'iPhone X'
//       || DeviceInfo.getModel() === 'iPhone XS Max'
//       || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 15 : 0
//   },
//   mainView: {
//     flex: 1,
//     justifyContent: 'space-between',
//   },
//   iconImg: {
//     height: 30,
//     width: 30,
//     alignSelf: 'center',
//     tintColor: 'gray'
//   },
//   roundView: {
//     height: 50,
//     width: 50,
//     borderRadius: 50 / 2,
//     backgroundColor: 'white',
//     justifyContent: 'center',
//     borderColor: 'rgba(40,40,40,0.9)',
//     borderWidth: 1.5
//   },
//   bottomView: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     bottom: 2,
//     position: 'absolute',
//     flex: 1,
//     width: '100%'
//   },
//   bigRoundView: {
//     height: 52,
//     width: 52,
//     borderRadius: 52 / 2,
//     backgroundColor: 'white',
//     justifyContent: 'center',
//     alignItems: 'center'
//   },
//   textStyle: {
//     width: '35%',
//     color: 'white',
//     top: '16%',
//     fontFamily: "Lato-Regular",
//     fontSize: 15
//   },
//   textStyle2: {
//     width: '30%',
//     color: 'white',
//     top: '30%',
//     position: 'absolute',
//     left: '22%',
//     fontFamily: "Lato-Regular",
//     fontSize: 15
//   },
//   textStyle3: {
//     width: '40%',
//     color: 'white',
//     top: '45%',
//     position: 'absolute',
//     left: '48%',
//     fontFamily: "Lato-Regular",
//     fontSize: 15
//   },
//   textStyle4: {
//     width: '23%',
//     color: 'white',
//     position: 'absolute',
//     right: -2,
//     top: '18%',
//     fontFamily: "Lato-Regular",
//     fontSize: 15
//   },
//   txtView: {
//     flex: 1,
//     width: '100%',
//     // flexDirection:'row'
//   },
//   lineView: {
//     position: 'absolute',
//     height: '100%',
//     width: '100%'
//   },
//   line1: {
//     borderRightWidth: 1,
//     borderColor: 'white',
//     height: '75%',
//     top: '23%',
//     left: '6.9%',
//     position: 'absolute',
//   },
//   line2: {
//     borderRightWidth: 1,
//     borderColor: 'white',
//     height: '55%',
//     top: '40%',
//     left: '35.5%',
//     position: 'absolute',
//   },
//   line3: {
//     borderRightWidth: 1,
//     borderColor: 'white',
//     height: '40%',
//     top: '55%',
//     left: '63.7%',
//     position: 'absolute',
//   },
//   line4: {
//     borderRightWidth: 1,
//     borderColor: 'white',
//     height: '69%',
//     top: '28%',
//     left: '92.5%',
//     position: 'absolute',
//   },
//   arrowStyle: {
//     height: 15,
//     width: 15,
//     top: '22.2%',
//     left: '4.9%',
//     position: 'absolute',
//     tintColor: 'white'
//   },
//   arrowStyle1: {
//     height: 15,
//     width: 15,
//     top: '39.3%',
//     left: '33.5%',
//     position: 'absolute',
//     tintColor: 'white'
//   },
//   arrowStyle2: {
//     height: 15,
//     width: 15,
//     top: '54.3%',
//     left: '61.8%',
//     position: 'absolute',
//     tintColor: 'white'
//   },
//   arrowStyle3: {
//     height: 15,
//     width: 15,
//     top: '27.3%',
//     left: '90.6%',
//     position: 'absolute',
//     tintColor: 'white'
//   }

// });

// const mapStateToProps = state => {
//   return {
//     search_updates: state.SearchList.search_updates,
//     suggested_profiles: state.SearchList.suggested_profiles,
//     authResult: state.SearchList.authResult,
//     isLoading: state.SearchList.isLoading,
//     isRefreshing: state.SearchList.isRefreshing,
//     isdata: state.SearchList.isdata,
//     SplashauthToken: state.Splash.authToken,
//     authResultAddFriend: state.Testimonial.authResultAddFriend,
//     friend: state.Chat.chatList,
//     searchArray: state.Splash.searchArray,
//   };
// };

// export default connect(
//   mapStateToProps,
//   {
//     getSearchList,
//     initialgetSearchListStateData,
//     onBottomTabChange,
//     getAddfriend,
//     saveUserChat,
//     getMyChatList,
//     clerchatnotification
//   }
// )(Search);
