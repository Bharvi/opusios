/* @flow */

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
  Modal,
  Share,
  Alert,
  Platform,
  ActivityIndicator,
} from "react-native";
import ItemPost from "./ItemPost";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import { connect } from "react-redux";
import { getMYPosts, getPostsSharepost, getPostsAbusePost } from "../Actions";
import { getUser } from "../Database/allSchema";
import { POSTIMAGEPATH } from "../Actions/type";

const userId = "";
const postID = "";
const shareText = "Test 2";

class Posts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prifilePicDialogVisible: false,
      userPhoto: "",
      post_photo: "",
      post_caption: "",
      page: 1,
      data: [],
      loading: false,
      error: null,
      refreshing: false,
      fetching_from_server: false,
      PostData: [
        {
          name: "Marcus Richards",
          des: "Yesterday, Halifax, N.S",
          title: "Good friends, good food and a lot of laughs."
        },
        {
          name: "Marcus Richards",
          des: "Yesterday, Halifax, N.S",
          title: "Good friends, good food and a lot of laughs."
        },
        {
          name: "Marcus Richards",
          des: "Yesterday, Halifax, N.S",
          title: "Good friends, good food and a lot of laughs."
        }
      ],
      pageCount: 1,
    };
    this.shareMessage = this.shareMessage.bind(this);
  }
  shareMessage() {
    console.log(this.state.post_caption);
    const image = POSTIMAGEPATH + this.state.post_photo;
    console.log(image);
    Share.share({
      message: this.state.post_caption
      // url: image ,
    });
  }


  componentWillReceiveProps(nextProp) {
    if (nextProp.authResultSharePost === "Success") {
      this.shareMessage();
    } else if (nextProp.authResultSharePost === "User id must be numeric.") {
      Alert.alert("Oops", "User id must be numeric.");
    } else if (nextProp.authResultSharePost === "Post id must be numeric.") {
      Alert.alert("Oops", "Post id must be numeric.");
    }
    if (nextProp.authResultAbusePost === "Success Abuse") {
      Alert.alert(
        "Success",
        "Report abuse successfully.",
        [
          {
            text: "OK",
            onPress: () => {
              cancelable: false;
            }
          }
        ],
        { cancelable: false }
      );
    } else if (
      nextProp.authResultAbusePost === "Already Post is Abuse by same User"
    ) {
      Alert.alert("Oops", "Already post is abuse by same user");
    } else if (nextProp.authResultAbusePost === "User id must be numeric.") {
      Alert.alert("Oops", "User id must be numeric.");
    } else if (nextProp.authResultAbusePost === "Post id must be numeric.") {
      Alert.alert("Oops", "Post id must be numeric.");
    }
  }
  componentDidMount() {
    getUser()
       .then(user => {
         if (user.length > 0) {
           // this.setState({ userData: user[0] });
           userId = user[0].id;
           console.log(userId);
           // this.dataLoad();
         }
       })
       .catch(error => {
         console.log(error);
       });
// this.willFocusSubscription = this.props.myNavigation.addListener(
//   'willFocus',
//   () => {
    this.dataLoad();
  // }
// );
}
componentWillUnmount() {
  this.willFocusSubscription.remove();
}



  dataLoad() {
    console.log(userId, this.state.pageCount)
    this.props.getMYPosts(107, this.state.pageCount);
  }

  onCommentRedirt(item) {
    this.props.myNavigation.navigate("CommentPage", { postID: item.post_id });
  }
  onProfileData(item) {
    console.log(item.user_photo);
    this.props.myNavigation.navigate("Profile", { user_id: item.user_id,
      fullname: item.fullname,
      profile_image: POSTIMAGEPATH + item.user_photo
    });
  }

  onSharePost(item) {
    console.log("on share post ", postID);
    this.setState({
      post_photo: POSTIMAGEPATH + item.post_photo,
      post_caption: item.post_caption
    });
    console.log(this.state.post_photo);
    console.log(this.state.post_caption);
    this.props.getPostsSharepost(userId, item.post_id, shareText);
  }
  onAbusePost(item) {
    console.log(userId);
    console.log(item.post_id);
    this.props.getPostsAbusePost(userId, item.post_id);
  }

  handleLoadMore = () => {
    this.setState({
      pageCount: this.state.pageCount + 1
    }, () => {
      console.log(this.state.pageCount);
      if(!this.props.isData){
      this.props.getMYPosts(107, this.state.pageCount, this.props.posts);
    }
  });
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
      {
      (!this.props.isLoading && this.props.posts != null && this.props.posts.length > 0  ? (
        <FlatList
          style={{ width: '100%' }}
          keyExtractor={(item, index) => index}
          data={this.props.posts}
          onEndReached={this.handleLoadMore.bind(this)}
          onEndThreshold={2}

          renderItem={({ item, index }) => {
            return (
              <ItemPost
                name={item.fullname}
                userprofileImage={{ uri: POSTIMAGEPATH + item.user_photo }}
                des={item.time}
                postTitle={item.post_caption}
                Image={POSTIMAGEPATH + item.post_photo }
                commentcount={item.comment_count}
                onPressComment={() => this.onCommentRedirt(item)}
                sharepost={() => this.onSharePost(item)}
                Abusepost={() => this.onAbusePost(item)}
                onPressUserIcon={() =>
                  this.setState({
                    prifilePicDialogVisible: true,
                    userPhoto: POSTIMAGEPATH + item.user_photo
                  })
                }
                openMyProfile={() => this.onProfileData(item)}
              />
            );
          }}
        />
        ) :
         <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            flex: 1,
            height: 500
          }}
        >
        <Image style={{ height: 130, width: 130}}
          source={require('../img/no-data-found.jpg')}/>
        </View>
      )
    }

        <Modal
          transparent={true}
          visible={this.state.prifilePicDialogVisible}
          onRequestClose={() =>
            this.setState({ prifilePicDialogVisible: false })
          }
        >
          <View
            style={{
              height: "100%",
              width: "100%",
              paddingTop: 130,
              paddingBottom: 100
            }}
          >
          <View style={{backgroundColor: 'rgba(0,0,0,0.3)', padding: 5 , justifyContent: 'center', alignContent: 'center' }}>
            <View style={{ alignItems: "center", justifyContent: "center" }}>
              <Image
                style={{ height: '100%', width: "100%" }}
                source={{ uri: this.state.userPhoto }}
                resizeMode="contain"
              />

              <TouchableOpacity
                style={styles.CloseViewSTyle}
                onPress={() =>
                  this.setState({ prifilePicDialogVisible: false })
                }
              >
                <Image
                  style={{ height: 18, width: 18 }}
                  source={require("../img/close_icon.png")}
                />
              </TouchableOpacity>
            </View>
          </View>
          </View>
        </Modal>
        {
          <ProgressDialog
            visible={this.props.isLoading}
            title="Loading"
            message="Please, wait..."
          />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  CloseViewSTyle: {
    position: "absolute",
    right: 2,
    top: 2,
    backgroundColor: "rgba(255,0,0,0.5)",
    width: 32,
    height: 32,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  item: {
    padding: 10,
  },
  separator: {
    height: 0.5,
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  text: {
    fontSize: 15,
    color: 'black',
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#800000',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
});
const mapStateToProps = state => {
  return {
    posts: state.Posts.posts,
    authResultSharePost: state.Posts.authResultSharePost,
    authResultAbusePost: state.Posts.authResultAbusePost,
    isLoading: state.Posts.isLoading,
    isdata: state.Posts.isdata,
  };
};

export default connect(
  mapStateToProps,
  {
    getMYPosts,
    getPostsSharepost,
    getPostsAbusePost
  }
)(Posts);
