/* @flow */

import React, { Component } from "react";
import { View, Text, StyleSheet, Image, FlatList, RefreshControl } from "react-native";
import ItemTestimonials from "./ItemTestimonials";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import { connect } from "react-redux";
import { getProfileTestimonial } from "../Actions";
import { getUser } from "../Database/allSchema";
import { POSTIMAGEPATH } from "../Actions/type";
import Loader from './Loader';

let userId = "";

class Testimonials extends Component {
  constructor (props) {
    super(props);
    this.state = {
      pageCount: 1,
      testimonialsData: [],
      loading: false,
      error: null,
      refreshing: false,
      fetching_from_server: false
    };
    // this.onEndReached = this.onEndReached.bind(this);
  }
  _onRefresh = () => {
    this.setState({ refreshing: true });
    getUser()
      .then(user => {
        this.setState({ refreshing: false });
        if (user.length > 0) {
          userId = user[0].id;
          this.setState({ pageCount: 1 });
          this.dataLoad(false);
        }
      })
      .catch(error => {
        this.setState({ refreshing: false });
        console.log(error);
      });

  }

  componentWillMount() {
    if (!this.props.ProfileTestimonial_list || this.props.ProfileTestimonial_list.length == 0) {
      getUser()
        .then(user => {
          if (user.length > 0) {
            userId = user[0].id;
            this.setState({ pageCount: 1 });
            this.dataLoad(true);
          }
        })
        .catch(error => {
          console.log(error);
        });
    }

    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        // getUser()
        // .then(user => {
        //   if (user.length > 0) {
        //     userId = user[0].id;
        //       this.setState({ pageCount: 1 });
        //       this.dataLoad();
        //   }
        // })
        // .catch(error => {
        //   console.log(error);
        // }); 
      }
    );
  }
  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  // componentDidMount() {
  //   getUser()
  //     .then(user => {
  //       if (user.length > 0) {
  //         // this.setState({ userData: user[0] });
  //         userId = user[0].id;
  //         console.log(userId);
  //         this.dataLoad();
  //
  //
  //       }
  //     })
  //     .catch(error => {
  //       console.log(error);
  //     });
  // }
  dataLoad(isLoading) {
    console.log(userId, this.state.pageCount);
    this.props.getProfileTestimonial({
      userId,
      page: this.state.pageCount,
      token: this.props.SplashauthToken,
      isLoading,
      isMyProfile: true
    });
  }

  handleLoadMore = () => {
    this.setState(
      {
        pageCount: this.state.pageCount + 1
      },
      () => {
        console.log(this.state.pageCount);
        if (this.props.isData) {
          this.props.getProfileTestimonial({
            userId,
            page: this.state.pageCount,
            Testimonials: this.props.ProfileTestimonial_list,
            token: this.props.SplashauthToken,
            isLoading: false,
            isMyProfile: true
          });
        }
      }
    );
  };

  onOtherProfile(item) {
    this.props.navigation.navigate("Profile", {
      user_id: item.user_id,
      fullname: item.fullname,
      profile_image: POSTIMAGEPATH + item.user_photo,
      form: 'MyProfile1'
    });
  }

  render() {
    return (
      <View style={ { flex: 1, backgroundColor: "white" } }>
        { !this.props.isLoading &&
          this.props.ProfileTestimonial_list != null &&
          this.props.ProfileTestimonial_list.length > 0 ? (
            <FlatList
              key={ this.props.ProfileTestimonial_list.length - 1 }
              keyExtractor={ item => item.testimonial_id }
              refreshControl={
                <RefreshControl
                  tintColor={ ['#CC181E'] }
                  refreshing={ this.state.refreshing }
                  onRefresh={ this._onRefresh.bind(this) }
                />
              }
              maxHeight={ null }
              style={ { backgroundColor: "white" } }
              data={ this.props.ProfileTestimonial_list }
              onEndReached={ this.handleLoadMore.bind(this) }
              onEndReachedThreshold={ 1 }
              showsHorizontalScrollIndicator={ false }
              renderItem={ ({ item, index }) => {
                return (
                  <ItemTestimonials
                    fireId={ item.firebase }
                    Name={ item.fullname }
                    userprofileImage={ { uri: POSTIMAGEPATH + item.user_photo } }
                    Date={ item.date }
                    Des={ item.description }
                    openMyProfile={ () => this.onOtherProfile(item) }
                    modaluserphoto={ { uri: POSTIMAGEPATH + item.user_photo } }
                  />
                );
              } }
            />
          ) : (
            <View
              style={ {
                justifyContent: "center",
                alignItems: "center",
                height: 400
              } }
            >
              {/* <Image
              style={{ height: 130, width: 130 }}
              source={require("../img/no-data-found.jpg")}
            /> */}
              <Text style={ { fontSize: 13, fontFamily: "Lato-Regular", color: '#686868', } }>No testimonials to show</Text>
            </View>
          ) }
        <Loader
          loading={ this.props.isLoading }

        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  CloseViewSTyle: {
    position: "absolute",
    right: 10,
    top: 10,
    backgroundColor: "rgba(255,0,0,0.5)",
    width: 32,
    height: 32,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  item: {
    padding: 10
  },
  separator: {
    height: 0.5,
    backgroundColor: "rgba(0,0,0,0.4)"
  },
  text: {
    fontSize: 15,
    color: "black"
  },
  footer: {
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: "#800000",
    borderRadius: 4,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  btnText: {
    color: "white",
    fontSize: 15,
    textAlign: "center"
  }
});

const mapStateToProps = state => {
  return {
    ProfileTestimonial_list: state.ProfileTestimonial.ProfileTestimonial_list,
    authResult: state.ProfileTestimonial.authResult,
    isLoading: state.ProfileTestimonial.isLoading,
    isdata: state.ProfileTestimonial.isdata,
    SplashauthToken: state.Splash.authToken
  };
};

export default connect(
  mapStateToProps,
  {
    getProfileTestimonial
  }
)(Testimonials);
