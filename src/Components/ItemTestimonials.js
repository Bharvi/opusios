/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity, Modal
} from 'react-native';
import { ProgressDialog, Dialog } from 'react-native-simple-dialogs';
import ImageLoad from './ImageLoad';
import UserProfileImg from '../Components/common/UserProfileImg';

export default class ItemTestimonials extends Component {
  constructor (props) {
    super(props);
    this.state = {
      prifilePicDialogVisible: false,
    };
  }
  render() {
    return (
      <View style={ styles.container }>
        <View style={ { flexDirection: 'row', padding: 12 } }>
          <TouchableOpacity style={ { flex: 2, flexDirection: 'row' } }
            activeOpacity={ 0.8 }
            onPress={ () => this.setState({ prifilePicDialogVisible: true }) }
          >
            {/* <ImageLoad
                        style={{ height: 60, width: 60, borderRadius: 60 / 2 }}
                        borderRadius={60 / 2}
                        source={this.props.userprofileImage}
                  /> */}
            <UserProfileImg
              style={ { height: 60, width: 60, borderRadius: 60 / 2 } }
              borderRadius={ 60 / 2 }
              source={ this.props.userprofileImage }
              dotSize={ 11 }
              fireId={ this.props.fireId }
              top={ '65%' }
              right={ '20%' }
            />
          </TouchableOpacity>
          <View style={ { flex: 7 } }>
            <TouchableOpacity
              style={ { flexDirection: 'row', justifyContent: 'space-between', paddingRight: 5, marginBottom: 5 } }
              activeOpacity={ 0.8 }
              onPress={ this.props.openMyProfile }>
              <Text allowFontScaling={ false } style={ styles.textStyle }>{ this.props.Name }</Text>
              <Text allowFontScaling={ false } style={ styles.dateStyle }>{ this.props.Date }</Text>
            </TouchableOpacity>
            <Text allowFontScaling={ false } style={ styles.desStyle }>{ this.props.Des }</Text>
          </View>
        </View>
        <Modal
          transparent={ true }
          visible={ this.state.prifilePicDialogVisible }
          onRequestClose={ () => this.setState({ prifilePicDialogVisible: false }) }
        >
          <View
            style={ {
              height: "100%",
              width: "100%",
              paddingTop: 130,
              paddingBottom: 100
            } }
          >
            <View style={ { backgroundColor: 'rgba(0,0,0,0.9)', padding: 5, justifyContent: 'center', alignContent: 'center' } }>
              <View style={ { alignItems: "center", justifyContent: "center" } }>
                <ImageLoad
                  style={ { height: '100%', width: "100%" } }
                  source={ this.props.modaluserphoto }
                  resizeMode="contain"
                />
                <TouchableOpacity
                  style={ styles.CloseViewSTyle }
                  onPress={ () =>
                    this.setState({ prifilePicDialogVisible: false })
                  }
                >
                  <Image
                    style={ { height: 18, width: 18 } }
                    source={ require("../img/close_icon.png") }
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderColor: '#e2e0e0'
  },
  textStyle: {
    fontFamily: 'Lato-Bold',
    fontSize: 16,
    color: '#6E6E6E'

  },
  dateStyle: {
    fontFamily: 'Lato-Regular',
    fontSize: 12,
    color: '#9E9E9E'
  },
  desStyle: {
    fontFamily: 'Lato-Regular',
    fontSize: 14,
    color: '#9E9E9E'
  },
  CloseViewSTyle: { position: "absolute", right: 2, top: 2, backgroundColor: 'rgba(255,0,0,0.5)', width: 32, height: 32, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }
});
