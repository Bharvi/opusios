import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';

class SearchBottom extends Component {
  render() {
    return (
    <View>
  <Image
   source={ require('../img/search_icon.png')}
   style={{ height: 25, width: 25 }}
   />
    </View>
    );
  }
}
export default SearchBottom;
