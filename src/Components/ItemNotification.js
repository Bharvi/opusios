/* @flow */

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image
} from "react-native";
import ImageLoad from './ImageLoad';
import UserProfileImg from '../Components/common/UserProfileImg';
let { width } = Dimensions.get("window");

let boxCount = 2;
let boxWidth = width / boxCount;

export default class ItemNotification extends Component {
  text() {
    if (this.props.des == "1") {
      return " has liked your photo.";
    } else if (this.props.des == "2") {
      return " has commented on your photo.";
    } else if (this.props.des == "3") {
      return " private message.";
    } else if (this.props.des == "4") {
      return " connect request message.";
    } else if (this.props.des == "5") {
      return " added you as friend message.";
    }
    else if (this.props.des == "8") {
      return " has shared your post.";
    }
  }
  render() {
    return (
      <View style={ styles.container }>
        <View style={ { flex: 3, padding: 10 } }>
          <TouchableOpacity style={ styles.viewDivideStyle }
            activeOpacity={ 0.8 }
            onPress={ this.props.onPressProfile }>
            <TouchableOpacity
              style={ { flexDirection: 'row' } }
              activeOpacity={ 0.8 }
              onPress={ this.props.onPressUserIcon }
            >
              {/* <ImageLoad
                style={{ height: 45, width: 45, borderRadius: 220 / 2 }}
                borderRadius={45 / 2}
                source={this.props.usericon}
              /> */}
              <UserProfileImg
                style={ { height: 45, width: 45, borderRadius: 45 / 2 } }
                borderRadius={ 45 / 2 }
                source={ this.props.usericon }
                dotSize={ 10 }
                fireId={ this.props.fireId }
                top={ '60%' }
                right={ '23%' }
              />
            </TouchableOpacity>
            <View style={ { width: '100%', flex: 1 } }>
              <Text
                allowFontScaling={ false }
                numberOfLines={ 1 }
                style={ {
                  flexDirection: "row",
                  marginLeft: 10,
                  // alignItems: "center",
                  // flexWrap: 'wrap'
                } }
              >
                <Text
                  allowFontScaling={ false }
                  style={ {
                    color: "#151515",
                    fontSize: 16,
                    fontFamily: "Lato-Bold"
                  } }
                  onPress={ this.props.onProfile }
                >
                  { this.props.name }
                </Text>
                <Text
                  allowFontScaling={ false }
                  style={ {
                    color: "#686868",
                    fontSize: 14,
                    fontFamily: "Lato-Regular"
                  } }
                >
                  { this.text() }
                </Text>
              </Text>
              <View style={ { marginLeft: 10 } }>
                <Text
                  allowFontScaling={ false }
                  style={ {
                    color: "#686868",
                    fontSize: 14,
                    fontFamily: "Lato-Regular"
                  } }
                >{ this.props.timing }
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffffff",
    width: null,
    flex: 3,
    borderBottomWidth: 1,
    borderColor: "#dddee4"
  },
  viewDivideStyle: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  }
});
