import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Platform,
  ActivityIndicator,
} from 'react-native';
import { connect } from "react-redux";
import { getMYPosts, getPostsSharepost, getPostsAbusePost } from "../Actions";
import { POSTIMAGEPATH } from "../Actions/type";
import ItemPost from "./ItemPost";

 class Demo extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      //Loading state used while loading the data for the first time
      serverData: [],
      //Data Source for the FlatList
      fetching_from_server: false,
      //Loading state used while loading more data
      seed: 1,
      page: 1,
      users: [],
      dataPosts: [],
      isLoading: false,
      isRefreshing: false,
    };
    this.offset = 1;

    //Index of the offset to load from web API
  }

  componentDidMount() {
   // this.loadUsers();
   this.setState({ isLoading: true });
   console.log(this.state.page);
   this.props.getMYPosts(107, this.state.page);
   this.setState({ users: this.props.posts });
   console.log(this.state.users);
   // this.setState({
   //   users: page === 1 ? this.props.posts : [...users, ...this.props.posts],
   //   isRefreshing: false,
   // });
   // console.log('Store Data', this.state.users);
   this.onScrollHandler();
 };

  // loadUsers = () => {
  //     const { users, seed, page } = this.state;
  //     this.setState({ isLoading: true });
  //     this.props.getMYPosts(107, this.state.page);
  //     this.setState({
  //       users: page === 1 ? this.props.posts : [...users, ...this.props.posts],
  //       isRefreshing: false,
  //     });
  //     console.log('Store Data', this.state.users);
  //   };



  // handleRefresh = () => {
  //    this.setState({
  //      page: this.state.page + 1,
  //      isRefreshing: true,
  //    }, () => {
  //        this.props.getMYPosts(107, this.state.page);
  //    });
  //  };

   // handleLoadMore = () => {
   //   this.setState({
   //     page: this.state.page + 1
   //   }, () => {
   //     this.props.getMYPosts(107, this.state.page);
   //     this.setState({
   //       users: [...this.state.users, ...this.props.posts],
   //       isRefreshing: false,
   //     });
   //     console.log(this.state.users);
   //   });
   // };

   addRecords = (page) => {
     console.log(page);
     // assuming this.state.dataPosts hold all the records
     const newRecords = []
     for(var i = page * 12, il = i + 12; i < il && i <
       this.state.dataPosts.length; i++){
       newRecords.push(this.state.dataPosts[i]);
     }
     this.setState({
       users: [...this.state.posts, ...newRecords]
     });
     console.log(this.state.users);
   }

   onScrollHandler = () => {
        this.setState({
          page: this.state.page + 1
        }, () => {
          this.addRecords(this.state.page);
        });
}


  render() {
    return (
      <View style={styles.container}>
        {
          <FlatList
            style={{ width: '100%' }}
            keyExtractor={(item, index) => index}
            data={this.state.users}
            extraData={this.state.users}
            renderItem={({ item, index }) => {
              return (
                <ItemPost
                  name={item.fullname}
                  userprofileImage={{ uri: POSTIMAGEPATH + item.user_photo }}
                  des={item.time}
                  postTitle={item.description}
                  Image={ POSTIMAGEPATH + item.post_photo }
                  commentcount={item.comment_count}
                  onPressComment={() => this.onCommentRedirt(item)}
                  sharepost={() => this.onSharePost(item)}
                  Abusepost={() => this.onAbusePost(item)}
                  onPressUserIcon={() =>
                    this.setState({
                      prifilePicDialogVisible: true,
                      userPhoto: POSTIMAGEPATH + item.user_photo
                    })
                  }
                  openMyProfile={() => this.onProfileData(item)}
                />
              );
            }}
            // ItemSeparatorComponent={() => <View style={styles.separator} />}
            // ListFooterComponent={this.renderFooter.bind(this)}
            //Adding Load More button as footer component
            onEndReached={this.onScrollHandler}
            onEndThreshold={0}
          />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
  },
  item: {
    padding: 10,
  },
  separator: {
    height: 0.5,
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  text: {
    fontSize: 15,
    color: 'black',
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#800000',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
});
const mapStateToProps = state => {
  return {
    posts: state.Posts.posts,
    authResultSharePost: state.Posts.authResultSharePost,
    authResultAbusePost: state.Posts.authResultAbusePost,
    isLoading: state.Posts.isLoading
  };
};

export default connect(
  mapStateToProps,
  {
    getMYPosts,
    getPostsSharepost,
    getPostsAbusePost
  }
)(Demo);
