import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform
} from 'react-native';
import DeviceInfo from 'react-native-device-info';

export default class HeaderSeting extends Component {
  render() {
    return (
      <View>
        <View style={{ backgroundColor: "#CC181E", height: 15 }}></View>
        <View style={{ backgroundColor: "#CC181E", paddingTop: (DeviceInfo.getModel() === 'iPhone X' || DeviceInfo.getModel() === 'iPhone XS Max' || DeviceInfo.getModel() === 'iPhone XS' || DeviceInfo.getModel() === 'iPhone XR') ? 15 : 0 }}/>
      
      <View style={styles.container}>

                <TouchableOpacity
                 style={{ height: 50, width: 100, position: 'absolute',
                  justifyContent: 'center', left: 0,  }}
                 onPress={this.props.onLeftPressed}
                 activeOpacity={0.2}
                >
                    <Image
                    style={[styles.backArrowStyle,{ marginLeft: 10 }]}
                    source={this.props.leftImage}
                    />
                </TouchableOpacity>

            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                <Text allowFontScaling={false} style={styles.textStyle}> {this.props.headertext} </Text>
            </View>
            {
                  <TouchableOpacity
                  activeOpacity={0.8}
                  style={{ height:50, width: 50, alignItems: 'center',justifyContent: 'center',alignContent: 'center',
                position: 'absolute', right: 0}}
                 onPress={this.props.onPressDone}

                >
                <View
                style={{ flexDirection: 'row', marginRight: 8 }}
                >
                    <Text allowFontScaling={false} style={styles.textStyleDone}>{this.props.textDone}</Text>
                      <Image
                      style={styles.backArrowStyle}
                      source={this.props.rightImage}
                      />
                </View>

              </TouchableOpacity>
            }
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    backgroundColor: '#CC181E'
  },
  textStyle: {
    fontSize: 16,
    color: '#ffffff',
    fontFamily: 'Lato-Regular'
  },
  textStyleDone: {
    fontSize: 16,
    color: '#CCffffff',
    textAlign: 'center',
    fontFamily: 'Lato-Regular',

  },
  backArrowStyle: {
    height: 20,
    width: 20,
  }
});
