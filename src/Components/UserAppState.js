import React, { Component, PureComponent } from 'react';
import { View, AppState } from 'react-native';
import firebase from "react-native-firebase";
import { getUser } from '../Database/allSchema';
import { setUserStatus } from '../Actions';
import { connect } from "react-redux";
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-community/async-storage';

//opus firebaseUId : 3vNHH1afRYWfXX6ob9RfvXbfsPn1

let userId;
let onlineUserID = '';
class UserAppState extends PureComponent {
    constructor (props) {
        super(props);
        this.state = {
            appState: AppState.currentState
        };
    }

    componentWillMount() {
        console.log('in UserAppState', this.state.appState);
    }
    componentDidMount() {
        AppState.addEventListener('change', this.handleAppStateChange);
    }
    componentWillUnmount() {
        AppState.removeEventListener('change', this.handleAppStateChange);
    }
    firebaseConnectivity = () => {
        //This function for set onlineStatus false in firebase when user internection lost it work after 60 seconds 
        let userStatusRef = firebase.database().ref('friends/' + onlineUserID);
        // console.log('userListRef', userStatusRef);
        const isOfflineForDatabase = {
            onlineStatus: 'false',
            last_changed: firebase.database.ServerValue.TIMESTAMP,
        }
        // Monitor connection state on 
        firebase.database().ref(".info/connected")
            .on("value", (snap) => {
                //snap.val() containt value true or false.
                //if userId connected to firebase server then return true otherwise false
                if (snap.val()) {
                    console.log('snap value is true..');
                    //when disconnected
                    userStatusRef.onDisconnect().update(isOfflineForDatabase)
                        .then(() => {
                            console.log('This user lost internet then update object.')
                            // userStatusRef.update(isOnlineForDatabase)
                            if (this.state.appState == 'active') {


                                firebase.database().ref()
                                    .child("friends/" + onlineUserID)
                                    .update({
                                        onlineStatus: 'true'
                                    });
                                this.props.setUserStatus(userId, '1', token)

                            }
                        })
                        .catch(() => {
                            console.log('This user lost internet then not update object.')
                        });
                } else {
                    return;
                }
            });
    }
    handleAppStateChange = async (nextAppState) => {
        this.setState({ appState: nextAppState })
        let userId;
        let isdeviceConnected;
        try {
            const authToken = await AsyncStorage.getItem('@token')
            if (authToken !== null) {
                console.log('authToken', authToken);
                token = authToken
            }
        } catch (e) {
            console.log('Error in get token', e);
        }
        getUser()
            .then((user) => {
                if (user && user.length > 0) {

                    // console.log('user[0].id', user[0].id);
                    userId = user[0].id;

                    if (user[0].id == 1) {
                        onlineUserID = 'afdsfsdfdsfsdfsdfsdf';
                    } else {
                        onlineUserID = user[0].fire_id
                        // onlineUserID = firebase.auth().currentUser.uid;
                    }

                    //find device have internet connection or not
                    NetInfo.fetch().then((connectionInfo) => {
                        if (connectionInfo.type == "none") {
                            console.log('device is offline')
                            isdeviceConnected = false
                        } else {
                            console.log('device is online')
                            isdeviceConnected = true
                            //firebase connectivity for user
                            // this.firebaseConnectivity()
                        }
                    });

                    console.log('in handleAppStateChange', nextAppState)
                    console.log('onlineUserID', onlineUserID)

                    if (nextAppState == 'active') {
                        console.log('App has come to the forbackground!');
                        try {
                            // firebase.database().ref()
                            //     .child("friends/" + onlineUserID)
                            //     .update({
                            //         onlineStatus: 'true'
                            //     });
                            // this.props.setUserStatus(userId, '1', token)
                            this.firebaseConnectivity()

                        } catch (err) {
                            console.log('err_userOnline', err)
                        }
                    }
                    else if (nextAppState == 'background' || nextAppState == 'inactive') {
                        console.log('App has come to the background!', userId);
                        try {
                            firebase.database().ref()
                                .child("friends/" + onlineUserID)
                                .update({
                                    onlineStatus: 'false'
                                });
                            this.props.setUserStatus(userId, '0', token)
                        } catch (err) {
                            console.log('err_userOnline', err)
                        }

                    }
                }
            })
    };
    render() {
        return <View />
    }
}

const mapStateToProps = state => {
    return {
        SplashauthToken: state.Splash.authToken,
    };
};

export default connect(
    mapStateToProps,
    {
        setUserStatus
    }
)(UserAppState);
