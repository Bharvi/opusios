/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity
} from 'react-native';

export default class CompleteYourProfileLayout extends Component {


  render() {
    return (
      <TouchableOpacity
       activeOpacity={0.8}
       onPress={this.props.onPress}
       style = {{ }}
      >
      {(this.props.item.seleted) ?
        <View
         style = {styles.fviewstyle}

        >
          <Text allowFontScaling={false} numberOfLines={1}  style = { styles.textstyles }>{this.props.item.name}</Text>
        </View>
      :

        <View
         style = {styles.viewStyle}
        >
          <Text allowFontScaling={false}  numberOfLines={1}  style = { styles.textstyle }>{this.props.item.name}</Text>
        </View>
      }
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({

  textstyle:{
    color: 'black',
    fontSize: 13,
    margin:2,
    alignSelf:'center',
    flexWrap: "wrap",
  },
  textstyles:{
    color: 'white',
    fontSize: 13,
    margin:2,
    alignSelf:'center'
  },
  viewStyle:{
    padding: 3 ,
    backgroundColor:'#E5E8E8',
    margin:2 ,
    borderRadius:3
  },
  fviewstyle:{
    padding: 3 ,
    backgroundColor:'black' ,
    margin:2 ,
    borderRadius:3
  }

});
