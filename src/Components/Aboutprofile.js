/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView
} from 'react-native';

import { connect } from 'react-redux';
import { getMYAbout, initialAboutStateData } from '../Actions';
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import Loader from './Loader';

let userId = "";

class Aboutprofile extends Component {	
	
	  //  componentDidMount() {
    //   setTimeout(() => {
    //     this.props.getMYAbout({userId: this.props.userId, token: this.props.SplashauthToken });
    //   }, 500);
            
    //   this.willFocusSubscription = this.props.navigation.addListener(
    //     'willFocus',
    //     () => {
    //       this.props.initialAboutStateData();
    //       setTimeout(() => {
    //         this.props.getMYAbout({userId: this.props.userId, token: this.props.SplashauthToken});
    //       }, 500);
            
    //     }
    //   );
    //   }
    //   componentWillUnmount() {
    //     this.willFocusSubscription.remove();
    //   }
	
	
  render() {
    return (
      <ScrollView>
      <View style={styles.container}>
              <View style={{ flexDirection: 'row', padding: 20 }}>
                    <Image style={styles.locationStyle} source={require('../img/location_icon.png')} />
                    <View style={{ marginLeft: 10, marginRight: 10, paddingBottom: 15, width: 280, borderBottomWidth: 1, borderColor: '#e2e0e0' }}>
                        <Text allowFontScaling={false}  style={styles.textStyle}>Location</Text>
                        <Text allowFontScaling={false}    style={styles.desTextStyle}>{ (this.props.About_list.location != '') && (this.props.About_list.location != null) ? this.props.About_list.location : 'Not Available' }</Text>
                    </View>
              </View>
              
              <View style={{ backgroundColor: '#e2e0e0', width: 1, height: '100%', position: 'absolute', top: 45, left: 32 }} />

              <View style={{ flexDirection: 'row', paddingLeft: 20, paddingRight: 20, paddingBottom: 20 }}>
                    <Image style={styles.locationStyle} source={require('../img/mail_icon.png')} />
                    <View style={{ marginLeft: 10, marginRight: 10, paddingBottom: 15, width: 280, borderBottomWidth: 1, borderColor: '#e2e0e0' }}>
                        <Text allowFontScaling={false}  style={styles.textStyle}>Profession</Text>
                        <Text allowFontScaling={false}  style={styles.desTextStyle}>{(this.props.About_list.profession != '') && (this.props.About_list.profession != null) ? this.props.About_list.profession : 'Not Available' }</Text>
                    </View>
              </View>
             
              {/* <View style={{ backgroundColor: '#e2e0e0', width: 1, height: 58, position: 'absolute', top: 111, left: 32, }} /> */}

              <View style={{ flexDirection: 'row', paddingLeft: 20, paddingRight: 20, paddingBottom: 18 }}>
                    <Image style={styles.locationStyle} source={require('../img/birthday_icon.png')} />
                    <View style={{ marginLeft: 10, marginRight: 10, paddingBottom: 15, width: 280, borderBottomWidth: 1, borderColor: '#e2e0e0' }}>
                        <Text allowFontScaling={false}  style={styles.textStyle}>Birthday</Text>
                        <Text allowFontScaling={false}  style={styles.desTextStyle}>{
                           (this.props.About_list.birth_date != '' && 
                           this.props.About_list.birth_date != null && 
                           this.props.About_list.birth_date != '0' &&
                           this.props.About_list.birth_date !== "Invalid date") ? this.props.About_list.birth_date : 'Not Available'  }</Text>
                    </View>
              </View>
              {/* <View style={{ backgroundColor: '#e2e0e0', width: 1, height: 60, position: 'absolute', top: 176, left: 32 }} /> */}

              <View style={{ flexDirection: 'row', paddingLeft: 20, paddingRight: 20, paddingBottom: 20 }}>
                    <Image style={styles.locationStyle} source={require('../img/mail_femail_icon.png')} />
                    <View style={{ marginLeft: 10, marginRight: 10, paddingBottom: 15, width: 280, borderBottomWidth: 1, borderColor: '#e2e0e0' }}>
                        <Text allowFontScaling={false}  style={styles.textStyle}>Sex</Text>
                        <Text allowFontScaling={false} style={styles.desTextStyle}>{(this.props.About_list.gender != null) ?((this.props.About_list.gender == "0") ? 'Female' : 'Male') : 'Not Available'}</Text>
                    </View>
              </View>
              {/* <View style={{ backgroundColor: '#e2e0e0', width: 1, height: 55, position: 'absolute', top: 244, left: 32 }} /> */}

              <View style={{ flexDirection: 'row', paddingLeft: 20, paddingRight: 20, paddingBottom: 20 }}>
                    <Image style={styles.locationStyle} source={require('../img/file_icon.png')} />
                    <View style={{ marginLeft: 10, marginRight: 10, paddingBottom: 10, }}>
                        <Text allowFontScaling={false}  style={styles.textStyle}>Brief Description</Text>
                        <Text allowFontScaling={false}  style={styles.desTextStyle}>{(this.props.About_list.brief_desc != '') && (this.props.About_list.brief_desc != null) ? this.props.About_list.brief_desc : 'Not Available'}</Text>
                    </View>
              </View>
              {/* <View style={{ backgroundColor: '#e2e0e0', width: 1, height: 55, position: 'absolute', top: 311, left: 32 }} /> */}
      </View>
	  <Loader
        loading={this.props.isLoading}
        
      />
    </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  locationStyle: {
    width: 25,
    height: 25,
  },
  textStyle: {
    fontSize: 14,
    fontFamily: 'Lato-Medium',
    color: '#7F7F7F'
  },
  desTextStyle: {
    fontSize: 16,
    fontFamily: 'Lato-Medium',
    color: '#333333'
  }
});
const mapStateToProps = state => {
    return {
      About_list: state.OtherProfile.About_list,
      authResult: state.OtherProfile.authResult,
      isLoading: state.OtherProfile.isLoading,
      SplashauthToken: state.Splash.authToken,
    };
};

export default connect(mapStateToProps, {
  getMYAbout,
  initialAboutStateData
})(Aboutprofile);
