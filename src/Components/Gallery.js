import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions
} from 'react-native';
import {
  TabView,
  TabBar,
  SceneMap,
  type Route,
  type NavigationState,
} from 'react-native-tab-view';
import Header from './Header';
// import All from './All';
import Photo from './Photo';
import Video from './Video';

type State = NavigationState<
  Route<{
    key: string,
    title: string,
  }>
>;

const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

const tabWidth = Dimensions.get('window').width / 2;

export default class Gallery extends React.Component<*, State> {
  static title = 'Scrollable top bar';
  static backgroundColor = '#147f7e';
  static appbarElevation = 0;

  state = {
    index: 0,
    routes: [
      // { key: 'all', title: 'All' },
      { key: 'photo', title: 'Photo' },
      { key: 'video', title: 'Video' },

    ],
  };
  _handleIndexChange = index =>
    this.setState({
      index,
    });

  _renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );

  _renderScene = SceneMap({
    // all: All,
    photo: Photo,
    video: Video,

  });


  OnPressed() {
    this.props.navigation.goBack(null);
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          onLeftPressed={() => this.OnPressed()}
          leftImage={require('../img/padding_close.png')}
          rightImage={require('../img/check_icon.png')}
          headertext="Gallery"
          onPressDone={() => this.OnPressed()}
        />
          <View style={{ flex: 1 }}>
          <TabView
            style={[styles.container, this.props.style]}
            navigationState={this.state}
            renderScene={this._renderScene}
            renderTabBar={this._renderTabBar}
            onIndexChange={this._handleIndexChange}
            initialLayout={initialLayout}
          />
          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabbar: {
    backgroundColor: '#CC181E',
  },
  tab: {
    width: tabWidth,
  },
  indicator: {
    backgroundColor: '#ffffff',
    height: 2.5,
  },
  label: {
    color: '#ffffff',
    fontSize: 14,
    fontFamily: 'Lato-Bold'
  },

});
