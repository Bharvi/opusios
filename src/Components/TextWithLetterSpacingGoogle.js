import React from 'react'
import { StyleSheet, View } from 'react-native'
import { LetterGoogle } from './LetterGoogle'

const spacingForLetterIndex = (letters, index, spacing) => (letters.length - 1 === index) ? 0 : spacing

export const TextWithLetterSpacingGoogle = (props) => {
  const { children, spacing, viewStyle, textStyle } = props
  const letters = children.split('')

  return <View style={[styles.container, viewStyle]}>
    {letters.map((letter, index) =>
      <LetterGoogle key={index} spacing={spacingForLetterIndex(letters, index, spacing)} textStyle={textStyle}>
        {letter}
      </LetterGoogle>
    )}
  </View>
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row'
  }
})
