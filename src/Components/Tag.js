/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  Alert,
  RefreshControl,
  ScrollView
} from 'react-native';

import ItemTags from './ItemTags';
import { connect } from 'react-redux';
import {
  getProfileTag, addtagErrorChange,
  addtagChange, getAddTAg, getDeleteTAg,
  initialTAGProfileStateData
} from '../Actions';
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import { getUser } from "../Database/allSchema";
import Loader from "./Loader";
// import { ScrollView } from 'react-native-gesture-handler';


let userId = "";
let numColumns = 3;


let formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);
  let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberOfElementsLastRow != numColumns && numberOfElementsLastRow != 0) {
    data.push({ key: 'blank-${numberOfElementsLastRow}', empty: true });
    numberOfElementsLastRow++;
  }
  return data;
}


class Tag extends Component {

  constructor (props) {
    super(props);
    this.state = {
      refreshing: false,
      TagsData: [
        { name: 'music' },
        { name: 'MBA' },
        { name: 'acting' },
      ],
      time: Date.now()
    };
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    getUser()
      .then(user => {

        if (user.length > 0) {
          userId = user[0].id;
          this.props.getProfileTag({ userId, token: this.props.SplashauthToken, isLoading: false });
          this.setState({ refreshing: false });
        }

      })
      .catch(error => {
        this.setState({ refreshing: false });
        console.log(error);
      });

  }
  componentWillMount() {
    if (!this.props.ProfileTag_list || this.props.ProfileTag_list.length == 0) {
      this.props.initialTAGProfileStateData();
      getUser()
        .then(user => {
          if (user.length > 0) {
            userId = user[0].id;
            this.props.getProfileTag({ userId, token: this.props.SplashauthToken, isLoading: true });
          }
        })
        .catch(error => {
          console.log(error);
        });
    }

    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        // getUser()
        // .then(user => {
        //   if (user.length > 0) {
        //     userId = user[0].id;
        //     this.props.getProfileTag({ userId, token: this.props.SplashauthToken });
        //   }
        // })
        // .catch(error => {
        //   console.log(error);
        // });
      }
    );
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }


  onaddTagChange(text) {
    this.props.addtagChange(text);
  }

  componentWillReceiveProps(nextProp) {
    if (nextProp.authResultuserTag === "Success") {
      setTimeout(() => {
        Alert.alert(
          'Success',
          'Add tag successfully',
          [
            { text: 'OK', onPress: () => this.GetTag() },
          ],
          { cancelable: false }
        )

      }, 500);

    } else if (nextProp.authResultuserTag === "tag already created") {
      // setTimeout(() => {
      //   Alert.alert("Oops", "Tag already created");

      // }, 500);

    }
    if (nextProp.authResultuserTagDelete === "Tag delete success") {
      // setTimeout(() => {
      //   Alert.alert(
      //   'Success',
      //   'Tag delete successfully',
      //   [
      //     { text: 'OK', onPress: () => this.onButtonPress()},
      //   ],
      //   { cancelable: false }
      // )

      // }, 500);

    } else if (nextProp.authResultuserTagDelete === "User id must be numeric.") {
      // Alert.alert("Oops", "User id must be numeric.");
    }
  }
  // componentWillMount() {
  //   getUser()
  //     .then(user => {
  //       if (user.length > 0) {
  //         // this.setState({ userData: user[0] });
  //         userId = user[0].id;
  //         console.log(userId);
  //       this.props.getProfileTag(userId );
  //       }
  //     })
  //     .catch(error => {
  //       console.log(error);
  //     });
  // }

  onButtonPress() {
    let { user_tag } = this.props;

    // reg= /(^\s+|\s+$)/g, '';
    if (/^(\w+\s?)*\s*$/.test(user_tag)) {
      if (user_tag.replace(/(^\s+|\s+$)/g, '') !== '') {
        let isValid = true;
        if (user_tag === "") {
          isValid = false;
          this.props.addtagErrorChange("Enter tag");
        }
        // else if(reg.test(user_tag)=== false) {
        //   isValid = false;
        //   this.props.addtagErrorChange("Enter valid tag");
        // } 
        else {
          this.props.addtagErrorChange("");
        }
        if (isValid === true) {
          // this.props.navigation.navigate('BottomTab', { myProps: this.props });

          this.props.getAddTAg({ userId, tag: user_tag, token: this.props.SplashauthToken });
        }
      }
    } else {
      setTimeout(() => {
        Alert.alert('Alert', 'Please enter valid character.');

      }, 500);
    }

  }

  DeleteTag(item) {
    console.log('delete tag name', item.user_tag);
    let deleteTag = item.user_tag
    Alert.alert(
      'Oops',
      'Are you sure, you want to delete ' + deleteTag + ' tag ?',
      [
        // { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
        // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => this.ApiCAll(item) },
        { text: 'No', onPress: () => { cancelable: false } },
      ],
      { cancelable: false }
    )
  }
  ApiCAll(item) {
    this.props.getDeleteTAg({ userId, tag_id: item.tag_id, token: this.props.SplashauthToken });

    setTimeout(() => {
      this.GetTag();

    }, 20);

  }
  GetTag() {
    this.props.getProfileTag({ userId, token: this.props.SplashauthToken, isLoading: true })
  }


  render() {
    return (
      <ScrollView
        refreshControl={
          <RefreshControl
            tintColor={ ['#CC181E'] }
            refreshing={ this.state.refreshing }
            onRefresh={ this._onRefresh.bind(this) }
          />
        }
        style={ { flex: 1, backgroundColor: 'white', } }>

        <View style={ { margin: 10 } }>
          <View
            style={ {
              flexDirection: "row",
              justifyContent: "space-between"
            } }
          >
            <View
              style={ {
                flex: 1,
                borderBottomWidth: 1,
                borderColor: "#A9A9A9",
                marginRight: 5,
              } }
            >
              <TextInput
                allowFontScaling={ false }
                underlineColorAndroid="transparent"
                returnKeyType="next"
                keyboardType="email-address"
                autoCapitalize="none"
                placeholder="Enter tag"
                style={ styles.textInputStyle }
                value={ this.props.user_tag }
                onChangeText={ this.onaddTagChange.bind(this) }
              />
            </View>
            <View style={ { width: 60, padding: 4 } }>
              <TouchableOpacity
                onPress={ () => this.onButtonPress() }
                style={ {
                  flex: 1,
                  backgroundColor: '#CC181E',
                  alignItems: 'center',
                  justifyContent: 'center'
                } }>
                <Text allowFontScaling={ false } style={ {
                  fontSize: 14, color: 'white', textAlign: 'center'
                } }>Add</Text>
              </TouchableOpacity>
              {/* <Button
                allowFontScaling={ false }
                textStyle={ {
                  fontSize: 14
                } }
                title="Add"
                color="#CC181E"
                style={ styles.dialogokTextStyle }
                onPress={ () => this.onButtonPress()
                }
              /> */}
            </View>
          </View>
          <Text allowFontScaling={ false } style={ styles.errorTextStyle }>
            { this.props.user_tagError }
          </Text>
          {
            !this.props.isLoading && this.props.ProfileTag_list != null && this.props.ProfileTag_list.length > 0 ? (
              <FlatList
                contentContainerStyle={ { flexDirection: 'row', flexWrap: 'wrap' } }
                data={ this.props.ProfileTag_list }
                // numColumns={numColumns}
                style={ { marginBottom: 10 } }
                renderItem={ ({ item, index }) => {
                  if (item.empty === true) {
                    return <View style={ [styles.item, styles.itemInvisible] } />;
                  }
                  return (
                    <TouchableOpacity
                      onPress={ () =>
                        this.DeleteTag(item)
                      }
                      style={ {
                        backgroundColor: "#eaeaea",
                        borderRadius: 5,
                        borderColor: "#e2e0e0",
                        borderWidth: 1,
                        justifyContent: "space-between",
                        alignItems: "center",
                        marginLeft: 5,
                        marginTop: 5,
                        padding: 3,
                        flexDirection: "row",

                      } }
                    >
                      <Text
                        allowFontScaling={ false }
                        numberOfLines={ 1 }
                        style={ {
                          fontSize: 14,
                          margin: 2,
                          color: "#676767",
                          fontFamily: "Lato-Regular",
                          paddingLeft: 5,
                          paddingRight: 5,

                        } }
                      >
                        { item.user_tag }
                      </Text>
                      <TouchableOpacity
                        onPress={ () =>
                          this.DeleteTag(item)
                        }
                      >
                        <Image
                          style={ {
                            height: 12,
                            width: 12,
                            tintColor: "black"
                          } }
                          source={ require("../img/close_icon.png") }
                        />
                      </TouchableOpacity>
                    </TouchableOpacity>
                  );
                } }
              />
            ) :
              <View
                style={ {
                  justifyContent: "center",
                  alignItems: "center",
                  height: 300
                } }
              >

                {/* <Image style={{ height: 130, width: 130 }}
                  source={require('../img/no-data-found.jpg')} /> */}
                <Text allowFontScaling={ false } style={ { fontSize: 13, fontFamily: "Lato-Regular", color: '#686868', } }>No tag to show</Text>
              </View>
          }
        </View>
        {
          // (this.props.ProfileTag_list != null && this.props.ProfileTag_list.length > 0 )?
          // <FlatList
          //   data={formatData(this.props.ProfileTag_list, numColumns)}
          //   showsHorizontalScrollIndicator={false}
          //   numColumns={numColumns}
          //   removeClippedSubviews={true}
          //   renderItem={({ item, index }) => {
          //   if (item.empty === true) {
          //   return <View style={[styles.item, styles.itemInvisible]} />;
          //  }
          //   return (
          //     <ItemTags
          //       tagName={item.user_tag}
          //
          //     />
          //     );
          // }}
          // />
          // :
          // <View
          //   style={{
          //     justifyContent: "center",
          //     alignItems: "center",
          //     flex: 1
          //   }}
          // >
          //
          // <Image style={{ height: 130, width: 130}}
          //   source={require('../img/no-data-found.jpg')}/>
          // </View>
        }
        {
          <Loader
            loading={ this.props.isLoading }
          />
        }
      </ScrollView>
    );
  }
}

const styles = {
  item: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    marginLeft: 5,
    marginRight: 5
  },

  itemInvisible: {
    backgroundColor: '#fff'
  },
  errorTextStyle: {
    fontSize: 14,
    color: "#F00",
    textAlign: "left",
    marginRight: 10,
    marginTop: 3,
    fontFamily: "Lato-Regular"
  },
  textInputStyle: {
    color: "#1d1d26",
    fontFamily: "Lato-Regular",
    paddingLeft: 10,
    fontSize: 16,
    height: 40
  }
};



const mapStateToProps = state => {
  return {
    ProfileTag_list: state.ProfileTag.ProfileTag_list,
    authResult: state.ProfileTag.authResult,
    isLoading: state.ProfileTag.isLoading,
    user_tag: state.ProfileTag.user_tag,
    user_tagError: state.ProfileTag.user_tagError,
    authResultuserTag: state.ProfileTag.authResultuserTag,
    authResultuserTagDelete: state.ProfileTag.authResultuserTagDelete,
    SplashauthToken: state.Splash.authToken,
  };
};

export default connect(mapStateToProps, {
  getProfileTag,
  addtagErrorChange,
  addtagChange,
  getAddTAg,
  getDeleteTAg,
  initialTAGProfileStateData

})(Tag);
