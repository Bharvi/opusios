/* @flow */

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  Modal,
  Alert,
  RefreshControl
} from "react-native";
import Itemfriend from "./Itemfriend";
import { ProgressDialog, Dialog } from "react-native-simple-dialogs";
import { connect } from "react-redux";
import { getMyFriend, getUnfriend, saveUserChat, clerchatnotification } from "../Actions";
import { getUser } from "../Database/allSchema";
import { POSTIMAGEPATH, PROFILEIMAGEPATH } from "../Actions/type";
import ImageLoad from './ImageLoad';
import Loader from './Loader';

let userId = "";
let postID = "";

class Friend extends Component {
  constructor (props) {
    super(props);
    this.state = {
      refreshing: false,
      prifilePicDialogVisible: false,
      userPhoto: "",
      page: 1,
      pageCount: 1,
      FriendData: [
        { name: "Erika Pluda", des: "@ErikaPluda" },
        { name: "Twnada Kernel", des: "@ErikaPluda" },
        { name: "Danial Pluda", des: "like you post" }
      ]
    };
  }

  // componentWillReceiveProps(nextProp) {
  //   if (nextProp.authResultFriend === "UnFriend successfully") {
  //     // Alert.alert(
  //     //   "Success",
  //     //   "Unfriend successfully",
  //     //   [
  //     //     {
  //     //       text: "OK",
  //     //       onPress: () => {
  //     //         cancelable: false,
  //     //           this.props.getMyFriend(userId, this.state.page)
  //     //       }
  //     //     }
  //     //   ],
  //     //   { cancelable: false }
  //     // );
  //   } else if (nextProp.authResultFriend === "Not a Friend") {
  //     setTimeout(() => {
  //       Alert.alert("Oops", "Not a friend");

  //     }, 500);

  //   } else if (nextProp.authResultFriend === "User id must be numeric.") {
  //     setTimeout(() => {
  //       Alert.alert("Oops", "User id must be numeric.");

  //     }, 500);

  //   }
  // }
  _onRefresh = () => {
    this.setState({ refreshing: true });
    getUser()
      .then(user => {
        this.setState({ refreshing: false });
        if (user.length > 0) {
          console.warn('willFocus');
          userId = user[0].id;
          this.setState({ pageCount: 1 }, () => {
            this.fetchData(false);
          })
        }
      })
      .catch(error => {
        this.setState({ refreshing: false });
        console.log(error);
      });

  }
  componentWillMount() {
    if (!this.props.friend || this.props.friend.length == 0) {
      getUser()
        .then(user => {
          if (user.length > 0) {
            console.warn('willFocus');
            userId = user[0].id;
            this.setState({ pageCount: 1 })
            this.fetchData(true);
          }
        })
        .catch(error => {
          console.log(error);
        });
    }

    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        getUser()
          .then(user => {
            if (user.length > 0) {
              console.warn('willFocus');
              userId = user[0].id;
              this.setState({ userId, pageCount: 2 }, () => {
                console.log('userId', userId);
              })
              this.fetchData();
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    );
  }
  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  fetchData = (isLoading) => {
    console.warn("page pageCount ", userId, this.state.pageCount);
    getUser()
      .then(user => {
        if (user.length > 0) {
          console.warn('willFocus');
          userId = user[0].id;
          this.props.getMyFriend({
            userId,
            page: this.state.pageCount,
            friend: this.props.friend,
            token: this.props.SplashauthToken,
            isLoading,
            isMyProfile: true
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  handleLoadMore = () => {
    this.setState(
      {
        pageCount: this.state.pageCount + 1
      },
      () => {
        console.warn('friend handle more', this.state.pageCount + this.state.userId);
        if (this.props.isdata) {
          getUser()
            .then(user => {
              if (user.length > 0) {
                console.warn('willFocus');
                userId = user[0].id;
                this.props.getMyFriend({
                  userId,
                  page: this.state.pageCount,
                  friend: this.props.friend,
                  token: this.props.SplashauthToken,
                  isLoading: false,
                  isMyProfile: true
                });
              }
            })
            .catch(error => {
              console.log(error);
            });

        }
      }
    );
  };



  onUnfriend(item) {
    console.log(userId);
    console.log(item.friend_id);
    Alert.alert(
      'Alert',
      'Are you sure, you want to unfriend?',
      [
        // { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
        // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => this.UnFriendApi(item) },
        { text: 'No', onPress: () => { cancelable: false } },
      ],
      { cancelable: false }
    )

  }
  UnFriendApi(item) {
    this.props.getUnfriend({ userId, touserid: item.friend_id, token: this.props.SplashauthToken });
    this.setState({ pageCount: 1 });
  }

  onPressItem(item) {
    console.log("onPressItem");
    this.props.saveUserChat(item.friend_id, item.fullname, item.firebase);
    this.props.clerchatnotification(userId, item.friend_id, this.props.SplashauthToken);
    console.log(item.friend_id);
    console.log(item.firebase, "chat page ");
    setTimeout(() => {
      this.props.navigation.navigate("ChatMessage", {
        toUserId: item.friend_id,
        fullname: item.fullname,
        to_fire_id: item.firebase
      });
    }, 500);
  }

  ProfileRedirct(item) {
    this.props.navigation.navigate("Profile", {
      user_id: item.friend_id,
      form: 'MyProfile1'
    });
  }


  render() {
    return (
      <View style={ { flex: 1, backgroundColor: "white" } }>
        { !this.props.isLoading && this.props.friend != null && this.props.friend.length > 0 ? (
          <FlatList
            key={ this.props.friend.length - 1 }
            keyExtractor={ item => item.firebase }
            style={ { marginBottom: 10 } }
            refreshControl={
              <RefreshControl
                tintColor={ ['#CC181E'] }
                refreshing={ this.state.refreshing }
                onRefresh={ this._onRefresh.bind(this) }
              />
            }
            data={ this.props.friend }
            onEndReached={ this.handleLoadMore.bind(this) }
            onEndReachedThreshold={ 2 }
            renderItem={ ({ item, index }) => {
              return (
                <Itemfriend
                  fireId={ item.firebase }
                  name={ item.fullname }
                  userprofileImage={ {
                    uri: POSTIMAGEPATH + item.profile_image
                  } }
                  onPressUserIcon={ () =>
                    this.setState({
                      prifilePicDialogVisible: true,
                      userPhoto: POSTIMAGEPATH + item.profile_image
                    })
                  }
                  commentImage={ require("../img/message_icon.png") }
                  onPress={ () => this.onPressItem(item) }
                  profilenameclick={ () => this.ProfileRedirct(item) }
                // unfriend={() => this.onUnfriend(item)}
                />
              );
            } }
          // pagingEnabled
          // onMomentumScrollEnd={this.handleLoadMore}
          />
        ) : (
            <View
              style={ {
                justifyContent: "center",
                alignItems: "center",
                flex: 1
              } }
            >
              {/* <Image style={{ height: 130, width: 130}}
            source={require('../img/friends-not-available.jpg')}/> */}
              <Text style={ { fontSize: 13, fontFamily: "Lato-Regular", color: '#686868', } }>No friends to show</Text>
            </View>
          ) }

        <Modal transparent={ true }
          visible={ this.state.prifilePicDialogVisible }
          onRequestClose={ () => this.setState({ prifilePicDialogVisible: false }) }
        >
          <View
            style={ {
              height: "100%",
              width: "100%",
              paddingTop: 130,
              paddingBottom: 100
            } }
          >
            <View style={ { backgroundColor: 'rgba(0,0,0,0.9)', padding: 5, justifyContent: 'center', alignContent: 'center' } }>
              <View style={ { alignItems: "center", justifyContent: "center" } }>
                <ImageLoad
                  style={ { height: '100%', width: "100%" } }
                  source={ { uri: this.state.userPhoto } }
                  resizeMode="contain"
                />
                <TouchableOpacity
                  style={ styles.CloseViewSTyle }
                  onPress={ () =>
                    this.setState({ prifilePicDialogVisible: false })
                  }
                >
                  <Image
                    style={ { height: 18, width: 18 } }
                    source={ require("../img/close_icon.png") }
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        <Loader
          loading={ this.props.isLoading } />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  CloseViewSTyle: {
    position: "absolute",
    right: 10,
    top: 10,
    backgroundColor: "rgba(255,0,0,0.5)",
    width: 32,
    height: 32,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  }
});
const mapStateToProps = state => {
  return {
    friend: state.Friend.friend,
    authResult: state.Friend.authResult,
    // authResultFriend: state.Friend.authResultFriend,
    isLoading: state.Friend.isLoading,
    isdata: state.Friend.isdata,
    SplashauthToken: state.Splash.authToken,
  };
};

export default connect(
  mapStateToProps,
  {
    getMyFriend,
    getUnfriend,
    saveUserChat,
    clerchatnotification
  }
)(Friend);
