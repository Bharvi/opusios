/* @flow */

import React, { Component } from 'react';
import {
  View,
  Image,
  StyleSheet,
  Dimensions
} from 'react-native';

let width = Dimensions.get('window').width / 3;

export default class ItemViewGallery extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={{
            width,
            height: width,
            borderWidth: 1,
            borderColor: 'white'
          }}
          source={{ uri: this.props.item.node.image.uri }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
