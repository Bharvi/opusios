import React, { PureComponent } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Modal, ImageBackground, ActivityIndicator } from 'react-native';
import ImageLoad from '../../Components/ImageLoad';
import firebase from "react-native-firebase";

export default class UserProfileImg extends PureComponent {
    constructor (props) {
        super(props);
        this.state = {
            prifilePicDialogVisible: false,
            isUserOnline: false,
        };
    }

    render() {
        let size = this.props.dotSize
        let fireId = this.props.fireId
        let { top, right } = this.props
        let path = this.props.source.uri
        let imgName = path.substring(path.lastIndexOf('/') + 1)
        let profile = this.props.profile
        let isUserOnline = this.props.isUserOnline

        firebase.database().ref('friends/' + fireId)
            .on('value', (snapshot) => {
                // user Data 
                // console.log('snapshot_user', snapshot.val());
                if (snapshot.val()) {
                    if (snapshot.val().onlineStatus == 'true') {
                        // isUserOnline = true
                        this.setState({ isUserOnline: true })
                    } else {
                        this.setState({ isUserOnline: false })

                    }
                } else {
                    this.setState({ isUserOnline: false })

                }
            });
        return (
            <TouchableOpacity
                activeOpacity={ 0.5 }
                onPress={ () => this.setState({ prifilePicDialogVisible: !this.state.prifilePicDialogVisible }) }
                style={ { flexDirection: 'row' } }>
                {/* <ImageLoad
                    style={ this.props.style }
                    source={ this.props.source }
                    borderRadius={ this.props.borderRadius }
                /> */}
                <View style={ [this.props.style, { backgroundColor: '#e9eef1', alignContent: 'center', justifyContent: 'center' }] }>
                    <ImageBackground
                        style={ [this.props.style, { alignContent: 'center', justifyContent: 'center' }] }
                        source={ require('../../img/Unknown.jpg') }
                        borderRadius={ this.props.borderRadius }
                        onLoadStart={ () => this.setState({ isLoading: true }) }
                        onLoadEnd={ () =>
                            setTimeout(() => {
                                this.setState({ isLoading: false })
                            }, 100)
                        }
                    >
                        <ActivityIndicator
                            size="small"
                            color="#808B96"
                            animating={ this.state.isLoading }
                            style={ { position: 'absolute', alignSelf: 'center' } } />

                        { (imgName || imgName != null || imgName != undefined) ?
                            <Image
                                style={ [this.props.style] }
                                source={ this.props.source }
                                borderRadius={ this.props.borderRadius }
                                onLoadEnd={ () => this.setState({ isLoading: false }) }
                            />
                            :
                            <Image
                                style={ [this.props.style] }
                                source={ require('../../img/Unknown.jpg') }
                                borderRadius={ this.props.borderRadius }
                                onLoadEnd={ () => this.setState({ isLoading: false }) }
                            />
                        }
                    </ImageBackground>
                </View>
                <View style={ [
                    styles.userStatusDot,
                    {
                        height: size,
                        width: size,
                        borderRadius: size / 2,
                        backgroundColor: (this.state.isUserOnline) ? '#65B644' : 'transparent',
                        // backgroundColor: '#65B644',
                        top: (profile) ? '73%' : '60%',
                        right: (profile) ? '25%' : '20%',
                        // top: top,
                        // right: right
                    }
                ] } />
                <Modal
                    transparent={ true }
                    visible={ this.state.prifilePicDialogVisible }
                    onRequestClose={ () =>
                        this.setState({ prifilePicDialogVisible: false })
                    }
                >
                    <View style={ styles.modalMainView }>
                        <View style={ styles.backView } >
                            <TouchableOpacity
                                style={ styles.CloseViewSTyle }
                                onPress={ () =>
                                    this.setState({ prifilePicDialogVisible: false })
                                }
                            >
                                <Image
                                    style={ { height: 18, width: 18 } }
                                    source={ require("../../img/close_icon.png") }
                                />
                            </TouchableOpacity>
                            <View style={ { alignItems: "center", justifyContent: "center", marginVertical: '9%' } }>
                                <View style={ { height: '100%', width: '100%' } }>
                                    <ImageBackground
                                        style={ { height: '100%', widht: '100%' } }
                                        source={ require('../../img/Unknown.jpg') }
                                        onLoadEnd={ () => this.setState({ isLoading: false }) }
                                    >
                                        <ActivityIndicator
                                            size="small"
                                            color="#808B96"
                                            animating={ this.state.isLoading }
                                            style={ { position: 'absolute', alignSelf: 'center' } } />

                                        { (imgName || imgName != null || imgName != undefined) ?
                                            <Image
                                                style={ { height: '100%', widht: '100%' } }
                                                source={ this.props.source }
                                                onLoadEnd={ () => this.setState({ isLoading: false }) }
                                            />
                                            :
                                            <Image
                                                style={ { height: '100%', widht: '100%' } }
                                                source={ require('../../img/Unknown.jpg') }
                                                onLoadEnd={ () => this.setState({ isLoading: false }) }
                                            />
                                        }
                                    </ImageBackground>
                                </View>
                                {/* <ImageLoad
                                    style={ { height: "100%", width: "100%" } }
                                    source={ this.props.source }
                                    resizeMode="contain"
                                /> */}
                                {/* <TouchableOpacity
                                    style={ styles.CloseViewSTyle }
                                    onPress={ () =>
                                        this.setState({ prifilePicDialogVisible: false })
                                    } >
                                    <Image
                                        style={ { height: 18, width: 18 } }
                                        source={ require('../../img/close_icon.png') }
                                    />
                                </TouchableOpacity> */}
                            </View>
                        </View>
                    </View>
                </Modal>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    userStatusDot: {
        right: '20%',
        // // right: '20%',
        top: '60%',
        // // bottom: '5%'
    },
    CloseViewSTyle: {
        position: "absolute",
        right: 5,
        top: 5,
        backgroundColor: "rgba(255,0,0,0.5)",
        width: 32,
        height: 32,
        borderRadius: 5,
        alignItems: "center",
        justifyContent: "center"
    },
    modalMainView: {
        height: "100%",
        width: "100%",
        paddingTop: 130,
        paddingBottom: 100
    },
    backView: {
        backgroundColor: "rgba(0,0,0,0.9)",
        padding: 5,
        justifyContent: "center",
        alignContent: "center"
    }
})
