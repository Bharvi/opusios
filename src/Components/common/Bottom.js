import React from 'react';
import { View, Text, TouchableOpacity, Image, Dimensions } from 'react-native';

 const { width } = Dimensions.get('window');

const boxCount = 5;
const boxWidth = width / boxCount;

//reset route
// const navigateToScreen = (route) => () => {
//   const resetAction = StackActions.reset({
//           index: 0,
//           actions: [NavigationActions.navigate({ routeName: route })],
//       });
//   this.props.navigation.dispatch(resetAction);
// };

const Bottom = (props) => {
  const navigateToScreen = (route) => {
    const resetAction = props.StackActions.reset({
            index: 0,
            actions: [props.NavigationActions.navigate({ routeName: route })],
        });
    props.myNavigation.dispatch(resetAction);
  };

   const { container, verticalViewline, viewDivideStyle, notiBadgeStyle } = styles;
		return (
        <View style={container}>
            <View style={{ flex: 5, flexDirection: 'row' }}>
                <TouchableOpacity
                  style={viewDivideStyle}
                  onPress={() => navigateToScreen('DrawerNavigation')}
                >
                             <Image
                                   style={{ height: 30, width: 30, tintColor: '#fff' }}
                                   source={props.HomeImage}
                             />
                </TouchableOpacity>

            <View style={verticalViewline} />

                <TouchableOpacity
                 style={viewDivideStyle}
                 onPress={() => navigateToScreen('CartNavigation')}
                >
                      <Image
                           style={{ height: 30, width: 30, tintColor: '#fff' }}
                           source={props.BagImage}
                      />
                </TouchableOpacity>

                 <View style={verticalViewline} />

                 <TouchableOpacity
                  style={viewDivideStyle}
                  onPress={() => props.myNavigation.navigate('MyProfile')}
                 >
                         <Image
                             style={{ height: 30, width: 30, tintColor: '#fff' }}
                             source={props.UserImage}
                         />
                </TouchableOpacity>

                <View style={styles.verticalViewline} />

                <TouchableOpacity
                 style={styles.viewDivideStyle}
                 onPress={() => navigateToScreen('NotificationNavigation')}
                >
                       <Image
                       style={{ height: 30, width: 30, tintColor: '#fff' }}
                       source={props.NotificationImage}
                       />
                </TouchableOpacity>
                {
                //  <View>
      		      //      <View style={notiBadgeStyle}>
      		      //         <Text style={{ color: 'white', fontSize: 12 }}> 1 </Text>
      		      //     </View>
                // </View>
                }
                <View style={verticalViewline} />

                    <TouchableOpacity
                     style={viewDivideStyle}
                    >
                           <Image
                           style={{ height: 30, width: 30, tintColor: '#fff' }}
                           source={props.SettingImage}
                           />
                    </TouchableOpacity>

            </View>
     </View>
			);
	};

const styles = {
  container: {
    backgroundColor: '#58585a',
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 50,
    flex: 5
  },
 viewDivideStyle: {
     flexDirection: 'row',
     flex: 1,
     alignItems: 'center',
     justifyContent: 'center',
     width: boxWidth
 },

verticalViewline: {
	height: 28,
    borderWidth: 0.3,
    justifyContent: 'center',
    borderColor: '#fff',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10
   },

   notiBadgeStyle: {
     position: 'absolute',
     right: 26,
     top: 13,
     backgroundColor: 'red',
     borderRadius: 2, width: 12,
     height: 12,
     justifyContent: 'center',
     alignItems: 'center',
   }
};
export { Bottom };
