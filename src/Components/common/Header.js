import React from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';

const Header = (props) => {
   const { textStyle, viewStyle, leftImageStyle, rightImageStyle, searchImageStyle } = styles;
   return (
      <View style={viewStyle}>
        <TouchableOpacity onPress={props.onLeftPressed}>
          <Image style={leftImageStyle} source={props.leftImage} />
        </TouchableOpacity>
        <View style={{ flex: 0.9, alignItems: 'center', justifyContent: 'center' }}>
             <Text numberOfLines={1} style={textStyle}>{props.headerText.toUpperCase()}</Text>
        </View>
              <View style={styles.rightViewStyle}>
                  <TouchableOpacity
                    style={{ position: 'absolute', right: 25 }}
                    // onPress={props.onSearchBtnPress}
                  >
                        <Image style={searchImageStyle} source={props.searchImage} />
                     </TouchableOpacity>
                 <TouchableOpacity
                  onPress={() => props.myNavigation.navigate('CartNavigation')}
                 >
                      <Image style={rightImageStyle} source={props.storeImage} />
                 </TouchableOpacity>
             </View>

     </View>
    );
};

const styles = {
  viewStyle: {
    backgroundColor: '#58585a',
    flexDirection: 'row',
    height: 50,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    alignItems: 'center'
  },
  textStyle: {
    fontSize: 16,
    color: '#fff',
    marginLeft: 10,
    marginRight: 10
  },
  leftImageStyle: {
    height: 22,
    width: 22,
    alignSelf: 'center',
  },
  rightImageStyle: {
    height: 30,
    width: 30,
    alignSelf: 'center'
  },
  searchImageStyle: {
    height: 30,
    width: 30,
    alignSelf: 'center'
  },
  rightViewStyle: {
    justifyContent: 'center',
    position: 'absolute',
    right: 10,
    alignSelf: 'center' }

};

export { Header };
