import {
  MYPOSTS_DATA, BASE_URL, MYPOSTS_ERROR_CHANGED,MYPOSTS_LOADING,
  LOADING_POSTS_SHAREPOST,
  SHAREPOST_POSTS_DATA,
  SHAREPOST_POSTS_SUCCESS,
  SHAREPOST_POSTS_FAILED,

LOADING_POSTS_SHAREPOST1,
  SHAREPOST_POSTS_DATA1,
  SHAREPOST_POSTS_SUCCESS1,
  SHAREPOST_POSTS_FAILED1,

  LOADING_POSTS_ABUSE,
  ABUSE_POSTS_DATA,
  ABUSE_POSTS_SUCCESS,
  ABUSE_POSTS_FAILED
} from '../Actions/type';

const INTIAL_STATE = {
  posts: [],
  authResult: '',
  authResultSharePost: '',
  authResultSharePost1: '',
  authResultAbusePost: '',
  isLoading: false,
  isLoadingInit: false,
  isdata: false,
};

export default (state = INTIAL_STATE, action) => {
  const responce = action.payload;
    switch (action.type) {
      case MYPOSTS_LOADING:
        return { ...state,
           isLoading: action.payload.isLoading,
           isLoadingInit: action.payload.isLoadingInit,
           authResult: '',
           authResultSharePost: '',
           authResultSharePost1: '',
           authResultAbusePost: '',
         };
      case MYPOSTS_DATA:
      console.warn("Reducers Post isData",action.payload.list);
          return { ...state,
            authResult: 'posts success',
            isLoading: false,
            isLoadingInit: false,
            posts: action.payload.list,
            authResultSharePost: '',
            authResultSharePost1: '',
            authResultAbusePost: '',
            isdata: action.payload.isData,
          };

      case MYPOSTS_ERROR_CHANGED:
          console.log('Error');
          return { ...state,
          authResult: 'fail',
          authResultSharePost: '',
          authResultSharePost1: '',
          authResultAbusePost: '',
          isLoading: false };

                    case LOADING_POSTS_SHAREPOST:
                      console.log("loading...");
                      return {
                        ...state,
                        isLoading: true,
                        authResult: "",
                        authResultSharePost: '',
                        authResultAbusePost: '',
                      };

                    case SHAREPOST_POSTS_SUCCESS:
                      return {
                        ...state,
                        authResult: '',
                        isLoading: false,
                        authResultSharePost: action.payload,
                        authResultAbusePost: '',

                      };
                      // case SHAREPOST_DATA:
                      //     console.log('response sharepost', action.payload.DATA);
                      //     return { ...state, authResult: action.payload,
                      //       isLoading: false,
                      //       DATA: action.payload.DATA,
                      //     };

                    case SHAREPOST_POSTS_FAILED:
                      return {
                        ...state,
                        authResult: '',
                        isLoading: false,
                        authResultSharePost: action.payload,
                        authResultAbusePost: '',


                      };

                    case LOADING_POSTS_SHAREPOST1:
                      console.log("loading...");
                      return {
                        ...state,
                        isLoading: true,
                        authResult: "",
                        authResultSharePost1: '',
                        authResultSharePost: '',
                        authResultAbusePost: '',
                      };

                    case SHAREPOST_POSTS_SUCCESS1:
                      return {
                        ...state,
                        authResult: '',
                        isLoading: false,
                        authResultSharePost: '',
                        authResultSharePost1: action.payload,
                        authResultAbusePost: '',

                      };
                      // case SHAREPOST_DATA:
                      //     console.log('response sharepost', action.payload.DATA);
                      //     return { ...state, authResult: action.payload,
                      //       isLoading: false,
                      //       DATA: action.payload.DATA,
                      //     };

                    case SHAREPOST_POSTS_FAILED1:
                      return {
                        ...state,
                        authResult: '',
                        isLoading: false,
                        authResultSharePost: '',
                        authResultSharePost1: action.payload,
                        authResultAbusePost: '',


                      };


                      case LOADING_POSTS_ABUSE:
                        console.log("loading...");
                        return {
                          ...state,
                          isLoading: true,
                          authResult: "",
                          authResultSharePost: '',
                          authResultSharePost1: '',
                          authResultAbusePost: '',
                        };

                      case ABUSE_POSTS_SUCCESS:
                        return {
                          ...state,
                          authResult: '',
                          isLoading: false,
                          authResultSharePost: '',
                          authResultSharePost1: '',
                          authResultAbusePost: action.payload,

                        };
                        // case SHAREPOST_DATA:
                        //     console.log('response sharepost', action.payload.DATA);
                        //     return { ...state, authResult: action.payload,
                        //       isLoading: false,
                        //       DATA: action.payload.DATA,
                        //     };

                      case ABUSE_POSTS_FAILED:
                        return {
                          ...state,
                          authResult: '',
                          isLoading: false,
                          authResultSharePost: '',
                          authResultSharePost1: '',
                          authResultAbusePost: action.payload,

                        };





      default:
        return state;
    }
};
