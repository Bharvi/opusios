import {
  FEEDLIST_DATA, FEEDLIST_ERROR_CHANGED,FEEDLIST_LOADING,
  FEEDLIST_INITIAL_STATE,
  LOADING_FEED_SHAREPOST,
  SHAREPOST_FEED_DATA,
  SHAREPOST_FEED_SUCCESS,
  SHAREPOST_FEED_FAILED,
  LOADING_FEED_ABUSE,
  ABUSE_FEED_DATA,
  ABUSE_FEED_SUCCESS,
  ABUSE_FEED_FAILED,
  ON_PRESS_LIKE
} from '../Actions/type';

const INTIAL_STATE = {
  Feed_list: [],
  latest_comment: [],
  notificationCount: '',
  authResult: '',
  authResultSharePost: '',
  authResultAbusePost: '',
  isdata: false,
  isLoading: false,
  isRefreshing: false

};

export default (state = INTIAL_STATE, action) => {
  const responce = action.payload;
    switch (action.type) {
      case FEEDLIST_LOADING:
        return { ...state, 
          isLoading: action.payload.isLoading,
          isRefreshing: action.payload.isRefreshing,
          authResult: '',
          authResultSharePost: '',
          authResultAbusePost: '',
         };
      case FEEDLIST_DATA:
          console.log('response FeddList', action.payload);
          console.log('response FeddList', action.payload.notificationCount);
          return { ...state,
            authResult: 'feed list success',
            authResultSharePost: '',
            authResultAbusePost: '',
            isLoading: false,
            isRefreshing: false,
            Feed_list: action.payload.list,
            notificationCount: action.payload.notificationCount,
            latest_comment: action.payload.latest_comment,
            isdata: action.payload.isData
          };
      case FEEDLIST_ERROR_CHANGED:
          console.log('Error');
          return { ...state, authResult: 'fail',
          authResultSharePost: '',
          authResultAbusePost: '',
           isLoading: false,
           isRefreshing: false };


          case LOADING_FEED_SHAREPOST:
            console.log("loading...");
            return {
              ...state,
              isLoading: true,
              isRefreshing: false,
              authResult: "",
              authResultSharePost: '',
              authResultAbusePost: '',
            };

          case SHAREPOST_FEED_SUCCESS:
            return {
              ...state,
              authResult: '',
              authResultSharePost: action.payload,
              authResultAbusePost: '',
              isLoading: false,
              isRefreshing: false,

            };
            // case SHAREPOST_DATA:
            //     console.log('response sharepost', action.payload.DATA);
            //     return { ...state, authResult: action.payload,
            //       isLoading: false,
            //       DATA: action.payload.DATA,
            //     };

          case SHAREPOST_FEED_FAILED:
            return {
              ...state,
              authResult: '',
              authResultSharePost: action.payload,
              authResultAbusePost: '',
              isLoading: false,
              isRefreshing: false,
            };


            case LOADING_FEED_ABUSE:
              console.log("loading...");
              return {
                ...state,
                isLoading: true,
                isRefreshing: false,
                authResult: "",
                authResultSharePost: '',
                authResultAbusePost: '',
              };

            case ABUSE_FEED_SUCCESS:
              return {
                ...state,
                authResult:'',
                authResultSharePost: '',
                authResultAbusePost:  action.payload,
                isLoading: false,
                isRefreshing: false,

              };
              // case SHAREPOST_DATA:
              //     console.log('response sharepost', action.payload.DATA);
              //     return { ...state, authResult: action.payload,
              //       isLoading: false,
              //       DATA: action.payload.DATA,
              //     };
              case ABUSE_FEED_FAILED:
                return {
                  ...state,
                  authResult: '',
                  authResultSharePost: '',
                  authResultAbusePost:  action.payload,
                  isLoading: false,
                  isRefreshing: false,
  
                };
              case ON_PRESS_LIKE:
                return {
                  ...state,
                  authResult: 'feed list on Like',
                  authResultSharePost: '',
                  Feed_list:  action.payload,
                  isLoading: false,
                  isRefreshing: false,
  
                };

      case FEEDLIST_INITIAL_STATE:
        return INTIAL_STATE;
      default:
        return state;
    }
};
