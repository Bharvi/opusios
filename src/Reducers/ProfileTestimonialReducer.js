import {
  ProfileTestimonial_DATA, BASE_URL, ProfileTestimonial_ERROR_CHANGED,ProfileTestimonial_LOADING, 
  TESTIMONIAL_DATA_PROFILE
} from '../Actions/type';

const INTIAL_STATE = {
  ProfileTestimonial_list: [],
  testimonialsProfile:[],
  authResult: '',
  isLoading: false,
  isdata: false,
};

export default (state = INTIAL_STATE, action) => {
  const responce = action.payload;
    switch (action.type) {
      case ProfileTestimonial_LOADING:
        return { ...state, isLoading: true };
      case ProfileTestimonial_DATA:
          return { ...state, authResult: 'success',
            isLoading: false,
            ProfileTestimonial_list: action.payload.list,
            isdata: action.payload.isData,
          };
      case TESTIMONIAL_DATA_PROFILE:
          return { ...state, authResult: 'success',
            isLoading: false,
            testimonialsProfile: action.payload.list,
            isdata: action.payload.isData,
          };
      case ProfileTestimonial_ERROR_CHANGED:
          console.log('Error');
          return { ...state, authResult: 'fail', isLoading: false };

      default:
        return state;
    }
};
