import {
  LOADING_LOGOUT,
  LOGOUT_DATA,
  LOGOUT_SUCCESS,
  LOGOUT_FAILED,

  LOADING_NOTIFICATIONSETING,
  NOTIFICATIONSETING_DATA,
  NOTIFICATIONSETING_SUCCESS,
  NOTIFICATIONSETING_SUCCESS1,
  NOTIFICATIONSETING_FAILED
} from "../Actions/type";

const INTIAL_STATE = {
  authResult: "",
  authResultNotification: "",
  isLoading: false
};

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {

    case LOADING_LOGOUT:
      console.log("loading...");
      return {
        ...state,
        isLoading: true,
        authResult: "",
        authResultNotification: "",
      };

    case LOGOUT_SUCCESS:
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,
        authResultNotification: "",

      };

    case LOGOUT_FAILED:
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,
        authResultNotification: "",

      };

    case LOADING_NOTIFICATIONSETING:
      console.log("loading...");
      return {
        ...state,
        isLoading: true,
        authResult: "",
        authResultNotification: "",
      };

    case NOTIFICATIONSETING_SUCCESS:
      return {
        ...state,
        authResultNotification: action.payload,
        isLoading: false,
        authResult: "",

      };
    case NOTIFICATIONSETING_SUCCESS1:
      return {
        ...state,
        authResultNotification: action.payload,
        isLoading: false,
        authResult: "",

      };

    case NOTIFICATIONSETING_FAILED:
      return {
        ...state,
        authResultNotification: action.payload,
        isLoading: false,
        authResult: "",

      };

    default:
      return state;
  }
};
