import {
  FRIEND_DATA, BASE_URL, FRIEND_ERROR_CHANGED,FRIEND_LOADING,FRIEND_INITIAL_STATE,
  UNFRIEND_DATA,  UNFRIEND_ERROR_CHANGED,UNFRIEND_LOADING, UNFRIEND_SUCCESS,PROFILE_FRIEND_DATA
} from '../Actions/type';

const INTIAL_STATE = {
  friend: [],
  authResult: '',
  authResultFriend: '',
  isLoading: false,
  isdata: false,
  profileFriend:[]
};

export default (state = INTIAL_STATE, action) => {
  const responce = action.payload;
    switch (action.type) {
      case FRIEND_LOADING:
        return { ...state, isLoading: true,
          authResultFriend: '',
          authResult:'',
         };
      case FRIEND_DATA:
          console.log("Reducers friend", action.payload);
          return { ...state, authResult: 'success',
            isLoading: false,
            friend: action.payload.list,
            authResultFriend: '',
            isdata: action.payload.isData,

          };
      case PROFILE_FRIEND_DATA:
          console.log("Reducers friend", action.payload);
          return { ...state, authResult: 'success',
            isLoading: false,
            isRefreshing:false,
            profileFriend: action.payload.list,
            authResultFriend: '',
            isdata: action.payload.isData,
          };
      case FRIEND_ERROR_CHANGED:
          console.log('Error');
          return { ...state, authResult: 'fail',
           isLoading: false,
           authResultFriend: '' };


          case UNFRIEND_LOADING:
            console.log("loading...");
            return {
              ...state,
              isLoading: true,
              authResult: "",
               authResultFriend: ''
            };

          case UNFRIEND_SUCCESS:
            return {
              ...state,
              authResult: '',
              isLoading: false,
               authResultFriend: action.payload,

            };

          case UNFRIEND_ERROR_CHANGED:
            return {
              ...state,
              authResult: '',
              isLoading: false,
               authResultFriend: action.payload,

            };
      case FRIEND_INITIAL_STATE:
        return INTIAL_STATE;
      default:
        return state;
    }
};
