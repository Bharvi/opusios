import { combineReducers } from "redux";
import LoginReducer from "./LoginReducer";
import SignUpReducer from "./SignUpReducer";
import ForgotPassReducer from "./ForgotPassReducer";
import EditProfileReducer from "./EditProfileReducer";
import CommentReducer from "./CommentReducer";
import FeedListReducer from "./FeedListReducer";
import MyProfileReducer from "./MyProfileReducer";
import ProfileTagReducer from "./ProfileTagReducer";
import CreatePostReducer from "./CreatePostReducer";
import ProfileTestimonialReducer from "./ProfileTestimonialReducer";
import NotificationReducer from "./NotificationReducer";
import TestimonialReducer from "./TestimonialReducer";
import SearchListReducer from "./SearchListReducer";
import SearchResultReducer from "./SearchResultReducer";
import PostsReducers from "./PostsReducers";
import FriendReducer from "./FriendReducer";
import SetingReducer from "./SetingReducer";
import AboutsusReducer from "./AboutsusReducer";
import TermsReducer from "./TermsReducer";
import PrivacyPolicyReducer from "./PrivacyPolicyReducer";
import BottomReducer from "./BottomReducer";
import SocailloginReducer from "./SocailloginReducer";
import SplashReducer from "./SplashReducer";
import CompeleProfileReducer from "./CompeleProfileReducer";
import OtherProfileReducer from "./OtherProfileReducer";
import Chatreduer from "./Chatreduer";
import UserStatusReducer from './UserStatusReducer'

export default combineReducers({
  login: LoginReducer,
  signup: SignUpReducer,
  forgotpass: ForgotPassReducer,
  editprofile: EditProfileReducer,
  comment: CommentReducer,
  FeedList: FeedListReducer,
  About: MyProfileReducer,
  ProfileTag: ProfileTagReducer,
  CreatePost: CreatePostReducer,
  ProfileTestimonial: ProfileTestimonialReducer,
  Notificationlist: NotificationReducer,
  Testimonial: TestimonialReducer,
  SearchList: SearchListReducer,
  SearchResult: SearchResultReducer,
  Posts: PostsReducers,
  Friend: FriendReducer,
  Setting: SetingReducer,
  Aboutsus: AboutsusReducer,
  Terms: TermsReducer,
  PrivacyPolicy: PrivacyPolicyReducer,
  bottom: BottomReducer,
  Socaillogin: SocailloginReducer,
  Splash: SplashReducer,
  CompeleProfile: CompeleProfileReducer,
  Chat: Chatreduer,
  OtherProfile: OtherProfileReducer,
  UserStatusReducer: UserStatusReducer
});
