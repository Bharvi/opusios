import {
  PROFECATION_CHANGED,
  LOCATION_CHANGED_COMPELETEPROFILE,
  PROFECATION_ERROR_CHANGED,
  LOCATION_ERROR_CHANGED_COMPELETEPROFILE,
  COMPELETEPROFILE_LOGIN,
  COMPELETEPROFILE_INITIAL_STATE,
  COMPELETEPROFILE_SUCCESS,
  COMPELETEPROFILE_FAILED,
  ALLTAGS_DATA,
  ON_TAG_PRESS_TYPE,
  ON_TAG_PRESS,
  ALLTAGS_LOADING,
  ALLTAGS_ERROR,
  ALLTAGS_INITIAL_STATE

} from "../Actions/type";

const INTIAL_STATE = {
  profession: "",
  professionError: "",
  location: "",
  locationError: "",
  isLoading: false,
  authResult: "",
  tags: [],
  professions: [],
  authResulttags: "",

};

export default (state = INTIAL_STATE, action) => {
  const response = action.payload;
  switch (action.type) {
    case PROFECATION_CHANGED:
      return {
        ...state,
        profession: action.payload,
        professionError: "",
        locationError: "",
        authResult: ""
      };
    case PROFECATION_ERROR_CHANGED:
      return { ...state, professionError: action.payload, authResult: "" };
    case LOCATION_CHANGED_COMPELETEPROFILE:
      return {
        ...state,
        location: action.payload,
        professionError: "",
        locationError: "",
        authResult: ""
      };
    case LOCATION_ERROR_CHANGED_COMPELETEPROFILE:
      return { ...state, locationError: action.payload, authResult: "" };

    case COMPELETEPROFILE_LOGIN:
      console.log("loading...");
      return {
        ...state,
        professionError: "",
        isLoading: true,
        locationError: "",
        authResult: ""
      };

    case COMPELETEPROFILE_SUCCESS:
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,
        profession: "",
        location: ""
      };

    case COMPELETEPROFILE_FAILED:
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,
        profession: "",
        location: ""
      };


      case ALLTAGS_LOADING:
           console.log("Loading...");
           return { ...state, isLoading: true };
         case ALLTAGS_DATA:
           console.log("data", action.payload.tags);
           console.log("data List", action.payload.professions);
           return {
             ...state,
             authResulttags: "success",
             isLoading: false,
             tags: action.payload.tags,
             professions: action.payload.professions,
           };
         case ALLTAGS_ERROR:
           return { ...state, authResulttags: "fail", isLoading: false };

         case ON_TAG_PRESS:
             return { ...state,
               tags: response
             };
         case ALLTAGS_INITIAL_STATE:
           return INTIAL_STATE;



    case COMPELETEPROFILE_INITIAL_STATE:
      return INTIAL_STATE;
    default:
      return state;
  }
};
