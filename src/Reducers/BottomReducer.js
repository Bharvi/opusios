import {
  ON_CHANGE_BOTTOM_TAB,
} from '../Actions/type';

const INTIAL_STATE = {
  issearchseleted: true,
  isuserseleted: false,
  isfeedseleted: false,
  ischatseleted: false,
};

export default (state = INTIAL_STATE, action) => {
    switch (action.type) {
      case ON_CHANGE_BOTTOM_TAB:
      return {
        ...state,
        [action.payload.prop]: action.payload.value,
      };
      default:
        return state;
    }
};
