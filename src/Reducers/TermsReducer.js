import {
  TERMS_DATA,
  BASE_URL,
  TERMS_ERROR_CHANGED,
  TERMS_LOADING
} from "../Actions/type";

const INTIAL_STATE = {
  Data: "",
  authResult: "",
  isLoading: false
};

export default (state = INTIAL_STATE, action) => {
  const responce = action.payload;
  switch (action.type) {
    case TERMS_LOADING:
      return { ...state, isLoading: true };
    case TERMS_DATA:
      console.log(action.payload);
      return {
        ...state,
        authResult: "success",
        isLoading: false,
        Data: action.payload
      };
    case TERMS_ERROR_CHANGED:
      console.log("Error");
      return { ...state, authResult: "fail", isLoading: false };

    default:
      return state;
  }
};
