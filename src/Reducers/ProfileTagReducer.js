import {
  ProfileTag_DATA, BASE_URL, ProfileTag_ERROR_CHANGED,ProfileTag_LOADING,
  ProfileaddTag_ERROR_CHANGED, PROFILEADDTAG_LOADING, ProfileADDTag_DATA,
  PROFILEADDTAG_SUCCESS, USERADDTAG_CHANGED, USERADDTAG_ERROR_CHANGED, PROFILEADDTAG_FAILED,
  PROFILEDELETE_LOADING,PROFILEDELETE_SUCCESS, PROFILEDELETE_FAILED,
  MYTAGPROFILE_INITAL_STATE

} from '../Actions/type';

const INTIAL_STATE = {
  ProfileTag_list: [],
  isLoading: false,
  user_tag: "",
  user_tagError: "",
  authResultuserTag: '',
  authResult: '',
  authResultuserTagDelete: ''
};

export default (state = INTIAL_STATE, action) => {
  const responce = action.payload;
    switch (action.type) {
      case ProfileTag_LOADING:
        return { ...state,
          isLoading: true,
          authResultuserTag: '',
          authResultuserTagDelete: '',
          authResult: '',

         };
      case ProfileTag_DATA:
      console.log('tag data list',action.payload);
          return { ...state, authResult: 'success',
            isLoading: false,
            ProfileTag_list: action.payload,
            user_tagError: '',
            user_tag:'',
            authResultuserTag: '',
            authResultuserTagDelete: ''
          };
      case ProfileTag_ERROR_CHANGED:
          console.log('Error');
          return { ...state, authResult: 'fail', isLoading: false ,
          authResultuserTag: '',
          authResultuserTagDelete: '',
          ProfileTag_list: [],
          };

          // case ADD_TAG:
          //   return { ...state,
          //     tagArray: action.payload.tagArray,
          //     user_tagError: action.payload.user_tagError,
          //     user_tag: action.payload.user_tag,
          //     authResult: '',
          //
          //   };
          // case REMOVE_TAG:
          //   return { ...state,
          //     tagArray: action.payload,
          //     user_tag: '',
          //     authResult: ''
          //   };

          case USERADDTAG_CHANGED:
            return {
              ...state,
              user_tag: action.payload,
              user_tagError: "",
              authResultuserTag: '',
              authResult: '',
              authResultuserTagDelete: ''
            };
          case USERADDTAG_ERROR_CHANGED:
            return { ...state, user_tagError: action.payload,
              authResultuserTag: '',
              authResult: '',
              authResultuserTagDelete: ''
             };

          case PROFILEADDTAG_LOADING:
            console.log("loading...");
            return {
              ...state,
              user_tagError: "",
              isLoading: true,
                authResultuserTag: '',
                authResult: '',
                authResultuserTagDelete: ''

            };

          case PROFILEADDTAG_SUCCESS:
            return {
              ...state,
              authResultuserTag: action.payload,
              isLoading: false,
              user_tag: "",
              authResult: '',
              authResultuserTagDelete: ''


            };

          case PROFILEADDTAG_FAILED:
            return {
              ...state,
              isLoading: false,
              user_tag: "",
              authResultuserTag: action.payload,
              authResult: '',
              authResultuserTagDelete: ''

            };

          case PROFILEDELETE_LOADING:
            console.log("loading...");
            return {
              ...state,
              isLoading: true,
              authResultuserTag: '',
              authResult: '',
              authResultuserTagDelete: ''

            };

          case PROFILEDELETE_SUCCESS:
            return {
              ...state,
              authResultuserTagDelete: action.payload,
              isLoading: false,
               authResultuserTag: '',
                authResult: '',


            };

          case PROFILEDELETE_FAILED:
            return {
              ...state,
              isLoading: false,
              authResultuserTagDelete: action.payload,
              authResultuserTag: '',
              authResult: '',
            };

            case MYTAGPROFILE_INITAL_STATE:
            return INTIAL_STATE;
      default:
        return state;
    }
};
