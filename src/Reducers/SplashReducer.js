import {
  GET_TOKEN,
  TOKEN_LOADDING,
  GET_SEARCH_DATA,
  CHECK_VERSION,
  LOADING,
} from '../Actions/type';

const INTIAL_STATE = {
  authToken: '',
  isUpdated: '',
  isLoading: false,
  searchArray: []
};

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {
    case TOKEN_LOADDING:
      console.log("loading...");
      return {
        ...state,
        isLoading: true,
        authToken: ""
      };
    case GET_TOKEN:
    console.log("splash reducer", action.payload);
      return { ...state,
        authToken: action.payload,
        isLoading: false
      };
      case CHECK_VERSION:
    console.log('data store:',action.payload);
      return{...state,
             isUpdated:action.payload,
             isLoading: false,
        };
      case GET_SEARCH_DATA:
        console.log("edrs", action.payload);
        return { ...state, searchArray: action.payload };
    default:
      return state;
  }
};
