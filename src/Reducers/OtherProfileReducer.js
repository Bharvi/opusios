import {
  PROFILE_DATA, PROFILE_ERROR_CHANGED, Pofile_About_ERROR_CHANGED, Pofile_About_DATA,
  OtherProfileTag_DATA, OtherProfileTag_ERROR, OtherProfile_Loading, OTHER_PROFILE_INITAL_STATE
} from '../Actions/type';

const INTIAL_STATE = {

  authResult: '',
  Data: [],
  About_list: [],
  ProfileTag_list: [],
  isLoading: false,
};

export default (state = INTIAL_STATE, action) => {
  const responce = action.payload;
  switch (action.type) {
    case OtherProfile_Loading:
      return {
        ...state,
        authResult: "",
        isLoading: true,
      };

    case PROFILE_DATA:

      return {
        ...state,
        authResult: "my profile data success",
        isLoading: false,
        Data: action.payload,
      };
    case PROFILE_ERROR_CHANGED:
      console.log("Error");
      return {
        ...state, authResult: "fail",
        isLoading: false
      };
    case Pofile_About_DATA:
      return {
        ...state,
        authResult: "aboutdata success",
        isLoadingAbout: false,
        About_list: action.payload,
        isLoading: false,
      };
    case Pofile_About_ERROR_CHANGED:
      console.log("Error");
      return {
        ...state, authResult: "fail",
        isLoading: false
      };

    case OtherProfileTag_DATA:
      console.log('tag data list', action.payload);
      return {
        ...state,
        authResult: 'success',
        isLoading: false,
        ProfileTag_list: action.payload
      };
    case OtherProfileTag_ERROR:
      console.log('Error');
      return {
        ...state,
        authResult: 'fail',
        isLoading: false,
        ProfileTag_list: [],
      };
    case OTHER_PROFILE_INITAL_STATE:
      console.log('Error');
      return { ...INTIAL_STATE };
    default:
      return state;
  }
};
