import {
  EMAIL_FORGOT_PASS_CHANGED, EMAIL_FORGOT_PASS_ERROR_CHANGED,
  FORGOT_PASS_LOADING, FORGOT_PASS_INITIAL_STATE, FORGOT_PASS_SUCCESS, FORGOT_PASS_FAILD

} from '../Actions/type';


const INTIAL_STATE = {
  email: '',
  emailError: '',
  isLoading: false,
  authResult: '',

};

export default (state = INTIAL_STATE, action) => {
    switch (action.type) {
      case EMAIL_FORGOT_PASS_CHANGED:
  return { ...state,
      email: action.payload,
      emailError: '',
    authResult: '', };
      case EMAIL_FORGOT_PASS_ERROR_CHANGED:
        return { ...state,
          emailError: action.payload,
          authResult: '',
   };
  case FORGOT_PASS_INITIAL_STATE:
      return INTIAL_STATE;

      case FORGOT_PASS_LOADING:
        console.log('loading...');
        return { ...state, emailError: '',
         isLoading: true,
         authResult: '', };

    case FORGOT_PASS_SUCCESS:
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,
        email: "",
      };

    case FORGOT_PASS_FAILD:
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,
        email: "",
      };

   default:
  return state;
    }
};
