import {
    USER_STATUS_SUCCESS,
    USER_STATUS_FAILED,
    USER_STATUS_ERROR
} from "../Actions/type";

const INTIAL_STATE = {
    authResult: '',
};


export default (state = INTIAL_STATE, action) => {
    switch (action.type) {
        case USER_STATUS_SUCCESS:
            // console.log("loading...");
            return {
                ...state
            };
        case USER_STATUS_FAILED:
            return {
                ...state
            };
        case USER_STATUS_ERROR:
            return {
                ...state
            };
        default:
            return state;
    }
};