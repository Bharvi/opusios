import {
  FRIEND_DATA1, BASE_URL, FRIEND_ERROR_CHANGED1, FRIEND_LOADING1,
  UNFRIEND_DATA1, UNFRIEND_ERROR_CHANGED1, UNFRIEND_LOADING, UNFRIEND_SUCCESS1,
  INITIAL_CHAT_LIST, CHAT_LIST_LOADING,
  SAVE_USER_DATA, CHAT_LIST_ERROR, CHAT_LIST_SUCCESS, CHAT_DETAIL_SUCCESS, CHAT_DETAIL_ERROR,
} from '../Actions/type';

const INTIAL_STATE = {
  friend: [],
  authResult: '',
  isLoading: false,
  isdata: false,
  user_id: '',
  fullname: '',
  to_fire_id: '',
  chatList: [],
  chatDetail: {},
  isLoadMore: false,

};

export default (state = INTIAL_STATE, action) => {
  const responce = action.payload;
  switch (action.type) {
    case FRIEND_LOADING1:
      return {
        ...state, isLoading: true,
        authResult: '',
      };
    case FRIEND_DATA1:
      console.log("Reducers friend", action.payload);
      return {
        ...state, authResult: 'success',
        isLoading: false,
        friend: action.payload.list,
        isdata: action.payload.isData,
      };
    case FRIEND_ERROR_CHANGED1:
      console.log('Error');
      return {
        ...state, authResult: 'fail',
        isLoading: false,
      };
    case SAVE_USER_DATA:
      console.log('SAVE_USER_DATA', action.payload.user_id, action.payload.fullname, action.payload.firebase);
      return {
        ...state,
        user_id: action.payload.user_id,
        fullname: action.payload.fullname,
        to_fire_id: action.payload.firebase,
      };
    case CHAT_LIST_SUCCESS:
      return {
        ...state, authResult: 'success chat list',
        isLoading: false,
        chatList: action.payload.list,
        isLoadMore: false
      };
    case CHAT_LIST_ERROR:
      return {
        ...state, authResult: 'fail chat list',
        isLoading: false,
        isLoadMore: false
      };
    case CHAT_DETAIL_SUCCESS:
      console.warn('CHAT_DETAIL_SUCCESS', action.payload);
      return {
        ...state, authResult: 'success chat Detail',
        isLoading: false,
        chatDetail: action.payload
      };
    case CHAT_DETAIL_ERROR:
      return {
        ...state, authResult: 'fail chat Detail',
        isLoading: false,
      };
    case CHAT_LIST_LOADING:
      return {
        ...state,
        isLoadMore: true
      }
    default:
      return state;
    case INITIAL_CHAT_LIST:
      return INTIAL_STATE
  }

};
