import {
  PRIVACYPOLICY_DATA,
  BASE_URL,
  PRIVACYPOLICY_ERROR_CHANGED,
  PRIVACYPOLICY_LOADING
} from "../Actions/type";

const INTIAL_STATE = {
  Data: "",
  authResult: "",
  isLoading: false
};

export default (state = INTIAL_STATE, action) => {
  const responce = action.payload;
  switch (action.type) {
    case PRIVACYPOLICY_LOADING:
      return { ...state, isLoading: true };
    case PRIVACYPOLICY_DATA:
      console.log(action.payload);
      return {
        ...state,
        authResult: "success",
        isLoading: false,
        Data: action.payload
      };
    case PRIVACYPOLICY_ERROR_CHANGED:
      console.log("Error");
      return { ...state, authResult: "fail", isLoading: false };

    default:
      return state;
  }
};
