import {
  COMMENT_CHANGED,
  COMMENT_ERROR_CHANGED,
  COMMENT_LOADING,
  COMMENT_INITIAL_STATE,
  COMMENT_SUCCESS,
  COMMENT_FAILED,
  LOADING_COMMENT_SHAREPOST,
  SHAREPOST_COMMENT_DATA,
  SHAREPOST_COMMENT_SUCCESS,
  SHAREPOST_COMMENT_FAILED,
  LOADING_COMMENT_ABUSE,
  ABUSE_COMMENT_DATA,
  ABUSE_COMMENT_SUCCESS,
  ABUSE_COMMENT_FAILED,
  COMMENTLIST_DATA, COMMENTLIST_ERROR_CHANGED, COMMENTLIST_LOADING,
  COMMENTLIST_INITIAL_STATE,
} from "../Actions/type";

const INTIAL_STATE = {
  comment: "",
  commentError: "",
  comments_list: [],
  postdata: '',
  isLoading: false,
  authResult: "",
  authResultSharePost: '',
  authResultAbusePost: '',
  isdata: false,

};

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {
    case COMMENT_CHANGED:
      return {
        ...state,
        comment: action.payload,
        commentError: "",
        authResult: "",
        authResultSharePost: '',
        authResultAbusePost: '',
      };
    case COMMENT_ERROR_CHANGED:
      return { ...state,
      commentError: action.payload,
      authResult: "",
      authResultSharePost: '',
      authResultAbusePost: '', };

    case COMMENT_INITIAL_STATE:
      return INTIAL_STATE;

    case COMMENT_LOADING:
      console.log("loading...");
      return { ...state,
         commentError: "",
          isLoading: true,
           authResult: "",
           authResultSharePost: '',
           authResultAbusePost: '',
           };

    case COMMENT_SUCCESS:
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,
        comment: "",
        authResultSharePost: '',
        authResultAbusePost: '',
      };

    case COMMENT_FAILED:
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,
        comment: "",
        authResultSharePost: '',
        authResultAbusePost: '',
      };



      case LOADING_COMMENT_SHAREPOST:
        console.log("loading...");
        return {
          ...state,
          isLoading: true,
          authResult: "",
          authResultSharePost: '',
          authResultAbusePost: '',
          comment: "",
          commentError: "",
        };

      case SHAREPOST_COMMENT_SUCCESS:
        return {
          ...state,
          authResult: '',
          isLoading: false,
          authResultSharePost: action.payload,
          authResultAbusePost: '',
          comment: "",
          commentError: "",

        };
        // case SHAREPOST_DATA:
        //     console.log('response sharepost', action.payload.DATA);
        //     return { ...state, authResult: action.payload,
        //       isLoading: false,
        //       DATA: action.payload.DATA,
        //     };

      case SHAREPOST_COMMENT_FAILED:
        return {
          ...state,
          authResult: '',
          isLoading: false,
          authResultSharePost: action.payload,
          authResultAbusePost: '',
          comment: "",
          commentError: "",

        };


        case LOADING_COMMENT_ABUSE:
          console.log("loading...");
          return {
            ...state,
            isLoading: true,
            authResult: "",
            authResultSharePost: '',
            authResultAbusePost: '',
            comment: "",
            commentError: "",
          };

        case ABUSE_COMMENT_SUCCESS:
          return {
            ...state,
            authResult: '',
            isLoading: false,
            authResultSharePost: '',
            authResultAbusePost: action.payload,
            comment: "",
            commentError: "",

          };
          // case SHAREPOST_DATA:
          //     console.log('response sharepost', action.payload.DATA);
          //     return { ...state, authResult: action.payload,
          //       isLoading: false,
          //       DATA: action.payload.DATA,
          //     };

        case ABUSE_COMMENT_FAILED:
          return {
            ...state,
            authResult: '',
            isLoading: false,
            authResultSharePost: '',
            authResultAbusePost: action.payload,
            comment: "",
            commentError: "",

          };


          case COMMENTLIST_LOADING:
            return { ...state,
               isLoading: true ,
               authResultSharePost: '',
               authResultAbusePost: '',
               comment: "",
               commentError: "",
             };
          case COMMENTLIST_DATA:
              console.log('response commentList', action.payload);
              console.log('response commentList', action.payload.postdata);
              return { ...state,
                authResult: 'success list',
                isLoading: false,
                comments_list: action.payload.list,
                postdata: action.payload.postdata,
                authResultSharePost: '',
                authResultAbusePost: '',
                comment: "",
                commentError: "",



              };
          case COMMENTLIST_ERROR_CHANGED:
              console.log('Error');
              return { ...state,
               authResult: 'fail',
               isLoading: false,
               authResultSharePost: '',
               authResultAbusePost: '',
               comment: "",
               commentError: "", };

          case COMMENTLIST_INITIAL_STATE:



    default:
      return state;
  }
};
