import {
  TESTIMONIAL_INITIAL_STATE,
  LOADING_TESTIMONIAL,
  TESTIMONIAL_FAILED,
  TESTIMONIAL_DATA,
  TESTIMONIAL_CHANGED,
  TESTIMONIAL_ERROR_CHANGED,
  TESTIMONIAL_SUCCESS,

  LOADING_ADDFRIEND,
  ADDFRIEND_DATA,
  ADDFRIEND_SUCCESS,
  ADDFRIEND_FAILED,

  LOADING_BLOCKFRIEND,
  BLOCKFRIEND_DATA,
  BLOCKFRIEND_SUCCESS,
  BLOCKFRIEND_FAILED,

  RATING_INITIAL_STATE,
  LOADING_RATING,
  RATING_DATA,
  RATING_CHANGED,
  RATING_ERROR_CHANGED,
  RATING_SUCCESS,
  RATING_FAILED,

  LOADING_CHATNOTIFICATION,
  CHATNOTIFICATION_SUCCESS,
  CHATNOTIFICATION_FAILED
} from "../Actions/type";

const INTIAL_STATE = {
  testimonial: '',
  testimonialError: '',
  rating: '',
  ratingError: '',
  authResultTestimonial: "",
  authResultAddFriend: "",
  authResultBlockFriend: "",
  authResultRetting: "",
  isLoading: false,
  authResultchatnotification: ''
};

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {
    case TESTIMONIAL_CHANGED:
      return {
        ...state,
        testimonial: action.payload,
        testimonialError: "",
        authResultTestimonial: "",
        authResultAddFriend: "",
        authResultBlockFriend: "",
        authResultRetting: "",
        authResultchatnotification: ''
      };
    case TESTIMONIAL_ERROR_CHANGED:
      return {
        ...state, testimonialError: action.payload,
        authResultTestimonial: "",
        authResultAddFriend: "",
        authResultBlockFriend: "",
        authResultRetting: "",
        authResultchatnotification: ''
      };

    case LOADING_TESTIMONIAL:
      console.log("loading...");
      return {
        ...state,
        testimonialError: "",
        isLoading: true,
        authResultTestimonial: "",
        authResultAddFriend: "",
        authResultBlockFriend: "",
        authResultRetting: "",
        authResultchatnotification: ''
      };

    case TESTIMONIAL_SUCCESS:
      return {
        ...state,
        authResultTestimonial: action.payload,
        isLoading: false,
        testimonial: "",
        authResultAddFriend: "",
        authResultBlockFriend: "",
        authResultRetting: "",
        authResultchatnotification: ''

      };

    case TESTIMONIAL_FAILED:
      return {
        ...state,
        isLoading: false,
        testimonial: "",
        authResultTestimonial: action.payload,
        authResultAddFriend: "",
        authResultBlockFriend: "",
        authResultRetting: "",
        authResultchatnotification: ''

      };

    case LOADING_ADDFRIEND:
      console.log("loading...");
      return {
        ...state,
        isLoading: true,
        authResultAddFriend: "",
        authResultTestimonial: "",
        authResultBlockFriend: "",
        authResultRetting: "",
        authResultchatnotification: ''
      };

    case ADDFRIEND_SUCCESS:
      return {
        ...state,
        authResultAddFriend: action.payload,
        isLoading: false,
        authResultTestimonial: "",
        authResultBlockFriend: "",
        authResultRetting: "",
        authResultchatnotification: ''

      };

    case ADDFRIEND_FAILED:
      return {
        ...state,
        authResultAddFriend: action.payload,
        isLoading: false,
        authResultTestimonial: "",
        authResultBlockFriend: "",
        authResultRetting: "",
        authResultchatnotification: ''

      };


    case LOADING_BLOCKFRIEND:
      console.log("loading...");
      return {
        ...state,
        isLoading: true,
        authResultTestimonial: "",
        authResultAddFriend: "",
        authResultBlockFriend: "",
        authResultRetting: "",
        authResultchatnotification: ''
      };

    case BLOCKFRIEND_SUCCESS:
      return {
        ...state,
        authResultBlockFriend: action.payload,
        isLoading: false,
        authResultTestimonial: "",
        authResultAddFriend: "",
        authResultRetting: "",
        authResultchatnotification: ''

      };

    case BLOCKFRIEND_FAILED:
      return {
        ...state,
        authResultBlockFriend: action.payload,
        isLoading: false,
        authResultTestimonial: "",
        authResultAddFriend: "",
        authResultRetting: "",
        authResultchatnotification: ''

      };

    case RATING_CHANGED:
      return {
        ...state,
        rating: action.payload,
        ratingError: "",
        authResultTestimonial: "",
        authResultAddFriend: "",
        authResultBlockFriend: "",
        authResultRetting: "",
        authResultchatnotification: ''
      };
    case RATING_ERROR_CHANGED:
      return {
        ...state, ratingError: action.payload,
        authResultTestimonial: "",
        authResultAddFriend: "",
        authResultBlockFriend: "",
        authResultRetting: "",
        authResultchatnotification: ''
      };

    case LOADING_RATING:
      console.log("loading...");
      return {
        ...state,
        ratingError: "",
        isLoading: true,
        authResultTestimonial: "",
        authResultAddFriend: "",
        authResultBlockFriend: "",
        authResultRetting: "",
        authResultchatnotification: ''
      };

    case RATING_SUCCESS:
      return {
        ...state,
        authResultRetting: action.payload,
        isLoading: false,
        rating: "",
        authResultTestimonial: "",
        authResultAddFriend: "",
        authResultBlockFriend: "",
        authResultchatnotification: ''

      };

    case RATING_FAILED:
      return {
        ...state,
        authResultRetting: action.payload,
        isLoading: false,
        rating: "",
        authResultTestimonial: "",
        authResultAddFriend: "",
        authResultBlockFriend: "",
        authResultchatnotification: ''


      };



      case LOADING_CHATNOTIFICATION:
      console.log("loading...");
      return {
        ...state,
        ratingError: "",
        isLoading: true,
        authResultTestimonial: "",
        authResultAddFriend: "",
        authResultBlockFriend: "",
        authResultRetting: "",
        authResultchatnotification: ''
      };

    case CHATNOTIFICATION_SUCCESS:
      return {
        ...state,
        authResultRetting: '',
        authResultchatnotification: action.payload,
        isLoading: false,
        rating: "",
        authResultTestimonial: "",
        authResultAddFriend: "",
        authResultBlockFriend: "",

      };

    case CHATNOTIFICATION_FAILED:
      return {
        ...state,
        authResultchatnotification: action.payload,
        authResultRetting: '',
        isLoading: false,
        rating: "",
        authResultTestimonial: "",
        authResultAddFriend: "",
        authResultBlockFriend: "",


      };


    case RATING_INITIAL_STATE:
      return INTIAL_STATE;


    case TESTIMONIAL_INITIAL_STATE:
      return INTIAL_STATE;
    default:
      return state;
  }
};
