import {
  NOTIFICATION_DATA,
  NOTIFICATION_ERROR_CHANGED,
  NOTIFICATION_LOADING,
  NOTIFICATION_INITIAL_STATE,
  BASE_URL,
  NOTIFICATION_ALL_DATA,
  NOTIFICATION_ALL_ERROR_CHANGED,
  NOTIFICATION_ALL_LOADING
} from '../Actions/type';

const INTIAL_STATE = {
  Notification_list: [],
  Notification_clearAll: [],
  authResult: '',
  isLoading: false,
  isLoadingclear: false,
};

export default (state = INTIAL_STATE, action) => {
  const responce = action.payload;
    switch (action.type) {
      case NOTIFICATION_LOADING:
        return { ...state, isLoading: true };
      case NOTIFICATION_DATA:
          console.log('response commentList', action.payload);
          return { ...state, authResult: 'success',
            isLoading: false,
            Notification_list: action.payload,
            // Notification_clearAll: action.payload,
          };
      case NOTIFICATION_ERROR_CHANGED:
          console.log('Error');
          return { ...state, authResult: 'fail', isLoading: false, Notification_list: [] };


          case NOTIFICATION_ALL_LOADING:
            return { ...state, isLoadingclear: true };
          case NOTIFICATION_ALL_DATA:
              console.log('response commentList', action.payload);
              return { ...state, authResult: 'success',
                isLoadingclear: false,
                Notification_clearAll: action.payload,
              };
          case NOTIFICATION_ALL_ERROR_CHANGED:
              console.log('Error');
              return { ...state, authResult: 'fail', isLoadingclear: false };


      case NOTIFICATION_INITIAL_STATE:
        return INTIAL_STATE;
      default:
        return state;
    }
};