import {
  SEARCHLIST_DATA, BASE_URL1, SEARCHLIST_ERROR_CHANGED, SEARCHLIST_LOADING,
  SEARCHLIST_INITIAL_STATE
  , SEARCH_UPDATE_LIST_SUCCESS, SEARCH_UPDATE_LIST_ERROR
} from '../Actions/type';

const INTIAL_STATE = {
  search_updates: [],
  suggested_profiles: [],
  authResult: '',
  isLoading: false,
  isdata: false,
  search_update_list: [],
  isRefreshing: false
};

export default (state = INTIAL_STATE, action) => {
  const responce = action.payload;
  switch (action.type) {
    case SEARCHLIST_LOADING:
      console.log('action.payload', action.payload)
      return {
        ...state,
        isLoading: action.payload.isLoading,
        isRefreshing: action.payload.isRefreshing
      };
    case SEARCHLIST_DATA:
      return {
        ...state, authResult: 'success',
        isLoading: false,
        search_updates: action.payload.search_updates,
        suggested_profiles: action.payload.list,
        isdata: action.payload.isData,
        isRefreshing: false

      };
    case SEARCHLIST_ERROR_CHANGED:
      console.log('Error', action.payload);
      return { ...state, authResult: 'fail', isLoading: false, isRefreshing: false };
    case SEARCH_UPDATE_LIST_ERROR:
      return { ...state, authResult: 'fail', isLoading: false, isRefreshing: false };
    case SEARCH_UPDATE_LIST_SUCCESS:
      console.log(action.payload);
      return {
        ...state, authResult: 'success',
        isLoading: false,
        isRefreshing: false,
        search_update_list: action.payload.list,
        isdata: action.payload.isData
      };

    case SEARCHLIST_INITIAL_STATE:
      return INTIAL_STATE;
    default:
      return state;
  }
};
