import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  EMAIL_ERROR_CHANGED,
  PASSWORD_ERROR_CHANGED,
  FULL_NAME_CHANGED,
  FULL_NAME_ERROR_CHANGED,
  CONFIRM_PASSWORD_CHANGED,
  CONFIRM_PASSWORD_ERROR_CHANGED,
  SIGNUP_LOGIN,
  SIGNUP_INITIAL_STATE,
  SIGNUP_LOADING,
  SIGUP_FAILED,
  BASE_URL,
  SIGUP_SUCCESS
} from "../Actions/type";

const INTIAL_STATE = {
  fullNameError: "",
  passwordError: "",
  confirmPasswordError: "",
  emailError: "",
  email: "",
  name: "",
  password: "",
  confirmPassword: "",
  isLoading: false,
  authResult: ""
};

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {
    case FULL_NAME_CHANGED:
      console.log("name" + action.payload);
      return {
        ...state,
        name: action.payload,
        emailError: "",
        passwordError: "",
        fullNameError: "",
        confirmPasswordError: "",
        authResult: ""
      };
    case FULL_NAME_ERROR_CHANGED:
      return { ...state, fullNameError: action.payload, authResult: "" };
    case EMAIL_CHANGED:
      return {
        ...state,
        email: action.payload,
        emailError: "",
        passwordError: "",
        fullNameError: "",
        confirmPasswordError: "",
        authResult: ""
      };
    case EMAIL_ERROR_CHANGED:
      return { ...state, emailError: action.payload, authResult: "" };
    case PASSWORD_CHANGED:
      return {
        ...state,
        password: action.payload,
        emailError: "",
        passwordError: "",
        fullNameError: "",
        confirmPasswordError: "",
        authResult: ""
      };
    case PASSWORD_ERROR_CHANGED:
      return { ...state, passwordError: action.payload, authResult: "" };
    case CONFIRM_PASSWORD_CHANGED:
      return {
        ...state,
        confirmPassword: action.payload,
        emailError: "",
        passwordError: "",
        fullNameError: "",
        confirmPasswordError: "",
        authResult: ""
      };
    case CONFIRM_PASSWORD_ERROR_CHANGED:
      return { ...state, confirmPasswordError: action.payload, authResult: "" };

    case SIGNUP_INITIAL_STATE:
      return INTIAL_STATE;

    case SIGNUP_LOADING:
      console.log("loading...");
      return {
        ...state,
        isLoading: true,
        authResult: "",
        fullNameError: "",
        passwordError: "",
        confirmPasswordError: "",
        emailError: "",
        
      };

    case SIGUP_SUCCESS:
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,
        email: "",
        name: "",
        password: "",
        confirmPassword: ""
      };

    case SIGUP_FAILED:
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,
        email: "",
        name: "",
        password: "",
        confirmPassword: ""
      };

    default:
      return state;
  }
};
