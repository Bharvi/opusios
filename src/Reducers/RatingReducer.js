import {
  RATING_INITIAL_STATE,
  LOADING_RATING,
  RATING_DATA,
  RATING_CHANGED,
  RATING_ERROR_CHANGED,
  RATING_SUCCESS,
  RATING_FAILED
} from "../Actions/type";

const INTIAL_STATE = {
  rating: '',
  ratingError: '',
  authResult: "",
  isLoading: false
};

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {
    case RATING_CHANGED:
      return {
        ...state,
        rating: action.payload,
        ratingError: "",
        authResult: ""
      };
    case RATING_ERROR_CHANGED:
      return { ...state, ratingError: action.payload, authResult: "" };

    case LOADING_RATING:
      console.log("loading...");
      return {
        ...state,
        ratingError: "",
        isLoading: true,
        authResult: ""
      };

    case RATING_SUCCESS:
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,
        rating: "",

      };

    case RATING_FAILED:
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,
        rating: "",

      };

    case RATING_INITIAL_STATE:
      return INTIAL_STATE;
    default:
      return state;
  }
};
