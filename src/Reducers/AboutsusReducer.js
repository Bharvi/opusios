import {
  ABOUTUS_DATA,
  BASE_URL,
  ABOUTUS_ERROR_CHANGED,
  ABOUTUS_LOADING
} from "../Actions/type";

const INTIAL_STATE = {
  Data: "",
  authResult: "",
  isLoading: false
};

export default (state = INTIAL_STATE, action) => {
  const responce = action.payload;
  switch (action.type) {
    case ABOUTUS_LOADING:
      return { ...state, isLoading: true };
    case ABOUTUS_DATA:
      console.log(action.payload);
      return {
        ...state,
        authResult: "success",
        isLoading: false,
        Data: action.payload
      };
    case ABOUTUS_ERROR_CHANGED:
      console.log("Error");
      return { ...state, authResult: "fail", isLoading: false };

    default:
      return state;
  }
};
