import {
  SEARCHRESULT_DATA, BASE_URL, SEARCHRESULT_ERROR_CHANGED,SEARCHRESULT_LOADING,
  SEARCHRESULT_INITIAL_STATE,
   LOADING_ADDFRIEND1,
  ADDFRIEND_DATA1,
  ADDFRIEND_SUCCESS1,
  ADDFRIEND_FAILED1,
} from '../Actions/type';

const INTIAL_STATE = {
  search_result: [],
  total_results: '',
  authResult: '',
  authResultAddFriend: "",
  isLoading: false,
  isdata: false,
};

export default (state = INTIAL_STATE, action) => {
  const responce = action.payload;
    switch (action.type) {
      case SEARCHRESULT_LOADING:
        return { ...state, isLoading: true,
        authResultAddFriend: "",
        authResult: '' };
      case SEARCHRESULT_DATA:
          console.log('response searchresult', action.payload);
          console.log('response searchresult', action.payload.list);
          console.log('response searchresult', action.payload.total_results);
          return { ...state, authResult: 'success',
            isLoading: false,
            search_result: action.payload.list,
            total_results: action.payload.total_results,
            isdata: action.payload.isData,
            authResultAddFriend: "",

          };
      case SEARCHRESULT_ERROR_CHANGED:
            console.log('Error', action.payload);
          return { ...state, authResult: 'fail', isLoading: false,
          authResultAddFriend: "", };


          case LOADING_ADDFRIEND1:
        console.log("loading...");
        return {
          ...state,
          isLoading: true,
          authResultAddFriend: "",
          authResult: '',
          
        };

      case ADDFRIEND_SUCCESS1:
        return {
          ...state,
          authResultAddFriend: action.payload,
          isLoading: false,
          authResult: '',
          

        };

      case ADDFRIEND_FAILED1:
        return {
          ...state,
          authResultAddFriend: action.payload,
          isLoading: false,
          authResult: '',
         

        };          

      case SEARCHRESULT_INITIAL_STATE:
        return INTIAL_STATE;
      default:
        return state;
    }
};
