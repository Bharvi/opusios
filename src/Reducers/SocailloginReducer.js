import {
  SOCAIL_LOGIN_LOADING,
  SOCAIL_LOGIN_FAILED,
  SOCAIL_LOGIN_SUCCESS,
  CHECK_SUCCESS,
  CHECK_FAILED,
  CHECK_ERROR,
  INITIAL_STATE_SOCIAL
} from "../Actions/type";

const INTIAL_STATE = {
  isLoading: false,
  authResult: "",
  authResultEmail: '',
  userSocialData: '',
  loginType: ''
};

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {

    case SOCAIL_LOGIN_LOADING:
      console.log("loading...");
      return {
        ...state,
        isLoading: action.payload,
        authResult: ""
      };

    case SOCAIL_LOGIN_SUCCESS:
      console.log("Socail login", action.payload);
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,

      };

    case SOCAIL_LOGIN_FAILED:
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,
      };

    case CHECK_SUCCESS:
      // console.log('action.payload.Data', action.payload.Data)
      return {
        ...state,
        authResultEmail: action.payload.text,
        userSocialData: action.payload.Data,
        loginType: action.payload.loginType
      }
    case CHECK_FAILED:
      console.log('action.payload.Data', action.payload.Data)
      return {
        ...state,
        authResultEmail: action.payload.text,
        userSocialData: action.payload.Data,
        loginType: action.payload.loginType
      }
    case CHECK_ERROR:
      return {
        ...state,
        authResultEmail: 'error in checkEmail',
        userSocialData: [],
      }
    default:
      return state;
    case INITIAL_STATE_SOCIAL:
      return INTIAL_STATE
  }
};
