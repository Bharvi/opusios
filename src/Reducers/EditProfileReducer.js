import {
  FULL_NAME_CHANGED,
  FULL_NAME_ERROR_CHANGED,
  USER_PHOTO_CHANGED,
  USER_PHOTO_ERROR_CHANGED,
  EMAIL_CHANGED,
  EMAIL_ERROR_CHANGED,
  SEX_CHANGED,
  SEX_ERROR_CHANGED,
  BIRTHDATE_CHANGED,
  BIRTHDATE_ERROR_CHANGED,
  PROFESSION_CHANGED,
  PROFESSION_ERROR_CHANGED,
  LOCATION_CHANGED,
  LOCATION_ERROR_CHANGED,
  BRIEF_DESCRIPTION_CHANGED,
  BRIEF_DESCRIPTION_ERROR_CHANGED,
  EDIT_PROFILE_LOADING,
  EDIT_PROFILE_INITIAL_STATE,
  LOADING_EDIT,
  USER_EDIT_ERROR,
  USER_EDIT_SUCCESS,
  AUTH_RESULT_CHANGE_EDIT,
  SET_PROFILE_DATA,
  USER_TAG,
  USER_TAG_ERROR_CHANGED,
  ADD_TAG,
  REMOVE_TAG



} from "../Actions/type";

const INTIAL_STATE = {
  fullname: "",
  fullnameError: "",
  user_photo: "",
  user_photoError: "",
  user_tag: "",
  user_tagError: "",
  email_id: "",
  email_idError: "",
  gender: "",
  genderError: "",
  birth_date: "",
  birth_dateError: "",
  profession_name: "",
  profession_nameError: "",
  location: "",
  locationError: "",
  brief_desc: "",
  brief_descError: "",
  isLoading: false,
  authResult: '',
  tagArray: []
};

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {
    case FULL_NAME_CHANGED:
      return {
        ...state,
        fullname: action.payload,
        email_idError: "",
        user_photoError: "",
        fullnameError: "",
        genderError: "",
        birth_dateError: "",
        profession_nameError: "",
        locationError: "",
        brief_descError: "",
        // user_tagError: ""
      };
    case FULL_NAME_ERROR_CHANGED:
      return { ...state, fullnameError: action.payload };

    case EMAIL_CHANGED:
      return {
        ...state,
        email_id: action.payload,
        email_idError: "",
        fullnameError: "",
        user_photoError: "",
        genderError: "",
        birth_dateError: "",
        profession_nameError: "",
        locationError: "",
        brief_descError: "",
        // user_tagError: "",
      };
    case EMAIL_ERROR_CHANGED:
      return { ...state, email_idError: action.payload };

    case USER_PHOTO_CHANGED:
      return {
        ...state,
        user_photo: action.payload,
        email_idError: "",
        fullnameError: "",
        user_photoError: "",
        genderError: "",
        birth_dateError: "",
        profession_nameError: "",
        locationError: "",
        brief_descError: "",
        // user_tagError: "",

      };
    case USER_PHOTO_ERROR_CHANGED:
      return { ...state, user_photoError: action.payload };

    case SEX_CHANGED:
      return {
        ...state,
        gender: action.payload,
        email_idError: "",
        fullnameError: "",
        user_photoError: "",
        genderError: "",
        birth_dateError: "",
        profession_nameError: "",
        locationError: "",
        brief_descError: "",
        // user_tagError: "",
      };
    case SEX_ERROR_CHANGED:
      return { ...state, genderError: action.payload };

    // case USER_TAG:
    //   return {
    //     ...state,
    //     user_tag: action.payload,
    //     email_idError: "",
    //     fullnameError: "",
    //     user_photoError: "",
    //     genderError: "",
    //     birth_dateError: "",
    //     profession_nameError: "",
    //     locationError: "",
    //     brief_descError: "",
    //     genderError: "",
    //     // user_tagError: "",
    //   };
    // case USER_TAG_ERROR_CHANGED:
    //   return { ...state, user_tagError: action.payload };

    case BIRTHDATE_CHANGED:
      return {
        ...state,
        birth_date: action.payload,
        email_idError: "",
        fullnameError: "",
        genderError: "",
        user_photoError: "",
        birth_dateError: "",
        profession_nameError: "",
        locationError: "",
        brief_descError: "",
        // user_tagError: "",
      };
    case BIRTHDATE_ERROR_CHANGED:
      return { ...state, birth_dateError: action.payload };

    case PROFESSION_CHANGED:
      return {
        ...state,
        profession_name: action.payload,
        email_idError: "",
        fullnameError: "",
        genderError: "",
        user_photoError: "",
        birth_dateError: "",
        profession_nameError: "",
        locationError: "",
        brief_descError: "",
        // user_tagError: "",
      };
    case PROFESSION_ERROR_CHANGED:
      return { ...state, profession_nameError: action.payload };

    case LOCATION_CHANGED:
      return {
        ...state,
        location: action.payload,
        email_idError: "",
        fullnameError: "",
        genderError: "",
        birth_dateError: "",
        user_photoError: "",
        profession_nameError: "",
        locationError: "",
        brief_descError: "",
        // user_tagError: "",
      };
    case LOCATION_ERROR_CHANGED:
      return { ...state, locationError: action.payload };

    case BRIEF_DESCRIPTION_CHANGED:
      return {
        ...state,
        brief_desc: action.payload,
        email_idError: "",
        fullnameError: "",
        genderError: "",
        user_photoError: "",
        birth_dateError: "",
        profession_nameError: "",
        locationError: "",
        brief_descError: "",
        // user_tagError: "",
      };
    case BRIEF_DESCRIPTION_ERROR_CHANGED:
      return { ...state, brief_descError: action.payload };

    case EDIT_PROFILE_INITIAL_STATE:
      return INTIAL_STATE;



    case AUTH_RESULT_CHANGE_EDIT:
    return { ...state,
      authResult: action.payload,
    };
    case LOADING_EDIT:
      console.log('loading...');
      return { ...state,
        isLoading: true,
      };
      case USER_EDIT_SUCCESS:
      console.log(action.payload);
      return { ...state,
        authResult: action.payload,
        isLoading: false,

        fullnameError: '',
        email_idError: '',
        genderError: '',
        birth_dateError: '',
        profession_nameError: '',
        locationError: '',
        brief_descError: '',
        user_photoError: "",
        // user_tagError: "",

      };
      case USER_EDIT_ERROR:
      console.log(action.payload);
      return { ...state,
        authResult: action.payload,
        isLoading: false,
        fullnameError: '',
        email_idError: '',
        genderError: '',
        birth_dateError: '',
        profession_nameError: '',
        locationError: '',
        brief_descError: '',
        user_photoError: "",
        // user_tagError: "",

      };
      case SET_PROFILE_DATA:
      console.log(action.payload);
      return { ...state,
        authResult: action.payload.authResult,
        isLoading: false,
        fullname: action.payload.fullname,
        email_id: action.payload.email_id,
        gender: action.payload.gender,
        birth_date: action.payload.birth_date,
        profession_name: action.payload.profession_name,
        location: action.payload.location,
        brief_desc: action.payload.brief_desc,
        user_photo: action.payload.user_photo,
        // tagArray: action.payload.tagArray,
      };
    // case ADD_TAG:
    //   return { ...state,
    //     tagArray: action.payload.tagArray,
    //     user_tagError: action.payload.user_tagError,
    //     user_tag: action.payload.user_tag,
    //     authResult: ''
    //   };
    // case REMOVE_TAG:
    //   return { ...state,
    //     tagArray: action.payload,
    //     user_tag: '',
    //     authResult: ''
    //   };
    default:
      return state;
  }
};
