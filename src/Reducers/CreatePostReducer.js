import {
  CREATE_POST_INITIAL_STATE,
  LOADING_CREATE_POST,
  CREATE_POST_FAILED,
  CREATEPOST_DATA,
  CAPTION_CHANGED,
  CAPTION_ERROR_CHANGED,
  IMAGEPOST_CHANGED,
  IMAGEPOST_ERROR_CHANGED,
  CREATEPOST_SUCCESS,
  CREATEPOST_FAILED
} from "../Actions/type";

const INTIAL_STATE = {
  caption: '',
  captionError: '',
  imagepost: '',
  imagepostError: '',
  CreatePost: "",
  authResult: "",
  isLoading: false
};

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {
    case CAPTION_CHANGED:
      return {
        ...state,
        caption: action.payload,
        captionError: "",
        imagepostError: "",
        authResult: ""
      };
    case CAPTION_ERROR_CHANGED:
      return { ...state, captionError: action.payload, authResult: "" };
    case IMAGEPOST_CHANGED:
      return {
        ...state,
        imagepost: action.payload,
        captionError: "",
        imagepostError: "",
        authResult: ""
      };
    case IMAGEPOST_ERROR_CHANGED:
      return { ...state, imagepostError: action.payload, authResult: "" };

    case LOADING_CREATE_POST:
      console.log("loading...");
      return {
        ...state,
        captionError: "",
        isLoading: true,
        imagepostError: "",
        authResult: ""
      };

    case CREATEPOST_SUCCESS:
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,
        caption: "",
        imagepost: ""
      };

    case CREATEPOST_FAILED:
      return {
        ...state,
        authResult: action.payload,
        isLoading: false,
        caption: "",
        imagepost: ""
      };

    case CREATE_POST_INITIAL_STATE:
      return INTIAL_STATE;
    default:
      return state;
  }
};
