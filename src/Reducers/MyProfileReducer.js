import {
  MYAbout_DATA, BASE_URL, MYAbout_ERROR_CHANGED,MYAbout_LOADING,
  MYPROFILE_LOADING, MYPROFILE_ERROR_CHANGED, MYPROFILE_DATA,
  ABOUT_INITIAL_STATE, MYPROFILE_INITAL_STATE
} from '../Actions/type';

const INTIAL_STATE = {
  About_list: '',
  Data: '',
  authResult: '',
  isLoading: false,
};

export default (state = INTIAL_STATE, action) => {
  const responce = action.payload;
    switch (action.type) {
      case MYAbout_LOADING:
        return { ...state, isLoading: true };
      case MYAbout_DATA:
          return { ...state, 
            authResult: "aboutdata success",
            isLoading: false,
            About_list: action.payload
          };
      case MYAbout_ERROR_CHANGED:
          console.log('Error');
          return { ...state, authResult: 'fail', isLoading: false };

      case MYPROFILE_LOADING:
        return { ...state, isLoading: true };
      case MYPROFILE_DATA:
          return { ...state, 
          authResult: "my profile data success",
            isLoading: false,
            Data: action.payload
          };
      case MYPROFILE_ERROR_CHANGED:
          console.log('Error');
          return { ...state, authResult: 'fail', isLoading: false };

          case ABOUT_INITIAL_STATE:
      return INTIAL_STATE;

      case MYPROFILE_INITAL_STATE:
      return INTIAL_STATE;
      
      default:
        return state;
    }
};
