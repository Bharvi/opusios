import axios from 'axios';
import {
  PROFECATION_CHANGED, PROFECATION_ERROR_CHANGED,
  COMPELETEPROFILE_LOGIN, COMPELETEPROFILE_INITIAL_STATE, BASE_URL, COMPELETEPROFILE_FAILED, COMPELETEPROFILE_SUCCESS,
  LOCATION_CHANGED_COMPELETEPROFILE, LOCATION_ERROR_CHANGED_COMPELETEPROFILE,
  ALLTAGS_DATA,
  ON_TAG_PRESS,
  ALLTAGS_LOADING,
  ALLTAGS_ERROR,
  ALLTAGS_INITIAL_STATE,
  GET_SEARCH_DATA,
  PROFILEIMAGEPATH
} from './type';
import { insertUser, deleteUser } from '../Database/allSchema';
import { StackActions, NavigationActions } from 'react-navigation';
import firebase from "react-native-firebase";
import { userChatHistory } from './ChatListAction';
import { getChatNotification } from './TestimonialAction';

export const profecationChange1 = (text) => {
  return {
    type: PROFECATION_CHANGED,
    payload: text
  };
};

export const profecationErrorChange1 = (text) => {
  return {
    type: PROFECATION_ERROR_CHANGED,
    payload: text
  };
};

export const locationChange1 = (text) => {
  return {
    type: LOCATION_CHANGED_COMPELETEPROFILE,
    payload: text
  };
};

export const locationErrorChange1 = (text) => {
  return {
    type: LOCATION_ERROR_CHANGED_COMPELETEPROFILE,
    payload: text
  };
};

export const initialcompeleteprofileStateData = () => {
  return {
    type: COMPELETEPROFILE_INITIAL_STATE,
    payload: ''
  };
};

export const getcompleteProfile = ({ userID, tagID, professions, location, token, myNavigation }) => {
  console.warn(userID);
  console.warn(tagID);
  console.warn(professions);
  console.warn(location);
  console.warn(token);

  var details = {
    userID, tagID, professions, location
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return (dispatch) => {
    dispatch({
      type: COMPELETEPROFILE_LOGIN
    });
    axios.post(`${BASE_URL}completeProfile`, formBody, {
      headers: {
        'Accept': 'application/json',
        'Authorization': token,
      }
    })
      .then(response => {
        console.log(response);
        if (response.data.STATUS) {
          saveUser(response.data.DATA.user_data, myNavigation, dispatch, token);
          getAllTag1(token, dispatch);
          console.log(saveUser);
          console.log('Hello');
        } else {
          dispatch({
            type: COMPELETEPROFILE_FAILED,
            payload: 'Invalid data'
          });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: COMPELETEPROFILE_FAILED,
          payload: 'Invalid data'
        });
      });
  };
};

const setOpusWelcomeMsg = (userData, token, dispatch) => {
  let firebaseCurrentUser = firebase.auth().currentUser.uid;
  let firebaseOpus = "afdsfsdfdsfsdfsdfsdf";
  let chatId = "";
  if (firebaseOpus > firebaseCurrentUser) { chatId = `${firebaseOpus}-${firebaseCurrentUser}`; }
  else { chatId = `${firebaseCurrentUser}-${firebaseOpus}`; }
  let chatRefData = firebase.database().ref().child("chat/" + chatId);
  var now = new Date().getTime();
  chatRefData.push({
    _id: now,
    text: 'Hello, ' + userData.fullname + '. Welcome to Opus. You can make new penpals here by just searching them by tags!  Feel free to reply to this message for any help. Happy penpalling!',
    createdAt: now,
    uid: firebaseOpus,
    fuid: firebaseCurrentUser,
    order: -1 * now,
    isRead: 1,
  });
  //set in history
  dispatch(userChatHistory(1, userData.user_id, token))
  //set notification
  setTimeout(() => {
    dispatch(
      getChatNotification({
        userID: 1,
        toUserId: userData.user_id,
        message: 'Hello, ' + userData.fullname + '. Welcome to Opus. You can make new penpals here by just searching them by tags!  Feel free to reply to this message for any help. Happy penpalling!',
        message_count: 1,
        token
      })
    )
  }, 3000);
};

const saveUser = (userData, myNavigation, dispatch, token) => {
  console.log(userData);
  deleteUser().then(() => {
    let deviceType = userData.device_type
    const newUser = {
      id: (userData.user_id == null) ? 1 : Number(userData.user_id),    // primary key
      fullname: (userData.fullname == null) ? '' : userData.fullname,
      email_id: (userData.email_id == null) ? '' : userData.email_id,
      user_password: (userData.user_password == null) ? '' : userData.user_password,
      device_type: (userData.device_type == null) ? '' : deviceType.toString(),
      user_photo: PROFILEIMAGEPATH + userData.user_photo,
      device_token: (userData.device_token == null) ? '' : userData.device_token,
      gender: (userData.gender != null) ? ((userData.gender == "0") ? 'Female' : 'Male') : 'null',
      profession_name: (userData.profession == null) ? '' : userData.profession,
      location: (userData.location == null) ? '' : userData.location,
      brief_desc: (userData.brief_desc == null) ? '' : userData.brief_desc,
      birth_date: (userData.birth_date == null) ? '' : userData.birth_date,
      fire_id: (userData.firebase == null) ? '' : userData.firebase,
      is_notify: (userData.is_notify == null) ? '' : userData.is_notify,
      user_tag: (userData.user_tag == null) ? '' : userData.user_tag,
    };
    insertUser(newUser).then(() => {
      console.log('Insert Suceess');
      console.log(newUser);
      setOpusWelcomeMsg(userData, token, dispatch);
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({
          routeName: 'BottomTab'
        })],
      });
      myNavigation.dispatch(resetAction);
      dispatch({
        type: COMPELETEPROFILE_SUCCESS,
        payload: 'Success'
      });
    }).catch((error) => {
      console.warn(`User Insert error${error}`);
      dispatch({
        type: COMPELETEPROFILE_FAILED,
        payload: 'Invalid data'
      });
    });
  }).catch((error) => {
    console.warn(error);
    dispatch({
      type: COMPELETEPROFILE_FAILED,
      payload: 'Invalid data'
    });
  });
};


export const getAllTags = ({ token }) => {
  console.log(token);
  console.log('aaaaa')
  return (dispatch) => {
    dispatch({
      type: ALLTAGS_LOADING
    });
    axios.get(`${BASE_URL}alltags`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': token,

      }
    })
      .then(response => {

        console.log('aavi gayu');
        if (response.data == '') {
          dispatch({ type: ALLTAGS_ERROR, payload: 'Something went wrong please try again' });
        } else {
          if (response.data.STATUS) {
            // console.log(response.data.DATA.tags);
            console.log(response.data.DATA.professions);
            // var results = response.data.DATA.professions;
            var tempprofessions = [];
            response.data.DATA.professions.map((item) =>
              tempprofessions.push({ label: item.name, value: item.id }));
            // console.log(tempprofessions);
            const tagsList = response.data.DATA.tags;
            const temptagsList = [];
            for (let i = 0; i < tagsList.length; i++) {
              temptagsList[i] = {
                ...tagsList[i],
                seleted: false
              };
            }
            console.log(JSON.stringify(temptagsList));
            dispatch({ type: ALLTAGS_DATA, payload: { tags: temptagsList, professions: tempprofessions } });
          } else {
            console.log('not true');
            dispatch({ type: ALLTAGS_ERROR, payload: 'Something went wrong please try again' });
          }
        }
      })
      .catch((error) => {
        console.log('ERROR  ' + error);
        dispatch({ type: ALLTAGS_ERROR, payload: 'Something went wrong please try again' });
      });
  };
};

export const onPressTagView = (index, tags) => {
  const temptagsList = [...tags];
  temptagsList[index] = { ...temptagsList[index], seleted: !temptagsList[index].seleted };
  return {
    type: ON_TAG_PRESS,
    payload: temptagsList
  };
};

const getAllTag1 = (token, dispatch) => {
  console.warn('in alltags method..');
  axios.get(BASE_URL + `alltags`, {
    headers: {
      Accept: 'application/json',
      Authorization: token
    }
  })
    .then(response => {
      console.log('getAllTag responce', response.data.DATA);

      let tempTags = [];
      let tempProfession = [];
      response.data.DATA.tags.map((item) => { tempTags.push(item.name) });
      response.data.DATA.professions.map((item) => { tempProfession.push(item.name) });
      dispatch({
        type: GET_SEARCH_DATA,
        payload: tempTags.concat(response.data.DATA.users, tempProfession, response.data.DATA.locations)
      });
    })
    .catch(error => {
      console.log('error in alltags', error);

    });
};
