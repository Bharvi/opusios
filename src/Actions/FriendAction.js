import axios from 'axios';
import {
  Alert,
} from "react-native";
import {
  FRIEND_INITIAL_STATE,
  FRIEND_DATA, BASE_URL, FRIEND_ERROR_CHANGED, FRIEND_LOADING,
  UNFRIEND_DATA, UNFRIEND_ERROR_CHANGED, UNFRIEND_LOADING, UNFRIEND_SUCCESS, PROFILE_FRIEND_DATA
} from './type';

export const getMyFriend = ({ userId, page, friend, token, isLoading, isMyProfile }) => {
  console.log(userId);
  console.warn("page", page);
  console.log(token);
  var details = {

  };

  var formBody = [];

  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return (dispatch) => {

    if (page === 1 && isLoading) {
      dispatch({
        type: FRIEND_LOADING
      });
    }


    axios.get(`${BASE_URL}friends?userID=${userId}&pageNo=${page}&pageCount=50`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': token,
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      }
    })
      .then(response => {
        // console.log('MYAbout success'+ response);
        console.log('aavi gayu');
        if (response.data == '') {
          dispatch({ type: FRIEND_ERROR_CHANGED, payload: 'Something went wrong please try again' });
        } else {
          if (response.data.STATUS) {

            // console.log(response.data.DATA);
            // dispatch({ type: FRIEND_DATA, payload: response.data.DATA });
            var isData = true;
            if (response.data.DATA.length === 0) {
              isData = false;
            }
            console.log(response.data.DATA);
            if (page === 1) {
              if (isMyProfile) {
                dispatch({ type: FRIEND_DATA, payload: { list: response.data.DATA, isData } });
              } else {
                dispatch({ type: PROFILE_FRIEND_DATA, payload: { list: response.data.DATA, isData } });
              }
            } else {
              if (response.data.DATA.length > 0) {
                console.warn("Hellow page");
                if (isMyProfile) {
                  dispatch({ type: FRIEND_DATA, payload: { list: friend.concat(response.data.DATA), isData } });
                } else {
                  dispatch({ type: PROFILE_FRIEND_DATA, payload: { list: friend.concat(response.data.DATA), isData } });
                }
                // posts.concat(response.data.DATA);

              }
            }
            // console.log(response.data.DATA.Feed_list);
            // dispatch({ type: MYAbout_DATA, payload: response.data.DATA.Feed_list});
          } else {
            console.log('not true');
          }
        }

      })
      .catch((error) => {
        console.log('FRIEND_ERROR_CHANGED  ' + error);
        dispatch({ type: FRIEND_ERROR_CHANGED, payload: 'Something went wrong please try again' });
      });
  };
};

export const getUnfriend = ({ userId, toUserId, token, isShowAlert }) => {
  console.log(userId);
  console.log(toUserId);
  console.log(token);

  var details = {
    userID: userId,
    toUserID: toUserId,
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {

    axios
      .post(`${BASE_URL}unfriends`, formBody, {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        }
      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS == true) {
          console.log('Hello');
          getMyFriend1(userId, token, dispatch);
          // dispatch({
          //   type: UNFRIEND_SUCCESS,
          //   payload: 'UnFriend successfully'
          // });
        }
        else if (response.data.Message == "Not a Friend") {
          // if(isShowAlert){
          // setTimeout(() => {

          //   Alert.alert("Oops", "Not a friend");

          // }, 500);
          // }

          // dispatch({
          //   type: UNFRIEND_ERROR_CHANGED,
          //   payload: 'Not a Friend'
          // });
        }
        else {
          // setTimeout(() => {

          //   Alert.alert("Oops", "Something wents wrong please try again later.");

          // }, 500);

          // }

          // dispatch({
          //   type: UNFRIEND_ERROR_CHANGED,
          //   payload: 'User id must be numeric.'
          // });
        }

      })
      .catch(error => {
        console.log(error);
        // if(isShowAlert){
        setTimeout(() => {
          Alert.alert("Oops", "Something wents wrong please try again later.");

        }, 500);
        // }

        // dispatch({
        //   type: UNFRIEND_ERROR_CHANGED,
        //   payload: 'User id must be numeric.'
        // });
      });
  };
};

const getMyFriend1 = (userId, token, dispatch) => {
  // dispatch({
  //   type: FRIEND_LOADING
  // });
  axios.get(`${BASE_URL}friends?userID=${userId}&pageNo=1&pageCount=10`, {
    headers: {
      'Accept': 'application/json',
      'Authorization': token,
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
    }
  })
    .then(response => {
      // console.log('MYAbout success'+ response);
      console.log('aavi gayu');
      if (response.data == '') {
        dispatch({ type: FRIEND_ERROR_CHANGED, payload: 'Something went wrong please try again' });
      } else {
        if (response.data.STATUS) {

          // console.log(response.data.DATA);
          // dispatch({ type: FRIEND_DATA, payload: response.data.DATA });
          var isData = true;
          if (response.data.DATA.length === 0) {
            isData = false;
          }
          console.log(response.data.DATA);
          dispatch({ type: FRIEND_DATA, payload: { list: response.data.DATA, isData } });


          // console.log(response.data.DATA.Feed_list);
          // dispatch({ type: MYAbout_DATA, payload: response.data.DATA.Feed_list});
        } else {
          console.log('not true');
          dispatch({ type: FRIEND_ERROR_CHANGED, payload: 'Something went wrong please try again' });
        }
      }

    })
    .catch((error) => {
      console.log('FRIEND_ERROR_CHANGED  ' + error);
      dispatch({ type: FRIEND_ERROR_CHANGED, payload: 'Something went wrong please try again' });
    });
};

export const initialGetFriendsList = () => {
  return {
    type: FRIEND_INITIAL_STATE,
    payload: ''
  };
};