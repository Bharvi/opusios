import axios from 'axios';
import {
   MYAbout_DATA, 
   BASE_URL, 
   MYAbout_ERROR_CHANGED,
   MYAbout_LOADING,
   MYPROFILE_LOADING, 
   MYPROFILE_ERROR_CHANGED, 
   MYPROFILE_DATA,
   ABOUT_INITIAL_STATE, 
   MYPROFILE_INITAL_STATE
} from './type';

export const getMYAbout = ({ userId, token ,isLoading}) => {
  console.log(userId);
  console.log(token);
  console.log('aaaaa');
  var details =  {

  };

  var formBody = [];

  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return (dispatch) => {
    if(isLoading) {
      dispatch({
        type: MYAbout_LOADING
      });
    }
    
    axios.get(`${BASE_URL}aboutUser?userID=${userId}`,{
      headers: {
        'Accept':'application/json',
        'Authorization': token,
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        }
      })
      .then(response => {
        if (response.data.STATUS) {
            console.log(response.data.DATA);
            dispatch({ type: MYAbout_DATA, payload: response.data.DATA });
        }else{
          dispatch({ type: MYAbout_ERROR_CHANGED, payload: 'Something went wrong please try again' });
        }
      })
      .catch((error) => {
        console.log('MYAbout_ERROR  '+error);
        dispatch({ type: MYAbout_ERROR_CHANGED, payload: 'Something went wrong please try again' });
      });
    };
};


export const getProfile = ({ userId,toUserID, token }) => {
  console.log(userId);
  console.log(toUserID);  
  console.log(token);
  console.log('aaaaa');
  var details =  {

  };

  var formBody = [];

  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return (dispatch) => {
    dispatch({
      type: MYPROFILE_LOADING,
    });
    axios.get(`${BASE_URL}user-profile?userID=${userId}&toUserID=${toUserID}`,{
      headers: {
        'Accept':'application/json',
        'Authorization': token
        }
      })
      .then(response => {
        if (response.data.STATUS) {
            console.log(response.data.DATA);
            dispatch({ type: MYPROFILE_DATA, payload: response.data.DATA });
        }else{
          dispatch({ type: MYPROFILE_ERROR_CHANGED, payload: 'Something went wrong please try again' });
        }
      })
      .catch((error) => {
        console.log('MYAbout_ERROR  '+error);
        dispatch({ type: MYPROFILE_ERROR_CHANGED, payload: 'Something went wrong please try again' });
      });
    };
};

export const initialAboutStateData = () => {
  return {
    type: ABOUT_INITIAL_STATE,
    payload: ""
  };
};

export const initialMyprofileStateData = () => {
  return {
    type: MYPROFILE_INITAL_STATE,
    payload: ""
  };
};