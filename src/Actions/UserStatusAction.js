import axios from 'axios';
import {
    BASE_URL,
    USER_STATUS_SUCCESS,
    USER_STATUS_FAILED,
    USER_STATUS_ERROR
} from './type';

export const setUserStatus = (userID, user_status, token) => {

    // console.log(token);
    const formData = new FormData();
    formData.append('userID', userID);
    formData.append('user_status', user_status);

    console.log('setUserStatus', formData);

    return (dispatch) => {
        axios.post(`${BASE_URL}userStatus`, formData,
            {
                headers: {
                    Accept: 'application/json',
                    Authorization: token
                }
            })
            .then((response) => {
                console.log('userStatus_response', response)
                if (response.msg == 'Updated Successfully') {
                    dispatch({
                        type: USER_STATUS_SUCCESS
                    });
                } else {
                    dispatch({
                        type: USER_STATUS_FAILED
                    });
                }
            })
            .catch((error) => {
                console.log('userStatus_response', error)
                dispatch({
                    type: USER_STATUS_ERROR
                });
            })
    };
};
