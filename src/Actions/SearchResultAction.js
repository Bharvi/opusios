import axios from 'axios';
import {
  SEARCHRESULT_DATA, BASE_URL, SEARCHRESULT_ERROR_CHANGED, SEARCHRESULT_LOADING,
  SEARCHRESULT_INITIAL_STATE,
  LOADING_ADDFRIEND1,
  ADDFRIEND_DATA1,
  ADDFRIEND_SUCCESS1,
  ADDFRIEND_FAILED1,
} from './type';

export const getSearchResult = ({ userId, page, tag, search_result, token }) => {
  console.log(userId);
  console.log(page);
  console.log(tag);
  console.log(token);
  var details = {

  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return (dispatch) => {
    console.log('bbbbbb');
    if (page === 1) {
      dispatch({
        type: SEARCHRESULT_LOADING
      });
    }
    axios.get(`${BASE_URL}searchresult?userID=${userId}&pageCount=30&pageNo=${page}&tag=${tag}`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': token
      }
    })
      .then(response => {
        // console.log('FeedList success'+ response);
        console.log('aavi gayu');
        console.log(response.data);
        if (response.data == '') {
          console.log('aavi gayu 1  ');
          // getFeedlist();
          dispatch({ type: SEARCHRESULT_ERROR_CHANGED, payload: 'Something went wrong please try again' });
        } else {
          if (response.data.STATUS) {
            console.log('Hello');
            console.log(response.data.DATA);
            // let a = response.data.DATA;
            // let feed_data = response.data.DATA.Feed_list;
            // console.log(feed_data);
            // console.log(a.Feed_list);

            var isData = true;
            if (response.data.DATA.search_result.length === 0) {
              isData = false;
            }
            console.log(response.data.DATA.search_result);
            if (page === 1) {
              dispatch({ type: SEARCHRESULT_DATA, payload: { list: response.data.DATA.search_result, total_results: response.data.DATA.total_results, isData } });
            } else {
              if (response.data.DATA.search_result.length > 0) {
                // posts.concat(response.data.DATA);
                // console.warn('total_results', response.data.DATA.total_results)
                dispatch({
                  type: SEARCHRESULT_DATA,
                  payload: {
                    list: search_result.concat(response.data.DATA.search_result),
                    total_results: response.data.DATA.total_results,
                    isData
                  }
                });
              }
            }
            // console.log(response.data.DATA.search_result);
            // dispatch({ type: FEEDLIST_DATA, payload: response.data.DATA.Feed_list});
          } else {
            console.log('not true');
            dispatch({ type: SEARCHRESULT_ERROR_CHANGED, payload: 'Something went wrong please try again' });
          }
        }
      })
      .catch((error) => {
        console.log('SEARCHRESULT_ERROR_CHANGED ' + error);
        dispatch({ type: SEARCHRESULT_ERROR_CHANGED, payload: 'Something went wrong please try again' });
      });
  };
};

export const initialgetsearchResultStateData = () => {
  return {
    type: SEARCHRESULT_INITIAL_STATE,
    payload: ''
  };
};


export const getAddfriend1 = ({ userID, toUserId, token }) => {
  console.log(userID);
  console.log(toUserId);
  console.log(token);

  var details = {
    userID,
    toUserID: toUserId
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_ADDFRIEND1
    });
    axios
      .post(`${BASE_URL}add-friend`, formBody, {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }
      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS == true) {
          console.log("Hello");
          dispatch({
            type: ADDFRIEND_SUCCESS1,
            payload: "Success Addfriend"
          });
        } else if (response.data.Message == "Already Friend") {
          dispatch({
            type: ADDFRIEND_FAILED1,
            payload: "Already Friend"
          });
        } else {
          dispatch({
            type: ADDFRIEND_FAILED1,
            payload: "User id must be numeric."
          });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: ADDFRIEND_FAILED1,
          payload: "User id must be numeric."
        });
      });
  };
};