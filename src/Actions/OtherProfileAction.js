import axios from 'axios';
import {
   BASE_URL, PROFILE_DATA, PROFILE_ERROR_CHANGED, Pofile_About_ERROR_CHANGED, Pofile_About_DATA,
   OtherProfileTag_DATA, OtherProfileTag_ERROR, OTHER_PROFILE_INITAL_STATE, OtherProfile_Loading
} from './type';

export const getOtherProfileTag = ({ userId, token, isLoading , isRefreshing}) => {
  console.log(userId);
  console.log(token);
  console.log('aaaaa');
  var details =  {

  };
  var formBody = [];

  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return (dispatch) => {
    axios.get(`${BASE_URL}tags?userID=${userId}`,{
      headers: {
        'Accept':'application/json',
        'Authorization': token,
        }
      })
      .then(response => {

        if (response.data.STATUS) {
            console.log(response.data.DATA);
            dispatch({ type: OtherProfileTag_DATA, payload: response.data.DATA });
        }else{
          dispatch({ type: OtherProfileTag_ERROR, payload: 'Something went wrong please try again' });
          console.log('not true');
        }

      })
      .catch((error) => {
        console.log('MYAbout_ERROR  '+error);
        dispatch({ type: OtherProfileTag_ERROR, payload: 'Something went wrong please try again' });
      });
    };
};

export const initialOtherProfileStateData = () => {
   return {    type: OTHER_PROFILE_INITAL_STATE,    payload: ""  };
 };

 export const getOtherProfile = ({ userId, toUserID, token }) => {
   console.log(userId);
   console.log(toUserID);
   console.log(token);
   console.log("aaaaa");
   var details = {};
   var formBody = [];
   for (var property in details) {
     var encodedKey = encodeURIComponent(property);
     var encodedValue = encodeURIComponent(details[property]);
     formBody.push(encodedKey + "=" + encodedValue);
   }
   formBody = formBody.join("&");
   return dispatch => {
     dispatch({
       type: OtherProfile_Loading
     });

     axios
       .get(`${BASE_URL}user-profile?userID=${userId}&toUserID=${toUserID}`, {
         headers: { Accept: "application/json", Authorization: token }
       })
       .then(response => {
         if (response.data.STATUS) {
           console.log(response.data.DATA);
           dispatch({ type: PROFILE_DATA, payload: response.data.DATA });
         } else {
           dispatch({
             type: PROFILE_ERROR_CHANGED,
             payload: "Something went wrong please try again"
           });
         }
       })
       .catch(error => {
         console.log("MYAbout_ERROR  " + error);
         dispatch({
           type: PROFILE_ERROR_CHANGED,
           payload: "Something went wrong please try again"
         });
       });
   };
 };

 export const getOtherProfileAbout = ({ userId, token }) => {
   console.log(userId);
   console.log(token);
   console.log("aaaaa");
   var details = {};
   var formBody = [];
   for (var property in details) {
     var encodedKey = encodeURIComponent(property);
     var encodedValue = encodeURIComponent(details[property]);
     formBody.push(encodedKey + "=" + encodedValue);
   }
   formBody = formBody.join("&");
   return dispatch => {

     axios
       .get(`${BASE_URL}aboutUser?userID=${userId}`, {
         headers: {
           Accept: "application/json",
           Authorization: token,
           "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
         }
       })
       .then(response => {
         if (response.data.STATUS) {
           console.log(response.data.DATA);
           dispatch({ type: Pofile_About_DATA, payload: response.data.DATA });
         } else {
           dispatch({
             type: Pofile_About_ERROR_CHANGED,
             payload: "Something went wrong please try again"
           });
         }
       })
       .catch(error => {
         console.log("MYAbout_ERROR  " + error);
         dispatch({
           type: Pofile_About_ERROR_CHANGED,
           payload: "Something went wrong please try again"
         });
       });
   };
 };
