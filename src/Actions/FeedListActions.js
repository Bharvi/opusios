import axios from 'axios';
import {
  FEEDLIST_DATA, BASE_URL, FEEDLIST_ERROR_CHANGED, FEEDLIST_LOADING,
  FEEDLIST_INITIAL_STATE,
  LOADING_FEED_SHAREPOST,
  SHAREPOST_FEED_DATA,
  SHAREPOST_FEED_SUCCESS,
  SHAREPOST_FEED_FAILED,
  LOADING_FEED_ABUSE,
  ABUSE_FEED_DATA,
  ABUSE_FEED_SUCCESS,
  ABUSE_FEED_FAILED,
  ON_PRESS_LIKE,
  LiKe_LOADING,


} from './type';

export const getFeedlist = ({ userId, page1, Feed_list, token, isLoading, isRefreshing }) => {
  console.log(userId);
  console.log("getFeedlist", page1);
  console.log(token);


  console.log('aaaaa');
  var details = {

  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return (dispatch) => {
    console.log('bbbbbb');
    if (page1 === 1) {
      dispatch({
        type: FEEDLIST_LOADING,
        payload: { isLoading, isRefreshing }
      });

    }

    axios.get(`${BASE_URL}feedList?userID=${userId}&pageNo=${page1}&pageCount=10`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': token,
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      }
    })
      .then(response => {
        // console.log('aavi gayu');
        console.log(response);

        if (response.data.STATUS) {
          var isData = true;
          if (response.data.DATA.Feed_list.length === 0) {
            isData = false;
          }
          // console.log(response.data.DATA.Feed_list);
          if (page1 === 1) {
            console.log("page count one show data");
            dispatch({ type: FEEDLIST_DATA, payload: { list: response.data.DATA.Feed_list, notificationCount: response.data.DATA.notificationCount, isData } });
          } else {
            // if (response.data.DATA.Feed_list.length > 0) {
            console.log("page count More0000000000");
            dispatch({ type: FEEDLIST_DATA, payload: { list: Feed_list.concat(response.data.DATA.Feed_list), notificationCount: response.data.DATA.notificationCount, isData } });
            // }
          }
        } else {
          console.log('not true');
          dispatch({ type: FEEDLIST_ERROR_CHANGED, payload: 'Something went wrong please try again' });
        }

      })
      .catch((error) => {
        console.log('FEEDLIST_ERROR  ' + error);
        dispatch({ type: FEEDLIST_ERROR_CHANGED, payload: 'Something went wrong please try again' });
      });
  };
};

export const initialgetallfeedlistStateData = () => {
  return {
    type: FEEDLIST_INITIAL_STATE,
    payload: ''
  };
};

export const getSharepost = ({ userId, postID, shareText, token }) => {
  console.log(userId);
  console.log(postID);
  console.log(token);

  var details = {
    userID: userId,
    postID: postID,
    shareText: 'test2'
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_FEED_SHAREPOST
    });
    axios
      .post(`${BASE_URL}sharePost`, formBody, {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        }
      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS == true) {
          console.log('Hello');
          dispatch({
            type: SHAREPOST_FEED_SUCCESS,
            payload: 'Success'
          });
        } else if (response.data.Message == "User id must be numeric.") {
          dispatch({
            type: SHAREPOST_FEED_FAILED,
            payload: 'User id must be numeric.'
          });
        }
        else {
          dispatch({
            type: SHAREPOST_FEED_FAILED,
            payload: 'Post id must be numeric.'
          });
        }

      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: SHAREPOST_FEED_FAILED,
          payload: 'Post id must be numeric.'
        });
      });
  };
};

export const getAbusePost = ({ userId, postID, token }) => {
  console.log(userId);
  console.log(postID);
  console.log(token);

  var details = {
    UserID: userId,
    postID: postID
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_FEED_ABUSE
    });
    axios
      .post(`${BASE_URL}abusePost`, details, {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }
      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS == true) {
          console.log("Hello");
          dispatch({
            type: ABUSE_FEED_SUCCESS,
            payload: "Success Abuse"
          });
        } else if (
          response.data.Message == "Already Post is Abuse by same User"
        ) {
          dispatch({
            type: ABUSE_FEED_FAILED,
            payload: "Already Post is Abuse by same User"
          });
        } else if (response.data.Message == "User id must be numeric.") {
          dispatch({
            type: ABUSE_FEED_FAILED,
            payload: "User id must be numeric."
          });
        } else {
          dispatch({
            type: ABUSE_FEED_FAILED,
            payload: "Post id must be numeric."
          });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: ABUSE_FEED_FAILED,
          payload: "Post id must be numeric."
        });
      });
  };
};

export const likeUnlikeApi = (userId, postID, token, isLike) => {
  console.log(userId, postID, token, isLike);

  return dispatch => {
    axios
      .get(`${BASE_URL}like-post?userID=${userId}&postID=${postID}&type=${isLike}`, {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }
      })
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });
  };
};

export const onPressLike = (item, index, feedList) => {
  let tempFeedList = feedList;
  tempFeedList[index] = {
    ...tempFeedList[index],
    is_liked: (tempFeedList[index].is_liked == "Y") ? "N" : "Y",
    like_count: (tempFeedList[index].is_liked == "Y") ? Number(tempFeedList[index].like_count) - 1 : Number(tempFeedList[index].like_count) + 1,
  };
  console.log(tempFeedList);

  return {
    type: ON_PRESS_LIKE,
    payload: tempFeedList
  }
};

// export const getLikePost = ({ userId,page1, postID, token }) => {
//   console.log(userId);
//   console.log(page1);
//   console.log(postID);

//   console.log(token);
//   console.log('aaaaa');
//   var details =  {

//   };

//   var formBody = [];

//   for (var property in details) {
//     var encodedKey = encodeURIComponent(property);
//     var encodedValue = encodeURIComponent(details[property]);
//     formBody.push(encodedKey + "=" + encodedValue);
//   }
//   formBody = formBody.join("&");

//   return (dispatch) => {
//     dispatch({
//       type: LiKe_LOADING
//     });
//     axios.get(`${BASE_URL}like-post?userID=270&postID=442&pageNo=1&pageCount=10&type=like`,{
//       headers: {
//         'Accept':'application/json',
//         'Authorization': token,
//         'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
//         }
//       })
//       .then(response => {
//         // console.log('MYAbout success'+ response);
//         console.log('aavi gayu');
//         if (response.data == '') {
//           dispatch({ type: MYAbout_ERROR_CHANGED, payload: 'Something went wrong please try again' });
//         } else {
//         if (response.data.STATUS) {
//             // response.data.DATA.gender =((response.data.DATA.gender != null) ? (response.data.DATA.gender == "0") ? 'Female' : 'Male') : 'null';
//             console.log(response.data.DATA);
//             dispatch({ type: MYAbout_DATA, payload: response.data.DATA });

//           // console.log(response.data.DATA.Feed_list);
//           // dispatch({ type: MYAbout_DATA, payload: response.data.DATA.Feed_list});
//         }else{
//           console.log('not true');
//         }
//         }
//       })
//       .catch((error) => {
//         console.log('MYAbout_ERROR  '+error);
//         dispatch({ type: MYAbout_ERROR_CHANGED, payload: 'Something went wrong please try again' });
//       });
//     };
// };
