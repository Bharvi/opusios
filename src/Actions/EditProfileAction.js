import axios from 'axios';
import {
  Alert
} from "react-native";
import {
  FULL_NAME_CHANGED,
  FULL_NAME_ERROR_CHANGED,
  EMAIL_CHANGED,
  EMAIL_ERROR_CHANGED,
  SEX_CHANGED,
  SEX_ERROR_CHANGED,
  BIRTHDATE_CHANGED,
  BIRTHDATE_ERROR_CHANGED,
  PROFESSION_CHANGED,
  PROFESSION_ERROR_CHANGED,
  LOCATION_CHANGED,
  LOCATION_ERROR_CHANGED,
  BRIEF_DESCRIPTION_CHANGED,
  BRIEF_DESCRIPTION_ERROR_CHANGED,
  EDIT_PROFILE_INITIAL_STATE,
  LOADING_EDIT, USER_EDIT_ERROR,
  SET_PROFILE_DATA,
  ADD_TAG,
  USER_TAG,
  USER_TAG_ERROR_CHANGED,
  BASE_URL,
  USER_EDIT_SUCCESS,
  REMOVE_TAG,
  GET_SEARCH_DATA,
  PROFILEIMAGEPATH,
  MYPROFILE_ERROR_CHANGED,
  MYPROFILE_DATA,
  MYAbout_DATA,
  MYAbout_ERROR_CHANGED,

} from './type';
import Moment from "moment";
import {
  deleteUser,
  updateUser
} from '../Database/allSchema';
import firebase from "react-native-firebase";

export const fullNameChangeEdit = (text) => {
  return {
    type: FULL_NAME_CHANGED,
    payload: text
  };
};

export const fullNameErrorChangeEdit = (text) => {
  return {
    type: FULL_NAME_ERROR_CHANGED,
    payload: text
  };
};

export const emailChangeEdit = (text) => {
  return {
    type: EMAIL_CHANGED,
    payload: text
  };
};

export const emailErrorChangeEdit = (text) => {
  return {
    type: EMAIL_ERROR_CHANGED,
    payload: text
  };
};

export const sexChangeEdit = (text) => {
  return {
    type: SEX_CHANGED,
    payload: text
  };
};

export const sexErrorChangeEdit = (text) => {
  return {
    type: SEX_ERROR_CHANGED,
    payload: text
  };
};

export const birthDateChangeEdit = (text) => {
  return {
    type: BIRTHDATE_CHANGED,
    payload: text
  };
};

export const bdayErrorChangeEdit = (text) => {
  return {
    type: BIRTHDATE_ERROR_CHANGED,
    payload: text
  };
};

export const professionChangeEdit = (text) => {
  return {
    type: PROFESSION_CHANGED,
    payload: text
  };
};

export const professionErrorChangeEdit = (text) => {
  return {
    type: PROFESSION_ERROR_CHANGED,
    payload: text
  };
};


export const locationChangeEdit = (text) => {
  return {
    type: LOCATION_CHANGED,
    payload: text
  };
};

export const locationErrorChangeEdit = (text) => {
  return {
    type: LOCATION_ERROR_CHANGED,
    payload: text
  };
};

export const briefDescriptionChangeEdit = (text) => {
  return {
    type: BRIEF_DESCRIPTION_CHANGED,
    payload: text
  };
};

export const briefDescriptionErrorChangeEdit = (text) => {
  return {
    type: BRIEF_DESCRIPTION_ERROR_CHANGED,
    payload: text
  };
};

// export const user_tagChangeEdit = (text) => {
// return {
//   type: USER_TAG,
//   payload: text
// };
// };
// export const user_tag_errorChangeEdit = (text) => {
// return {
//   type: USER_TAG_ERROR_CHANGED,
//   payload: text
// };
// };

export const initialEditProfileStateData = () => {
  return {
    type: EDIT_PROFILE_INITIAL_STATE,
    payload: ''
  };
};


// export const addTag = (
//   tag,
//   tagArray
// ) => {
//   if (tag === '') {
//     return {
//       type: ADD_TAG,
//       payload: {
//         array: tagArray
//       }
//     };
//   } else {
//     const temp = emailArray;
//     temp.push(email);
//     console.log(temp);
//     return {
//       type: ADD_TAG,
//       payload: {
//         array: emailArray,
//         emailError: '',
//         email: ''
//       }
//     };
//   }
// };





// export const userEdit = ({ id, fullname, email_id, gender, birth_date, profession_name, location, brief_desc, profile_picture }) => {
//   console.log(id);
//   console.log(fullname);
//   console.log(email_id);
//   console.log(gender);
//   console.log(birth_date);
//   console.log(profession_name);
//   console.log(location);
//   console.log(brief_desc);
//   console.log(profile_picture);
//
//   const formData = new FormData();
//
//
//   var details =  {
//       user_id: id,
//       fullname,
//       email: email_id,
//       gender,
//       birth_date,
//       profession: profession_name,
//       location,
//       brief_desc,
//       tags:'e',
//       profile_picture,
//   };
//
//   var formBody = [];
//   for (var property in details) {
//     var encodedKey = encodeURIComponent(property);
//     var encodedValue = encodeURIComponent(details[property]);
//     formBody.push(encodedKey + "=" + encodedValue);
//   }
//   formBody = formBody.join("&");
//
//   return (dispatch) => {
//     dispatch({
//       type: LOADING_EDIT
//     });
//     axios.post(`${BASE_URL}client?action=create&ws=editProfile`,formBody,{
//         headers: {
//             'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
//           }
//       })
//       .then(response => {
//         console.log(response);
//         if (response.data.STATUS) {
//           console.log('EditProfile Action');
//           // console.log(response.data.DATA.user_data);
//           saveUser(id, response.data.DATA.user_data, dispatch)
//           console.log(saveUser);
//           console.log(dispatch);
//           // console.log(response.data.data);
//           // console.log(saveUser);
//         } else {
//           dispatch({
//             type: USER_EDIT_ERROR,
//             payload: 'User not found.'
//           });
//         }
//       })
//       .catch(error => {
//         console.log(error);
//         dispatch({
//           type: USER_EDIT_ERROR,
//           payload: 'User not found.'
//         });
//       });
//   };
// };

export const userEdit = ({ id, fullname, email_id, gender, birth_date, profession_name, location, brief_desc, profile_picture, myNavigation, token }) => {
  console.log(id);
  console.log(fullname);
  console.log(email_id);
  console.log(gender);
  console.log(birth_date);
  console.log(profession_name);
  console.log(location);
  // console.log(brief_desc);
  console.log(profile_picture);
  console.log(token);

  const formData = new FormData();

  if (profile_picture) {
    formData.append('profile_picture', {
      uri: profile_picture,
      type: 'image/jpeg',
      name: fullname + '.jpeg',
    });
  }
  formData.append('user_id', id);
  formData.append('fullname', fullname);
  formData.append('email', email_id);
  formData.append('user_id', id);
  formData.append('gender', gender);
  formData.append('birth_date', birth_date);
  formData.append('profession', profession_name);
  formData.append('location', location);
  formData.append('brief_desc', (brief_desc) ? brief_desc : '');


  // var details =  {
  //     user_id: id,
  //     fullname,
  //     email: email_id,
  //     gender,
  //     birth_date,
  //     profession: profession_name,
  //     location,
  //     brief_desc,
  //     tags:'e',
  //     profile_picture,
  // };
  //
  // var formBody = [];
  // for (var property in formData) {
  //   var encodedKey = encodeURIComponent(property);
  //   var encodedValue = encodeURIComponent(formData[property]);
  //   formBody.push(encodedKey + "=" + encodedValue);
  // }
  // formBody = formBody.join("&");

  return (dispatch) => {
    dispatch({
      type: LOADING_EDIT
    });
    axios.post(`${BASE_URL}edit-profile`,
      formData,
      {
        headers: {
          'Accept': 'multipart/form-data',
          'Authorization': token,
          'Content-Type': 'multipart/form-data',
        }
      }
    )
      .then(response => {
        console.log(response);
        if (response.data.STATUS) {
          //   console.log('EditProfile Action');
          //   // console.log(response.data.DATA.user_data);
          // saveUser(id, response.data.DATA.user_data, dispatch);
          try {
            saveUser(response.data.DATA.user_data, myNavigation, dispatch, token);
            getAllTag(token, dispatch);
          } catch (error) {

          }

          //   console.log(saveUser);
          // } else {
          //   dispatch({
          //     type: USER_EDIT_ERROR,
          //     payload: 'User not found.'
          //   });
        }
      })
      .catch(error => {
        console.log(error.response);
        // saveUser(id, fullname, email_id, gender, birth_date, profession_name, location, brief_desc, profile_picture, myNavigation,  dispatch);
      });
  };
};

const saveUser = (userData, myNavigation, dispatch, token) => {

  let gender = 'null';
  if (userData.gender == "0") {
    gender = 'Female'
  } else if (userData.gender == "1") {
    gender = 'Male'
  }

  const newUser = {
    id: (userData.user_id == null) ? 1 : Number(userData.user_id),
    fullname: (userData.fullname == null) ? '' : userData.fullname,
    email_id: (userData.email_id == null) ? '' : userData.email_id,
    user_photo: PROFILEIMAGEPATH + userData.user_photo,
    // gender: (userData.gender == "0") ? 'Female' : 'Male',
    gender: gender,
    profession_name: (userData.profession == null) ? '' : userData.profession,
    location: (userData.location == null) ? '' : userData.location,
    brief_desc: (userData.brief_desc) ? userData.brief_desc : '',
    fire_id: (userData.firebase == null) ? '' : userData.firebase,
    is_notify: (userData.is_notify == null) ? '' : userData.is_notify,
    birth_date: (userData.birth_date == null) ? '' : userData.birth_date,
    user_tag: '',


    // id:(id == null) ? 1 : Number(id),
    // fullname: (fullname== null) ? '' : fullname,
    // email_id: (email_id == null ) ? '' : email_id,
    // user_photo: (profile_picture == null) ? '' : profile_picture,
    // gender: (gender == "0") ? 'Female' : 'Male',
    // profession_name: (profession_name == null) ? '' : profession_name,
    // location: (location == null) ? '' : location,
    // brief_desc: (brief_desc == null) ? '' : brief_desc,
    // birth_date: (birth_date == null) ? '' : birth_date ,
    // user_tag: '',

  };
  // console.log(newUser);
  try {
    updateUser(newUser).then(() => {
      console.log('Insert Suceess');
      firebase.database().ref()
        .child("friends/" + userData.firebase)
        .update({
          user_id: userData.user_id,
          email: userData.social_id + '@opus.com',
          uid: userData.firebase,
          name: userData.fullname,
          token: userData.device_token,
          profile_picture: userData.user_photo
        });
      setTimeout(() => {
        Alert.alert(
          'Success',
          'Profile update successfully.',
          [
            {
              text: 'OK', onPress: () => getMyProfile({
                userId: userData.user_id,
                token,
                dispatch,
                myNavigation
              })
            },
          ],
          { cancelable: false }
        )

      }, 500);


      dispatch({
        type: USER_EDIT_SUCCESS,
        payload: 'record update successfully'
      });
    }).catch((error) => {
      console.log(`User Insert error${error}`);
      dispatch({
        type: USER_EDIT_ERROR,
        payload: 'record update error'
      });
    });
  } catch (error) {

  }

};

export const setProfileData = (user) => {
  return {
    type: SET_PROFILE_DATA,
    payload: {
      fullname: user.fullname,
      email_id: user.email_id,
      user_photo: user.user_photo,
      gender: user.gender,
      profession_name: user.profession_name,
      location: user.location,
      brief_desc: (user.brief_desc) ? user.brief_desc : '',
      birth_date: user.birth_date,
      // tagArray: (user.user_tag).split(','),
    }
  };
};

export const getMyProfile = ({ userId, toUserID, token, dispatch, myNavigation }) => {
  console.log(userId);
  console.log(toUserID);
  console.log(token);
  console.log("aaaaa");
  var details = {};
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");
  axios
    .get(`${BASE_URL}user-profile?userID=${userId}&toUserID=${toUserID}`, {
      headers: { Accept: "application/json", Authorization: token }
    })
    .then(response => {
      if (response.data.STATUS) {
        console.log(response.data.DATA);
        getMYAboutData(userId, token, dispatch)
        myNavigation('MyProfile');
        dispatch({ type: MYPROFILE_DATA, payload: response.data.DATA });
      } else {
        myNavigation('MyProfile');
        dispatch({
          type: MYPROFILE_ERROR_CHANGED,
          payload: "Something went wrong please try again"
        });
      }
    })
    .catch(error => {
      myNavigation('MyProfile')
      console.log("MYAbout_ERROR  " + error);
      dispatch({
        type: MYPROFILE_ERROR_CHANGED,
        payload: "Something went wrong please try again"
      });
    });
};
// export const addTag = (
//   tag,
//   tagArray
// ) => {
//   if (tag === '') {
//     return {
//       type: ADD_TAG,
//       payload: {
//         tagArray: tagArray,
//         user_tagError: 'Enter tag',
//         user_tag: tag
//       }
//     };
//   } else {
//     const temp = tagArray;
//     temp.push(tag);
//     console.log(temp);
//     return {
//       type: ADD_TAG,
//       payload: {
//         tagArray: tagArray,
//         user_tagError: '',
//         user_tag: ''
//       }
//     };
//   }
// };
//
// export const removeTag = (
//   tagArray,
//   index
// ) => {
//   const temp = [...tagArray];
//   temp.splice(index, 1);
//   console.log(temp);
//   return {
//     type: REMOVE_TAG,
//     payload: temp
//   };
// };

const getAllTag = (token, dispatch) => {
  console.log("in alltags method..");
  axios
    .get(`${BASE_URL}alltags`, {
      headers: {
        Accept: "application/json",
        Authorization: token
      }
    })
    .then(response => {
      console.log("getAllTag responce", response.data.DATA);

      let tempTags = [];
      let tempProfession = [];
      response.data.DATA.tags.map(item => {
        tempTags.push(item.name);
      });
      response.data.DATA.professions.map(item => {
        tempProfession.push(item.name);
      });
      dispatch({
        type: GET_SEARCH_DATA,
        payload: tempTags.concat(
          response.data.DATA.users,
          tempProfession,
          response.data.DATA.locations
        )
      });
    })
    .catch(error => {
      console.log("error in alltags", error);
    });
};

export const getMYAboutData = (userId, token, dispatch) => {
  console.log(userId);
  console.log(token);
  console.log("aaaaa");
  var details = {};
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");
  axios
    .get(`${BASE_URL}aboutUser?userID=${userId}`, {
      headers: {
        Accept: "application/json",
        Authorization: token,
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
      }
    })
    .then(response => {
      if (response.data.STATUS) {
        console.log(response.data.DATA);
        dispatch({ type: MYAbout_DATA, payload: response.data.DATA });
      } else {
        dispatch({
          type: MYAbout_ERROR_CHANGED,
          payload: "Something went wrong please try again"
        });
      }
    })
    .catch(error => {
      console.log("MYAbout_ERROR  " + error);
      dispatch({
        type: MYAbout_ERROR_CHANGED,
        payload: "Something went wrong please try again"
      });
    });
};
