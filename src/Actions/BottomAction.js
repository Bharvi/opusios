import { ON_CHANGE_BOTTOM_TAB
  } from './type';

  export const onBottomTabChange = ({
    prop,
    value
  }) => {
    return {
      type: ON_CHANGE_BOTTOM_TAB,
      payload: {
        prop,
        value
      }
    };
  };
