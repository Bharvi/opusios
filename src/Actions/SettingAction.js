
import axios from "axios";
import {
  Alert
} from "react-native";
import {
  BASE_URL,
  LOADING_LOGOUT,
  LOGOUT_DATA,
  LOGOUT_SUCCESS,
  LOGOUT_FAILED,

  LOADING_NOTIFICATIONSETING,
  NOTIFICATIONSETING_DATA,
  NOTIFICATIONSETING_SUCCESS,
  NOTIFICATIONSETING_SUCCESS1,
  NOTIFICATIONSETING_FAILED,
  POSTIMAGEPATH,
  PROFILEIMAGEPATH
} from "./type";
import { insertUser, deleteUser, updateUser } from '../Database/allSchema';



export const getLogout = ({ userID, token }) => {
  console.log(userID);
  console.log(token);

  var details = {
    userID
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_LOGOUT
    });
    axios
      .post(`${BASE_URL}logout`, formBody, {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        }
      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS == true) {
          console.log('Hello');
          dispatch({
            type: LOGOUT_SUCCESS,
            payload: 'Success'
          });
        }
        else {
          dispatch({
            type: LOGOUT_FAILED,
            payload: 'User id must be numeric.'
          });
        }

      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: LOGOUT_FAILED,
          payload: 'User id must be numeric.'
        });
      });
  };
};

export const getNotificationSeting = ({ userID, is_notify, token }) => {
  console.log(userID);
  console.log(is_notify);
  console.log(token);

  var details = {
    userID,
    is_notify
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_NOTIFICATIONSETING
    });
    axios
      .post(`${BASE_URL}notification-setting`, details, {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }
      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS == true) {
          if (response.data.DATA.user_data.is_notify == "Y") {
            console.log('Yes');
            saveUser(response.data.DATA.user_data, dispatch);
            // Alert.alert(
            //   'Success',
            //   'Notification on',
            //   [
            //     { text: 'OK', onPress: () =>{ cancelable: false } },
            //   ],
            //   { cancelable: false }
            // )
            dispatch({
              type: NOTIFICATIONSETING_SUCCESS,
              payload: 'Success'
            });

          }
          else {
            console.log('No');
            saveUserOff(response.data.DATA.user_data, dispatch);
            // Alert.alert(
            //   'Success',
            //   'Notification off',
            //   [
            //     { text: 'OK', onPress: () => { cancelable: false } },
            //   ],
            //   { cancelable: false }
            // )
            dispatch({
              type: NOTIFICATIONSETING_SUCCESS1,
              payload: 'Success'
            });
          }

        }
        else {
          dispatch({
            type: NOTIFICATIONSETING_FAILED,
            payload: 'User id must be numeric.'
          });
        }

      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: NOTIFICATIONSETING_FAILED,
          payload: 'User id must be numeric.'
        });
      });
  };
};

// const saveUser = (userData,dispatch) => {
//     // console.log(userData,'firenew');
//     deleteUser().then(() => {
//         const newUser = {
//           id:(userData.user_id == null) ? 1 : Number(userData.user_id),    // primary key
//           fullname: (userData.fullname == null) ? '' : userData.fullname,
//           email_id: (userData.email_id == null) ? '' : userData.email_id,
//           user_password: (userData.user_password == null) ? '' : userData.user_password,
//           device_type: (userData.device_type == null) ? '' : userData.device_type,
//
//           // user_photo:"http://www.opus.projectdemo.website/public/uploads/images/"+ userData.user_photo,
//           user_photo:`${POSTIMAGEPATH}${userData.user_photo}`,
//           // user_photo:(userData.user_photo == null) ? '' : userData.user_photo,
//           device_token: (userData.device_token == null) ? '' : userData.device_token,
//           gender: (userData.gender == "0") ? 'Female' : 'Male',
//           profession_name:(userData.profession == null) ? '' : userData.profession,
//           location: (userData.location == null) ? '' : userData.location,
//           brief_desc: (userData.brief_desc == null) ? '' : userData.brief_desc,
//           birth_date: (userData.birth_date == null) ? '' : userData.birth_date,
//           fire_id: (userData.firebase == null) ? '' : userData.firebase,
//           is_notify: (userData.is_notify == null) ? '' : userData.is_notify,
//           user_tag: (userData.user_tag == null) ? '' : userData.user_tag,
//         };
//         console.log(newUser)
//         insertUser(newUser).then(() => {
//           console.log('Insert Suceess');
//           console.log(newUser);
//           // Alert.alert(
//           //   'Success',
//           //   'Notification on',
//           //   [
//           //     { text: 'OK', onPress: () =>{ cancelable: false } },
//           //   ],
//           //   { cancelable: false }
//           // )
//           dispatch({
//             type: NOTIFICATIONSETING_SUCCESS,
//             payload: 'Success'
//           });
//         }).catch((error) => {
//           console.log(`User Insert error${error}`);
//           dispatch({
//             type: NOTIFICATIONSETING_FAILED,
//             payload: 'User id must be numeric.'
//           });
//         });
//     }).catch((error) => {
//       console.log(error);
//       dispatch({
//         type: NOTIFICATIONSETING_FAILED,
//         payload: 'User id must be numeric.'
//       });
//     });
// };
const saveUserOff = (userData, dispatch) => {
  // // console.log(userData,'firenew');
  // deleteUser().then(() => {
  //     const newUser = {
  //       id:(userData.user_id == null) ? 1 : Number(userData.user_id),    // primary key
  //       fullname: (userData.fullname == null) ? '' : userData.fullname,
  //       email_id: (userData.email_id == null) ? '' : userData.email_id,
  //       user_password: (userData.user_password == null) ? '' : userData.user_password,
  //       device_type: (userData.device_type == null) ? '' : userData.device_type,
  //
  //       // user_photo:"http://www.opus.projectdemo.website/public/uploads/images/"+ userData.user_photo,
  //       user_photo:`${POSTIMAGEPATH}${userData.user_photo}`,
  //       // user_photo:(userData.user_photo == null) ? '' : userData.user_photo,
  //       device_token: (userData.device_token == null) ? '' : userData.device_token,
  //       gender: (userData.gender == "0") ? 'Female' : 'Male',
  //       profession_name:(userData.profession == null) ? '' : userData.profession,
  //       location: (userData.location == null) ? '' : userData.location,
  //       brief_desc: (userData.brief_desc == null) ? '' : userData.brief_desc,
  //       birth_date: (userData.birth_date == null) ? '' : userData.birth_date,
  //       fire_id: (userData.firebase == null) ? '' : userData.firebase,
  //       is_notify: (userData.is_notify == null) ? '' : userData.is_notify,
  //       user_tag: (userData.user_tag == null) ? '' : userData.user_tag,
  //     };
  //     console.log(newUser)
  //     insertUser(newUser).then(() => {
  //       console.log('Insert Suceess');
  //       console.log(newUser);
  //       // Alert.alert(
  //       //   'Success',
  //       //   'Notification off',
  //       //   [
  //       //     { text: 'OK', onPress: () => { cancelable: false } },
  //       //   ],
  //       //   { cancelable: false }
  //       // )
  //       dispatch({
  //         type: NOTIFICATIONSETING_SUCCESS1,
  //         payload: 'Success'
  //       });
  //     }).catch((error) => {
  //       console.log(`User Insert error${error}`);
  //       dispatch({
  //         type: NOTIFICATIONSETING_FAILED,
  //         payload: 'User id must be numeric.'
  //       });
  //     });
  // }).catch((error) => {
  //   console.log(error);
  //   dispatch({
  //     type: NOTIFICATIONSETING_FAILED,
  //     payload: 'User id must be numeric.'
  //   });
  // });

  const newUser = {
    id: (userData.user_id == null) ? 1 : Number(userData.user_id),
    fullname: (userData.fullname == null) ? '' : userData.fullname,
    email_id: (userData.email_id == null) ? '' : userData.email_id,
    user_photo: PROFILEIMAGEPATH + userData.user_photo,
    gender: (userData.gender == "0") ? 'Female' : 'Male',
    profession_name: (userData.profession == null) ? '' : userData.profession,
    location: (userData.location == null) ? '' : userData.location,
    brief_desc: (userData.brief_desc == null) ? '' : userData.brief_desc,
    fire_id: (userData.firebase == null) ? '' : userData.firebase,
    is_notify: (userData.is_notify == null) ? '' : userData.is_notify,
    birth_date: (userData.birth_date == null) ? '' : userData.birth_date,
    user_tag: '',


    // id:(id == null) ? 1 : Number(id),
    // fullname: (fullname== null) ? '' : fullname,
    // email_id: (email_id == null ) ? '' : email_id,
    // user_photo: (profile_picture == null) ? '' : profile_picture,
    // gender: (gender == "0") ? 'Female' : 'Male',
    // profession_name: (profession_name == null) ? '' : profession_name,
    // location: (location == null) ? '' : location,
    // brief_desc: (brief_desc == null) ? '' : brief_desc,
    // birth_date: (birth_date == null) ? '' : birth_date ,
    // user_tag: '',

  };
  updateUser(newUser).then(() => {
    console.log('Update Suceess');
    // Alert.alert(
    //   'Success',
    //   'Profile update successfully.',
    //   [
    //     { text: 'OK', onPress: () => myNavigation('MyProfile') },
    //   ],
    //   { cancelable: false }
    // )

    dispatch({
      type: NOTIFICATIONSETING_SUCCESS1,
      payload: 'Success'
    });
  }).catch((error) => {
    console.log(`Update error${error}`);
    dispatch({
      type: NOTIFICATIONSETING_FAILED,
      payload: 'User id must be numeric.'
    });
  });
};


const saveUser = (userData, dispatch) => {
  const newUser = {
    id: (userData.user_id == null) ? 1 : Number(userData.user_id),
    fullname: (userData.fullname == null) ? '' : userData.fullname,
    email_id: (userData.email_id == null) ? '' : userData.email_id,
    user_photo: PROFILEIMAGEPATH + userData.user_photo,
    gender: (userData.gender == "0") ? 'Female' : 'Male',
    profession_name: (userData.profession == null) ? '' : userData.profession,
    location: (userData.location == null) ? '' : userData.location,
    brief_desc: (userData.brief_desc == null) ? '' : userData.brief_desc,
    fire_id: (userData.firebase == null) ? '' : userData.firebase,
    is_notify: (userData.is_notify == null) ? '' : userData.is_notify,
    birth_date: (userData.birth_date == null) ? '' : userData.birth_date,
    user_tag: '',


    // id:(id == null) ? 1 : Number(id),
    // fullname: (fullname== null) ? '' : fullname,
    // email_id: (email_id == null ) ? '' : email_id,
    // user_photo: (profile_picture == null) ? '' : profile_picture,
    // gender: (gender == "0") ? 'Female' : 'Male',
    // profession_name: (profession_name == null) ? '' : profession_name,
    // location: (location == null) ? '' : location,
    // brief_desc: (brief_desc == null) ? '' : brief_desc,
    // birth_date: (birth_date == null) ? '' : birth_date ,
    // user_tag: '',

  };
  updateUser(newUser).then(() => {
    console.log('Update Suceess');
    // Alert.alert(
    //   'Success',
    //   'Profile update successfully.',
    //   [
    //     { text: 'OK', onPress: () => myNavigation('MyProfile') },
    //   ],
    //   { cancelable: false }
    // )

    dispatch({
      type: NOTIFICATIONSETING_SUCCESS,
      payload: 'Success'
    });
  }).catch((error) => {
    console.log(`Update error${error}`);
    dispatch({
      type: NOTIFICATIONSETING_FAILED,
      payload: 'User id must be numeric.'
    });
  });
};



// import axios from "axios";
// import {
//   Alert
// } from "react-native";
// import {
//   BASE_URL,
//   LOADING_LOGOUT,
//   LOGOUT_DATA,
//   LOGOUT_SUCCESS,
//   LOGOUT_FAILED,

//   LOADING_NOTIFICATIONSETING,
//   NOTIFICATIONSETING_DATA,
//   NOTIFICATIONSETING_SUCCESS,
//   NOTIFICATIONSETING_SUCCESS1,
//   NOTIFICATIONSETING_FAILED,
//   PROFILEIMAGEPATH

// } from "./type";
// import { insertUser, deleteUser, updateUser } from '../Database/allSchema';



// export const getLogout = ({ userID, token }) => {
//   console.log(userID);
//   console.log(token);

//   var details = {
//     userID
//   };

//   var formBody = [];
//   for (var property in details) {
//     var encodedKey = encodeURIComponent(property);
//     var encodedValue = encodeURIComponent(details[property]);
//     formBody.push(encodedKey + "=" + encodedValue);
//   }
//   formBody = formBody.join("&");

//   return dispatch => {
//     dispatch({
//       type: LOADING_LOGOUT
//     });
//     axios
//       .post(`${BASE_URL}logout`, formBody, {
//         headers: {
//           'Accept': 'application/json',
//           'Authorization': token,
//           'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
//         }
//       })
//       .then(response => {
//         console.log(response);
//         if (response.data.STATUS == true) {
//           console.log('Hello');
//           dispatch({
//             type: LOGOUT_SUCCESS,
//             payload: 'Success'
//           });
//         }
//         else {
//           dispatch({
//             type: LOGOUT_FAILED,
//             payload: 'User id must be numeric.'
//           });
//         }

//       })
//       .catch(error => {
//         console.log(error);
//         dispatch({
//           type: LOGOUT_FAILED,
//           payload: 'User id must be numeric.'
//         });
//       });
//   };
// };

// export const getNotificationSeting = ({ userID, is_notify, token }) => {
//   console.log(userID);
//   console.log(is_notify);
//   console.log(token);

//   var details = {
//     userID,
//     is_notify
//   };

//   var formBody = [];
//   for (var property in details) {
//     var encodedKey = encodeURIComponent(property);
//     var encodedValue = encodeURIComponent(details[property]);
//     formBody.push(encodedKey + "=" + encodedValue);
//   }
//   formBody = formBody.join("&");

//   return dispatch => {
//     dispatch({
//       type: LOADING_NOTIFICATIONSETING
//     });
//     axios
//       .post(`${BASE_URL}notification-setting`, details, {
//         headers: {
//           'Accept': 'application/json',
//           'Authorization': token,
//         }
//       })
//       .then(response => {
//         console.log(response);
//         if (response.data.STATUS == true) {
//           if (response.data.DATA.user_data.is_notify == "Y") {
//             console.log('Yes');
//             saveUser(response.data.DATA.user_data, dispatch);

//             // Alert.alert(
//             //   'Success',
//             //   'Notification on',
//             //   [
//             //     { text: 'OK', onPress: () =>{ cancelable: false } },
//             //   ],
//             //   { cancelable: false }
//             // )
//             dispatch({
//               type: NOTIFICATIONSETING_SUCCESS,
//               payload: 'Success'
//             });

//           }
//           else {
//             console.log('No');
//             saveUserOff(response.data.DATA.user_data, dispatch);
//             // Alert.alert(
//             //   'Success',
//             //   'Notification off',
//             //   [
//             //     { text: 'OK', onPress: () => { cancelable: false } },
//             //   ],
//             //   { cancelable: false }
//             // )
//             dispatch({
//               type: NOTIFICATIONSETING_SUCCESS1,
//               payload: 'Success'
//             });
//           }

//         }
//         else {
//           dispatch({
//             type: NOTIFICATIONSETING_FAILED,
//             payload: 'User id must be numeric.'
//           });
//         }

//       })
//       .catch(error => {
//         console.log(error);
//         dispatch({
//           type: NOTIFICATIONSETING_FAILED,
//           payload: 'User id must be numeric.'
//         });
//       });
//   };
// };

// // const saveUser = (userData,dispatch) => {
// //     // console.log(userData,'firenew');
// //     deleteUser().then(() => {
// //         const newUser = {
// //           id:(userData.user_id == null) ? 1 : Number(userData.user_id),    // primary key
// //           fullname: (userData.fullname == null) ? '' : userData.fullname,
// //           email_id: (userData.email_id == null) ? '' : userData.email_id,
// //           user_password: (userData.user_password == null) ? '' : userData.user_password,
// //           device_type: (userData.device_type == null) ? '' : userData.device_type,
// //
// //           // user_photo:"http://www.opus.projectdemo.website/public/uploads/images/"+ userData.user_photo,
// //           user_photo:`${POSTIMAGEPATH}${userData.user_photo}`,
// //           // user_photo:(userData.user_photo == null) ? '' : userData.user_photo,
// //           device_token: (userData.device_token == null) ? '' : userData.device_token,
// //           gender: (userData.gender == "0") ? 'Female' : 'Male',
// //           profession_name:(userData.profession == null) ? '' : userData.profession,
// //           location: (userData.location == null) ? '' : userData.location,
// //           brief_desc: (userData.brief_desc == null) ? '' : userData.brief_desc,
// //           birth_date: (userData.birth_date == null) ? '' : userData.birth_date,
// //           fire_id: (userData.firebase == null) ? '' : userData.firebase,
// //           is_notify: (userData.is_notify == null) ? '' : userData.is_notify,
// //           user_tag: (userData.user_tag == null) ? '' : userData.user_tag,
// //         };
// //         console.log(newUser)
// //         insertUser(newUser).then(() => {
// //           console.log('Insert Suceess');
// //           console.log(newUser);
// //           // Alert.alert(
// //           //   'Success',
// //           //   'Notification on',
// //           //   [
// //           //     { text: 'OK', onPress: () =>{ cancelable: false } },
// //           //   ],
// //           //   { cancelable: false }
// //           // )
// //           dispatch({
// //             type: NOTIFICATIONSETING_SUCCESS,
// //             payload: 'Success'
// //           });
// //         }).catch((error) => {
// //           console.log(`User Insert error${error}`);
// //           dispatch({
// //             type: NOTIFICATIONSETING_FAILED,
// //             payload: 'User id must be numeric.'
// //           });
// //         });
// //     }).catch((error) => {
// //       console.log(error);
// //       dispatch({
// //         type: NOTIFICATIONSETING_FAILED,
// //         payload: 'User id must be numeric.'
// //       });
// //     });
// // };
// const saveUserOff = (userData, dispatch) => {
//   // // console.log(userData,'firenew');
//   // deleteUser().then(() => {
//   //     const newUser = {
//   //       id:(userData.user_id == null) ? 1 : Number(userData.user_id),    // primary key
//   //       fullname: (userData.fullname == null) ? '' : userData.fullname,
//   //       email_id: (userData.email_id == null) ? '' : userData.email_id,
//   //       user_password: (userData.user_password == null) ? '' : userData.user_password,
//   //       device_type: (userData.device_type == null) ? '' : userData.device_type,
//   //
//   //       // user_photo:"http://www.opus.projectdemo.website/public/uploads/images/"+ userData.user_photo,
//   //       user_photo:`${POSTIMAGEPATH}${userData.user_photo}`,
//   //       // user_photo:(userData.user_photo == null) ? '' : userData.user_photo,
//   //       device_token: (userData.device_token == null) ? '' : userData.device_token,
//   //       gender: (userData.gender == "0") ? 'Female' : 'Male',
//   //       profession_name:(userData.profession == null) ? '' : userData.profession,
//   //       location: (userData.location == null) ? '' : userData.location,
//   //       brief_desc: (userData.brief_desc == null) ? '' : userData.brief_desc,
//   //       birth_date: (userData.birth_date == null) ? '' : userData.birth_date,
//   //       fire_id: (userData.firebase == null) ? '' : userData.firebase,
//   //       is_notify: (userData.is_notify == null) ? '' : userData.is_notify,
//   //       user_tag: (userData.user_tag == null) ? '' : userData.user_tag,
//   //     };
//   //     console.log(newUser)
//   //     insertUser(newUser).then(() => {
//   //       console.log('Insert Suceess');
//   //       console.log(newUser);
//   //       // Alert.alert(
//   //       //   'Success',
//   //       //   'Notification off',
//   //       //   [
//   //       //     { text: 'OK', onPress: () => { cancelable: false } },
//   //       //   ],
//   //       //   { cancelable: false }
//   //       // )
//   //       dispatch({
//   //         type: NOTIFICATIONSETING_SUCCESS1,
//   //         payload: 'Success'
//   //       });
//   //     }).catch((error) => {
//   //       console.log(`User Insert error${error}`);
//   //       dispatch({
//   //         type: NOTIFICATIONSETING_FAILED,
//   //         payload: 'User id must be numeric.'
//   //       });
//   //     });
//   // }).catch((error) => {
//   //   console.log(error);
//   //   dispatch({
//   //     type: NOTIFICATIONSETING_FAILED,
//   //     payload: 'User id must be numeric.'
//   //   });
//   // });

//   const newUser = {
//     id: (userData.user_id == null) ? 1 : Number(userData.user_id),
//     fullname: (userData.fullname == null) ? '' : userData.fullname,
//     email_id: (userData.email_id == null) ? '' : userData.email_id,
//     user_photo: PROFILEuserData.user_photo,
//     gender: (userData.gender == "0") ? 'Female' : 'Male',
//     profession_name: (userData.profession == null) ? '' : userData.profession,
//     location: (userData.location == null) ? '' : userData.location,
//     brief_desc: (userData.brief_desc == null) ? '' : userData.brief_desc,
//     fire_id: (userData.firebase == null) ? '' : userData.firebase,
//     is_notify: (userData.is_notify == null) ? '' : userData.is_notify,
//     birth_date: (userData.birth_date == null) ? '' : userData.birth_date,
//     user_tag: '',


//     // id:(id == null) ? 1 : Number(id),
//     // fullname: (fullname== null) ? '' : fullname,
//     // email_id: (email_id == null ) ? '' : email_id,
//     // user_photo: (profile_picture == null) ? '' : profile_picture,
//     // gender: (gender == "0") ? 'Female' : 'Male',
//     // profession_name: (profession_name == null) ? '' : profession_name,
//     // location: (location == null) ? '' : location,
//     // brief_desc: (brief_desc == null) ? '' : brief_desc,
//     // birth_date: (birth_date == null) ? '' : birth_date ,
//     // user_tag: '',

//   };
//   updateUser(newUser).then(() => {
//     console.log('Update Suceess');
//     // Alert.alert(
//     //   'Success',
//     //   'Profile update successfully.',
//     //   [
//     //     { text: 'OK', onPress: () => myNavigation('MyProfile') },
//     //   ],
//     //   { cancelable: false }
//     // )

//     dispatch({
//       type: NOTIFICATIONSETING_SUCCESS1,
//       payload: 'Success'
//     });
//   }).catch((error) => {
//     console.log(`Update error${error}`);
//     dispatch({
//       type: NOTIFICATIONSETING_FAILED,
//       payload: 'User id must be numeric.'
//     });
//   });
// };


// const saveUser = (userData, dispatch) => {
//   const newUser = {
//     id: (userData.user_id == null) ? 1 : Number(userData.user_id),
//     fullname: (userData.fullname == null) ? '' : userData.fullname,
//     email_id: (userData.email_id == null) ? '' : userData.email_id,
//     user_photo: PROFILEIMAGEPATH + userData.user_photo,
//     gender: (userData.gender == "0") ? 'Female' : 'Male',
//     profession_name: (userData.profession == null) ? '' : userData.profession,
//     location: (userData.location == null) ? '' : userData.location,
//     brief_desc: (userData.brief_desc == null) ? '' : userData.brief_desc,
//     fire_id: (userData.firebase == null) ? '' : userData.firebase,
//     is_notify: (userData.is_notify == null) ? '' : userData.is_notify,
//     birth_date: (userData.birth_date == null) ? '' : userData.birth_date,
//     user_tag: '',


//     // id:(id == null) ? 1 : Number(id),
//     // fullname: (fullname== null) ? '' : fullname,
//     // email_id: (email_id == null ) ? '' : email_id,
//     // user_photo: (profile_picture == null) ? '' : profile_picture,
//     // gender: (gender == "0") ? 'Female' : 'Male',
//     // profession_name: (profession_name == null) ? '' : profession_name,
//     // location: (location == null) ? '' : location,
//     // brief_desc: (brief_desc == null) ? '' : brief_desc,
//     // birth_date: (birth_date == null) ? '' : birth_date ,
//     // user_tag: '',

//   };
//   updateUser(newUser).then(() => {
//     console.log('Update Suceess');
//     // Alert.alert(
//     //   'Success',
//     //   'Profile update successfully.',
//     //   [
//     //     { text: 'OK', onPress: () => myNavigation('MyProfile') },
//     //   ],
//     //   { cancelable: false }
//     // )

//     dispatch({
//       type: NOTIFICATIONSETING_SUCCESS,
//       payload: 'Success'
//     });
//   }).catch((error) => {
//     console.log(`Update error${error}`);
//     dispatch({
//       type: NOTIFICATIONSETING_FAILED,
//       payload: 'User id must be numeric.'
//     });
//   });
// };
