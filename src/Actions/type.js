// export const MAIN_URL = 'https://api.khesed.com';
export const MAIN_URL = 'https://api.opuspenpal.com';

export const BASE_URL = MAIN_URL + '/api/';
export const AUTH_URL = MAIN_URL + '/oauth/token';
export const POSTIMAGEPATH = MAIN_URL + '/uploads/images/img_thumbs/';
export const PROFILEIMAGEPATH = MAIN_URL + '/uploads/images/img_thumbs/';

//Login page
export const EMAIL_CHANGED = 'email_change';
export const EMAIL_ERROR_CHANGED = 'email_error_change';
export const PASSWORD_CHANGED = 'password_change';
export const PASSWORD_ERROR_CHANGED = 'password_error_change';
export const LOADING_LOGIN = 'loading_login';
export const LOGIN_FAILED = 'loading_failed';
export const LOGIN_SUCCESS = 'login_success';

//Forgot type
export const EMAIL_FORGOT_PASS_CHANGED = 'email_forgot_pass_change';
export const EMAIL_FORGOT_PASS_ERROR_CHANGED = 'email_forgot_pass_error_change';
export const FORGOT_PASS_LOADING = 'forgot_pass_loading';
export const FORGOT_PASS_DATA = 'forgot_pass_data';
export const FORGOT_PASS_FAILD = 'forgot_pass_faild';
export const USER_FORGOTPASSWORD = 'user_forgotpassword';
export const FORGOT_PASS_INITIAL_STATE = 'forgot_pass_initial_state';
export const FORGOT_PASS_SUCCESS = 'forgot_pass_success';

//SingUp
export const SIGNUP_LOADING = 'signup_loading';
export const SIGUP_FAILED = 'signup_failed';
export const SIGUP_SUCCESS = 'signup_success';
export const FULL_NAME_CHANGED = 'full_name_changed';
export const FULL_NAME_ERROR_CHANGED = 'full_name_error_change';
export const CONFIRM_PASSWORD_CHANGED = 'confirm_password_changed';
export const CONFIRM_PASSWORD_ERROR_CHANGED = 'confirm_password_error_changed';
export const SIGNUP_INITIAL_STATE = 'sign_up_state';
export const LOGIN_INITIAL_STATE = 'login_initial_state';
export const EDIT_PROFILE_INITIAL_STATE = 'edit_profile_initial_state';

//EDitProfile
export const EDIT_PROFILE_LOADING = 'edit_profile_loading';
export const SEX_CHANGED = 'sex_changed';
export const SEX_ERROR_CHANGED = 'sex_error_changed';
export const BIRTHDATE_CHANGED = 'birthdate_changed';
export const BIRTHDATE_ERROR_CHANGED = 'birthdate_error_changed';
export const PROFESSION_CHANGED = 'profession_changed';
export const PROFESSION_ERROR_CHANGED = 'profession_error_changed';
export const LOCATION_CHANGED = 'location_changed';
export const LOCATION_ERROR_CHANGED = 'location_error_changed';
export const BRIEF_DESCRIPTION_CHANGED = 'brief_description_changed';
export const BRIEF_DESCRIPTION_ERROR_CHANGED = 'brief_description_error_changed';
export const LOADING_EDIT = 'loading_edit';
export const USER_EDIT_SUCCESS = 'USER_edit_SUCCESS';
export const USER_EDIT_ERROR = 'USER_edit_ERROR';
export const AUTH_RESULT_CHANGE_EDIT = 'auth_result_change_edit';
export const SET_PROFILE_DATA = 'set_profile_data';
export const USER_PHOTO_CHANGED = 'user_photo_changed';
export const USER_PHOTO_ERROR_CHANGED = 'user_photo_error_change';
export const USER_TAG = 'user_tag';
export const ADD_TAG = 'add_tag';
export const REMOVE_TAG = 'remove_tag';
export const TEXT_ON_CHANGE = 'text_on_change';
export const ON_SUBMIT_ERROR = 'on_submit_error';
export const USER_TAG_ERROR_CHANGED = 'user_tag_error_changed';


//Commemt page
export const COMMENT_CHANGED = 'comment_changed';
export const COMMENT_ERROR_CHANGED = 'comment_error_changed';
export const COMMENT_LOADING = 'comment_loading';
export const COMMENT_SUCCESS = 'comment_success';
export const COMMENT_FAILED = 'comment_failed';
export const COMMENT_INITIAL_STATE = 'comment_initial_state';
export const COMMENTLIST_DATA = 'commentlist_data';
export const COMMENTLIST_LOADING = 'commentlist_loading';
export const COMMENTLIST_INITIAL_STATE = 'COMMENTLIST_INITIAL_STATE';
export const COMMENTLIST_ERROR_CHANGED = 'commentlist_error_changed';

// Feedlist
export const FEEDLIST_DATA = 'feedlist_changed';
export const FEEDLIST_ERROR_CHANGED = 'feedlist_error_changed';
export const FEEDLIST_LOADING = 'feedlist_loading';
export const FEEDLIST_INITIAL_STATE = 'feedlist_initial_state';
export const ON_PRESS_LIKE = 'ON_PRESS_LIKE';

// About
export const MYAbout_DATA = 'myabout_changed';
export const MYAbout_ERROR_CHANGED = 'myabout_error_changed';
export const MYAbout_LOADING = 'myabout_loading';

// profile tag
export const ProfileTag_DATA = 'profiletag_changed';
export const ProfileTag_ERROR_CHANGED = 'profiletag_error_changed';
export const ProfileTag_LOADING = 'profiletag_loading';
export const PROFILEDELETE_SUCCESS = 'PROFILEDELETE_SUCCESS';
export const PROFILEDELETE_FAILED = 'PROFILEDELETE_FAILED';
export const PROFILEDELETE_LOADING = 'PROFILEDELETE_LOADING';
export const PROFILE_DATA = "PROFILE_DATA";
export const PROFILE_ERROR_CHANGED = "PROFILE_ERROR_CHANGED";
export const Pofile_About_ERROR_CHANGED = "Pofile_About_ERROR_CHANGED";
export const Pofile_About_DATA = "Pofile_About_DATA";
export const OTHER_PROFILE_INITAL_STATE = "OTHER_PROFILE_INITAL_STATE";
export const OtherProfileTag_DATA = "OtherProfileTag_DATA";
export const OtherProfileTag_ERROR = "OtherProfileTag_ERROR";
export const OtherProfile_Loading = "OtherProfile_Loading";

// profile testimonials
export const ProfileTestimonial_DATA = 'profiletestimonial_changed';
export const ProfileTestimonial_ERROR_CHANGED = 'profiletestimonial_error_changed';
export const ProfileTestimonial_LOADING = 'profiletestimonial_loading';

//CraetePost
export const CREATEPOST_DATA = 'create_post_data';
export const CREATE_POST_FAILED = 'create_post_failed';
export const LOADING_CREATE_POST = 'loading_createpost';
export const CAPTION_CHANGED = 'caption_changed';
export const CAPTION_ERROR_CHANGED = 'caption_error_changed';
export const IMAGEPOST_CHANGED = 'imagepost_changed';
export const IMAGEPOST_ERROR_CHANGED = 'imagepost_error_changed';
export const CREATEPOST_SUCCESS = 'createpost_success';
export const CREATEPOST_FAILED = 'createpost_failed';
export const CREATE_POST_INITIAL_STATE = 'createpost_initial_state';


//Notificatuin
export const NOTIFICATION_DATA = 'notification_data';
export const NOTIFICATION_ERROR_CHANGED = 'notification_error_changed';
export const NOTIFICATION_LOADING = 'notification_loading';
export const NOTIFICATION_INITIAL_STATE = 'NOTIFICATION_INITIAL_STATE';

//Testimonial page
export const TESTIMONIAL_INITIAL_STATE = 'TESTIMONIAL_INITIAL_STATE';
export const LOADING_TESTIMONIAL = 'loadfng_testimonial';
export const TESTIMONIAL_DATA = 'testimonial_post_data';
export const TESTIMONIAL_CHANGED = 'testimonial_changed';
export const TESTIMONIAL_ERROR_CHANGED = 'testimonial_error_changed';
export const TESTIMONIAL_SUCCESS = 'testimonial_success';
export const TESTIMONIAL_FAILED = 'testimonial_Failed';
export const TESTIMONIAL_DATA_PROFILE = 'TESTIMONIAL_DATA_PROFILE';

// Search Page
export const SEARCHLIST_DATA = 'searchlist_data';
export const SEARCHLIST_ERROR_CHANGED = 'searchlist_error_changed';
export const SEARCHLIST_LOADING = 'searchlist_loading';
export const SEARCHLIST_INITIAL_STATE = 'SEARCHLIST_INITIAL_STATE';

//Searchresult
export const SEARCHRESULT_DATA = 'searchresult_data';
export const SEARCHRESULT_ERROR_CHANGED = 'searchresult_error_changed';
export const SEARCHRESULT_LOADING = 'searchresult_loading';
export const SEARCHRESULT_INITIAL_STATE = 'SEARCHRESULT_INITIAL_STATE';

//Reating Type
export const RATING_INITIAL_STATE = 'RATING_INITIAL_STATE';
export const LOADING_RATING = 'loading_rating';
export const RATING_DATA = 'rating_data';
export const RATING_CHANGED = 'rating_changed';
export const RATING_ERROR_CHANGED = 'rating_error_changed';
export const RATING_SUCCESS = 'rating_success';
export const RATING_FAILED = 'rating_Failed';

//addFriend
export const LOADING_ADDFRIEND = 'loading_addfriend';
export const ADDFRIEND_DATA = 'addfriend_data';
export const ADDFRIEND_SUCCESS = 'friend_success';
export const ADDFRIEND_FAILED = 'addfriend_Failed';

//sharepost Feed_Type
export const LOADING_FEED_SHAREPOST = 'loading_feed_sharepost';
export const SHAREPOST_FEED_DATA = 'sharepost_feed_data';
export const SHAREPOST_FEED_SUCCESS = 'sharepost_feed_success';
export const SHAREPOST_FEED_FAILED = 'sharepost_feed_Failed';


// Abusepost Feed_Type
export const LOADING_FEED_ABUSE = 'loading_feed_abuse';
export const ABUSE_FEED_DATA = 'abuse_feed_data';
export const ABUSE_FEED_SUCCESS = 'abuse_feed_success';
export const ABUSE_FEED_FAILED = 'abuse_feed_Failed';



//sharepost Comment_Type
export const LOADING_COMMENT_SHAREPOST = 'loading_comment_sharepost';
export const SHAREPOST_COMMENT_DATA = 'sharepost_comment_data';
export const SHAREPOST_COMMENT_SUCCESS = 'sharepost_comment_success';
export const SHAREPOST_COMMENT_FAILED = 'sharepost_comment_Failed';

// Abuse post Comment_Type
export const LOADING_COMMENT_ABUSE = 'loading_comment_abuse';
export const ABUSE_COMMENT_DATA = 'abuse_comment_data';
export const ABUSE_COMMENT_SUCCESS = 'abuse_comment_success';
export const ABUSE_COMMENT_FAILED = 'abuse_comment_Failed'

//sharepost POSTS_Type
export const LOADING_POSTS_SHAREPOST = 'loading_posts_sharepost';
export const SHAREPOST_POSTS_DATA = 'sharepost_posts_data';
export const SHAREPOST_POSTS_SUCCESS = 'sharepost_posts_success';
export const SHAREPOST_POSTS_FAILED = 'sharepost_posts_Failed';

//sharepost POSTS_Type
export const LOADING_POSTS_SHAREPOST1 = 'loading_posts_sharepost1';
export const SHAREPOST_POSTS_DATA1 = 'sharepost_posts_data1';
export const SHAREPOST_POSTS_SUCCESS1 = 'sharepost_posts_success1';
export const SHAREPOST_POSTS_FAILED1 = 'sharepost_posts_Failed1';

// Abuse post POSTS_Type
export const LOADING_POSTS_ABUSE = 'loading_posts_abuse';
export const ABUSE_POSTS_DATA = 'abuse_posts_data';
export const ABUSE_POSTS_SUCCESS = 'abuse_posts_success';
export const ABUSE_POSTS_FAILED = 'abuse_posts_Failed';


//MYPOSTS
export const MYPOSTS_DATA = 'myposts_changed';
export const MYPOSTS_ERROR_CHANGED = 'myposts_error_changed';
export const MYPOSTS_LOADING = 'myposts_loading';

//Friend type
export const FRIEND_INITIAL_STATE = 'FRIEND_INITIAL_STATE';
export const FRIEND_DATA = 'friend_changed';
export const FRIEND_ERROR_CHANGED = 'friend_error_changed';
export const FRIEND_LOADING = 'friend_loading';
export const PROFILE_FRIEND_DATA = 'PROFILE_FRIEND_DATA';

//UnFriend type
export const UNFRIEND_DATA = 'unfriend_changed';
export const UNFRIEND_ERROR_CHANGED = 'Unfriend_error_changed';
export const UNFRIEND_LOADING = 'Unfriend_loading';
export const UNFRIEND_SUCCESS = 'unfriend_success';

//Logout type
export const LOGOUT_DATA = 'logout_changed';
export const LOGOUT_FAILED = 'logout_failed';
export const LOADING_LOGOUT = 'logout_loading';
export const LOGOUT_SUCCESS = 'logout_success';

//Aboutus
export const ABOUTUS_DATA = 'aboutus_changed';
export const ABOUTUS_ERROR_CHANGED = 'aboutus_error_changed';
export const ABOUTUS_LOADING = 'aboutus_loading';

//Terterms
export const TERMS_DATA = 'terms_changed';
export const TERMS_ERROR_CHANGED = 'terms_error_changed';
export const TERMS_LOADING = 'terms_loading';

//PrivacyPolicy
export const PRIVACYPOLICY_DATA = 'PrivacyPolicy_changed';
export const PRIVACYPOLICY_ERROR_CHANGED = 'PrivacyPolicy_error_changed';
export const PRIVACYPOLICY_LOADING = 'PrivacyPolicy_loading';

//BlockFriend
export const LOADING_BLOCKFRIEND = 'loading_blockfriend';
export const BLOCKFRIEND_DATA = 'blockfriend_data';
export const BLOCKFRIEND_SUCCESS = 'blockfriend_success';
export const BLOCKFRIEND_FAILED = 'blockfriend_Failed';

//BottomTab
export const ON_CHANGE_BOTTOM_TAB = 'ON_CHANGE_BOTTOM_TAB';


//SocailPage type
export const SOCAIL_LOGIN_LOADING = 'socail_login_loading';
export const SOCAIL_LOGIN_FAILED = 'socail_login_failed';
export const SOCAIL_LOGIN_SUCCESS = 'socail_login_success';

//ADD Tag
export const PROFILEADDTAG_LOADING = 'profileaddtag_loading';
export const ProfileaddTag_ERROR_CHANGED = 'ProfileaddTag_ERROR_CHANGED';
export const ProfileADDTag_DATA = 'ProfileADDTag_DATA';
export const PROFILEADDTAG_SUCCESS = 'PROFILEADDTAG_SUCCESS';
export const PROFILEADDTAG_FAILED = 'PROFILEADDTAG_FAILED';
export const USERADDTAG_CHANGED = 'USERADDTAG_CHANGED';
export const USERADDTAG_ERROR_CHANGED = 'USERADDTAG_ERROR_CHANGED';

// Splash Screen type
export const GET_TOKEN = 'get_token';
export const TOKEN_SUCCESS = 'token_success';
export const TOKEN_FAILED = 'token_fill';
export const TOKEN_LOADDING = 'token_loadding';
export const GET_SEARCH_DATA = 'GET_SEARCH_DATA';

//Myprofile Type
export const MYPROFILE_DATA = 'MYPROFILE_DATA';
export const MYPROFILE_ERROR_CHANGED = 'MYPROFILE_ERROR_CHANGED';
export const MYPROFILE_LOADING = 'MYPROFILE_LOADING';

// CompeleteProfile type
export const PROFECATION_CHANGED = 'PROFECATION_CHANGED';
export const PROFECATION_ERROR_CHANGED = 'PROFECATION_ERROR_CHANGED';
export const COMPELETEPROFILE_LOGIN = 'COMPELETEPROFILE_LOGIN';
export const COMPELETEPROFILE_INITIAL_STATE = 'COMPELETEPROFILE_INITIAL_STATE';
export const COMPELETEPROFILE_FAILED = 'COMPELETEPROFILE_FAILED';
export const COMPELETEPROFILE_SUCCESS = 'COMPELETEPROFILE_SUCCESS';
export const LOCATION_CHANGED_COMPELETEPROFILE = 'LOCATION_CHANGED_COMPELETEPROFILE';
export const LOCATION_ERROR_CHANGED_COMPELETEPROFILE = 'LOCATION_ERROR_CHANGED_COMPELETEPROFILE';

//alltages
export const ALLTAGS_DATA = 'alltags_data';
export const ON_TAG_PRESS = 'on_tag_press';
export const ALLTAGS_LOADING = 'alltags_loading';
export const ALLTAGS_ERROR = 'alltags_error';
export const ALLTAGS_INITIAL_STATE = 'alltags_initial_state';

//Chatlist type
export const FRIEND_DATA1 = 'friend_changed1';
export const FRIEND_ERROR_CHANGED1 = 'friend_error_changed1';
export const FRIEND_LOADING1 = 'friend_loading1';
export const SAVE_USER_DATA = 'SAVE_USER_DATA';
export const CHAT_LIST_LOADING = 'CHAT_LIST_LOADING';
export const INITIAL_CHAT_LIST = 'INITIAL_CHAT_LIST';
export const CHAT_LIST_SUCCESS = 'CHAT_LIST_SUCCESS';
export const CHAT_LIST_ERROR = 'CHAT_LIST_ERROR';
export const CHAT_DETAIL_SUCCESS = 'CHAT_DETAIL_SUCCESS';
export const CHAT_DETAIL_ERROR = 'CHAT_DETAIL_ERROR';

// AddFriendin serachResult
//addFriend
export const LOADING_ADDFRIEND1 = 'loading_addfriend1';
export const ADDFRIEND_DATA1 = 'addfriend_data1';
export const ADDFRIEND_SUCCESS1 = 'friend_success1';
export const ADDFRIEND_FAILED1 = 'addfriend_Failed1';

// SettingAction Notification seting
export const LOADING_NOTIFICATIONSETING = 'LOADING_NOTIFICATIONSETING';
export const NOTIFICATIONSETING_DATA = 'NOTIFICATIONSETING_DATA';
export const NOTIFICATIONSETING_SUCCESS = 'NOTIFICATIONSETING_SUCCESS';
export const NOTIFICATIONSETING_SUCCESS1 = 'NOTIFICATIONSETING_SUCCESS1';
export const NOTIFICATIONSETING_FAILED = 'NOTIFICATIONSETING_FAILED';

//check version Type
export const CHECK_VERSION = 'check_app_version';
export const LOADING = 'loading_splace_screen';

//SearchUpdate
export const SEARCH_UPDATE_LIST_SUCCESS = 'SEARCH_UPDATE_LIST_SUCCESS';
export const SEARCH_UPDATE_LIST_ERROR = 'SEARCH_UPDATE_LIST_ERROR';


// initial State
export const ABOUT_INITIAL_STATE = 'ABOUT_INITIAL_STATE';
export const MYPROFILE_INITAL_STATE = 'MYPROFILE_INITAL_STATE';
export const MYTAGPROFILE_INITAL_STATE = 'MYTAGPROFILE_INITAL_STATE';

// chatnotificatioin type
export const LOADING_CHATNOTIFICATION = 'LOADING_CHATNOTIFICATION';
export const CHATNOTIFICATION_SUCCESS = 'CHATNOTIFICATION_SUCCESS';
export const CHATNOTIFICATION_FAILED = 'CHATNOTIFICATION_FAILED';

//NotoficationclearAll
export const NOTIFICATION_ALL_DATA = "NOTIFICATION_ALL_DATA";
export const NOTIFICATION_ALL_ERROR_CHANGED = "NOTIFICATION_ALL_ERROR_CHANGED";
export const NOTIFICATION_ALL_LOADING = "NOTIFICATION_ALL_LOADING";

//User online-offline status type
export const USER_STATUS_SUCCESS = "CHAT_LIST_SUCCESS";
export const USER_STATUS_FAILED = "CHAT_LIST_ERROR";
export const USER_STATUS_ERROR = "CHAT_DETAIL_ERROR";

//user email check exist or not
export const INITIAL_STATE_SOCIAL = 'INITIAL_STATE_SOCIAL';
export const CHECK_SUCCESS = 'CHECK_SUCCESS';
export const CHECK_FAILED = 'CHECK_FAILED';
export const CHECK_ERROR = 'CHECK_ERROR';