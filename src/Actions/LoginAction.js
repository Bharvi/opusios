import axios from 'axios';
import { EMAIL_CHANGED, PASSWORD_CHANGED, EMAIL_ERROR_CHANGED, PASSWORD_ERROR_CHANGED,
      LOADING_LOGIN, LOGIN_INITIAL_STATE, BASE_URL, LOGIN_FAILED, LOGIN_SUCCESS
  } from './type';
import { insertUser, deleteUser } from '../Database/allSchema';

  export const emailChangeLogin = (text) => {
  return {
    type: EMAIL_CHANGED,
    payload: text
  };
};

export const emailErrorChangeLogin = (text) => {
  return {
    type: EMAIL_ERROR_CHANGED,
    payload: text
  };
};

export const passwordChangeLogin = (text) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  };
};

export const passwordErrorChangeLogin = (text) => {
  return {
    type: PASSWORD_ERROR_CHANGED,
    payload: text
  };
};

export const initialLoginStateData = () => {
  return {
    type: LOGIN_INITIAL_STATE,
    payload: ''
  };
};

export const login = (email, password, device_type ) => {
  console.log(email);
  console.log(password);

  var details =  {
      email,
      password,
      device_type,
      device_token: '1234555'
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return (dispatch) => {
    dispatch({
      type: LOADING_LOGIN
    });
    axios.post(`${BASE_URL}client?action=create&ws=login`,formBody,{
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          }
      })
      .then(response => {
        console.log(response);
        if(response.data.STATUS) {
          saveUser(response.data.DATA.user_data, password, email, dispatch);
          console.log(saveUser);
          console.log('Hello');
        } else {
          dispatch({
            type: LOGIN_FAILED,
            payload: 'Invalid Email or Password'
          });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: LOGIN_FAILED,
          payload: 'Invalid Email or Password'
        });
      });
  };
};

const saveUser = (userData, email, password, dispatch) => {
    console.log(email);
    console.log(password);
    console.log(userData);
    deleteUser().then(() => {
        const newUser = {
          id:(userData.user_id == null) ? 1 : Number(userData.user_id),    // primary key
          fullname: (userData.fullname == null) ? '' : userData.fullname,
          email_id: (userData.email_id == null) ? '' : userData.email_id,
          user_password: (userData.user_password == null) ? '' : userData.user_password,
          device_type: (userData.device_type == null) ? '' : userData.device_type,

          // user_photo:"http://www.opus.projectdemo.website/public/uploads/images/"+ userData.user_photo,
          user_photo:`http://www.opus.projectdemo.website/public/uploads/images/${userData.user_photo}`,

          device_token: (userData.device_token == null) ? '' : userData.device_token,
          gender: (userData.gender == "0") ? 'Female' : 'Male',
          profession_name: (userData.profession_name == null) ? '' : userData.profession_name,
          location: (userData.location == null) ? '' : userData.location,
          brief_desc: (userData.brief_desc == null) ? '' : userData.brief_desc,
          birth_date: (userData.birth_date == null) ? '' : userData.birth_date,
          user_tag: (userData.user_tag == null) ? '' : userData.user_tag,
        };
        insertUser(newUser).then(() => {
          console.log('Insert Suceess');
          console.log(newUser);
          dispatch({
            type: LOGIN_SUCCESS,
            payload: 'Success'
          });
        }).catch((error) => {
          console.log(`User Insert error${error}`);
          dispatch({
            type: LOGIN_FAILED,
            payload: 'Invalid Email or Password'
          });
        });
    }).catch((error) => {
      console.log(error);
      dispatch({
        type: LOGIN_FAILED,
        payload: 'Invalid Email or Password'
      });
    });
};
