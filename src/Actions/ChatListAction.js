import axios from 'axios';
import {
  FRIEND_DATA1, BASE_URL, FRIEND_ERROR_CHANGED1, FRIEND_LOADING1,
  CHAT_LIST_ERROR, CHAT_LIST_SUCCESS,
  UNFRIEND_DATA1, UNFRIEND_ERROR_CHANGED1, UNFRIEND_LOADING, UNFRIEND_SUCCESS1, SAVE_USER_DATA,
  CHAT_DETAIL_ERROR, CHAT_DETAIL_SUCCESS, INITIAL_CHAT_LIST, CHAT_LIST_LOADING

} from './type';
import { insertChatList, deleteChatList } from '../Database/allSchema';
import firebase from "react-native-firebase";

export const initialChatList = () => {
  return (dispatch) => {
    dispatch({
      type: INITIAL_CHAT_LIST
    });
  }
}

export const getMyFriendChat = ({ userId, page, friend, token }) => {
  console.log(userId);
  console.log(page);
  console.log(token);
  var details = {

  };

  var formBody = [];

  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return (dispatch) => {

    if (page === 1) {
      dispatch({
        type: FRIEND_LOADING1
      });
    }


    axios.get(`${BASE_URL}friends?userID=${userId}&pageNo=${page}&pageCount=10`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': token,
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      }
    })
      .then(response => {
        // console.log('MYAbout success'+ response);
        console.log('aavi gayu');
        if (response.data == '') {
          dispatch({ type: FRIEND_ERROR_CHANGED1, payload: 'Something went wrong please try again' });
        } else {
          if (response.data.STATUS) {

            // console.log('successssss',response.data.DATA);
            // dispatch({ type: FRIEND_DATA, payload: response.data.DATA });
            var isData = true;
            if (response.data.DATA.length === 0) {
              isData = false;
            }
            console.log('chatlist', response.data.DATA);

            if (page === 1) {
              console.log('page1', response.data.DATA)
              dispatch({ type: FRIEND_DATA1, payload: { list: response.data.DATA, isData } });
              console.log('list', this.props.list);
            } else {
              if (response.data.DATA.length > 0) {
                // posts.concat(response.data.DATA);
                dispatch({ type: FRIEND_DATA1, payload: { list: friend.concat(response.data.DATA), isData } });
              }
            }

            // console.log(response.data.DATA.Feed_list);
            // dispatch({ type: MYAbout_DATA, payload: response.data.DATA.Feed_list});
          } else {
            console.log('not true');
            dispatch({ type: FRIEND_ERROR_CHANGED1, payload: 'Something went wrong please try again' });
          }
        }

      })
      .catch((error) => {
        console.log('FRIEND_ERROR_CHANGED' + error);
        dispatch({ type: FRIEND_ERROR_CHANGED1, payload: 'Something went wrong please try again' });
      });
  };
};

export const saveUserChat = (user_id, fullname, firebase) => {
  console.log('Action call')
  // console.log(name)
  return {
    type: SAVE_USER_DATA,
    payload: { user_id, fullname, firebase }
  };
};

export const getMyChatList = (userId, token, userFireId, pageNo, list, isFromChatFirebase) => {
  // userChatList
  // console.log('pageNo', pageNo)
  // console.log('userFireId', userFireId)

  return (dispatch) => {
    axios.post(`${BASE_URL}userChatList`, {
      userID: userId,
      // pageNo,
      // pageCount: 300,
      // userFireId
    },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }
      })
      .then((response) => {
        dispatch({
          type: CHAT_LIST_LOADING
        });
        if (response.data.STATUS) {
          console.log('response userchatList', response.data.DATA);

          let chatFriendList = [];
          let firebaseCurrentUser;
          let chatList = response.data.DATA

          console.log('chatList.length', chatList.length)

          let isDataInChat = (chatList.length == 0) ? false : true;

          //get user's firebaseId and if admin then set afdsfsdfdsfsdfsdfsdf
          if (userId == 1) {
            console.log('in userid 1');
            firebaseCurrentUser = 'afdsfsdfdsfsdfsdfsdf';
          } else {
            firebaseCurrentUser = userFireId;
          }
          // for loop for set data from firebase on user's chatlist response
          for (let index = 0; index < chatList.length; index++) {
            let element = chatList[index];
            let chatId = "";
            let i = index + 1;
            //set chatId to get chat messages of two users
            if (firebaseCurrentUser > element.firebase) {
              chatId = `${firebaseCurrentUser}-${element.firebase}`;
            }
            else {
              chatId = `${element.firebase}-${firebaseCurrentUser}`;
            }
            // console.log('chatId', chatId)
            let chatRefData = firebase.database().ref().child("chat/" + chatId).orderByChild("order").limitToFirst(1);
            chatRefData.on('value', (chatData) => {
              // console.log('chatData', chatData.val())
              if (chatData.val() == null) {
                let i = index + 1
                // if user only register not complete profile then not add them
                // chatFriendList.push(element)
              }
              chatData.forEach(recentData => {
                element.chatId = chatId
                element.recent_msg = recentData.val().text;
                element.recent_time = new Date(recentData.val().createdAt);
                if (firebaseCurrentUser != recentData.val().uid) {
                  // console.log('recentData.val().isRead', recentData.val().isRead)
                  element.isReadMsg = (recentData.val().isRead) ? recentData.val().isRead : 0;
                }
                chatFriendList.push(element)
              });

              // console.log('index', i)
              // console.log('chatFriendList.length', chatFriendList)
              // console.log('chatFriendList.length', chatFriendList.length, response.data.DATA.length)
              // console.log('response.data.DATA.length', response.data.DATA.length)

              if ((chatFriendList.length == response.data.DATA.length)
                || (i == response.data.DATA.length)) {

                chatFriendList.sort((d1, d2) => (new Date(d2.recent_time)) - (new Date(d1.recent_time)));

                // let newArray = [];
                // chatFriendList.forEach(obj => {
                //   if (!newArray.some(obj.firebase === obj.firebase)) {
                //     newArray.push({ ...obj })
                //   }
                // });
                // let chatListData = newArray
                // console.log('chatList.length', chatListData.length)

                dispatch({
                  type: CHAT_LIST_SUCCESS,
                  payload: { list: chatFriendList, isDataInChat: isDataInChat }
                });

                // if (userId != 1) {
                //   chatFriendList.sort((d1, d2) => (new Date(d2.recent_time)) - (new Date(d1.recent_time)));
                //   // tempchat.sort((d1, d2) => (new Date(d2.lst_msg_time)) - (new Date(d1.lst_msg_time)));
                // }
                // // console.log('chatList_action', tempchat)
                // if (isFromChatFirebase && list.length > 0) {
                //   console.log('this from firebase change', list)
                //   // console.log('tempchat', tempchat)
                //   let tempNewChatList = chatFriendList.concat(list)
                //   let newTemp = []
                //   tempNewChatList.forEach(obj => {
                //     if (!newTemp.some(o => o.firebase === obj.firebase)) {
                //       newTemp.push({ ...obj })
                //     }
                //   });
                //   let data = newTemp
                //   // if (userId != 1) {
                //   data.sort((d1, d2) => (new Date(d2.recent_time)) - (new Date(d1.recent_time)));
                //   // }
                //   console.log('data chatliist', data)
                //   dispatch({
                //     type: CHAT_LIST_SUCCESS,
                //     payload: { list: data, isDataInChat: isDataInChat }
                //   });
                // } else if (pageNo > 1) {
                //   console.log('page number 1')
                //   let newArr = list.concat(chatFriendList)
                //   let data = []
                //   newArr.forEach(obj => {
                //     if (!data.some(o => o.firebase === obj.firebase)) {
                //       data.push({ ...obj })
                //     }
                //   });
                //   let tempChat = data
                //   dispatch({
                //     type: CHAT_LIST_SUCCESS,
                //     payload: { list: tempChat, isDataInChat: isDataInChat }
                //   });
                // } else {
                //   console.log('page number else')
                //   dispatch({
                //     type: CHAT_LIST_SUCCESS,
                //     payload: { list: chatFriendList, isDataInChat: isDataInChat }
                //   });
                // }
              }
            })
          }
          // //for loop for set data from firebase on user's chatlist response
          // for (let index = 0; index < chatList.length; index++) {
          //   let element = chatList[index];
          //   let chatId = "";
          //   let i = index + 1;
          //   //set chatId to get chat messages of two users
          //   if (firebaseCurrentUser > element.firebase) {
          //     chatId = `${firebaseCurrentUser}-${element.firebase}`;
          //   }
          //   else {
          //     chatId = `${element.firebase}-${firebaseCurrentUser}`;
          //   }

          //   // console.log('chatId', chatId)
          //   let chatRefData = firebase.database().ref().child("chat/" + chatId).orderByChild("order").limitToFirst(1);
          //   chatRefData.on('value', (chatData) => {
          //     // console.log('chatData', chatData.val())
          //     if (chatData.val() == null) {
          //       let i = index + 1
          //     }
          //     chatData.forEach(recentData => {
          //       element.chatId = chatId
          //       element.recent_msg = recentData.val().text;
          //       element.recent_time = new Date(recentData.val().createdAt);
          //       if (firebaseCurrentUser != recentData.val().uid) {
          //         console.log('recentData.val().isRead', recentData.val().isRead)
          //         element.isReadMsg = (recentData.val().isRead) ? recentData.val().isRead : 0;
          //       }
          //       chatFriendList.push(element)
          //     });
          //     console.log('index', i)
          //     // console.log('chatFriendList.length', chatFriendList)
          //     console.log('chatFriendList.length', chatFriendList.length, response.data.DATA.length)
          //     // console.log('response.data.DATA.length', response.data.DATA.length)

          //     if ((chatFriendList.length == response.data.DATA.length)
          //       || (i == response.data.DATA.length)
          //     ) {

          //       let newArray = []
          //       chatFriendList.forEach(obj => {
          //         if (!newArray.some(o => o.to_user_id === obj.to_user_id)) {
          //           newArray.push({ ...obj })
          //         }
          //       });
          //       let tempchat = newArray;
          //       // tempchat.sort((d1, d2) => (new Date(d2.lst_msg_time)) - (new Date(d1.lst_msg_time)));

          //       // console.log('chatList_action', tempchat)

          //       if (isFromChatFirebase && list.length > 0) {
          //         console.log('this from firebase change', list)
          //         console.log('tempchat', tempchat)
          //         let tempNewChatList = tempchat.concat(list)
          //         let newTemp = []
          //         tempNewChatList.forEach(obj => {
          //           if (!newTemp.some(o => o.to_user_id === obj.to_user_id)) {
          //             newTemp.push({ ...obj })
          //           }
          //         });
          //         // console.log('newTemp', newTemp);
          //         dispatch({
          //           type: CHAT_LIST_SUCCESS,
          //           payload: { list: newTemp, isDataInChat: isDataInChat }
          //         });
          //       } else if (pageNo > 1) {
          //         console.log('page number 1')
          //         dispatch({
          //           type: CHAT_LIST_SUCCESS,
          //           payload: { list: list.concat(tempchat), isDataInChat: isDataInChat }
          //         });
          //       } else {
          //         console.log('page number else')
          //         dispatch({
          //           type: CHAT_LIST_SUCCESS,
          //           payload: { list: tempchat, isDataInChat: isDataInChat }
          //         });
          //       }
          //     }
          //   })

          // }

          // if (pageNo > 1) {

          //   dispatch({
          //     type: CHAT_LIST_SUCCESS,
          //     payload: { list: list.concat(chatList), isDataInChat: isDataInChat }
          //   });
          // } else {

          //   dispatch({
          //     type: CHAT_LIST_SUCCESS,
          //     payload: { list: chatList, isDataInChat: isDataInChat }
          //   });
          // }

        } else {
          console.log('CHAT_LIST_STATUS_FAILED', response);
          dispatch({
            type: CHAT_LIST_ERROR,
            payload: 'Something went wrong please try again'
          });
        }
      })
      .catch((err) => {
        console.log('CHAT_LIST_ERROR', err);
        dispatch({
          type: CHAT_LIST_ERROR,
          payload: 'Something went wrong please try again'
        });
      })
  }
}
export const getMyChatListIos = (userId, token, userFireId) => {
  return (dispatch) => {
    axios.post(`${BASE_URL}userChatList`, {
      userID: userId
    },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }
      })
      .then((response) => {
        if (response.data.STATUS) {
          let chatFriendList = [];
          let firebaseCurrentUser;
          if (userId == 1) {
            firebaseCurrentUser = 'afdsfsdfdsfsdfsdfsdf';
          } else {
            firebaseCurrentUser = userFireId;
            console.log('firebaseCurrentUser', userFireId);
          }
          for (let index = 0; index < response.data.DATA.length; index++) {

            let element = response.data.DATA[index];
            let chatId = "";

            if (firebaseCurrentUser > element.firebase) {
              chatId = `${firebaseCurrentUser}-${element.firebase}`;
            }
            else {
              chatId = `${element.firebase}-${firebaseCurrentUser}`;
            }

            element.chatId = chatId;
            element.firebaseCurrentUser = firebaseCurrentUser;

            let chatRefData = firebase.database().ref().child("chat/" + chatId).orderByChild("order").limitToFirst(1);
            let userData = firebase.database().ref('friends/' + element.firebase)
            userData.once('value', (snapshot) => {
              // user Data 
              if (snapshot.val()) {
                if (snapshot.val().onlineStatus == 'true') {
                  element.isUserOnline = true
                } else {
                  element.isUserOnline = false
                }
              } else {
                // isUserOnline = false
                element.isUserOnline = false
              }
            });
            chatRefData.on('value', (chatData) => {
              chatData.forEach(recentData => {

                element.lst_msg = recentData.val().text;
                element.lst_msg_time = new Date(recentData.val().createdAt);
                if (firebaseCurrentUser != recentData.val().uid) {
                  element.isReadMsg = (recentData.val().isRead) ? recentData.val().isRead : 0;
                }
                chatFriendList.push(element)
              });


              console.log('chatFriendList.length', chatFriendList)
              console.log('chatFriendList.length', chatFriendList.length)
              console.log('response.data.DATA.length', response.data.DATA.length)

              if (
                (chatFriendList.length + 1 >= response.data.DATA.length - 1)
                ||
                (chatFriendList.length == 1 && response.data.DATA.length == 1)
                ||
                (chatFriendList.length == response.data.DATA.length)
              ) {

                let newArray = []
                chatFriendList.forEach(obj => {
                  if (!newArray.some(o => o.to_user_id === obj.to_user_id)) {
                    newArray.push({ ...obj })
                  }
                });
                let chatList = newArray;
                chatList.sort((d1, d2) => (new Date(d2.lst_msg_time)) - (new Date(d1.lst_msg_time)));
                console.log('chatList_action', chatList)

                dispatch({
                  type: CHAT_LIST_SUCCESS,
                  payload: chatList
                });

                let chatJson = JSON.stringify(chatFriendList)
                // console.log('chatlistpage 111', chatJson);
                deleteChatList().then(() => {
                  const chat = { id: 1, chatListJSON: chatJson }
                  insertChatList(chat).then((res) => {
                    // dispatch({
                    //   type: CHAT_LIST_SUCCESS,
                    //   payload: chatFriendList
                    // });
                    // console.log('chatlist', res);
                  }).catch((err) => {
                    console.log('chatlist_err', err);
                  })
                }).catch((err) => {
                  console.log('Error in delete chatlist', err)
                })
              }

            })
          }
        } else {
          console.warn('CHAT_LIST_SUCCESS  ' + error);
          dispatch({
            type: CHAT_LIST_ERROR,
            payload: 'Something went wrong please try again'
          });
        }
      })
      .catch(() => {
        console.warn('CHAT_LIST_SUCCESS  ' + error);
        dispatch({
          type: CHAT_LIST_ERROR,
          payload: 'Something went wrong please try again'
        });
      })
  }
}
export const getMyChatListOld = ({ userId, token }) => {
  // console.log('id', userId)
  return (dispatch) => {
    // dispatch({
    //   type: FRIEND_LOADING1
    // });
    axios.post(`${BASE_URL}userChatList`, {
      userID: userId
    },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }
      })
      .then(response => {
        console.warn('chatlistpage', response.data.STATUS);
        if (response.data.STATUS) {
          let chatFriendList = [];
          let firebaseCurrentUser;
          if (userId == 1) {
            firebaseCurrentUser = 'afdsfsdfdsfsdfsdfsdf';
          } else {
            firebaseCurrentUser = firebase.auth().currentUser.uid;
            console.log('firebaseCurrentUser', firebase.auth().currentUser.uid);
          }


          let chatJson = '';
          for (let index = 0; index < response.data.DATA.length; index++) {
            let element = response.data.DATA[index];
            let chatId = "";
            if (firebaseCurrentUser > element.firebase) {
              chatId = `${firebaseCurrentUser}-${element.firebase}`;
            }
            else {
              chatId = `${element.firebase}-${firebaseCurrentUser}`;
            }

            element.chatId = chatId;
            element.firebaseCurrentUser = firebaseCurrentUser;

            let chatRefData = firebase.database().ref().child("chat/" + chatId).orderByChild("order").limitToFirst(1);
            let count = firebase.database().ref().child("chat/" + chatId).orderByKey("isRead").limitToLast(1);

            chatRefData.once("value", snap1 => {
              // let m = snap1.toJSON();
              // console.log("value", snap1.toJSON());
              snap1.forEach(child1 => {
                // console.log('lst_msg..  ', child1.val().text);
                // console.log('lst_msg_time  ', new Date(child1.val().createdAt));
                element.lst_msg = child1.val().text;
                // element.lst_msg = '';
                element.lst_msg_time = new Date(child1.val().createdAt);
                // element.lst_msg_time = new Date();
                count.once("value", snap2 => {
                  // let cData = snap2.toJSON()
                  snap2.forEach(child2 => {
                    let isReadMsg = 0;
                    if (firebaseCurrentUser != child2.val().uid) {
                      console.log("value", child2.val().isRead);
                      isReadMsg = (child2.val().isRead) ? child2.val().isRead : 0;
                    }
                    element.isReadMsg = isReadMsg;
                    chatFriendList.push(element);
                    console.log(chatFriendList.length, response.data.DATA.length);

                    if (chatFriendList.length == response.data.DATA.length - 1) {
                      chatJson = JSON.stringify(chatFriendList)
                      // console.log('chatlistpage 111', chatJson);
                      deleteChatList().then(() => {
                        const chat = { id: 1, chatListJSON: chatJson }
                        insertChatList(chat).then((res) => {
                          dispatch({
                            type: CHAT_LIST_SUCCESS,
                            payload: chatFriendList
                          });
                          console.log('chatlist', res);
                        }).catch((err) => {
                          console.log('chatlist_err', err);
                        })
                      }).catch((err) => {
                        console.log('Error in delete chatlist', err)
                      })
                    }
                    // resolve(element)
                  });
                })
              });
            });
            // })
            // );
          }

        }
        else {
          dispatch({
            type: CHAT_LIST_ERROR,
            payload: 'Something went wrong please try again'
          });
        }
      })
      .catch((error) => {
        console.warn('CHAT_LIST_SUCCESS  ' + error);
        dispatch({
          type: CHAT_LIST_ERROR,
          payload: 'Something went wrong please try again'
        });
      });
  };
};

// export const getMyChatList = ({ userId, token }) => {
//   console.log(userId);
//   console.log(token);

//   return (dispatch) => {
//     // dispatch({
//     //   type: FRIEND_LOADING1
//     // });
//     axios.post(`${BASE_URL}userChatList`, {
//       userID: userId
//     },
//       {
//         headers: {
//           'Accept': 'application/json',
//           'Authorization': token,
//         }
//       })
//       .then(response => {
//         // console.warn('chatlistpage', response.data.DATA);
//         if (response.data.STATUS) {
//           let chatFriendList = [];
//           let firebaseCurrentUser;
//           if(userId == 1){
//             firebaseCurrentUser = "afdsfsdfdsfsdfsdfsdf";
//           }else{
//             firebaseCurrentUser = firebase.auth().currentUser.uid;
//           }
//           console.log(firebaseCurrentUser);
//           for (let index = 0; index < response.data.DATA.length; index++) {
//             const element = response.data.DATA[index];
//             let chatId = "";
//             if (firebaseCurrentUser > element.firebase) {
//               chatId = `${firebaseCurrentUser}-${element.firebase}`;
//             }
//             else {
//               chatId = `${element.firebase}-${firebaseCurrentUser}`;
//             }
//             let chatRefData = firebase.database().ref().child("chat/" + chatId).orderByChild("order").limitToFirst(1);
//             let count = firebase.database().ref().child("chat/" + chatId).orderByKey("isRead").limitToLast(1);

//             chatRefData.on("value", snap1 => {
//               // let m = snap1.toJSON();
//               // console.log("value",snap1.toJSON());
//               snap1.forEach(child1 => {
//                 console.log('lst_msg..  ', child1.val().text);
//                 console.log('lst_msg_time  ', new Date(child1.val().createdAt));
//                 element.lst_msg = child1.val().text;
//                 element.lst_msg_time = new Date(child1.val().createdAt);
//                 count.on("value", snap2 => {
//                   // let cData = snap2.toJSON()
//                   snap2.forEach(child2 => {
//                     let isReadMsg = 0;
//                     if (firebaseCurrentUser != child2.val().uid) {
//                       console.log("value", child2.val().isRead);
//                       isReadMsg = (child2.val().isRead) ? child2.val().isRead : 0;
//                     }
//                     element.isReadMsg = isReadMsg;
//                     chatFriendList.push(element);
//                   });
//                 });
//               });
//             });

//           }
//           setTimeout(() => {
//             // console.warn('chatlistpage', chatFriendList);

//             chatJson = JSON.stringify(chatFriendList)
//             console.log('chatlistpage 111', chatJson);
//             deleteChatList().then(() => {
//               const chat = { id: 1, chatListJSON: chatJson }
//               insertChatList(chat).then((res) => {
//                 console.log('chatlist', res);
//               })
//                 .catch((err) => {
//                   console.log('chatlist_err', err);
//                 })
//             })
//               .catch((err) => {
//                 console.log('Error in delete chatlist', err)
//               })
//             // chatFriendList.sort((d1, d2) => (d2.lst_msg_time) - (d1.lst_msg_time));
//             // dispatch({ type: CHAT_LIST_SUCCESS,
//             //   payload: chatFriendList
//             //   });

//           }, 3000);
//         } else {
//           dispatch({
//             type: CHAT_LIST_ERROR,
//             payload: 'Something went wrong please try again'
//           });
//         }
//       })
//       .catch((error) => {
//         console.warn('CHAT_LIST_SUCCESS  ' + error);
//         dispatch({
//           type: CHAT_LIST_ERROR,
//           payload: 'Something went wrong please try again'
//         });
//       });
//   };
// };

export const userChatHistory = (userId, toUserId, token) => {
  console.log(userId);
  console.log(token);
  return (dispatch) => {

    axios.post(`${BASE_URL}userChatHistory`, {
      userID: userId,
      toUserId
    },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }
      })
      .then(response => {
        console.warn(response.data);
      })
      .catch((error) => {
        console.warn('CHAT_LIST_SUCCESS  ' + error);
      });
  };
};

export const unfriendApi = (userId, touserid, token) => {
  console.log(userId);
  console.log(touserid);
  console.log(token);

  var details = {
    userID: userId,
    toUserID: touserid,
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    axios
      .post(`${BASE_URL}unfriends`, formBody, {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        }
      })
      .then(response => {
        console.warn(response);
      })
      .catch(error => {
        console.warn(error);
      });
  };
};

export const dotDisable = (userId, touserid, token) => {
  console.warn("userId" + userId);
  console.warn("touserid" + touserid);
  console.log(token);

  return dispatch => {
    axios
      .post(`${BASE_URL}isDotEnable`, {
        userID: userId,
        toUserID: touserid
      }, {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }
      })
      .then(response => {
        console.warn(response);
      })
      .catch(error => {
        console.warn(error);
      });
  };
};

export const getChatDetail = (userId, toUserID, token, isShowLoaing) => {
  // console.warn('userId',userId);
  // console.log(token);
  // console.warn('toUserID',toUserID);

  return (dispatch) => {
    if (isShowLoaing) {
      dispatch({
        type: FRIEND_LOADING1
      });
    }
    axios.post(`${BASE_URL}userChatIsBlock`, {
      userID: userId,
      toUserID: toUserID
    },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }
      })
      .then(response => {
        console.warn(response.data);
        if (response.data.STATUS) {
          dispatch({
            type: CHAT_DETAIL_SUCCESS,
            payload: response.data.DATA
          });
          clerchatnotification1(userId, toUserID, token, dispatch);

        } else {
          dispatch({
            type: CHAT_DETAIL_ERROR,
            payload: 'Something went wrong please try again'
          });
        }
      })
      .catch((error) => {
        console.warn('CHAT_LIST_SUCCESS  ' + error);
        dispatch({
          type: CHAT_DETAIL_ERROR,
          payload: 'Something went wrong please try again'
        });
      });
  };
};

export const clerchatnotification = (userId, friendid, token) => {
  console.warn("userId" + userId);
  console.warn("friendid" + friendid);
  console.warn("abc", token);

  return dispatch => {
    axios
      .get(`${BASE_URL}clearChatNotifications?userID=${userId}&friendId=${friendid}`, {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }
      })
      .then(response => {
        if (response.data.STATUS) {

          console.warn(" Hello one", response.data.STATUS);

        } else {
          console.warn(" Hello one Error", response.data.STATUS);
        }
      })
      .catch(error => {
        console.warn(error);

      });
  };
};

const clerchatnotification1 = (userId, friendid, token, dispatch) => {
  console.warn("userId" + userId);
  console.warn("friendid" + friendid);
  console.warn("abc", token);

  // return dispatch => {
  axios
    .get(`${BASE_URL}clearChatNotifications?userID=${userId}&friendId=${friendid}`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': token,
      }
    })
    .then(response => {
      if (response.data.STATUS) {

        console.warn(" Hello one", response.data.STATUS);

      } else {
        console.warn(" Hello one Error", response.data.STATUS);
      }
    })
    .catch(error => {
      console.warn(error);

    });
  // };
};