import axios from 'axios';
import {
  SOCAIL_LOGIN_LOADING, SOCAIL_LOGIN_FAILED, SOCAIL_LOGIN_SUCCESS, BASE_URL,
  POSTIMAGEPATH, PROFILEIMAGEPATH, CHECK_SUCCESS, CHECK_FAILED, CHECK_ERROR, INITIAL_STATE_SOCIAL
} from './type';
import { insertUser, deleteUser } from '../Database/allSchema';
import { StackActions, NavigationActions } from 'react-navigation';
import firebase from "react-native-firebase";
import { setUserStatus } from './UserStatusAction';
export const setLoading = (isLoading) => {
  return {
    type: SOCAIL_LOGIN_LOADING,
    payload: isLoading
  }
}

export const initialSocialLogin = () => {
  console.log('initialSocialLogin')
  return (dispatch) => {
    dispatch({
      type: INITIAL_STATE_SOCIAL
    });
  }
}


export const checkUserExists = (email, token, userLoginData, type) => {
  console.log('email', email);

  return (dispatch) => {
    axios.post(`${BASE_URL}checkEmail`,
      { email_id: email },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': token
        }
      })
      .then(response => {
        if (response.data.STATUS) {

          //user already exist in system

          console.log('userDATA', response.data.DATA.user_data);

          let userData = response.data.DATA
          let temp = Object.assign({ loginType: type }, userData);

          console.log('tempExist', temp)
          dispatch({
            type: CHECK_SUCCESS,
            payload: { Data: temp, text: 'User Exist.' }
          });
        } else {
          let userData = userLoginData[0]
          let temp = Object.assign({ loginType: type }, userData);
          console.log('tempNew', temp)

          dispatch({
            type: CHECK_FAILED,
            payload: { Data: temp, text: response.data.DATA }
          });

        }
      })
      .catch(error => {
        console.log('error in check email', error);
        dispatch({
          type: CHECK_ERROR,
          payload: 'Error in usercheck.'
        });
      });
  };
};

export const Socaillogin = (social_id, fire_id, fullname, email, profile_picture, device_type, device_token, token, myNavigation, social_type) => {
  // console.warn(social_id);
  // console.warn(fullname);
  // console.warn(email);
  // console.warn(profile_picture);
  // console.warn(device_type);
  console.warn(device_token);
  // console.warn(token);
  // console.warn(fire_id,'fire_id');

  var details = {
    social_id,
    fullname,
    email,
    profile_picture,
    device_type,
    device_token,
    fire_id,
    social_type
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");
  return (dispatch) => {

    console.log(token, 'ajax');
    axios.post(`${BASE_URL}sociallogin`, formBody, {
      headers: {
        'Accept': 'application/json',
        'Authorization': token
      }
    })
      .then(response => {
        console.warn('social_login', response);
        initialSocialLogin()
        if (response.data.STATUS) {
          let userData = response.data.DATA.user_data;
          dispatch(setUserStatus(userData.user_id, '1', token))

          firebase.database().ref()
            .child("friends/" + userData.firebase)
            .update({
              user_id: userData.user_id,
              email: userData.social_id + '@opus.com',
              uid: userData.firebase,
              name: userData.fullname,
              token: userData.device_token,
              profile_picture: userData.user_photo,
              onlineStatus: 'true'
            });
          if (response.data.is_register_user === 'YES') {
            // console.log(response.data.DATA.user_data,'final');
            if (userData.profession != null) {
              saveUser(response.data.DATA.user_data, fullname, email, myNavigation, dispatch);
            } else {
              dispatch({
                type: SOCAIL_LOGIN_SUCCESS,
                payload: 'Success'
              });
              myNavigation.navigate('CompleteYourProfile', { userId: response.data.DATA.user_data.user_id });
            }
          }
          else {
            dispatch({
              type: SOCAIL_LOGIN_SUCCESS,
              payload: 'Success'
            });
            myNavigation.navigate('CompleteYourProfile', { userId: response.data.DATA.user_data.user_id });
          }
        } else {
          dispatch({
            type: SOCAIL_LOGIN_FAILED,
            payload: 'Social Login Failed.'
          });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: SOCAIL_LOGIN_FAILED,
          payload: 'Social Login Failed.'
        });
      });
  };
};

const saveUser = (userData, fullname, email, myNavigation, dispatch) => {
  console.log(email);
  console.log(fullname);
  console.log(userData, 'firenew');
  deleteUser().then(() => {
    const newUser = {
      id: (userData.user_id == null) ? 1 : Number(userData.user_id),    // primary key
      fullname: (userData.fullname == null) ? '' : userData.fullname,
      email_id: (userData.email_id == null) ? '' : userData.email_id,
      user_password: (userData.user_password == null) ? '' : userData.user_password,
      device_type: (userData.device_type == null) ? '' : userData.device_type + '',
      user_photo: PROFILEIMAGEPATH + userData.user_photo,
      device_token: (userData.device_token == null) ? '' : userData.device_token,
      gender: (userData.gender != null) ? ((userData.gender == "0") ? 'Female' : 'Male') : 'null',
      profession_name: userData.profession,
      location: (userData.location == null) ? '' : userData.location,
      brief_desc: (userData.brief_desc == null) ? '' : userData.brief_desc,
      birth_date: (userData.birth_date == null) ? '' : userData.birth_date,
      fire_id: (userData.firebase == null) ? '' : userData.firebase,
      is_notify: (userData.is_notify == null) ? '' : userData.is_notify,
      user_tag: (userData.user_tag == null) ? '' : userData.user_tag,


    };
    console.log(newUser);
    insertUser(newUser).then(() => {
      console.warn('Insert Suceess');
      console.log(newUser);
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({
          routeName: 'BottomTab'
        })],
      });
      myNavigation.dispatch(resetAction);
      dispatch({
        type: SOCAIL_LOGIN_SUCCESS,
        payload: 'Success'
      });
    }).catch((error) => {
      console.warn(`User Insert error ${error}`);
      dispatch({
        type: SOCAIL_LOGIN_FAILED,
        payload: 'Social Login Failed.'
      });
    });
  }).catch((error) => {
    console.warn(`User DELETE error ${error}`);
    dispatch({
      type: SOCAIL_LOGIN_FAILED,
      payload: 'Social Login Failed.'
    });
  });
};
