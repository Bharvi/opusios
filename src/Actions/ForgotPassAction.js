import axios from 'axios';
import { EMAIL_FORGOT_PASS_CHANGED, EMAIL_FORGOT_PASS_ERROR_CHANGED,  FORGOT_PASS_INITIAL_STATE,
  FORGOT_PASS_LOADING, FORGOT_PASS_FAILD, FORGOT_PASS_SUCCESS, BASE_URL
  } from './type';

  export const emailChangeForgotPass = (text) => {
  return {
    type: EMAIL_FORGOT_PASS_CHANGED,
    payload: text
  };
};

export const emailErrorChangeForgotPass = (text) => {
  return {
    type: EMAIL_FORGOT_PASS_ERROR_CHANGED,
    payload: text
  };
};

export const initialForgotPassStateData = () => {
  return {
    type: FORGOT_PASS_INITIAL_STATE,
    payload: ''
  };
};

export const ForgotPassword = (email ) => {
  console.log(email );

  var details =  {
      email_id : email,
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return (dispatch) => {
    dispatch({
      type: FORGOT_PASS_LOADING
    });
    axios.post(`${BASE_URL}client?action=create&ws=forgotpass`,formBody,{
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          }
      })
      .then(response => {
        console.log(response);
        if(response.data.STATUS) {
          console.log('Hello');
          dispatch({
            type: FORGOT_PASS_SUCCESS,
            payload: 'Success'
          });
        } else {
          dispatch({
            type: FORGOT_PASS_FAILD,
            payload: 'Email ID does not exist.'
          });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: FORGOT_PASS_FAILD,
          payload: 'Email ID does not exist.'
        });
      });
  };
};
