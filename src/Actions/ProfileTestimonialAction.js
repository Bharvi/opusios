import axios from "axios";
import {
  ProfileTestimonial_DATA,
  BASE_URL,
  ProfileTestimonial_ERROR_CHANGED,
  ProfileTestimonial_LOADING,
  TESTIMONIAL_DATA_PROFILE
} from "./type";

export const getProfileTestimonial = ({ userId, page, Testimonials, token, isLoading, isMyProfile }) => {
  console.log(userId);
  console.log(page);
  console.log(token);
  console.log("aaaaa");
  var details = {};

  var formBody = [];

  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    if (page === 1 && isLoading) {
      dispatch({
        type: ProfileTestimonial_LOADING
      });
    }
    axios
      .get(
        `${BASE_URL}testimonials?userID=${userId}&pageNo=${page}&pageCount=20`,

        {
          headers: {
            'Accept': 'application/json',
            'Authorization': token,
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          }
        }
      )
      .then(response => {
        // console.log('MYAbout success'+ response);
        if (response.data == "") {
          dispatch({
            type: ProfileTestimonial_ERROR_CHANGED,
            payload: "Something went wrong please try again"
          });
        } else {
          if (response.data.STATUS) {
            var isData = true;
            if (response.data.DATA.length === 0) {
              isData = false;
            }
            console.log(response.data.DATA);
            if (page === 1) {
              if (isMyProfile) {
                dispatch({ type: ProfileTestimonial_DATA, payload: { list: response.data.DATA, isData } });
              } else {
                dispatch({ type: TESTIMONIAL_DATA_PROFILE, payload: { list: response.data.DATA, isData } });
              }
            }
            else {
              if (response.data.DATA.length > 0) {
                // posts.concat(response.data.DATA);
                if (isMyProfile) {
                  dispatch({ type: ProfileTestimonial_DATA, payload: { list: Testimonials.concat(response.data.DATA), isData } });
                } else {
                  dispatch({ type: TESTIMONIAL_DATA_PROFILE, payload: { list: Testimonials.concat(response.data.DATA), isData } });
                }
              }
            }
          } else {
            console.log("not true");
            dispatch({
              type: ProfileTestimonial_LOADING,
              payload: "Something went wrong please try again"
            });
          }
        }
      })
      .catch(error => {
        console.log("MYAbout_ERROR  " + error);
        dispatch({
          type: ProfileTestimonial_LOADING,
          payload: "Something went wrong please try again"
        });
      });
  };
};
