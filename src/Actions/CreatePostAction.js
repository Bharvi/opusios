import axios from "axios";
import {
  BASE_URL,
  CREATE_POST_INITIAL_STATE,
  LOADING_CREATE_POST,
  CREATE_POST_FAILED,
  CREATEPOST_DATA,
  CAPTION_CHANGED,
  CAPTION_ERROR_CHANGED,
  IMAGEPOST_CHANGED,
  IMAGEPOST_ERROR_CHANGED,
  CREATEPOST_SUCCESS,
  CREATEPOST_FAILED

} from "./type";

export const initialCreatePostStateData = () => {
  return {
    type: CREATE_POST_INITIAL_STATE,
    payload: ""
  };
};
export const captionChange = (text) => {
return {
  type: CAPTION_CHANGED,
  payload: text
};
};

export const captionErrorChange = (text) => {
return {
  type: CAPTION_ERROR_CHANGED,
  payload: text
};
};

export const imagepostChange = (text) => {
return {
  type: IMAGEPOST_CHANGED,
  payload: text
};
};

export const imagepostErrorChange = (text) => {
return {
  type: IMAGEPOST_ERROR_CHANGED,
  payload: text
};
};

export const getCratePost = ({ userId, caption, media, token } ) => {
  console.log(userId);
  console.log(caption);
  console.log(media);
  console.log(token);


  const formData = new FormData();

  if(media != null) {
    formData.append('media', {
      uri: media,
      type: 'image/jpeg',
      name: '.jpeg',
    });
  }
  formData.append('userID',userId);
  formData.append('caption',caption);
  formData.append('media',media);

  // var details = {
  //   userID: userID,
  //   caption,
  //   media
  // };



  var formBody = [];
  for (var property in formData) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(formData[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_CREATE_POST
    });
    axios
      .post(`${BASE_URL}createPost`, formData, {
        headers: {
          'Accept':'application/json',
          'Authorization': token,
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          }
      })
      .then(response => {
        console.log(response);
        if(response.data.STATUS == true) {
          console.log('Hello');
          dispatch({
            type: CREATEPOST_SUCCESS,
            payload: 'Success'
          });
        } else {
          dispatch({
            type: CREATEPOST_FAILED,
            payload: 'User id must be numeric.'
          });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: CREATEPOST_FAILED,
          payload: 'select image'
        });
      });
  };
};
