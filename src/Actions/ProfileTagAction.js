import axios from 'axios';
import {
  Alert
} from "react-native";
import {
   ProfileTag_DATA, BASE_URL,  ProfileTag_ERROR_CHANGED, ProfileTag_LOADING,
   ProfileaddTag_ERROR_CHANGED, PROFILEADDTAG_LOADING, ProfileADDTag_DATA,
   PROFILEADDTAG_SUCCESS, PROFILEADDTAG_FAILED, USERADDTAG_ERROR_CHANGED, USERADDTAG_CHANGED,
   PROFILEDELETE_LOADING,PROFILEDELETE_SUCCESS, PROFILEDELETE_FAILED,
   MYTAGPROFILE_INITAL_STATE,
   GET_SEARCH_DATA
} from './type';


// export const user_tagChangeEdit = (text) => {
// return {
//   type: USER_TAG,
//   payload: text
// };
// };
// export const user_tag_errorChangeEdit = (text) => {
// return {
//   type: USER_TAG_ERROR_CHANGED,
//   payload: text
// };
// };

export const addtagChange = (text) => {
return {
  type: USERADDTAG_CHANGED,
  payload: text
};
};

export const addtagErrorChange = (text) => {
return {
  type: USERADDTAG_ERROR_CHANGED,
  payload: text
};
};

export const getProfileTag = ({ userId, token , isLoading}) => {
  console.log(userId);
  console.log(token);
  console.log('aaaaa');
  var details =  {

  };
  var formBody = [];

  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return (dispatch) => {
    if(isLoading) {
      dispatch({
        type: ProfileTag_LOADING
      });
    }
    
    axios.get(`${BASE_URL}tags?userID=${userId}`,{
      headers: {
        'Accept':'application/json',
        'Authorization': token,
        }
      })
      .then(response => {
        // console.log('MYAbout success'+ response);
        console.log('aavi gayu');
        if (response.data == '') {
          dispatch({ type: ProfileTag_ERROR_CHANGED, payload: 'Something went wrong please try again' });
        } else {
        if (response.data.STATUS) {
            console.log(response.data.DATA);
            dispatch({ type: ProfileTag_DATA, payload: response.data.DATA });

          // console.log(response.data.DATA.Feed_list);
          // dispatch({ type: MYAbout_DATA, payload: response.data.DATA.Feed_list});
        }else{
          dispatch({ type: ProfileTag_ERROR_CHANGED, payload: 'Something went wrong please try again' });
          console.log('not true');
        }
        }
      })
      .catch((error) => {
        console.log('MYAbout_ERROR  '+error);
        dispatch({ type: ProfileTag_LOADING, payload: 'Something went wrong please try again' });
      });
    };
};



export const getAddTAg = ({userId, tag, token} )=> {
  console.log(userId);
  console.log(tag);
  console.log(token);

  var details = {
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: PROFILEADDTAG_LOADING
    });
    axios
      .get(`${BASE_URL}add-user-tag?userID=${userId}&tag=${tag}`, {
        headers: {
        Accept: 'application/json',
        Authorization: token
      }
      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS == true) {
          console.log("Hello");
          dispatch({
            type: PROFILEADDTAG_SUCCESS,
            payload: "Success"
          });
          getAllTag(token, dispatch);
        } else if (response.data.Message == "tag already created") {
          setTimeout(() => {
            Alert.alert("Oops", "Tag already created");

          }, 500);
          dispatch({
            type: PROFILEADDTAG_FAILED,
            payload: "tag already created"
          });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: PROFILEADDTAG_FAILED,
          payload: "User id must be numeric."
        });
      });
  };
};


export const getDeleteTAg = ({userId, tag_id, token} )=> {
  console.log(userId);
  console.log(tag_id);
  console.log(token);

  var details = {
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: PROFILEDELETE_LOADING
    });
    axios
      .get(`${BASE_URL}user-tag-delete?userID=${userId}&tag_id=${tag_id}`, {
        headers: {
        Accept: 'application/json',
        Authorization: token
      }
      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS == true) {
          console.log("Hello");
          dispatch({
            type: PROFILEDELETE_SUCCESS,
            payload: "Tag delete success"
          });
          getAllTag(token, dispatch);
        }
        // else if (response.data.Message == "tag already created") {
        //   dispatch({
        //     type: PROFILEDELETE_FAILED,
        //     payload: "tag already created"
        //   });
        // }
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: PROFILEDELETE_FAILED,
          payload: "User id must be numeric."
        });
      });
  };
};


// export const addTag = (
//   tag,
//   tagArray
// ) => {
//   if (tag === '') {
//     return {
//       type: ADD_TAG,
//       payload: {
//         tagArray: tagArray,
//         user_tagError: 'Enter tag',
//         user_tag: tag
//       }
//     };
//   } else {
//     const temp = tagArray;
//     temp.push(tag);
//     console.log(temp);
//     return {
//       type: ADD_TAG,
//       payload: {
//         tagArray: tagArray,
//         user_tagError: '',
//         user_tag: ''
//       }
//     };
//   }
// };
//
// export const removeTag = (
//   tagArray,
//   index
// ) => {
//   const temp = [...tagArray];
//   temp.splice(index, 1);
//   console.log(temp);
//   return {
//     type: REMOVE_TAG,
//     payload: temp
//   };
// };

export const initialTAGProfileStateData = () => {
  return {
    type: MYTAGPROFILE_INITAL_STATE,
    payload: ""
  };
};


const getAllTag = (token, dispatch) => {
  console.warn('in alltags method..');
    axios.get(BASE_URL+`alltags`, {
        headers: {
          Accept: 'application/json',
          Authorization: token
        }
      })
      .then(response => {
        console.log('getAllTag responce',response.data.DATA);

        let tempTags = [];
        let tempProfession = [];
        response.data.DATA.tags.map((item) => { tempTags.push(item.name) });
        response.data.DATA.professions.map((item) => { tempProfession.push(item.name) });
        dispatch({ type: GET_SEARCH_DATA,
          payload: tempTags.concat(response.data.DATA.users, tempProfession, response.data.DATA.locations)  });
      })
      .catch(error => {
        console.log('error in alltags',error);

  });
};
