import axios from 'axios';
import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  EMAIL_ERROR_CHANGED,
  PASSWORD_ERROR_CHANGED,
  FULL_NAME_CHANGED,
  FULL_NAME_ERROR_CHANGED,
  CONFIRM_PASSWORD_CHANGED,
  CONFIRM_PASSWORD_ERROR_CHANGED,
  SIGNUP_INITIAL_STATE,
  SIGNUP_LOADING,
  SIGUP_FAILED,
  BASE_URL,
  SIGUP_SUCCESS
  

} from "./type";
import { insertUser, deleteUser } from '../Database/allSchema';

export const fullNameChange = text => {
  console.log("name" + text);
  return {
    type: FULL_NAME_CHANGED,
    payload: text
  };
};

export const fullNameErrorChangeSignup = text => {
  return {
    type: FULL_NAME_ERROR_CHANGED,
    payload: text
  };
};

export const emailChangeSignup = text => {
  return {
    type: EMAIL_CHANGED,
    payload: text
  };
};

export const emailErrorChangeSignup = text => {
  return {
    type: EMAIL_ERROR_CHANGED,
    payload: text
  };
};

export const passwordChangeSignup = text => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  };
};

export const passwordErrorChangeSignup = text => {
  return {
    type: PASSWORD_ERROR_CHANGED,
    payload: text
  };
};

export const confirmpassChangeSignup = text => {
  return {
    type: CONFIRM_PASSWORD_CHANGED,
    payload: text
  };
};

export const confirmpassErrorChangeSignup = text => {
  return {
    type: CONFIRM_PASSWORD_ERROR_CHANGED,
    payload: text
  };
};

export const initialSIgnupStateData = () => {
  return {
    type: SIGNUP_INITIAL_STATE,
    payload: ""
  };
};

export const SigupApi = (
{ fullname,
  email,
  password,
  user_photo,
  device_type,
  device_token }
) => {
  console.log(fullname);
  console.log(email);
  console.log(password);
  console.log(user_photo);
  console.log(device_type);

  const formData = new FormData();

  if(user_photo != null) {
    formData.append('user_photo', {
      uri: user_photo,
      type: 'image/jpeg',
      name: fullname+'.jpeg',
    });
  }
  // formData.append('user_id',id);
  formData.append('fullname',fullname);
  formData.append('email_id',email);
  formData.append('user_password',password);
  formData.append('device_type',device_type);
  formData.append('device_token',device_token);

  // var details = {
  //   fullname: name,
  //   email_id: email,
  //   user_password: password,
  //   user_photo,
  //   device_type,
  //   device_token:  "1234555",
  //   social_id
  // };

  var formBody = [];
  for (var property in formData) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(formData[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: SIGNUP_LOADING
    });
    axios
      .post(`${BASE_URL}client?action=create&ws=register`, formData, {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
        }
      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS) {
          console.log("Hello");
          dispatch({
            type: SIGUP_SUCCESS,
            payload: 'Success'
          });
          // saveUser(fullname, email, password, user_photo, dispatch);
          // console.log(saveUser);
        } else if (response.data.Message == "Email ID already exists.") {
          dispatch({
            type: SIGUP_FAILED,
            payload: "Email ID already exists."
          });
        } else {
          dispatch({
            type: SIGUP_FAILED,
            payload: "Registration Failed."
          });
        }
      })
      .catch(error => {
        console.log(error);
        // dispatch({
        //   type: SIGUP_FAILED,
        //   payload: "Registration Failed."
        // });
        dispatch({
          type: SIGUP_SUCCESS,
          payload: 'Success'
        });

      });
  };
};

// const saveUser = (fullname, email, password, user_photo, dispatch) => {
//     console.log(fullname);
//     console.log(email);
//     console.log(password);
//     console.log(user_photo);
//     deleteUser().then(() => {
//         const newUser = {
//           id: 1,    // primary key
//           fullname: (fullname == null) ? '' : fullname,
//           email_id: (email == null) ? '' : email,
//           user_password: (password == null) ? '' : password,
//           user_photo: (user_photo == null) ? '' : user_photo,
//           device_type: '',
//           user_photo:"http://www.opus.projectdemo.website/public/uploads/images/"+ userData.user_photo,
//           device_token: '',
//           gender: '',
//           profession_name: '',
//           location: '',
//           brief_desc: '',
//           birth_date: '',
//           user_tag: '',
//         };
//         insertUser(newUser).then(() => {
//           console.log('Insert Suceess');
//           console.log(newUser);
//           dispatch({
//             type: SIGUP_SUCCESS,
//             payload: 'Success'
//           });
//         }).catch((error) => {
//           console.log(`User Insert error${error}`);
//           dispatch({
//             type: SIGUP_FAILED,
//             payload: 'Registration Failed.'
//           });
//         });
//     }).catch((error) => {
//       console.log(error);
//       dispatch({
//         type: SIGUP_FAILED,
//         payload: 'Registration Failed.'
//       });
//     });
// };
