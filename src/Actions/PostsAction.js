import axios from 'axios';
import {
   MYPOSTS_DATA, BASE_URL, MYPOSTS_ERROR_CHANGED,MYPOSTS_LOADING,

   LOADING_POSTS_SHAREPOST,
   SHAREPOST_POSTS_DATA,
   SHAREPOST_POSTS_SUCCESS,
   SHAREPOST_POSTS_FAILED,

   LOADING_POSTS_SHAREPOST1,
   SHAREPOST_POSTS_DATA1,
   SHAREPOST_POSTS_SUCCESS1,
   SHAREPOST_POSTS_FAILED1,

   LOADING_POSTS_ABUSE,
   ABUSE_POSTS_DATA,
   ABUSE_POSTS_SUCCESS,
   ABUSE_POSTS_FAILED
} from './type';

export const getMYPosts = ({ userId, page, posts, token , isLoading , isLoadingInit}) => {
  console.log(userId);
  console.log(page);
  console.log(token);
  console.warn(isLoadingInit);
  var details =  {

  };
 
  var formBody = [];

  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");
  return (dispatch) => {

    if (page === 1) {
   
        dispatch({
        type: MYPOSTS_LOADING,
        payload: {isLoading , isLoadingInit}
        });

    }
    axios.get(`${BASE_URL}posts?userID=${userId}&pageNo=${page}&pageCount=10`,{
          headers: {
            'Accept':'application/json',
            'Authorization': token,
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
            }
      })
      .then(response => {
        // console.log('MYAbout success'+ response);
        console.log('post action');
        console.log(response);
        console.log('page = '+page);
        if (response.data == '') {
          dispatch({ type: MYPOSTS_ERROR_CHANGED, payload: 'Something went wrong please try again' });
        } else {
        if (response.data.STATUS) {
            // response.data.DATA.gender = (response.data.DATA.gender == "0") ? 'Female' : 'Male';
            var isData = true;
           
            if(response.data.DATA.length === 0 ){
              isData = false;


            }
            console.log(response.data.DATA);
            if (page === 1) {
              dispatch({ type: MYPOSTS_DATA, payload: {list: response.data.DATA, isData} });
            } else {
              if (response.data.DATA.length > 0) {
              // posts.concat(response.data.DATA);
              dispatch({ type: MYPOSTS_DATA, payload: {list: posts.concat(response.data.DATA), isData } });
              }
            }

          // console.log(response.data.DATA.Feed_list);
          // dispatch({ type: MYAbout_DATA, payload: response.data.DATA.Feed_list});
        }else{

          dispatch({ type: MYPOSTS_ERROR_CHANGED, payload: 'Something went wrong please try again' });
          console.log('not true');
        }
        }
      })
      .catch((error) => {
        console.log('MYPOSTS_ERROR_CHANGED  '+error);
        dispatch({ type: MYPOSTS_ERROR_CHANGED, payload: 'Something went wrong please try again' });
      });
    };
};

export const getPostsSharepost = ({userId, postID, shareText, token}) => {
  console.log("Method call ");
  console.log(userId);
  console.log(postID);
  console.log(token);

  var details = {
    userID: userId ,
    postID: postID,
    shareText : 'test2'
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_POSTS_SHAREPOST
    });
    axios
      .post(`${BASE_URL}sharePost`, formBody, {
        headers: {
          'Accept':'application/json',
          'Authorization': token,
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          }
      })
      .then(response => {
        console.log(response);
        if(response.data.STATUS == true) {
          console.log('Hello');
          dispatch({
            type: SHAREPOST_POSTS_SUCCESS,
            payload: 'Success'
          });
        } else if (response.data.Message == "User id must be numeric."){
          dispatch({
            type: SHAREPOST_POSTS_FAILED,
            payload: 'User id must be numeric.'
          });
        }
        else {
          dispatch({
            type: SHAREPOST_POSTS_FAILED,
            payload: 'Post id must be numeric.'
          });
        }

      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: SHAREPOST_POSTS_FAILED,
          payload: 'Post id must be numeric.'
        });
      });
  };
};

export const getPostsAbusePost = ({userId, postID, token}) => {
  console.log(userId);
  console.log("User Postid",postID);
  console.log(token);

  var details = {
    UserID: userId,
    postID: postID
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_POSTS_ABUSE
    });
    axios
      .post(`${BASE_URL}abusePost`, formBody, {
        headers: {
          'Accept':'application/json',
          'Authorization': token,
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          }
      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS == true) {
          console.log("Hello");
          dispatch({
            type: ABUSE_POSTS_SUCCESS,
            payload: "Success Abuse"
          });
        } else if (
          response.data.Message == "Already Post is Abuse by same User"
        ) {
          dispatch({
            type: ABUSE_POSTS_FAILED,
            payload: "Already Post is Abuse by same User"
          });
        } else if (response.data.Message == "User id must be numeric.") {
          dispatch({
            type: ABUSE_POSTS_FAILED,
            payload: "User id must be numeric."
          });
        } else {
          dispatch({
            type: ABUSE_POSTS_FAILED,
            payload: "Post id must be numeric."
          });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: ABUSE_POSTS_FAILED,
          payload: "Post id must be numeric."
        });
      });
  };
};
export const getPostsSharepost1 = ({userId, postID, shareText, token}) => {
  console.log("Method call ");
  console.log(userId);
  console.log(postID);
  console.log(token);

  var details = {
    userID: userId ,
    postID: postID,
    shareText : 'test2'
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_POSTS_SHAREPOST1
    });
    axios
      .post(`${BASE_URL}sharePost`, formBody, {
        headers: {
          'Accept':'application/json',
          'Authorization': token,
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          }
      })
      .then(response => {
        console.log(response);
        if(response.data.STATUS == true) {
          console.log('Hello');
          dispatch({
            type: SHAREPOST_POSTS_SUCCESS1,
            payload: 'Success'
          });
        } else if (response.data.Message == "User id must be numeric."){
          dispatch({
            type: SHAREPOST_POSTS_FAILED1,
            payload: 'User id must be numeric.'
          });
        }
        else {
          dispatch({
            type: SHAREPOST_POSTS_FAILED1,
            payload: 'Post id must be numeric.'
          });
        }

      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: SHAREPOST_POSTS_FAILED1,
          payload: 'Post id must be numeric.'
        });
      });
  };
};
