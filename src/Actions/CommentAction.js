import axios from "axios";
import {
  COMMENT_CHANGED,
  COMMENT_ERROR_CHANGED,
  COMMENT_LOADING,
  COMMENT_INITIAL_STATE,
  COMMENT_SUCCESS,
  COMMENT_FAILED,
  BASE_URL,
  COMMENTLIST_DATA,
  COMMENTLIST_ERROR_CHANGED,
  COMMENTLIST_LOADING,
  COMMENTLIST_INITIAL_STATE,
  LOADING_COMMENT_SHAREPOST,
  SHAREPOST_COMMENT_DATA,
  SHAREPOST_COMMENT_SUCCESS,
  SHAREPOST_COMMENT_FAILED,
  LOADING_COMMENT_ABUSE,
  ABUSE_COMMENT_DATA,
  ABUSE_COMMENT_SUCCESS,
  ABUSE_COMMENT_FAILED
} from "./type";

export const commentChange = text => {
  return {
    type: COMMENT_CHANGED,
    payload: text
  };
};

export const commentErrorChange = text => {
  return {
    type: COMMENT_ERROR_CHANGED,
    payload: text
  };
};

export const initialCommentStateData = () => {
  return {
    type: COMMENT_INITIAL_STATE,
    payload: ""
  };
};

export const getComment = ({userID, postID, comment, token }) => {
  console.log(userID);
  console.log(postID);
  console.log(comment);
  console.log(token);

  var details = {
    userID: userID,
    postID: postID,
    comment
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: COMMENT_LOADING
    });
    axios
      .post(`${BASE_URL}createComment`, formBody, {
        headers: {
        'Accept':'application/json',
        'Authorization': token,
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        }
      })
      .then(response => {
        console.log(response);
          console.warn('commentlist');
          getCommentlist1(userID, 1, postID, [], token,dispatch);
      })
      .catch(error => {
        console.warn(error);
        getCommentlist1(userID, 1, postID, [], token,dispatch);
      });
  };
};


const getCommentlist1 = (userID, page, postID, comments_list, token,dispatch) => {
  console.log(userID);
  console.log(postID);
  console.log(page);
  console.log(token);
  console.log("aaaaa");
  var details = {};

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  // return dispatch => {
    console.log("bbbbbb");
    if (page === 1) {
      dispatch({
        type: COMMENTLIST_LOADING
      });
    }

    axios
      .get(
        `${BASE_URL}commentList?userID=${userID}&postID=${postID}&pageNo=${page}&pageCount=10`,
        {
          headers: {
          'Accept':'application/json',
          'Authorization': token,
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          }
        }
      )
      .then(response => {
        // console.log('FeedList success'+ response);
        console.log("aavi commentlist gayu");
        console.log(response.data);
        if (response.data == "") {
          console.log("aavi commentlist gayu 1  ");
          // getFeedlist();
          dispatch({
            type: COMMENTLIST_ERROR_CHANGED,
            payload: "Something went wrong please try again"
          });
        } else {
          if (response.data.STATUS) {
            console.warn('commentlist dsfsdfsdf');
            
            // console.log("Hello");
            // console.log(response.data.DATA);
            // let a = response.data.DATA;
            // let feed_data = response.data.DATA.comments_list;
            // console.log(feed_data);
            // dispatch({ type: COMMENTLIST_DATA, payload: response.data.DATA });

            var isData = true;
            if(response.data.DATA.comments_list.length === 0 ){
              isData = false;
            }
            console.log(response.data.DATA.comments_list);
            if (page === 1) {
              dispatch({ type: COMMENTLIST_DATA, payload: {list: response.data.DATA.comments_list,  postdata: response.data.DATA.postdata ,  isData} });
            } else {
              if (response.data.DATA.length > 0) {
              // posts.concat(response.data.DATA);
              dispatch({ type: COMMENTLIST_DATA, payload: {list: comments_list.concat(response.data.DATA.comments_list), isData} });
              }
            }

            // console.log(response.data.DATA.Feed_list);
            // dispatch({ type: FEEDLIST_DATA, payload: response.data.DATA.Feed_list});
          }else{
            console.log('not true');
            dispatch({
              type: COMMENTLIST_ERROR_CHANGED,
              payload: "Something went wrong please try again"
            });
          }
          }



      })
      .catch(error => {
        console.log("COMMENTLIST_ERROR_CHANGED  " + error);
        dispatch({
          type: COMMENTLIST_ERROR_CHANGED,
          payload: "Something went wrong please try again"
        });
      });
  // };
};

export const getCommentlist = ({userID, page, postID, comments_list, token}) => {
  console.log(userID);
  console.log(postID);
  console.log(page);
  console.log(token);
  console.log("aaaaa");
  var details = {};

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    console.log("bbbbbb");
    if (page === 1) {
      dispatch({
        type: COMMENTLIST_LOADING
      });
    }

    axios
      .get(
        `${BASE_URL}commentList?userID=${userID}&postID=${postID}&pageNo=${page}&pageCount=10`,
        {
          headers: {
          'Accept':'application/json',
          'Authorization': token,
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          }
        }
      )
      .then(response => {
        // console.log('FeedList success'+ response);
        console.log("aavi commentlist gayu");
        console.log(response.data);
        if (response.data == "") {
          console.log("aavi commentlist gayu 1  ");
          // getFeedlist();
          dispatch({
            type: COMMENTLIST_ERROR_CHANGED,
            payload: "Something went wrong please try again"
          });
        } else {
          if (response.data.STATUS) {
            // console.log("Hello");
            // console.log(response.data.DATA);
            // let a = response.data.DATA;
            // let feed_data = response.data.DATA.comments_list;
            // console.log(feed_data);
            // dispatch({ type: COMMENTLIST_DATA, payload: response.data.DATA });

            var isData = true;
            if(response.data.DATA.comments_list.length === 0 ){
              isData = false;
            }
            console.log(response.data.DATA.comments_list);
            if (page === 1) {
              dispatch({ type: COMMENTLIST_DATA, payload: {list: response.data.DATA.comments_list,  postdata: response.data.DATA.postdata ,  isData} });
            } else {
              if (response.data.DATA.length > 0) {
              // posts.concat(response.data.DATA);
              dispatch({ type: COMMENTLIST_DATA, payload: {list: comments_list.concat(response.data.DATA.comments_list), isData} });
              }
            }

            // console.log(response.data.DATA.Feed_list);
            // dispatch({ type: FEEDLIST_DATA, payload: response.data.DATA.Feed_list});
          }else{
            console.log('not true');
            dispatch({
              type: COMMENTLIST_ERROR_CHANGED,
              payload: "Something went wrong please try again"
            });
          }
          }



      })
      .catch(error => {
        console.log("COMMENTLIST_ERROR_CHANGED  " + error);
        dispatch({
          type: COMMENTLIST_ERROR_CHANGED,
          payload: "Something went wrong please try again"
        });
      });
  };
};

export const initialgetallcommentlistStateData = () => {
  return {
    type: COMMENTLIST_INITIAL_STATE,
    payload: ""
  };
};

export const getCommentSharepost = ({userID, postID, shareText, token}) => {
  console.log(userID);
  console.log(postID);
  console.log(token);

  var details = {
    userID: userID,
    postID: postID,
    shareText : 'test2'
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_COMMENT_SHAREPOST
    });
    axios
      .post(`${BASE_URL}sharePost`, formBody, {
        headers: {
          'Accept':'application/json',
          'Authorization': token,
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          }
      })
      .then(response => {
        console.log(response);
        if(response.data.STATUS == true) {
          console.log('Hello');
          dispatch({
            type: SHAREPOST_COMMENT_SUCCESS,
            payload: 'Success'
          });
        } else if (response.data.Message == "User id must be numeric."){
          dispatch({
            type: SHAREPOST_COMMENT_FAILED,
            payload: 'User id must be numeric.'
          });
        }
        else {
          dispatch({
            type: SHAREPOST_COMMENT_FAILED,
            payload: 'Post id must be numeric.'
          });
        }

      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: SHAREPOST_COMMENT_FAILED,
          payload: 'Post id must be numeric.'
        });
      });
  };
};

export const getCommentAbusePost = ({userID, postID, token}) => {
  console.log(userID);
  console.log(postID);
  console.log(token);

  var details = {
    UserID: userID,
    postID: postID
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_COMMENT_ABUSE
    });
    axios
      .post(`${BASE_URL}abusePost`, formBody, {
        headers: {
          'Accept':'application/json',
          'Authorization': token,
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          }
      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS == true) {
          console.log("Hello");
          dispatch({
            type: ABUSE_COMMENT_SUCCESS,
            payload: "Success Abuse"
          });
        } else if (
          response.data.Message == "Already Post is Abuse by same User"
        ) {
          dispatch({
            type: ABUSE_COMMENT_FAILED,
            payload: "Already Post is Abuse by same User"
          });
        } else if (response.data.Message == "User id must be numeric.") {
          dispatch({
            type: ABUSE_COMMENT_FAILED,
            payload: "User id must be numeric."
          });
        } else {
          dispatch({
            type: ABUSE_COMMENT_FAILED,
            payload: "Post id must be numeric."
          });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: ABUSE_COMMENT_FAILED,
          payload: "Post id must be numeric."
        });
      });
  };
};
