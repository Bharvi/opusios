import axios from 'axios';
import {
   ABOUTUS_DATA, BASE_URL, ABOUTUS_ERROR_CHANGED,ABOUTUS_LOADING
} from './type';

export const getMYAboutus = ({ token }) => {
  console.log('aaaaa');
  console.log(token);
  var details =  {

  };

  var formBody = [];

  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return (dispatch) => {
    dispatch({
      type: ABOUTUS_LOADING
    });
    axios.get(`${BASE_URL}cms-page?type= About Us`,{
        headers: {
        'Accept':'application/json',
        'Authorization': token,
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        }
      })
      .then(response => {
        // console.log('MYAbout success'+ response);
        console.log('aavi gayu');
        if (response.data == '') {
          dispatch({ type: ABOUTUS_ERROR_CHANGED, payload: 'Something went wrong please try again' });
        } else {
        if (response.data.STATUS) {
            console.log(response.data.DATA);
            dispatch({ type: ABOUTUS_DATA, payload: response.data.DATA });

          
        }
        }
      })
      .catch((error) => {
        console.log('ABOUTUS_ERROR_CHANGED  '+error);
        dispatch({ type: ABOUTUS_ERROR_CHANGED, payload: 'Something went wrong please try again' });
      });
    };
};
