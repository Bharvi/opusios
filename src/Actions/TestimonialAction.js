import axios from "axios";
import { Alert } from "react-native";
import {
  BASE_URL,
  TESTIMONIAL_INITIAL_STATE,
  LOADING_TESTIMONIAL,
  TESTIMONIAL_DATA,
  TESTIMONIAL_CHANGED,
  TESTIMONIAL_ERROR_CHANGED,
  TESTIMONIAL_SUCCESS,
  TESTIMONIAL_FAILED,
  LOADING_ADDFRIEND,
  ADDFRIEND_DATA,
  ADDFRIEND_SUCCESS,
  ADDFRIEND_FAILED,
  LOADING_BLOCKFRIEND,
  BLOCKFRIEND_DATA,
  BLOCKFRIEND_SUCCESS,
  BLOCKFRIEND_FAILED,
  RATING_INITIAL_STATE,
  LOADING_RATING,
  RATING_DATA,
  RATING_CHANGED,
  RATING_ERROR_CHANGED,
  RATING_SUCCESS,
  RATING_FAILED,

  LOADING_CHATNOTIFICATION,
  CHATNOTIFICATION_SUCCESS,
  CHATNOTIFICATION_FAILED,


} from "./type";

export const initialtestimonialStateData = () => {
  return {
    type: TESTIMONIAL_INITIAL_STATE,
    payload: ""
  };
};
export const testimonialChange = text => {
  return {
    type: TESTIMONIAL_CHANGED,
    payload: text
  };
};

export const testimonialErrorChange = text => {
  return {
    type: TESTIMONIAL_ERROR_CHANGED,
    payload: text
  };
};

export const getTestimonial = ({ userID, toUserId, testimonial, token }) => {
  console.log(userID);
  console.log(toUserId);
  console.log(testimonial);
  console.log(token);

  var details = {
    id: userID,
    touser_id: toUserId,
    testimonial
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_TESTIMONIAL
    });
    axios
      .post(`${BASE_URL}addTestimonial`, formBody, {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }

      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS == true) {
          console.log("Hello");
          dispatch({
            type: TESTIMONIAL_SUCCESS,
            payload: "Success Testimonial"
          });
        } else {
          dispatch({
            type: TESTIMONIAL_FAILED,
            payload: "Testimonial should not be null."
          });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: TESTIMONIAL_FAILED,
          payload: "Testimonial should not be null."
        });
      });
  };
};

export const getAddfriend = ({ userID, toUserId, token, dispatch }) => {
  console.log(userID);
  console.log(toUserId);
  console.log(token);

  var details = {
    userID,
    toUserID: toUserId
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_ADDFRIEND
    });
    axios
      .post(`${BASE_URL}add-friend`, formBody, {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }
      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS == true) {
          console.log("Hello");
          dispatch({
            type: ADDFRIEND_SUCCESS,
            payload: "Success Addfriend"
          });
        } else if (response.data.Message == "Already Friend") {
          dispatch({
            type: ADDFRIEND_FAILED,
            payload: "Already Friend"
          });
        } else {
          dispatch({
            type: ADDFRIEND_FAILED,
            payload: "User id must be numeric."
          });
        }
      })
      .catch(error => {
        console.log(error.response);
        dispatch({
          type: ADDFRIEND_FAILED,
          payload: "User id must be numeric."
        });
      });
  };
};

export const getBlockfriend = ({ userID, toUserId, token }) => {
  console.log(userID);
  console.log(toUserId);
  console.log(token);

  var details = {
    userID,
    toUserID: toUserId
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_BLOCKFRIEND
    });
    axios
      .post(`${BASE_URL}abuseUser`, formBody, {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }
      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS == true) {
          console.log("Hello");
          setTimeout(() => {
            Alert.alert(
              'Success',
              'Friend block successfully',
              [
                { text: 'OK', onPress: () => { cancelable: false } },
              ],
              { cancelable: false }
            )

          }, 500);
          dispatch({
            type: BLOCKFRIEND_SUCCESS,
            payload: "Success Block"
          });
        } else if (response.data.Message == "User has already been reported") {
          setTimeout(() => {
            Alert.alert("Oops", "User has already been reported");

          }, 500);
          dispatch({
            type: BLOCKFRIEND_FAILED,
            payload: "User has already been reported"
          });
        } else {
          setTimeout(() => {
            Alert.alert("Oops", "User id must be numeric.");

          }, 500);
          dispatch({
            type: BLOCKFRIEND_FAILED,
            payload: "User id must be numeric."
          });
        }
      })
      .catch(error => {
        console.log(error);
        setTimeout(() => {
          Alert.alert("Oops", "User id must be numeric.");

        }, 500);
        dispatch({
          type: BLOCKFRIEND_FAILED,
          payload: "User id must be numeric."
        });
      });
  };
};

export const initialratingStateData = () => {
  return {
    type: RATING_INITIAL_STATE,
    payload: ""
  };
};
export const ratingChange = rating => {
  return {
    type: RATING_CHANGED,
    payload: rating
  };
};

export const ratingErrorChange = text => {
  return {
    type: RATING_ERROR_CHANGED,
    payload: text
  };
};

export const getRating = ({ userID, toUserId, rating, token }) => {
  console.log(userID);
  console.log(toUserId);
  console.log(rating);
  console.log(token);

  var details = {
    fromUserID: userID,
    toUserID: toUserId,
    rating
  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_RATING
    });
    axios
      .post(`${BASE_URL}giveRating`, formBody, {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }

      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS == true) {
          console.log("Hello");
          dispatch({
            type: RATING_SUCCESS,
            payload: "Success Rating"
          });
        } else {
          dispatch({
            type: RATING_FAILED,
            payload: "You have already rated this user."
          });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: RATING_FAILED,
          payload: "You have already rated this user."
        });
      });
  };
};





export const getChatNotification = ({ userID, toUserId, message, token, message_count }) => {
  console.log("Data base user chatnotification", userID);
  console.log(" mokavani chat notification", toUserId);
  console.log(message);
  console.log(token);

  var details = {
    toUserId: toUserId,
    fromUserId: userID,
    message: message,
    message_count: message_count
  };

  // var formBody = [];
  // for (var property in details) {
  //   var encodedKey = encodeURIComponent(property);
  //   var encodedValue = encodeURIComponent(details[property]);
  //   formBody.push(encodedKey + "=" + encodedValue);
  // }
  // formBody = formBody.join("&");

  return dispatch => {
    dispatch({
      type: LOADING_CHATNOTIFICATION
    });
    axios
      .post(`${BASE_URL}chatNotification`, details, {
        headers: {
          'Accept': 'application/json',
          'Authorization': token,
        }

      })
      .then(response => {
        console.log(response);
        if (response.data.STATUS == true) {
          console.log("Hello");
          dispatch({
            type: CHATNOTIFICATION_SUCCESS,
            payload: "Success chatnotification"
          });
        } else {
          dispatch({
            type: CHATNOTIFICATION_FAILED,
            payload: "Feiled"
          });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: CHATNOTIFICATION_FAILED,
          payload: "Feiled"
        });
      });
  };
};

// project-523490573991