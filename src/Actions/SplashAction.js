import axios from 'axios';
import {
  CHECK_VERSION,
  LOADING,
  GET_TOKEN,
  TOKEN_LOADDING,
  TOKEN_SUCCESS,
  TOKEN_FAILED,
  SAVE_USER_DATA,
  BASE_URL,
  GET_SEARCH_DATA,
  AUTH_URL,
} from './type';
import {getUser} from '../Database/allSchema';
import {Alert, Linking, BackHandler} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import RNExitApp from 'react-native-exit-app';

export const getToken = (
  myNavigation,
  StackActions,
  NavigationActions,
  fromNotification,
  notificationData,
) => {
  return dispatch => {
    dispatch({
      type: TOKEN_LOADDING,
    });
    axios
      .post(AUTH_URL, {
        client_id: '4',
        grant_type: 'client_credentials',
        client_secret: '8uLe8B6SI6HASqphVsRAdQb6yyVaDf9VwWdXDHqI',
      })
      .then(response => {
        console.log('response.data.access_token', response.data.access_token);

        // getAllTag(`${response.data.token_type} ${response.data.access_token}`, dispatch);
        // dispatch({ type: GET_TOKEN, payload: response.data.data.access_token  });
        //     goToScreen(response, myNavigation, StackActions, NavigationActions, dispatch, fromNotification, notificationData);
        //   // saveUser(response.data, dispatch);

        checkVersion(
          response.data.access_token,
          myNavigation,
          StackActions,
          NavigationActions,
          dispatch,
          fromNotification,
          notificationData,
        );
      })
      .catch(error => {
        console.log(error.response);
        // dispatch({
        //   type: GET_TOKEN,
        //   payload: error
        // });
        // const resetAction = StackActions.reset({
        //   index: 0,
        //   actions: [NavigationActions.navigate({
        //     routeName: 'SocialLoginPage'
        //   })],
        // });
        // myNavigation.dispatch(resetAction);
        // setTimeout(() => {
        //   Alert.alert(
        //     "Alert",
        //     "Please check internet connection.",
        //     [{ text: "OK", onPress: () => RNExitApp.exitApp() }],
        //   );
        // }, 100);
      });
  };
};

export const goToScreen = async (
  response,
  myNavigation,
  StackActions,
  NavigationActions,
  dispatch,
  fromNotification,
  notificationData,
) => {
  await getUser()
    .then(user => {
      console.log('in gotoscreen', response);
      console.log(user.length);
      if (user && user.length > 0) {
        console.log(`user_id${user[0].id}`);
        // console.log('Pathik token', response);
        dispatch({
          type: GET_TOKEN,
          payload: response,
        });

        if (fromNotification) {
          try {
            notificationArray = notificationData.split(',');
            if (notificationArray[0] === 'post') {
              myNavigation.replace('CommentPage', {
                postID: notificationArray[1],
                isUserFromNotification: true,
              });
            } else if (notificationArray[0] === 'friend') {
              myNavigation.replace('BottomTabProfileFirst', {
                user_id: notificationArray[1],
                isUserFromNotification: true,
              });
            } else if (notificationArray[0] === 'chat') {
              dispatch({
                type: SAVE_USER_DATA,
                payload: {
                  user_id: notificationArray[1],
                  fullname: notificationArray[2],
                  firebase: notificationArray[3],
                },
              });
              setTimeout(() => {
                myNavigation.replace('ChatMessage', {
                  toUserId: notificationArray[1],
                  fullname: notificationArray[2],
                  to_fire_id: notificationArray[3],
                  isUserFromNotification: true,
                });
              }, 50);
            }
          } catch (e) {
            console.log(e);
          }
        } else {
          // const resetAction = StackActions.reset({
          //   index: 0,
          //   actions: [NavigationActions.navigate({
          //     routeName: 'BottomTab'
          //   })],
          // });
          // myNavigation.dispatch(resetAction);
          myNavigation.replace('BottomTab');
        }
      } else {
        dispatch({
          type: GET_TOKEN,
          payload: response,
        });
        // const resetAction = StackActions.reset({
        //   index: 0,
        //   actions: [NavigationActions.navigate({
        //     routeName: 'SocialLoginPage'
        //   })],
        // });
        // myNavigation.dispatch(resetAction);
        myNavigation.replace('SocialLoginPage');
      }
    })
    .catch(error => {
      // console.log(error);
      dispatch({
        type: GET_TOKEN,
        payload: response,
      });
      // const resetAction = StackActions.reset({
      //   index: 0,
      //   actions: [NavigationActions.navigate({
      //     routeName: 'SocialLoginPage'
      //   })],
      // });
      // myNavigation.dispatch(resetAction);
      // myNavigation.replace('SocialLoginPage')
      setTimeout(() => {
        Alert.alert(
          'Alert',
          'Somthing went wrong please try again later.',
          [{text: 'OK', onPress: () => BackHandler.exitApp()}],
          {cancelable: false},
        );
      }, 500);
    });
};

// const saveUser = (userData, dispatch) => {
//     console.log(userData);
//     deleteUser().then(() => {
//         const newUser = {
//           id:(userData.user_id == null) ? 1 : Number(userData.user_id),    // primary key
//           fullname: '',
//           email_id: '',
//           user_password: '',
//           device_type: '',
//           // user_photo:"http://www.opus.projectdemo.website/public/uploads/images/"+ userData.user_photo,
//           user_photo:'',
//           device_token: '',
//           gender: '',
//           profession_name: '',
//           location: '',
//           brief_desc:'',
//           birth_date: '',
//           user_tag:'',
//           access_token: (userData.access_token == null) ? '' : userData.access_token,
//         };
//         insertUser(newUser).then(() => {
//           console.log('Insert Suceess');
//           console.log(newUser);
//           dispatch({
//             type: TOKEN_SUCCESS,
//             payload: 'Success'
//           });
//         }).catch((error) => {
//           console.log(`User Insert error${error}`);
//           dispatch({
//             type: TOKEN_FAILED,
//             payload: 'token fill'
//           });
//         });
//     }).catch((error) => {
//       console.log(error);
//       dispatch({
//         type: TOKEN_FAILED,
//         payload: 'token fill'
//       });
//     });
// };
export const checkVersion = (
  token,
  myNavigation,
  StackActions,
  NavigationActions,
  dispatch,
  fromNotification,
  notificationData,
) => {
  console.warn('version in action', DeviceInfo.getVersion());
  axios
    .post(
      `${BASE_URL}getVersion`,
      {
        version: DeviceInfo.getVersion(),
        type: 'ios',
      },
      {
        headers: {
          Accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
      },
    )
    .then(response => {
      console.log('check_version_responce:', response);
      if (response.data.success) {
        // console.warn("ACTION Success",response.data.success);
        goToScreen(
          `Bearer ${token}`,
          myNavigation,
          StackActions,
          NavigationActions,
          dispatch,
          fromNotification,
          notificationData,
        );
        // goToScreen(
        //   `Bearer ${token}`,
        //   myNavigation,
        //   StackActions,
        //   NavigationActions,
        //   dispatch,
        //   fromNotification,
        //   notificationData
        // );
        getAllTag(`Bearer ${token}`, dispatch);
      } else {
        setTimeout(() => {
          Alert.alert(
            'New Version Available',
            'There is a newer version available for download! Please update the app by visiting the app Store.',
            [
              {
                text: 'Update',
                onPress: () =>
                  Linking.openURL(
                    'https://apps.apple.com/in/app/opus-mobile/id1459987656',
                  ),
              },
            ],
            {cancelable: false},
          );
        }, 10);
      }
    })
    .catch(err => {
      console.log('Err in checkVersion :', err);
    });
};

export const checkVersionold = version => {
  console.log('version in action', version);

  return dispatch => {
    dispatch({
      type: LOADING,
    });
    axios
      .post(
        `https://khesed.com/public/api/getVersion`,
        {
          version: version,
          type: 'android',
        },
        {
          headers: {
            Accept: 'application/json',
            Authorization:
              'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjMwNmU5Mzg4NDZhY2QxMDc1NTg3NmRiMTM0YzdjY2M3NzBjYmIxYjdlMGY0N2YyZGJlNjFiNzgwYjYxNTk2MDhlNDQxNTdkY2FmNTA1YjFhIn0.eyJhdWQiOiIzIiwianRpIjoiMzA2ZTkzODg0NmFjZDEwNzU1ODc2ZGIxMzRjN2NjYzc3MGNiYjFiN2UwZjQ3ZjJkYmU2MWI3ODBiNjE1OTYwOGU0NDE1N2RjYWY1MDViMWEiLCJpYXQiOjE1NDY2MDY1NTgsIm5iZiI6MTU0NjYwNjU1OCwiZXhwIjoxNTc4MTQyNTU4LCJzdWIiOiIiLCJzY29wZXMiOltdfQ.RJbiGcsiW3KHNFqEhorqWXd80XbxLbSrCGxWKObYMpU7_R_hxRoAmJfu2sFA_37n8PU7SkwXRAs2X6KHVoADuYM3Lmr194OcJSMqbNL4Ofi4QkwEGPSTYlG4HTUJIHwhihdpMhR3Wq5nOxlcPoRGIL6y86jfl5SR039wyTFlW8OJb_MU83yG48zxvXCMJoPmmUX8v3JpXl3tVBikoabhxc4LtKcfcOAzNO4LasCL2F5Vjf5eE75cOQ9upXbUugzjb_RhPqJhQR6YDYY2oGQ6LB1cCiEv6qN28_8iftm-TCD4Z8NvkRMNVQjkH4RG4y2Fr1N3pTaRmVI_jaX7_Gjwtd8X2M0TuPhibzIeR-ZmZF_KrEISL7ck6cucKwpftDvAkdXRWkGFBjwSGvddBtFj62QDQjPtjwpSnWTiPzcNY1Qg6W1MIp5QBwP1Hu_9-cMgs6xXIvKB2c1BpZ_Wd1IVQdePM40LOecKPNHhNEwcYJyDfAGExcsPa-iafOVqOqKI1I_7D8CLZAwPw3XxDHLWQRes2kNHZPkywYjcJWknl6JYWCP-mex6Sh7iTEtDdpKu7npUilO93yfZn9Hdgxh6nWaKQ65vJLRLwPX37YBryL80X8_u10loZ-UBtKfo6w_BNCnDAx_-pOWQos74UB5p1aX9zaps_1Ny4YKSni3iqpM',
          },
        },
      )
      .then(response => {
        console.log('check_version_responce:', response);
        if (response.data.success) {
          console.log('ACTION', response.data.success);
          dispatch({
            type: CHECK_VERSION,
            payload: 'true',
          });
        } else {
          console.log('ACTION', response.data.success);
          dispatch({
            type: CHECK_VERSION,
            payload: 'false',
          });
        }
      })
      .catch(err => {
        dispatch({
          type: CHECK_VERSION,
          payload: 'false',
        });
      });
  };
};

const getAllTag = (token, dispatch) => {
  console.log('in alltags method..');
  axios
    .get(BASE_URL + `alltags`, {
      headers: {
        Accept: 'application/json',
        Authorization: token,
      },
    })
    .then(response => {
      console.log('getAllTag responce', response.data.DATA);

      let tempTags = [];
      let tempProfession = [];
      response.data.DATA.tags.map(item => {
        tempTags.push(item.name);
      });
      response.data.DATA.professions.map(item => {
        tempProfession.push(item.name);
      });
      dispatch({
        type: GET_SEARCH_DATA,
        payload: tempTags.concat(
          response.data.DATA.users,
          tempProfession,
          response.data.DATA.locations,
        ),
      });
    })
    .catch(error => {
      console.log('error in alltags', error);
    });
};
