import axios from 'axios';
import {
  SEARCHLIST_DATA,
  BASE_URL,
  SEARCHLIST_ERROR_CHANGED,
  SEARCHLIST_LOADING,
  SEARCHLIST_INITIAL_STATE,
  SEARCH_UPDATE_LIST_SUCCESS,
  SEARCH_UPDATE_LIST_ERROR,
  GET_SEARCH_DATA
} from './type';

export const getSearchList = ({ userId, page, suggested_profiles, search_updates, token, isLoading, isRefreshing }) => {
  console.log('getSearchList in');
  // console.log(page);
  // console.log(token);
  let except_id = [];
  suggested_profiles.map((item) => {
    except_id.push(item.user_id)
  })

  var details = {

  };

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return (dispatch) => {
    if (page === 1) {
      dispatch({
        type: SEARCHLIST_LOADING,
        payload: { isLoading, isRefreshing }
      });
    }
    axios.get(`${BASE_URL}search?userID=${userId}&pageNo=${page}&pageCount=30&except_id=${except_id.toString()}`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': token,
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      }
    })
      .then(response => {
        console.log('seearch response', response.data);
        if (response.data == '') {
          dispatch({ type: SEARCHLIST_ERROR_CHANGED, payload: 'Something went wrong please try again' });
        } else {
          if (response.data.STATUS) {
            // getAllTag(token, dispatch);
            var isData = true;
            if (response.data.DATA.suggested_profiles.length === 0) {
              isData = false;
            };
            let tempSearchUpdates = response.data.DATA.search_updates
            let searchUpdates = tempSearchUpdates.slice(0, 3)

            if (page === 1) {

              dispatch({
                type: SEARCHLIST_DATA,
                payload: {
                  list: response.data.DATA.suggested_profiles,
                  search_updates: searchUpdates,
                  isData
                }
              });

              // dispatch({ type: FEEDLIST_DATA, payload: {list: response.data.DATA.Feed_list,  notificationCount: response.data.DATA.notificationCount ,  isData} });
            } else {
              // if (response.data.DATA.suggested_profiles.length > 0) {
              // posts.concat(response.data.DATA);
              dispatch({ type: SEARCHLIST_DATA, payload: { list: suggested_profiles.concat(response.data.DATA.suggested_profiles), search_updates: search_updates, isData } });
              // dispatch({ type: FEEDLIST_DATA, payload: {list: Feed_list.concat(response.data.DATA.Feed_list), isData} });
              // }
            }


            // console.log(response.data.DATA.Feed_list);
            // dispatch({ type: FEEDLIST_DATA, payload: response.data.DATA.Feed_list});
          } else {
            console.log('not true');
            dispatch({ type: SEARCHLIST_ERROR_CHANGED, payload: 'Something went wrong please try again' });
          }
        }
      })
      .catch((error) => {
        console.log('SEARCHLIST_ERROR_CHANGED  ' + error);
        dispatch({ type: SEARCHLIST_ERROR_CHANGED, payload: 'Something went wrong please try again' });
      });
  };
};

export const initialgetSearchListStateData = () => {
  return {
    type: SEARCHLIST_INITIAL_STATE,
    payload: ''
  };
};

export const getSearchUpdateList = (userId, page, searchUpdateList, token) => {
  return (dispatch) => {
    if (page === 1) {
      dispatch({
        type: SEARCHLIST_LOADING,
        payload: { isLoading: true, isRefreshing: false }
      });
    }
    axios.get(`${BASE_URL}search?userID=${userId}&pageNo=${page}&pageCount=10`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': token,
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      }
    })
      .then(response => {
        console.log(response.data);
        if (response.data.STATUS) {
          var isData = true;
          if (response.data.DATA.search_updates.length === 0) {
            isData = false;
          }
          // if (response.data.DATA.search_updates.length > 0) {
          if (page === 1) {
            dispatch({
              type: SEARCH_UPDATE_LIST_SUCCESS,
              payload: { list: response.data.DATA.search_updates, isData }
            });
          } else {
            dispatch({
              type: SEARCH_UPDATE_LIST_SUCCESS,
              payload: { list: searchUpdateList.concat(response.data.DATA.search_updates), isData }
            });
          }
          // } else {
          // dispatch({ type: SEARCH_UPDATE_LIST_ERROR, payload: 'Something went wrong please try again' });
          // }
        } else {
          dispatch({ type: SEARCH_UPDATE_LIST_ERROR, payload: 'Something went wrong please try again' });
        }
      })
      .catch((error) => {
        console.log('SEARCHLIST_ERROR_CHANGED ' + error);
        dispatch({ type: SEARCH_UPDATE_LIST_ERROR, payload: 'Something went wrong please try again' });
      });
  };
};


const getAllTag = (token, dispatch) => {
  console.log("in alltags method..");
  axios
    .get(`${BASE_URL}alltags`, {
      headers: {
        Accept: "application/json",
        Authorization: token
      }
    })
    .then(response => {
      console.log("getAllTag responce", response.data.DATA);

      let tempTags = [];
      let tempProfession = [];
      response.data.DATA.tags.map(item => {
        tempTags.push(item.name);
      });
      response.data.DATA.professions.map(item => {
        tempProfession.push(item.name);
      });
      dispatch({
        type: GET_SEARCH_DATA,
        payload: tempTags.concat(
          response.data.DATA.users,
          tempProfession,
          response.data.DATA.locations
        )
      });
    })
    .catch(error => {
      console.log("error in alltags", error);
    });
};