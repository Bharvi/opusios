import axios from "axios";
import {
  NOTIFICATION_DATA,
  NOTIFICATION_ERROR_CHANGED,
  NOTIFICATION_LOADING,
  NOTIFICATION_INITIAL_STATE,
  BASE_URL,
  NOTIFICATION_ALL_DATA,
  NOTIFICATION_ALL_ERROR_CHANGED,
  NOTIFICATION_ALL_LOADING

} from "./type";



export const getnotificationlist = ({userId, token}) => {
  console.log(userId);
  console.log(token);

  console.log("aaaaa");
  var details = {};

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    console.log("bbbbbb");
    dispatch({
      type: NOTIFICATION_LOADING
    });
    axios
      .get(
        `${BASE_URL}notification?userID=${userId}`,
        {
          headers: {
          'Accept':'application/json',
          'Authorization': token,
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          }
        }
      )
      .then(response => {
        // console.log('FeedList success'+ response);
        console.log("aavi notificationlist gayu");
        console.log(response.data);
        if (response.data == "") {
          console.log("aavi notificationlist gayu 1  ");
          // getFeedlist();
          dispatch({
            type: NOTIFICATION_ERROR_CHANGED,
            payload: "Something went wrong please try again"
          });
        } else {
          if (response.data.STATUS) {
            console.log("Hello");
            console.log(response.data.DATA);
            let a = response.data.DATA;
            let feed_data = response.data.DATA;
            dispatch({ type: NOTIFICATION_DATA, payload: response.data.DATA });

            // console.log(response.data.DATA.Feed_list);
            // dispatch({ type: FEEDLIST_DATA, payload: response.data.DATA.Feed_list});
          } else {
            console.log("not true");
            dispatch({
              type: NOTIFICATION_ERROR_CHANGED,
              payload: "Something went wrong please try again"
            });
          }
        }
      })
      .catch(error => {
        console.log("NOTIFICATION_ERROR_CHANGED  " + error);
        dispatch({
          type: NOTIFICATION_ERROR_CHANGED,
          payload: "Something went wrong please try again"
        });
      });
  };
};

export const getNotification_clearAll = ({userId, token}) => {
  console.log(userId);
  console.log(token);
  console.log("aaaaa");
  var details = {};

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  return dispatch => {
    console.log("bbbbbb");
    dispatch({
      type: NOTIFICATION_ALL_LOADING
    });
    axios
      .get(
        `${BASE_URL}clearAllNotifications?userID=${userId}`,

        {
          headers: {
          'Accept':'application/json',
          'Authorization': token,
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          }
        }
      )
      .then(response => {
        // console.log('FeedList success'+ response);
        console.log("aavi notificationlistclearAll gayu");
        console.log(response.data);
        if (response.data == "") {
          console.log("aavi notificationlistclearAll gayu 1  ");
          // getFeedlist();
          dispatch({
            type: NOTIFICATION_ALL_ERROR_CHANGED,
            payload: "Something went wrong please try again"
          });
        } else {
          if (response.data.STATUS) {
            console.log("Hello");
            console.log(response.data.DATA);
            let a = response.data.DATA;
            let feed_data = response.data.DATA;
            console.log(feed_data);
            dispatch({ type: NOTIFICATION_ALL_DATA, payload: response.data.DATA });

            // console.log(response.data.DATA.Feed_list);
            // dispatch({ type: FEEDLIST_DATA, payload: response.data.DATA.Feed_list});
          } else {
            console.log("not true");
            dispatch({
              type: NOTIFICATION_ALL_ERROR_CHANGED,
              payload: "Something went wrong please try again"
            });
          }
        }
      })
      .catch(error => {
        console.log("NOTIFICATION_ERROR_CHANGED  " + error);
        dispatch({
          type: NOTIFICATION_ALL_ERROR_CHANGED,
          payload: "Something went wrong please try again"
        });
      });
  };
};

export const initialgetallnotificationStateData = () => {
  return {
    type: NOTIFICATION_INITIAL_STATE,
    payload: ""
  };
};
